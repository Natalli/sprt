# Hockey Betting Game by A Live Current Media™ Production

## Firebase
https://betting-game-hockey.firebaseapp.com

## NHL Colors
https://teamcolorcodes.com/category/nhl-team-color-codes/

## NHL API Documentation
https://github.com/dword4/nhlapi
https://github.com/plaberge/nhlstats

## NHL API Schema
https://github.com/erunion/sport-api-specifications/blob/master/nhl/nhl.yaml

## ESPN Hidden API
https://gist.github.com/akeaswaran/b48b02f1c94f873c6655e7129910fc3b

## ESPN Official API Reference
http://www.espn.com/apis/devcenter/io-docs.html

## Android Google Play Siging
keytool -genkey -v -keystore sprtmtrx.keystore -alias SPRTMTRX -keyalg RSA -keysize 2048 -validity 10000

## Quirks

#### iOS
Automate Export Compliance 
https://github.com/status-im/status-react/issues/4823

## NativeAudio
Requires 'www' assets root folder to be replaced to 'public'
https://github.com/floatinghotpot/cordova-plugin-nativeaudio/issues/159