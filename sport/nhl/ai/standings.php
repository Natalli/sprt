<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json;charset=UTF-8");

//$json = file_get_contents('nhl.json');

//echo 'standings:' . $_SERVER["QUERY_STRING"];

$url =  'https://statsapi.web.nhl.com/api/v1/standings?' . $_SERVER["QUERY_STRING"];

//echo $url;


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
curl_close($ch); 

echo $output;

?>