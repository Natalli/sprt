<?php
header("Access-Control-Allow-Origin: *");
require_once('const.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Los_Angeles");

// 2:19 average game
// 3 periods 20 minutes each with 17 break
// 15.5 whistles 1.5 minute each = 8-17-26 minutes of pause
// 3 min break 
// overtime up to 5 min till the first goal scorred - 23% ( 7% overtime and ? in shoutout)
// break 2 min 
// shout outs - 33% of 
// sudden death

const DUR_PERIOD = 20 * 60;
const DUR_BREAK = 17 * 60;
const DUR_STOPPPAGE = 60;
const DUR_GAME = DUR_PERIOD * 3 + DUR_BREAK * 2 + 3 * 60;  //( 60 + 47 ) * 60; 
//const START_SHIFT = 31*25;
//const ADMIN = !empty($_GET['admin']);
const TMP = 'tmp/';



//$test_rez = calcOffsetPosition('2019021176', $test_postion);
//echo gmdate("i:s", $test_rez) . "($test_rez)\n";
//$test_rez = calcCurPosition('2019021176', $test_postion);
//echo gmdate("i:s", $test_rez) . "($test_rez)\n";

function calcOffsetPosition($gamePk, $real_position) {
    $timelines = array(
        0 => array(68=>33, 107=>28, 113=>95, 233=>64, 299=>22, 325=>88, 427=>73, 460=>25, 527=>28, 639=>52, 779=>67, 918=>91, 1053=>62, 1059=>1 , 1241=>-2),
        1 => array(23=>83, 91=>74, 225=>97, 257=>74, 377=>83, 428=>73, 535=>94, 545=>76, 563=>89, 654=>60, 681=>53, 750=>24, 905=>47, 1027=>46, 1059=>1, 1159=>40 , 1241=>-40),
        2 => array(9=>88, 55=>58, 74=>92, 202=>31, 242=>73, 263=>72, 309=>29, 353=>41, 388=>52, 508=>60, 535=>82, 557=>48, 574=>29, 694=>20, 755=>91, 978=>21, 1037=>26, 1059=>1, 1159=>50 , 1241=>-60),
        3 => array(6=>78, 100=>75, 162=>33, 186=>63, 212=>74, 248=>97, 258=>41, 374=>49, 405=>38, 442=>34, 541=>48, 580=>80, 684=>53, 750=>59, 982=>50, 1150=>30, 1159=>1, 1159=>40 , 1241=>-60),
        4 => array(12=>39, 81=>37, 120=>28, 280=>55, 314=>98, 342=>73, 363=>36, 461=>56, 559=>77, 571=>35, 605=>89, 645=>31, 674=>85, 734=>48, 774=>32, 783=>47, 800=>41, 946=>79, 1049=>71, 1139=>40, 1159=>20 , 1441=>-60),
        5 => array(46=>67, 69=>47, 222=>86, 337=>73, 364=>50, 380=>49, 402=>21, 421=>51, 430=>83, 479=>43, 492=>42, 533=>88, 559=>22, 660=>52, 717=>53, 835=>54, 1055=>35, 1159=>30 , 1541=>-10),
        6 => array(9=>20, 24=>82, 54=>54, 224=>91, 231=>68, 240=>21, 352=>75, 390=>65, 434=>60, 600=>66, 616=>54, 724=>31, 735=>94, 744=>63, 941=>86, 1063=>20, 1159=>20 , 1511=>-10),
        7 => array(41=>88, 157=>35, 310=>43, 340=>63, 401=>22, 416=>54, 445=>37, 483=>75, 500=>23, 518=>78, 526=>66, 556=>84, 727=>23, 768=>52, 801=>33, 937=>97, 1079=>54, 1159=>1 , 1511=>-20),
        8 => array(95=>84, 219=>91, 239=>35, 255=>70, 281=>84, 308=>52, 413=>30, 436=>40, 534=>31, 570=>50, 599=>77, 712=>67, 739=>85, 783=>95, 818=>71, 826=>39, 1119=>33 , 1129=>-30 , 1421=>-2),
        9 => array(8=>93, 21=>24, 131=>34, 233=>90, 340=>83, 388=>28, 445=>20, 472=>72, 504=>76, 539=>22, 575=>76, 689=>39, 725=>73, 876=>48, 1018=>77, 1059=>1 , 1400=>-20 , 1500=>-20),
        10 => array(68=>33, 107=>28, 113=>95, 233=>64, 299=>22, 325=>88, 427=>73, 460=>25, 527=>28, 639=>52, 779=>67, 918=>91, 1143=>62, 1159=>1 , 1241=>-2, 1441=>-2, 1641=>-2),
        11 => array(23=>83, 91=>74, 225=>97, 257=>74, 377=>83, 428=>73, 535=>94, 545=>76, 563=>89, 654=>60, 681=>53, 750=>24, 905=>47, 1127=>60, 1159=>1, 1159=>1, 1159=>1, 1159=>1 , 1261=>-2),
        12 => array(9=>88, 55=>58, 74=>92, 202=>31, 242=>73, 263=>72, 309=>29, 353=>41, 388=>52, 508=>60, 535=>82, 557=>48, 574=>29, 694=>20, 755=>91, 878=>21, 1047=>26, 1059=>1 , 1241=>-60)
    );
    $shift_index = 0;
    if ( intval($gamePk) ) {
        $shift_index = intval(substr(trim($gamePk), -1));
        if (!$shift_index || $shift_index > 9) {
            $shift_index = 0;
        }
    }


    if ( $timelines[$shift_index] && $real_position ) {
        $game_offset = 0;
        $total_game = 3*DUR_PERIOD + 2*DUR_BREAK + array_sum($timelines[$shift_index]) + array_sum($timelines[$shift_index+1]) + array_sum($timelines[$shift_index+2]) + 0; // TODO: add ovetime?
        //echo "\ntotal game duration: $total_game \n";
        foreach (array(0, 1, 2) as $period) {
            //echo "\nchecking period $period with array ".($shift_index+$period)."\n";
            //echo var_dump($timelines[$shift_index+$period]);
            $timeline = $timelines[$shift_index+$period];
            $total_period = (DUR_PERIOD+array_sum($timeline));
            //echo "array_sum(timeline):" . array_sum($timeline) . "\n";
            //echo "total period duration: $total_period \n\n";
            $completed_period_offset = 0;
            for($i=0; $i<$period; $i++) {
                $completed_period_offset += array_sum($timelines[$shift_index+$i]);
            }
            $prev_period = $period*DUR_PERIOD + $period*DUR_BREAK + $completed_period_offset;
            //echo "prev_period: $prev_period\n";
            //echo "completed_period_offset: $completed_period_offset\n";
            //$total_game = $game_offset + array_sum($timeline) + ($period+1)*DUR_PERIOD + $period*DUR_BREAK;
            ////echo "total_game: $total_game\n";
            ////echo var_dump($timeline);
            if( $real_position > 0 && $real_position < $total_game ) {
                ////echo "checking 123\n";
                $prev_game_position = 0;
                $prev_game_offset = 0;
                $game_offset = 0;
                foreach ( $timeline as $game_position => $whistle_duration ) {
                    //echo "checking game_position: $game_position => whistle_duration: $whistle_duration \n";

                    //echo '($real_position - $prev_period):'.($real_position - $prev_period)."\n";
                    //echo "prev_game_position: $prev_game_position\n";
                    //echo "game_position: $game_position\n";
                    //echo "prev_game_offset: $prev_game_offset\n";
                    //echo "game_position + prev_game_offset: " . ($game_position+$prev_game_offset) . "\n";

                    if ( ($real_position - $prev_period) > ($game_position + $prev_game_offset) && 
                         ($real_position - $prev_period) <= ($game_position + $prev_game_offset + $whistle_duration) ) {
                        // whistle stop
                        //echo "whistle stop\n";
                        //echo "period shift: ".($real_position - $prev_period - $game_position )."\n";
                        return $real_position - $prev_period - $game_position + $completed_period_offset;
                        //return $prev_game_offset + ($real_position - $prev_period - $prev_game_position );
                    }
                    else if ( ($real_position - $prev_period) > ($prev_game_position + $prev_game_offset) &&
                              ($real_position - $prev_period) <= ($game_position + $prev_game_offset) ) {
                        // in game
                        //echo "in game\n";
                        //echo "period shift: ".($prev_game_offset)."\n";
                        return $prev_game_offset + $completed_period_offset;
                    }
                    else if ( ($real_position - $prev_period) > array_key_last($timelines[$shift_index]) + array_sum($timelines[$shift_index]) &&
                              ($real_position - $prev_period) <= $total_period + DUR_BREAK ) {
                        //echo "no breaks left\n";
                        $no_break_period_offset = 0;
                        for($i=0; $i<$period+1; $i++) {
                            $no_break_period_offset += array_sum($timelines[$shift_index+$i]);
                        }
                        return $no_break_period_offset;
                    }

                    $prev_game_position = $game_position;
                    $game_offset += $whistle_duration;
                    $prev_game_offset = $game_offset;
                }
                ////echo "total period offset at " . ($period+1) . ": $game_offset \n";

            }
            else if ( $real_position > $total_game ) {
                //echo "game over\n";
                return array_sum($timelines[$shift_index]) + array_sum($timelines[$shift_index+1]) + array_sum($timelines[$shift_index+2]);
            }
            else {
                //echo 'game is not started?';
                return 0;
            }
        }    

    }

}

function getGameStartDelay($gamePk = '') {
    // game start delay
    $delay_lookup = array( 
        0 => 80,
        1 => 237,
        2 => 365,
        3 => 543,
        4 => 433,
        5 => 638,
        6 => 412,
        7 => 519,
        8 => 396,
        9 => 423
    );

    $shift_index = 0;
    if ( intval($gamePk) ) {
        $shift_index = intval(substr(trim($gamePk), -1));
        if (!$shift_index || $shift_index > 9) {
            $shift_index = 0;
        }
    }
    return $delay_lookup[$shift_index];  
}

function getGameDuration($gamePk = '') {
    // game start delay
    return getGameStartDelay($gamePk) + DUR_GAME;
}

function calcCurPosition($gamePk, $cumul_position) {
    //$test_rez = calcCurPosition('2019021176', $test_postion);
    //echo gmdate("i:s", $test_rez) . "($test_rez)\n";    
    $timelines = array(
        0 => array(68=>33, 107=>28, 113=>95, 233=>64, 299=>22, 325=>88, 427=>73, 460=>25, 527=>28, 639=>52, 779=>67, 818=>91, 833=>62),
        1 => array(23=>83, 91=>74, 225=>97, 257=>74, 377=>83, 428=>73, 535=>94, 545=>76, 563=>89, 654=>60, 681=>53, 750=>24, 805=>47, 827=>46),
        2 => array(9=>88, 55=>58, 74=>92, 202=>31, 242=>73, 263=>72, 309=>29, 353=>41, 388=>52, 508=>60, 535=>82, 557=>48, 574=>29, 694=>20, 755=>91, 778=>21, 837=>26),
        3 => array(6=>78, 100=>75, 162=>33, 186=>63, 212=>74, 248=>97, 258=>41, 374=>49, 405=>38, 442=>34, 541=>48, 580=>80, 684=>53, 750=>59, 782=>50),
        4 => array(12=>39, 81=>37, 120=>28, 280=>55, 314=>98, 342=>73, 363=>36, 461=>56, 559=>77, 571=>35, 605=>89, 645=>31, 674=>85, 734=>48, 774=>32, 783=>47, 800=>41, 826=>79, 839=>71),
        5 => array(46=>67, 69=>47, 222=>86, 337=>73, 364=>50, 380=>49, 402=>21, 421=>51, 430=>83, 479=>43, 492=>42, 533=>88, 559=>22, 660=>52, 717=>53, 735=>54, 755=>35),
        6 => array(9=>20, 24=>82, 54=>54, 224=>91, 231=>68, 240=>21, 352=>75, 390=>65, 434=>60, 600=>66, 616=>54, 724=>31, 735=>94, 744=>63, 821=>86, 833=>20),
        7 => array(41=>88, 157=>35, 310=>43, 340=>63, 401=>22, 416=>54, 445=>37, 483=>75, 500=>23, 518=>78, 526=>66, 556=>84, 727=>23, 768=>52, 801=>33, 813=>97, 829=>54),
        8 => array(95=>84, 219=>91, 239=>35, 255=>70, 281=>84, 308=>52, 413=>30, 436=>40, 534=>31, 570=>50, 599=>77, 712=>67, 739=>85, 783=>95, 818=>71, 826=>39),
        9 => array(8=>93, 21=>24, 131=>34, 233=>90, 340=>83, 388=>28, 445=>20, 472=>72, 504=>76, 539=>22, 575=>76, 689=>39, 725=>73, 776=>48, 818=>77)
    );
    $shift_index = 0;
    if ( intval($gamePk) ) {
        $shift_index = intval(substr(trim($gamePk), -1));
        if (!$shift_index || $shift_index > 9) {
            $shift_index = 0;
        }
    }

    $timeline = $timelines[$shift_index];


    if ( $timeline && $cumul_position ) {
        $cumul_stoppage = 0;
        $prev_play_position = 0;
        foreach ($timeline as $play_position => $stopage_duration) {
            echo "$play_position => $stopage_duration = " . ($play_position+$cumul_stoppage).':'.($play_position+$cumul_stoppage+$stopage_duration) . "\n";

            if ( $cumul_position > $prev_play_position+$cumul_stoppage && $cumul_position < $play_position+$cumul_stoppage ) { // play time
                // return $play_position+$cumul_stoppage - cumul_position
                echo "play time\n";
                return $cumul_position - $cumul_stoppage;
                //return $play_position+$cumul_stoppage - $cumul_position;
            } 
            else if ( $cumul_position > $play_position+$cumul_stoppage && $cumul_position < $play_position+$cumul_stoppage+$stopage_duration ) { // whistle stoppage
                // return frozen play_position
                echo "whistle stoppage\n";
                return $play_position;
            }
            $cumul_stoppage += $stopage_duration;
            $prev_play_position = $play_position;
        }
    }

}

function getGameOffset($gamePk = '') {
    // stoppage duration L: 7*60=420, M: 23.25*60=1395, H: 36*60=2160
    $offset_lookup = array( 
        0 => array(),
        1 => 874,
        2 => 423,
        3 => 1322,
        4 => 1834,
        5 => 638,
        6 => 2160,
        7 => 519,
        8 => 1139,
        9 => 1478,
        10 => 847,
        11 => 1047,
        12 => 1926,
    );
    $shift_index = 0;
    if ( intval($gamePk) ) {
        $shift_index = intval(substr(trim($gamePk), -1));
        if (!$shift_index || $shift_index > 9) {
            $shift_index = 0;
        }
    }

    //return $delay_lookup[$shift_index] + $stoppage_lookup[$shift_index] + $stoppage_lookup[$shift_index+1] + $stoppage_lookup[$shift_index+2] + DUR_GAME;
    return DUR_GAME;
}

function getGameStatus( $game, $duration, $gamePage = false ) {
    if( !empty($game) ) {
        echo_log ("\nDURATION1: ".convertToMinsSecs($duration)."\n");
        if( $gamePage ) {
            $status = $game['gameData']['status'];
            $linescore = $game['liveData']['linescore'];
        } else {
            $status = $game['status'];
            $linescore = $game['linescore'];
        }
        //echo "\n$duration\n";
        if ( $duration > 0 && $duration < getGameDuration($game['gamePk']) ) {
            //echo "\ngetGameStatus1\n";
            echo_log ("\n".json_encode($status)."\n");
            // set game status            
            $status['abstractGameState'] = 'Live';
            $status['codedGameState'] = '2';
            $status['detailedState'] = 'In Progress';
            $status['statusCode'] = '2';
            // calc period and time remaining
            //echo_log ("\nPeriod new1: ".($duration/(DUR_PERIOD + DUR_BREAK))."\n");
            //echo_log ("\nPeriod new2: ".floor($duration/(DUR_PERIOD + DUR_BREAK))."\n");
            if ( $duration < (DUR_PERIOD + DUR_BREAK) ) { // period 1
                $linescore['currentPeriod'] = '1';
                if ( $duration > DUR_PERIOD ) {
                    $linescore['currentPeriodTimeRemaining'] = 'END';
                }
                else {
                    $linescore['currentPeriodTimeRemaining'] = convertToMinsSecs((DUR_PERIOD - $duration));
                }
            }
            else if ( $duration < (DUR_PERIOD + DUR_BREAK) * 2 ) { // period 2
                $linescore['currentPeriod'] = '2';
                if ( $duration > (DUR_PERIOD*2 + DUR_BREAK) ) {
                    $linescore['currentPeriodTimeRemaining'] = 'END';
                }
                else {
                    $linescore['currentPeriodTimeRemaining'] = convertToMinsSecs(((DUR_PERIOD*2 + DUR_BREAK) - $duration));
                }
            }
            else if ( $duration < (DUR_PERIOD + DUR_BREAK) * 3 ) { // period 3
                $linescore['currentPeriod'] = '3';
                if ( $duration > (DUR_PERIOD*3 + DUR_BREAK*2) ) {
                    $linescore['currentPeriodTimeRemaining'] = 'END';
                    $saved_score = val_fetch($game['gamePk']);
                    //echo var_dump($saved_score);
                    if ( empty($saved_score) ) {

                    }
                    resolveTie($game['gamePk'], $saved_score['teams'], $period=3, $duration, 'END');
                }
                else {
                    $linescore['currentPeriodTimeRemaining'] = convertToMinsSecs(((DUR_PERIOD*3 + DUR_BREAK*2) - $duration));
                }
            }

            echo_log ("\n".json_encode($status)."\n");
        }
        else if ($game['gamePk'] == '2019021239' || $game['gamePk'] == '2019021240') {
            $status['abstractGameState'] = 'Preview';
            $status['codedGameState'] = '1';
            $status['detailedState'] = 'Preview';
            $status['statusCode'] = '1';
        }
        else if ( $duration > getGameDuration($game['gamePk']) ) {
            //echo "\ngetGameStatus2\n";
            $status['abstractGameState'] = 'Final';
            $status['codedGameState'] = '7';
            $status['detailedState'] = 'Final';
            $status['statusCode'] = '7';
        }
        else if ( $status['statusCode'] == 9 ) {
            //echo "\ngetGameStatus3\n";
            $status['abstractGameState'] = 'Preview';
            $status['codedGameState'] = '1';
            $status['detailedState'] = 'Preview';
            $status['statusCode'] = '1';
        }
        else {
            //echo "\ngetGameStatus4\n";
        }

        if( $gamePage ) {
            //game.liveData?.linescore.teams.away.goals
            $game['gameData']['status'] = $status;
            $game['liveData']['linescore'] = $linescore;
        } else {
            $game['status'] = $status;
            $game['linescore'] = $linescore;
        }
        //game.linescore.currentPeriodTimeRemaining    
    }

    return $game;
}

function getGameTeams( $game, $duration ) {
    $game_stored = val_fetch($game['gamePk']);
    if ( !empty($game_stored) ) {
        echo_log ("\n".'GAME STORED:'."");
        echo_log ("\n".json_encode($game_stored)."");
        if ( !empty($game_stored['teams']['away']['score']) ) {
            $game['teams']['away']['score'] = $game_stored['teams']['away']['score'];
            $game['linescore']['teams']['away']['goals'] = $game_stored['teams']['away']['score'];
        } else {
            $game['teams']['away']['score'] = 0;
            $game['linescore']['teams']['away']['goals'] = 0;
        }
        if ( !empty($game_stored['teams']['home']['score']) ) {
            $game['teams']['home']['score'] = $game_stored['teams']['home']['score'];
            $game['linescore']['teams']['home']['goals'] = $game_stored['teams']['home']['score'];
        } else {
            $game['teams']['home']['score'] = 0;
            $game['linescore']['teams']['home']['goals'] = 0;
        }
        //$game = array_merge_recursive($game, $game_stored);
        echo_log ("\n".'GAME MERGED:'."");
        echo_log ("\n".json_encode($game)."");
    }
    // {{ game.teams.away.score }} - {{ game.teams.home.score }}
    if( !empty($game) && $duration > 0 && $duration < getGameDuration($game['gamePk']) ) {
        echo_log ("\nDURATION2: ".convertToMinsSecs($duration)."\n");
        $linescore = $game['linescore'];
        echo_log(json_encode($linescore));
        $currentPeriod = $linescore['currentPeriod'];
        $currentPeriodTimeRemaining = $linescore['currentPeriodTimeRemaining'];
//echo var_dump($linescore);
        $game['plays'] = empty($game['plays']) ? array() : $game['plays'];
        $game['plays']['currentPlay'] = array( 'about' => array('period' => $currentPeriod, 'periodTimeRemaining' => $currentPeriodTimeRemaining));
        if ( $duration > 0 && $duration < getGameDuration($game['gamePk']) 
            && !empty($linescore['currentPeriodTimeRemaining']) ) {
            //&& !empty($linescore['currentPeriodTimeRemaining']) && $linescore['currentPeriodTimeRemaining'] !== 'END' ) {
        //echo "\n123\n";
            $teams = $game['teams'];
            echo_log ("\n".'TEAMS1:'."");
            echo_log ("\n".json_encode($game['teams'])."\n");
            //echo ("\n".json_encode($game['teams'])."\n");
            $game['teams'] = getScores($game['gamePk'], $teams, $currentPeriod, $duration, $linescore['currentPeriodTimeRemaining']);
            $game['linescore']['teams']['away']['goals'] = $game['teams']['away']['score'];
            $game['linescore']['teams']['home']['goals'] = $game['teams']['home']['score'];
//echo "\n123123\n";
            //game.liveData?.plays.currentPlay?.about.periodTimeRemaining

            //echo var_dump($linescore['teams']);
            echo_log ("\n".'TEAMS2:'."");
            echo_log ("\n".json_encode($game['teams'])."\n");
        }
        else {
            echo_log ("\nBREAK\n");
        }
    }
    else {
        //echo "\n123\n";
    }
    return $game;
}

function calcGoalProbability($away, $home) {
    //echo var_dump($away);
    //echo var_dump($home);

    //echo "AWAY: " . $away['name'] . "\n";
    //echo "HOME: " . $home['name'] . "\n\n";

    $data['away']['away'] = $away['away']['wins'] / $away['away']['losses'];
    $data['home']['home'] = $home['home']['wins'] / $home['home']['losses'];
    //echo "away_away: ".$data['away']['away']."\n";
    //echo "home_home: ".$data['home']['home']."\n";

    $data['away']['wl'] = $away['wins'] / $away['losses'];
    $data['home']['wl'] = $home['wins'] / $home['losses'];
    //echo "away_wl: ".$data['away']['wl']."\n";
    //echo "home_wl: ".$data['home']['wl']."\n";

    $data['away']['last10'] = $away['lastTen']['wins'] / $away['lastTen']['losses'];
    $data['home']['last10'] = $home['lastTen']['wins'] / $home['lastTen']['losses'];
    //echo "away_last10: ".$data['away']['last10']."\n";
    //echo "home_last10: ".$data['home']['last10']."\n";

    $data['away']['gfga'] = $away['goalsScored'] / $away['goalsAgainst'];
    $data['home']['gfga'] = $home['goalsScored'] / $home['goalsAgainst'];
    //echo "away_gfga: ".$data['away']['gfga']."\n";
    //echo "home_gfga: ".$data['home']['gfga']."\n";

    //echo $away['name'] . '(' . round(array_sum($data['away']), 2) . ') @ '.$home['name'].'(' . round(array_sum($data['home']), 2) . "): ";

    return $data;
}


function getScores($gamePk, $teams, $period, $duration, $periodTimeRemaining) {
    echo_log("\ngetScores\n");
    echo_log("\nCurrent Time: ".($duration).' of '.(DUR_PERIOD)."\n");
    $min = 0;
    $max = 4 * $period;
    //mt_rand($min, $max);
    ///
    if ( !empty($_GET['admin']) && !mt_rand(0, 47) && $periodTimeRemaining !== 'END') {
        $away_chance = 40;
        $home_chance = 60;
        if ( !empty($teams) && !empty($teams['away']) && !empty($teams['away']['leagueRecord']) 
            && !empty($teams['away']['leagueRecord']['wins']) ) {
            //
            $stats = getTeamStats($season = '20192020');
            $probability = calcGoalProbability($stats[ intval($teams['away']['team']['id']) ], $stats[ intval($teams['home']['team']['id']) ]);
            //execute('2019021153', $probability, $stats[16], $stats[7]);
            $away_chance = array_sum($probability['away']);
            $home_chance = array_sum($probability['home']);
            if( $away_chance > 10) $away_chance = 10;
            if( $away_chance < 2) $away_chance = 2;
            if( $home_chance > 10) $home_chance = 10;
            if( $home_chance < 2) $home_chance = 2;
            //@$away_chance = intval($teams['away']['leagueRecord']['wins'])/(intval($teams['away']['leagueRecord']['losses'])+1);
            //@$home_chance = 0.1+intval($teams['home']['leagueRecord']['wins'])/(intval($teams['home']['leagueRecord']['losses'])+1);
            //$away_chance = round($away_chance*10);
            //$home_chance = round($home_chance*10);
        }
        echo_log("\naway_chance: $away_chance\n");
        echo_log("home_chance: $home_chance\n");

        $team_index = prob_rand( array('away' => 1000*$away_chance, 'home' => 1000*$home_chance) );
        //echo_log2("\nteam_id: $team_id\n");
        $teams[$team_index]['score'] = $teams[$team_index]['score']+1;
        
        echo_log("\n".$team_index." scored goal!!!\n");
    }
    echo_log(TMP.$gamePk);

    val_store($gamePk, array( 'teams' => array( 'away' => array('score' => $teams['away']['score']), 'home' => array('score' => $teams['home']['score']) ) ) );
    return $teams;
}

function getTeamStats( $season ) {
    $data = val_fetch($season);
    $stats = array();
    if ( !empty($season) && !empty($data) && !empty($data['records']) ) {
        foreach ($data['records'] as $league) {
            if ( !empty($league['teamRecords']) ) {
                //echo var_dump($league['teamRecords']);
                foreach ($league['teamRecords'] as $team) {
                    if ( !empty($team['leagueRecord']) ) {
                        $team_stats = $team['leagueRecord'];
                        $team_stats['name'] = $team['team']['name'];
                        $team_stats['goalsScored'] = $team['goalsScored'];
                        $team_stats['goalsAgainst'] = $team['goalsAgainst'];
                        $team_stats['points'] = $team['points'];
                        if ( !empty($team['records']['overallRecords']) ) {
                            foreach ( $team['records']['overallRecords'] as $overallRecord) {
                                $team_stats[$overallRecord['type']] = $overallRecord;
                            }
                        }
                        $stats[$team['team']['id']] = $team_stats;
                    }
                }
            }
        }
    }
    return $stats;
}

function prob_rand(array $weightedValues) {
    $rand = mt_rand(1, (int) array_sum($weightedValues));
    foreach ($weightedValues as $key => $value) {
        $rand -= $value;
        if ($rand <= 0) {
        return $key;
        }
    }
}
    
function resolveTie($gamePk, $teams, $period, $duration, $periodTimeRemaining) {
    //echo("\nresolveTie\n");
    //echo("\nCurrent Time: ".($duration).' of '.(DUR_PERIOD)."\n");
    $min = 0;
    $max = 4 * $period;
    //echo var_dump($teams);
    //mt_rand($min, $max);
    ///
    //echo $_GET['admin']."\n";
    //echo $periodTimeRemaining."\n";
    if( !empty($_GET['admin']) && $periodTimeRemaining === 'END' && $teams['away']['score'] === $teams['home']['score'] ) {
        $team_index = mt_rand(0, 1) ? 'away':'home';
        $teams[$team_index]['score'] = $teams[$team_index]['score']+1;
        //echo("\n".$team_index." scored goal!!!\n");
    }
    //echo("\nresolveTie end\n");

    ///echo(TMP.$gamePk);

    val_store($gamePk, array( 'teams' => array( 'away' => array('score' => $teams['away']['score']), 'home' => array('score' => $teams['home']['score']) ) ) );
    return true;
}
    
function convertToMinsSecs($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $minutes = floor($time / 60);
    $seconds = floor($time % 60);
    return sprintf($format, $minutes, $seconds);
}

function echo_log($text, $debug = null ) {
    $debug = is_null($debug) ? empty($_GET['startDate']) : $debug;
    //if ($debug) echo $text;
}
function echo_log2($text, $debug = true ) {
    $debug = is_null($debug) ? empty($_GET['startDate']) : $debug;
    //if ($debug) 
    echo $text;
}


function val_store($key, $value) {
    return file_put_contents(TMP.$key.'.json', json_encode($value));
}

function val_fetch($key) {
    @$content = file_get_contents(TMP.$key.'.json');
    return $content ? json_decode($content, true) : array();
}

function val_delete($key) {
    return empty($key) || !file_exists(TMP.$key.'.json') ? false : unlink(TMP.$key.'.json');
}

if (! function_exists("array_key_last")) {
    function array_key_last($array) {
        if (!is_array($array) || empty($array)) {
            return NULL;
        }
    
        return array_keys($array)[count($array)-1];
    }
}

?>
