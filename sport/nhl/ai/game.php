<?php
require_once('include.php');

//$json = file_get_contents('nhl.json');

//echo 'game:' . $_SERVER["QUERY_STRING"];

$url =  'https://statsapi.web.nhl.com/api/v1/game/'.$_GET['params_id'].'/feed/live';

//echo $url;


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
curl_close($ch); 

$game = json_decode($output, true);

$game['liveData']['gamePk'] = $game['gamePk'];

//echo '!@#';
//echo $game['gameData']['status']['statusCode'];

if( !empty($game) && $game['gameData']['status']['statusCode'] == 9 ) {
    $game['gameData']['status']['abstractGameState'] = 'Preview';
    $game['gameData']['status']['codedGameState'] = '1';
    $game['gameData']['status']['detailedState'] = 'Preview';
    $game['gameData']['status']['statusCode'] = '1';
}


$now = time();
//$duration = $now - strtotime($game['gameData']['datetime']['dateTime']) - 3*60*60;
$calcOffsetPosition = calcOffsetPosition($game['gamePk'], $now - strtotime($game['gameData']['datetime']['dateTime']) - getGameStartDelay($game['gamePk']) - START_SHIFT);
$duration = $now - strtotime($game['gameData']['datetime']['dateTime']) - getGameStartDelay($game['gamePk']) - START_SHIFT - $calcOffsetPosition;
//$duration = $now - strtotime($game['gameDate']) - getGameStartDelay($game['gamePk']) - 4*60*60 -1;
//echo $duration;
//echo var_dump($game['liveData']);
//echo var_dump($game);
$game = getGameStatus($game, $duration, $gamePage = true);
$game['liveData'] = getGameTeams($game['liveData'], $duration, $gamePage = true);
//echo var_dump($game);


//echo var_dump($game);

//echo $output;


echo json_encode($game);


?>