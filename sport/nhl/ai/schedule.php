<?php

require_once('include.php');


//echo 'admin: ' . !empty($_GET['admin']);

//
$json = file_get_contents('nhl.json');

$json_data = json_decode($json, true);

//print_r($json_data);


if ( !empty($json_data['dates']) ) {
    foreach ( $json_data['dates'] AS $key => $challenge ) {
        echo_log ($challenge['date'] .'=='. date("Y-m-d"). "\n\n");
        if ( $challenge['date'] <= date("Y-m-d") && !empty($challenge['games']) ) {
            echo_log ('current: ' . time() . "\n");
            foreach ($challenge['games'] as $id => $game) {
                echo_log ("gamePk: ${game['gamePk']}: \n");
                echo_log ('time: ' . $game['gameDate'] . "\n");
                echo_log ('diff: ' . (strtotime($game['gameDate']) - time()));
                echo_log (' min: ' . (strtotime($game['gameDate']) - time()) / 60 . "\n");
                $now = time();
                //echo "real_offse:" . ($now - strtotime($game['gameDate']) - getGameStartDelay($game['gamePk']) - START_SHIFT) . "\n";
                $calcOffsetPosition = calcOffsetPosition($game['gamePk'], $now - strtotime($game['gameDate']) - getGameStartDelay($game['gamePk']) - START_SHIFT);
                //$calcOffsetPosition = 0;
                //echo "calcOffsetPosition: $calcOffsetPosition\n\n";
                $duration = $now - strtotime($game['gameDate']) - getGameStartDelay($game['gamePk']) - START_SHIFT - $calcOffsetPosition;
                $game = getGameStatus($game, $duration);
                $game = getGameTeams($game, $duration);
                $challenge['games'][$id] = $game;
                echo_log ("\n".json_encode($game)."\n\n");

            }
            
            //echo_log (json_encode($challenge['games']) . "\n\n";

            echo_log ("\n\n");            
        }
        $json_data['dates'][$key] = $challenge;
    }

}

if ( !empty($_GET['startDate']) ) echo json_encode($json_data);

?>