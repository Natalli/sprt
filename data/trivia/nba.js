const firebase = require("firebase");
// Required for side-effects
require("firebase/compat/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyALYE3vbv4585BzYEYI5zko35YH_iks0Ww",
    authDomain: "betting-game-hockey.firebaseapp.com",
    projectId: "betting-game-hockey"});

var db = firebase.firestore();

var data = [
  {
    "name": "Atlanta",
    "nickname": "Hawks",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/atl.png",
    "stadium": "State Farm Arena",
    "coach": "Nate McMillan",
    "championshipYear": 1958,
    "championshipCount": 1,
    "starPlayer": "Trae Young",
    "starNumber": 11,
    "college": "Oklahoma",
    "draftRoundPlace": "1/5",
    "draftRound": 1,
    "draftPlace": 5,
    "draftBy": "Dallas"
  },
  {
    "name": "Boston",
    "nickname": "Celtics",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/bos.png",
    "stadium": "TD Garden",
    "coach": "Brad Stevens",
    "championshipYear": 2008,
    "championshipCount": 17,
    "starPlayer": "Jayson Tatum",
    "starNumber": 0,
    "college": "Duke",
    "draftRoundPlace": "1/3",
    "draftRound": 1,
    "draftPlace": 3,
    "draftBy": "Boston"
  },
  {
    "name": "Brooklyn",
    "nickname": "Nets",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/bkn.png",
    "stadium": "Barclay's Center",
    "coach": "Steve Nash",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Kevin Durant",
    "starNumber": 7,
    "college": "Texas",
    "draftRoundPlace": "1/2",
    "draftRound": 1,
    "draftPlace": 2,
    "draftBy": "Seattle"
  },
  {
    "name": "Charlotte",
    "nickname": "Hornets",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/cha.png",
    "stadium": "Spectrum Center",
    "coach": "James Borrego",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Gordon Hayward",
    "starNumber": 20,
    "college": "Butler",
    "draftRoundPlace": "1/9",
    "draftRound": 1,
    "draftPlace": 9,
    "draftBy": "Utah"
  },
  {
    "name": "Chicago",
    "nickname": "Bulls",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/chi.png",
    "stadium": "United Center",
    "coach": "Billy Donovan",
    "championshipYear": 1998,
    "championshipCount": 6,
    "starPlayer": "Zach Lavine",
    "starNumber": 8,
    "college": "UCLA",
    "draftRoundPlace": "1/13",
    "draftRound": 1,
    "draftPlace": 13,
    "draftBy": "Minnesota"
  },
  {
    "name": "Cleveland",
    "nickname": "Cavaliers",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/cle.png",
    "stadium": "Rocket Mortgage Fieldhouse",
    "coach": "J. B. Bickerstaff",
    "championshipYear": 2016,
    "championshipCount": 1,
    "starPlayer": "Kevin Love",
    "starNumber": 0,
    "college": "UCLA",
    "draftRoundPlace": "1/5",
    "draftRound": 1,
    "draftPlace": 5,
    "draftBy": "Memphis"
  },
  {
    "name": "Dallas",
    "nickname": "Mavericks",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/dal.png",
    "stadium": "American Airlines Center",
    "coach": "Rick Carlisle",
    "championshipYear": 2011,
    "championshipCount": 1,
    "starPlayer": "Luka Doncic",
    "starNumber": 77,
    "college": "Did Not Attend",
    "draftRoundPlace": "1/3",
    "draftRound": 1,
    "draftPlace": 3,
    "draftBy": "Atlanta"
  },
  {
    "name": "Denver",
    "nickname": "Nuggets",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/den.png",
    "stadium": "Ball Arena",
    "coach": "Michael Malone",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Nikola Jokic",
    "starNumber": 15,
    "college": "Did Not Attend",
    "draftRoundPlace": "2/41",
    "draftRound": 2,
    "draftPlace": 41,
    "draftBy": "Denver"
  },
  {
    "name": "Detroit",
    "nickname": "Pistons",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/det.png",
    "stadium": "Little Caesars Arena",
    "coach": "Dwane Casey",
    "championshipYear": 2004,
    "championshipCount": 3,
    "starPlayer": "Blake Griffin",
    "starNumber": 23,
    "college": "Oklahoma",
    "draftRoundPlace": "1/1",
    "draftRound": 1,
    "draftPlace": 1,
    "draftBy": "Clippers"
  },
  {
    "name": "Golden State",
    "nickname": "Warriors",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/gs.png",
    "stadium": "Chase Center",
    "coach": "Steve Kerr",
    "championshipYear": 2018,
    "championshipCount": 6,
    "starPlayer": "Stephen Curry",
    "starNumber": 30,
    "college": "Davidson",
    "draftRoundPlace": "1/7",
    "draftRound": 1,
    "draftPlace": 7,
    "draftBy": "Golden State"
  },
  {
    "name": "Houston",
    "nickname": "Rockets",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/hou.png",
    "stadium": "Toyota Center",
    "coach": "Stephen Silas",
    "championshipYear": 1995,
    "championshipCount": 2,
    "starPlayer": "Christian Wood",
    "starNumber": 35,
    "college": "UNLV",
    "draftRoundPlace": "Undrafted/Undrafted",
    "draftRound": "Undrafted",
    "draftPlace": "Undrafted",
    "draftBy": "Undrafted"
  },
  {
    "name": "Indiana",
    "nickname": "Pacers",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/ind.png",
    "stadium": "Bankers Life Fieldhouse",
    "coach": "Nate Bjorkgren",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Malcolm Brogdon",
    "starNumber": 7,
    "college": "Virginia",
    "draftRoundPlace": "2/36",
    "draftRound": 2,
    "draftPlace": 36,
    "draftBy": "Milwaukee"
  },
  {
    "name": "LA",
    "nickname": "Clippers",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/lac.png",
    "stadium": "Staples Centre",
    "coach": "Tyronn Lue",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Kawhi Leonard",
    "starNumber": 2,
    "college": "San Diego State",
    "draftRoundPlace": "1/15",
    "draftRound": 1,
    "draftPlace": 15,
    "draftBy": "Indiana"
  },
  {
    "name": "Los Angeles",
    "nickname": "Lakers",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/lal.png",
    "stadium": "Staples Centre",
    "coach": "Frank Vogel",
    "championshipYear": 2020,
    "championshipCount": 17,
    "starPlayer": "LeBron James",
    "starNumber": 23,
    "college": "Did Not Attend",
    "draftRoundPlace": "1/1",
    "draftRound": 1,
    "draftPlace": 1,
    "draftBy": "Cleveland"
  },
  {
    "name": "Memphis",
    "nickname": "Grizzlies",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/mem.png",
    "stadium": "FedexForum",
    "coach": "Taylor Jenkins",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Ja Morant",
    "starNumber": 12,
    "college": "Murray State",
    "draftRoundPlace": "1/2",
    "draftRound": 1,
    "draftPlace": 2,
    "draftBy": "Memphis"
  },
  {
    "name": "Miami",
    "nickname": "Heat",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/mia.png",
    "stadium": "American Airlines Arena",
    "coach": "Erik Spoelstra",
    "championshipYear": 2013,
    "championshipCount": 3,
    "starPlayer": "Jimmy Butler",
    "starNumber": 22,
    "college": "Marquette",
    "draftRoundPlace": "1/30",
    "draftRound": 1,
    "draftPlace": 30,
    "draftBy": "Chicago"
  },
  {
    "name": "Milwaukee",
    "nickname": "Bucks",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/mil.png",
    "stadium": "Fiserv Forum",
    "coach": "Mike Budenholzer",
    "championshipYear": 1971,
    "championshipCount": 1,
    "starPlayer": "Giannis Antetokounmpo",
    "starNumber": 34,
    "college": "Did Not Attend",
    "draftRoundPlace": "1/15",
    "draftRound": 1,
    "draftPlace": 15,
    "draftBy": "Milwaukee"
  },
  {
    "name": "Minnesota",
    "nickname": "Timberwolves",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/min.png",
    "stadium": "Target Center",
    "coach": "Chris Finch",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Karl-Anthony Towns",
    "starNumber": 32,
    "college": "Kentucky",
    "draftRoundPlace": "1/1",
    "draftRound": 1,
    "draftPlace": 1,
    "draftBy": "Minnesota"
  },
  {
    "name": "New Orleans",
    "nickname": "Pelicans",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/no.png",
    "stadium": "Smoothie King Center",
    "coach": "Stan Van Gundy",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Brandon Ingram",
    "starNumber": 14,
    "college": "Duke",
    "draftRoundPlace": "1/2",
    "draftRound": 1,
    "draftPlace": 2,
    "draftBy": "Lakers"
  },
  {
    "name": "New York",
    "nickname": "Knicks",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/ny.png",
    "stadium": "Madison Square Garden",
    "coach": "Tom Thibodeau",
    "championshipYear": 1973,
    "championshipCount": 2,
    "starPlayer": "Mitchell Robinson",
    "starNumber": 23,
    "college": "Did Not Attend",
    "draftRoundPlace": "2/36",
    "draftRound": 2,
    "draftPlace": 36,
    "draftBy": "New York"
  },
  {
    "name": "Oklahoma City",
    "nickname": "Thunder",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/okc.png",
    "stadium": "Chesapeake Energy Arena",
    "coach": "Mark Daigneault",
    "championshipYear": 1979,
    "championshipCount": 1,
    "starPlayer": "Shai Gilgeous-Alexander",
    "starNumber": 2,
    "college": "Kentucky",
    "draftRoundPlace": "1/11",
    "draftRound": 1,
    "draftPlace": 11,
    "draftBy": "Charlotte"
  },
  {
    "name": "Orlando",
    "nickname": "Magic",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/orl.png",
    "stadium": "Amway Center",
    "coach": "Steve Clifford",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Nikola Vucevic",
    "starNumber": 9,
    "college": "USC",
    "draftRoundPlace": "1/6",
    "draftRound": 1,
    "draftPlace": 6,
    "draftBy": "Philadelphia"
  },
  {
    "name": "Philadelphia",
    "nickname": "76ers",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/phi.png",
    "stadium": "Wells Fargo Center",
    "coach": "Doc Rivers",
    "championshipYear": 1983,
    "championshipCount": 3,
    "starPlayer": "Joel Embiid",
    "starNumber": 21,
    "college": "Kansas",
    "draftRoundPlace": "1/3",
    "draftRound": 1,
    "draftPlace": 3,
    "draftBy": "Philadelphia"
  },
  {
    "name": "Phoenix",
    "nickname": "Suns",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/phx.png",
    "stadium": "Phoenix Suns Arena",
    "coach": "Monty Williams",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Devin Booker",
    "starNumber": 1,
    "college": "Kentucky",
    "draftRoundPlace": "1/13",
    "draftRound": 1,
    "draftPlace": 13,
    "draftBy": "Phoenix"
  },
  {
    "name": "Portland",
    "nickname": "Trail Blazers",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/por.png",
    "stadium": "Moda Center",
    "coach": "Terry Stotts",
    "championshipYear": 1977,
    "championshipCount": 1,
    "starPlayer": "Damian Lillard",
    "starNumber": 0,
    "college": "Weber State",
    "draftRoundPlace": "1/6",
    "draftRound": 1,
    "draftPlace": 6,
    "draftBy": "Portland"
  },
  {
    "name": "Sacramento",
    "nickname": "Kings",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/sac.png",
    "stadium": "Golden 1 Center",
    "coach": "Luke Walton",
    "championshipYear": 1951,
    "championshipCount": 1,
    "starPlayer": "De'Aaron Fox",
    "starNumber": 5,
    "college": "Kentucky",
    "draftRoundPlace": "1/5",
    "draftRound": 1,
    "draftPlace": 5,
    "draftBy": "Sacramento"
  },
  {
    "name": "San Antonio",
    "nickname": "Spurs",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/sa.png",
    "stadium": "AT & T Center",
    "coach": "Gregg Popovich",
    "championshipYear": 2014,
    "championshipCount": 5,
    "starPlayer": "DeMar DeRozan",
    "starNumber": 10,
    "college": "USC",
    "draftRoundPlace": "1/9",
    "draftRound": 1,
    "draftPlace": 9,
    "draftBy": "Toronto"
  },
  {
    "name": "Toronto",
    "nickname": "Raptors",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/tor.png",
    "stadium": "Scotiabank Stadium",
    "coach": "Nick Nurse",
    "championshipYear": 2019,
    "championshipCount": 1,
    "starPlayer": "Kyle Lowry",
    "starNumber": 7,
    "college": "Villanova",
    "draftRoundPlace": "1/24",
    "draftRound": 1,
    "draftPlace": 24,
    "draftBy": "Memphis"
  },
  {
    "name": "Utah",
    "nickname": "Jazz",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/utah.png",
    "stadium": "Vivint Arena",
    "coach": "Quin Snyder",
    "championshipYear": "Never Won",
    "championshipCount": 0,
    "starPlayer": "Rudy Gobert",
    "starNumber": 27,
    "college": "Did Not Attend",
    "draftRoundPlace": "1/27",
    "draftRound": 1,
    "draftPlace": 27,
    "draftBy": "Denver"
  },
  {
    "name": "Washington",
    "nickname": "Wizards",
    "logo": "https://a.espncdn.com/i/teamlogos/nba/500/wsh.png",
    "stadium": "Capital One Arena",
    "coach": "Scott Brooks",
    "championshipYear": 1978,
    "championshipCount": 1,
    "starPlayer": "Bradley Beal",
    "starNumber": 3,
    "college": "Florida",
    "draftRoundPlace": "1/3",
    "draftRound": 1,
    "draftPlace": 3,
    "draftBy": "Washington"
  }
];

qs = [
  {
     "id":1,
     "text":"Match the NBA Team with its Nickname",
     "pairs":[
        "name",
        "nickname"
     ],
     "difficulty":1
  },
  {
     "difficulty":7,
     "id":2,
     "text":"Match the NBA Team with its Stadium",
     "pairs":[
        "name",
        "stadium"
     ]
  },
  {
     "id":3,
     "difficulty":4,
     "pairs":[
        "name",
        "coach"
     ],
     "text":"Match the NBA Team with its Coach"
  },
  {
     "text":"Match the NBA Team with its most recent Championship Year",
     "id":4,
     "pairs":[
        "name",
        "championshipYear"
     ]
  },
  {
     "id":5,
     "text":"Match the NBA Team with the number of Championships Won",
     "pairs":[
        "name",
        "championshipCount"
     ]
  },
  {
     "pairs":[
        "name",
        "starPlayer"
     ],
     "text":"Match the NBA Team with its Star Player",
     "id":6
  },
  {
     "id":7,
     "text":"Match the Star Player with his Jersey #",
     "pairs":[
        "starPlayer",
        "starNumber"
     ]
  },
  {
     "text":"Match the NBA Star Player with his College Team",
     "id":8,
     "pairs":[
        "starPlayer",
        "college"
     ]
  },
  {
     "text":"Match the Star Player with the NBA Team that Drafted Him",
     "id":9,
     "pairs":[
        "starPlayer",
        "draftBy"
     ]
  },
  {
     "id":10,
     "text":"Match the Star Player with his Draft Round and Draft Position",
     "pairs":[
        "starPlayer",
        "draftRoundPlace"
     ]
  }
];

if (false)
data.forEach(obj => {
    Object.keys(obj).forEach(key => obj[key] === '' && delete obj[key]);
    db.collection("/trivia/nba/team").add(obj).then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
});

if (false)
qs.forEach((obj, i) => {
  console.log(i, obj);
  db.collection("/trivia/nba/question").doc(String(i+1)).set(obj).then(function(docRef) {
    console.log("Document written with ID: ", (i+1));
  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
  });

});