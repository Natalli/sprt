const firebase = require("firebase");
// Required for side-effects
require("firebase/compat/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyALYE3vbv4585BzYEYI5zko35YH_iks0Ww",
    authDomain: "betting-game-hockey.firebaseapp.com",
    projectId: "betting-game-hockey"});

var db = firebase.firestore();

var data = [
  {
    "team": "Atlanta",
    "nickname": "Falcons",
    "logo": "https://content.sportslogos.net/logos/7/173/full/299.png",
    "stadium": "Mercedes Benz Stadium",
    "coach": "Dan Quinn",
    "quarterback": "Matt Ryan",
    "position": 2,
    "superBowl": "",
    "bowlCount": ""
  },
  {
    "team": "Chicago",
    "nickname": "Bears",
    "logo": "https://content.sportslogos.net/logos/7/169/full/364.png",
    "stadium": "Soldier Field",
    "coach": "Matt Nagy",
    "quarterback": "Mitchell Trubisky",
    "position": 10,
    "superBowl": 1986,
    "bowlCount": 1
  },
  {
    "team": "Los Angeles",
    "nickname": "Rams",
    "logo": "https://content.sportslogos.net/logos/7/6446/full/1660_los_angeles__chargers-primary-20201.png",
    "stadium": "SoFi Stadium",
    "coach": "Sean McVay",
    "quarterback": "Jared Goff",
    "position": 16,
    "superBowl": 2000,
    "bowlCount": 1
  },
  {
    "team": "Buffalo",
    "nickname": "Bills",
    "logo": "https://content.sportslogos.net/logos/7/149/full/n0fd1z6xmhigb0eej3323ebwq.png",
    "stadium": "Bills Stadium",
    "coach": "Sean McDermott",
    "quarterback": "Josh Allen",
    "position": 17,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Washington",
    "nickname": "Football Team",
    "logo": "https://content.sportslogos.net/logos/7/6741/full/8837_washington_football_team-wordmark-20201.png",
    "stadium": "FedExField",
    "coach": "Ron Rivera",
    "quarterback": "Dwayne Haskins",
    "position": 7,
    "superBowl": 1992,
    "bowlCount": 3
  },
  {
    "team": "Cleveland",
    "nickname": "Browns",
    "logo": "https://content.sportslogos.net/logos/7/155/full/7855_cleveland_browns-primary-2015.png",
    "stadium": "FirstEnergy Stadium",
    "coach": "Kevin Stefanski",
    "quarterback": "Baker Mayfield",
    "position": 6,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Tennessee",
    "nickname": "Titans",
    "logo": "https://content.sportslogos.net/logos/7/160/full/1053.png",
    "stadium": "Nissan Stadium",
    "coach": "Mike Vrabel",
    "quarterback": "Ryan Tannehill",
    "position": 17,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Las Vegas",
    "nickname": "Raiders",
    "logo": "https://content.sportslogos.net/logos/7/6708/full/8521_las_vegas_raiders-primary-20201.png",
    "stadium": "Allegiant Stadium",
    "coach": "Jon Gruden",
    "quarterback": "Derek Carr",
    "position": 4,
    "superBowl": 1984,
    "bowlCount": 3
  },
  {
    "team": "New England",
    "nickname": "Patriots",
    "logo": "https://content.sportslogos.net/logos/7/151/full/y71myf8mlwlk8lbgagh3fd5e0.png",
    "stadium": "Gilette Stadium",
    "coach": "Bill Bellichick",
    "quarterback": "Cam Newton",
    "position": 1,
    "superBowl": 2019,
    "bowlCount": 6
  },
  {
    "team": "San Francisco",
    "nickname": "49ers",
    "logo": "https://content.sportslogos.net/logos/7/156/full/970.png",
    "stadium": "Levi's Stadium",
    "coach": "Kyle Shanahan",
    "quarterback": "Jimmy Garoppolo",
    "position": 10,
    "superBowl": 1995,
    "bowlCount": 5
  },
  {
    "team": "New York",
    "nickname": "Giants",
    "logo": "https://content.sportslogos.net/logos/7/166/full/919.gif",
    "stadium": "MetLife Stadium",
    "coach": "Joe Judge",
    "quarterback": "Daniel Jones",
    "position": 8,
    "superBowl": 2012,
    "bowlCount": 4
  },
  {
    "team": "Cincinnati",
    "nickname": "Bengals",
    "logo": "https://content.sportslogos.net/logos/7/154/full/cincinnati_bengals_logo_primary_2021_sportslogosnet-2049.png",
    "stadium": "Paul Brown Stadium",
    "coach": "Zac Taylor",
    "quarterback": "Joe Burrow",
    "position": 9,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Philadelphia",
    "nickname": "Eagles",
    "logo": "https://content.sportslogos.net/logos/7/167/full/960.png",
    "stadium": "Lincoln Financial Field",
    "coach": "Doug Pederson",
    "quarterback": "Carson Wentz",
    "position": 11,
    "superBowl": 2018,
    "bowlCount": 1
  },
  {
    "team": "New York",
    "nickname": "Jets",
    "logo": "https://content.sportslogos.net/logos/7/152/full/9116_new_york_jets-primary-2019.png",
    "stadium": "MetLife Stadium",
    "coach": "Adam Gase",
    "quarterback": "Sam Darnold",
    "position": 14,
    "superBowl": 1969,
    "bowlCount": 1
  },
  {
    "team": "Indianapolis",
    "nickname": "Colts",
    "logo": "https://content.sportslogos.net/logos/7/158/full/593.png",
    "stadium": "Lucas Oil Stadium",
    "coach": "Frank Reich",
    "quarterback": "Philip Rivers",
    "position": 17,
    "superBowl": 2007,
    "bowlCount": 2
  },
  {
    "team": "Carolina",
    "nickname": "Panthers",
    "logo": "https://content.sportslogos.net/logos/7/174/full/f1wggq2k8ql88fe33jzhw641u.png",
    "stadium": "Bank of America Stadium",
    "coach": "Matt Rhule",
    "quarterback": "Teddy Bridgewater",
    "position": 5,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Los Angeles",
    "nickname": "Chargers",
    "logo": "https://content.sportslogos.net/logos/7/6446/full/1660_los_angeles__chargers-primary-20201.png",
    "stadium": "SoFi Stadium",
    "coach": "Anthony Lynn",
    "quarterback": "Justin Herbert",
    "position": 10,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Tampa Bay",
    "nickname": "Buccaneers",
    "logo": "https://content.sportslogos.net/logos/7/180/full/pfiobtreaq7j0pzvadktsc6jv.png",
    "stadium": "Raymond James Stadium",
    "coach": "Bruce Arians",
    "quarterback": "Tom Brady",
    "position": 12,
    "superBowl": 2003,
    "bowlCount": 1
  },
  {
    "team": "Denver",
    "nickname": "Broncos",
    "logo": "https://content.sportslogos.net/logos/7/161/full/9ebzja2zfeigaziee8y605aqp.png",
    "stadium": "Empower Field at Mile High",
    "coach": "Vic Fangio",
    "quarterback": "Drew Lock",
    "position": 3,
    "superBowl": 2016,
    "bowlCount": 3
  },
  {
    "team": "Detroit",
    "nickname": "Lions",
    "logo": "https://content.sportslogos.net/logos/7/170/full/1398_detroit_lions-primary-2017.png",
    "stadium": "Ford Field",
    "coach": "Matt Patricia",
    "quarterback": "Matthew Stafford",
    "position": 9,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Arizona",
    "nickname": "Cardinals",
    "logo": "https://content.sportslogos.net/logos/7/177/full/kwth8f1cfa2sch5xhjjfaof90.png",
    "stadium": "State Farm Stadium",
    "coach": "Kliff Kingsbury",
    "quarterback": "Kyler Murray",
    "position": 1,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Dallas",
    "nickname": "Cowboys",
    "logo": "https://content.sportslogos.net/logos/7/165/full/406.png",
    "stadium": "AT&T Stadium",
    "coach": "Mike McCarthy",
    "quarterback": "Dak Prescott",
    "position": 4,
    "superBowl": 1996,
    "bowlCount": 5
  },
  {
    "team": "Seattle",
    "nickname": "Seahawks",
    "logo": "https://content.sportslogos.net/logos/7/180/full/pfiobtreaq7j0pzvadktsc6jv.png",
    "stadium": "CenturyLink Field",
    "coach": "Pete Carroll",
    "quarterback": "Russell Wilson",
    "position": 3,
    "superBowl": 2014,
    "bowlCount": 1
  },
  {
    "team": "Green Bay",
    "nickname": "Packers",
    "logo": "https://content.sportslogos.net/logos/7/171/full/dcy03myfhffbki5d7il3.png",
    "stadium": "Lambeau Field",
    "coach": "Matt Lafleur",
    "quarterback": "Aaron Rodgers",
    "position": 12,
    "superBowl": 2011,
    "bowlCount": 4
  },
  {
    "team": "New Orleans",
    "nickname": "Saints",
    "logo": "https://content.sportslogos.net/logos/7/175/full/907.png",
    "stadium": "Mercedes Benz Superdome",
    "coach": "Sean Payton",
    "quarterback": "Drew Brees",
    "position": 9,
    "superBowl": 2010,
    "bowlCount": 1
  },
  {
    "team": "Miami",
    "nickname": "Dolphins",
    "logo": "https://content.sportslogos.net/logos/7/150/full/7306_miami_dolphins-primary-2018.png",
    "stadium": "Hard Rock Stadium",
    "coach": "Brian Flores",
    "quarterback": "Ryan Fitzpatrick",
    "position": 14,
    "superBowl": 1974,
    "bowlCount": 2
  },
  {
    "team": "Jacksonville",
    "nickname": "Jaguars",
    "logo": "https://content.sportslogos.net/logos/7/159/full/8856_jacksonville_jaguars-alternate-2013.png",
    "stadium": "TIAA Bank Field",
    "coach": "Doug Marrone",
    "quarterback": "Gardner Minshew",
    "position": 15,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Kansas City",
    "nickname": "Chiefs",
    "logo": "https://content.sportslogos.net/logos/7/162/full/857.png",
    "stadium": "Arrowhead Stadium",
    "coach": "Andy Reid",
    "quarterback": "Patrick Mahomes",
    "position": 15,
    "superBowl": 2020,
    "bowlCount": 2
  },
  {
    "team": "Baltimore",
    "nickname": "Ravens",
    "logo": "https://content.sportslogos.net/logos/7/153/full/318.png",
    "stadium": "M&T Bank Stadium",
    "coach": "John Harbaugh",
    "quarterback": "Lamar Jackson",
    "position": 8,
    "superBowl": 2013,
    "bowlCount": 2
  },
  {
    "team": "Minnesota",
    "nickname": "Vikings",
    "logo": "https://content.sportslogos.net/logos/7/172/full/2704_minnesota_vikings-primary-20131.png",
    "stadium": "US Bank Stadium",
    "coach": "Mike Zimmer",
    "quarterback": "Kirk Cousins",
    "position": 8,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Houston",
    "nickname": "Texans",
    "logo": "https://content.sportslogos.net/logos/7/157/full/570.png",
    "stadium": "NRG Stadium",
    "coach": "Bill O'Brien",
    "quarterback": "Deshaun Watson",
    "position": 4,
    "superBowl": "",
    "bowlCount": 0
  },
  {
    "team": "Pittsburgh",
    "nickname": "Steelers",
    "logo": "https://content.sportslogos.net/logos/7/156/full/970.png",
    "stadium": "Heinz Field",
    "coach": "Mike Tomlin",
    "quarterback": "Ben Roethlisberger",
    "position": 7,
    "superBowl": 2009,
    "bowlCount": 6
  }
];

data.forEach(function(obj) {
    Object.keys(obj).forEach(key => obj[key] === '' && delete obj[key]);
    db.collection("/trivia/nfl/team").add(obj).then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
});

