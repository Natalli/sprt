const firebase = require("firebase");
// Required for side-effects
require("firebase/compat/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyALYE3vbv4585BzYEYI5zko35YH_iks0Ww",
    authDomain: "betting-game-hockey.firebaseapp.com",
    projectId: "betting-game-hockey"});

var db = firebase.firestore();

var data = [
  {
    "name": "Anaheim",
    "nickname": "Ducks",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Anaheim-Ducks-logo-2016-17.gif",
    "stadium": "Honda Center",
    "coach": "Dallas Eakins",
    "captain": "Ryan Getzlaf",
    "captainNumber": 15,
    "draftRoundPlace": "1/19",
    "draftBy": "Anaheim",
    "cupCount": 1,
    "cupRecent": 2007
  },
  {
    "name": "Arizona",
    "nickname": "Coyotes",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Arizona-Coyotes-logo-2016-17-462x480.png",
    "stadium": "Gila River Arena",
    "coach": "Rick Tocchet",
    "captain": "Oliver Ekman-Larsson",
    "captainNumber": 23,
    "draftRoundPlace": "1/6",
    "draftBy": "Arizona",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Boston",
    "nickname": "Bruins",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Boston-Bruins-logo-2016-17-266x266.png",
    "stadium": "TD Garden",
    "coach": "Bruce Cassidy",
    "captain": "Patrice Bergeron",
    "captainNumber": 37,
    "draftRoundPlace": "2/45",
    "draftBy": "Boston",
    "cupCount": 6,
    "cupRecent": 2011
  },
  {
    "name": "Buffalo",
    "nickname": "Sabres",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2011/10/Buffalo-Sabres-266x266.png",
    "stadium": "KeyBank Center",
    "coach": "Ralph Krueger",
    "captain": "Jack Eichel",
    "captainNumber": 9,
    "draftRoundPlace": "1/2",
    "draftBy": "Buffalo",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Calgary",
    "nickname": "Flames",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Calgary-Flames-logo-2016-17-561x480.png",
    "stadium": "Scotiabank Saddledome",
    "coach": "Geoff Ward",
    "captain": "Mark Giordano",
    "captainNumber": 5,
    "draftRoundPlace": "Undrafted",
    "draftBy": "Undrafted",
    "cupCount": 1,
    "cupRecent": 1989
  },
  {
    "name": "Carolina",
    "nickname": "Hurricanes",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Carolina-Hurricanes-logo-2016-17-265x160.png",
    "stadium": "PNC Arena",
    "coach": "Rob Brind'amour",
    "captain": "Jordan Staal",
    "captainNumber": 11,
    "draftRoundPlace": "1/2",
    "draftBy": "Pittsburgh",
    "cupCount": 1,
    "cupRecent": 2006
  },
  {
    "name": "Chicago",
    "nickname": "Blackhawks",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Chicago-Blackhawks-1999-Present-Primary-Logo-550x480.png",
    "stadium": "United Center",
    "coach": "Jeremy Colliton",
    "captain": "Jonathan Toews",
    "captainNumber": 19,
    "draftRoundPlace": "1/3",
    "draftBy": "Chicago",
    "cupCount": 6,
    "cupRecent": 2015
  },
  {
    "name": "Colorado",
    "nickname": "Avalanche",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Colorado-Avalanche-logo-2016-17-575x469.png",
    "stadium": "Ball Arena",
    "coach": "Jared Bednar",
    "captain": "Gabriel Landeskog",
    "captainNumber": 92,
    "draftRoundPlace": "1/2",
    "draftBy": "Colorado",
    "cupCount": 2,
    "cupRecent": 2001
  },
  {
    "name": "Columbus",
    "nickname": "Blue Jackets",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Columbus-Blue-Jackets-logo-2016-17-557x480.png",
    "stadium": "Nationwide Arena",
    "coach": "John Tortorella",
    "captain": "Nick Foligno",
    "captainNumber": 71,
    "draftRoundPlace": "1/28",
    "draftBy": "Ottawa",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Dallas",
    "nickname": "Stars",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2014/10/7917_dallas_stars-primary-2014-310x257.png",
    "stadium": "American Airlines Arena",
    "coach": "Rick Bownes",
    "captain": "Jamie Benn",
    "captainNumber": 14,
    "draftRoundPlace": "5/129",
    "draftBy": "Dallas",
    "cupCount": 1,
    "cupRecent": 1999
  },
  {
    "name": "Detroit",
    "nickname": "Red Wings",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Detroit-Red-Wings-logo-2016-17-575x427.png",
    "stadium": "Little Caesars Arena",
    "coach": "Jeff Blashill",
    "captain": "Dylan Larkin",
    "captainNumber": 71,
    "draftRoundPlace": "1/15",
    "draftBy": "Detroit",
    "cupCount": 11,
    "cupRecent": 2008
  },
  {
    "name": "Edmonton",
    "nickname": "Oilers",
    "logo": "https://a.espncdn.com/i/teamlogos/nhl/500/edm.png",
    "stadium": "Rogers Place",
    "coach": "Dave Tippett",
    "captain": "Connor McDavid",
    "captainNumber": 97,
    "draftRoundPlace": "1/1",
    "draftBy": "Edmonton",
    "cupCount": 5,
    "cupRecent": 1990
  },
  {
    "name": "Florida",
    "nickname": "Panthers",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Florida-Panthers-logo-2016-17-430x480.png",
    "stadium": "BB&T Center",
    "coach": "Joel Quenneville",
    "captain": "Aleksander Barkov",
    "captainNumber": 16,
    "draftRoundPlace": "1/2",
    "draftBy": "Florida",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Los Angeles",
    "nickname": "Kings",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Los-Angeles-Kings-logo-2016-17-397x480.png",
    "stadium": "Staples Center",
    "coach": "Todd McLellan",
    "captain": "Anze Kopitar",
    "captainNumber": 11,
    "draftRoundPlace": "1/11",
    "draftBy": "Los Angeles",
    "cupCount": 2,
    "cupRecent": 2014
  },
  {
    "name": "Minnesota",
    "nickname": "Wild",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Minnesota-Wild-logo-2016-17-575x375.png",
    "stadium": "Xcel Energy Center",
    "coach": "Dean Evason",
    "captain": "Jared Spurgeon",
    "captainNumber": 46,
    "draftRoundPlace": "6/156",
    "draftBy": "Islanders",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Montreal",
    "nickname": "Canadiens",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Montreal-Canadiens-logo-2016-17-575x383.png",
    "stadium": "Bell Centre",
    "coach": "Claude Julien",
    "captain": "Shea Weber",
    "captainNumber": 6,
    "draftRoundPlace": "2/49",
    "draftBy": "Nashville",
    "cupCount": 24,
    "cupRecent": 1993
  },
  {
    "name": "Nashville",
    "nickname": "Predators",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Nashville-Predators-logo-2016-17-575x340.png",
    "stadium": "Bridgestone Arena",
    "coach": "John Hynes",
    "captain": "Roman Josi",
    "captainNumber": 59,
    "draftRoundPlace": "2/38",
    "draftBy": "Nashville",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "New Jersey",
    "nickname": "Devils",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/New-Jersey-Devils-logo-2016-17-469x480.png",
    "stadium": "Prudential Center",
    "coach": "Lindy Ruff",
    "captain": "Nico Hischier",
    "captainNumber": 13,
    "draftRoundPlace": "1/1",
    "draftBy": "New Jersey",
    "cupCount": 3,
    "cupRecent": 2003
  },
  {
    "name": "New York",
    "nickname": "Islanders",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2011/10/Buffalo-Sabres-266x266.png",
    "stadium": "Nassau Coliseum",
    "coach": "Barry Trotz",
    "captain": "Anders Lee",
    "captainNumber": 27,
    "draftRoundPlace": "6/152",
    "draftBy": "Islanders",
    "cupCount": 4,
    "cupRecent": 1983
  },
  {
    "name": "New York",
    "nickname": "Rangers",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/New-York-Rangers-logo-2016-17-498x480.png",
    "stadium": "Madison Square Gardens",
    "coach": "David Quinn",
    "captain": "No Captain",
    "captainNumber": "n/a",
    "draftRoundPlace": "n/a",
    "draftBy": "n/a",
    "cupCount": 4,
    "cupRecent": 1994
  },
  {
    "name": "Ottawa",
    "nickname": "Senators",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Ottawa-Senators-logo-2016-17-406x480.png",
    "stadium": "Canadian Tire Centre",
    "coach": "D. J. Smith",
    "captain": "No Captain",
    "captainNumber": "n/a",
    "draftRoundPlace": "n/a",
    "draftBy": "n/a",
    "cupCount": 0,
    "cupRecent": 1927
  },
  {
    "name": "Philadelphia",
    "nickname": "Flyers",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Philadelphia-Flyers-logo-2016-17-560x390.png",
    "stadium": "Wells Fargo Center",
    "coach": "Alain Vigneault",
    "captain": "Claude Giroux",
    "captainNumber": 28,
    "draftRoundPlace": "1/22",
    "draftBy": "Philadelphia",
    "cupCount": 2,
    "cupRecent": 1975
  },
  {
    "name": "Pittsburgh",
    "nickname": "Penguins",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Pittsburgh-Penguins-logo-2016-17-511x480.png",
    "stadium": "PPG Paints Arena",
    "coach": "Mike Sulivan",
    "captain": "Sidney Crosby",
    "captainNumber": 87,
    "draftRoundPlace": "1/1",
    "draftBy": "Pittsburgh",
    "cupCount": 5,
    "cupRecent": 2017
  },
  {
    "name": "San Jose",
    "nickname": "Sharks",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/San-Jose-Sharks-logo-2016-17-575x467.png",
    "stadium": "SAP Center",
    "coach": "Bob Boughner",
    "captain": "Logan Couture",
    "captainNumber": 39,
    "draftRoundPlace": "1/9",
    "draftBy": "San Jose",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "St. Louis",
    "nickname": "Blues",
    "logo": "https://a.espncdn.com/i/teamlogos/nhl/500/stl.png",
    "stadium": "Enterprise Arena",
    "coach": "Craig Berube",
    "captain": "Ryan O'Reilly",
    "captainNumber": 90,
    "draftRoundPlace": "2/33",
    "draftBy": "Colorado",
    "cupCount": 1,
    "cupRecent": 2019
  },
  {
    "name": "Tampa Bay",
    "nickname": "Lighning",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Tampa-Bay-Lightning-logo-2016-17-514x480.png",
    "stadium": "Amalie Arena",
    "coach": "Jon Cooper",
    "captain": "Steven Stamkos",
    "captainNumber": 91,
    "draftRoundPlace": "1/1",
    "draftBy": "Tampa Bay",
    "cupCount": 2,
    "cupRecent": 2020
  },
  {
    "name": "Toronto",
    "nickname": "Maple Leafs",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Toronto-Maple-Leafs-logo-2016-17-430x480.png",
    "stadium": "Scotiabank Arena",
    "coach": "Sheldon Keefe",
    "captain": "John Tavares",
    "captainNumber": 91,
    "draftRoundPlace": "1/1",
    "draftBy": "Islanders",
    "cupCount": 13,
    "cupRecent": 1967
  },
  {
    "name": "Vancouver",
    "nickname": "Canucks",
    "logo": "https://www.logolynx.com/topic/vancouver+canucks#&gid=1&pid=4",
    "stadium": "Rogers Arena",
    "coach": "Travis Green",
    "captain": "Bo Horvat",
    "captainNumber": 53,
    "draftRoundPlace": "1/9",
    "draftBy": "Vancouver",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Vegas",
    "nickname": "Golden Knights",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Vegas-Golden-Knights-logo-355x480.png",
    "stadium": "T-Mobile Arena",
    "coach": "Peter DeBoer",
    "captain": "Mark Stone",
    "captainNumber": 61,
    "draftRoundPlace": "6/178",
    "draftBy": "Ottawa",
    "cupCount": 0,
    "cupRecent": "n/a"
  },
  {
    "name": "Washington",
    "nickname": "Capitals",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Washington-Capitals-logo-2016-17-575x362.png",
    "stadium": "Capital One Arena",
    "coach": "Peter Laviolette",
    "captain": "Alexander Ovechkin",
    "captainNumber": 8,
    "draftRoundPlace": "1/1",
    "draftBy": "Washington",
    "cupCount": 1,
    "cupRecent": 2018
  },
  {
    "name": "Winnipeg",
    "nickname": "Jets",
    "logo": "https://s3951.pcdn.co/wp-content/uploads/2015/09/Winnipeg-Jets-logo-2016-17-480x480.png",
    "stadium": "Bell MTS Place",
    "coach": "Paul Maurice",
    "captain": "Blake Wheeler",
    "captainNumber": 26,
    "draftRoundPlace": "1/5",
    "draftBy": "Arizona",
    "cupCount": 0,
    "cupRecent": "n/a"
  }
];

qs = [
  {
     "id":1,
     "text":"Match the NHL Team with its Nickname",
     "pairs":[
        "name",
        "nickname"
     ],
     "difficulty":1
  },
  {
    "id":2,
    "difficulty":7,
     "text":"Match the NHL Team with its Stadium",
     "pairs":[
        "name",
        "stadium"
     ]
  },
  {
     "id":3,
     "difficulty":4,
     "pairs":[
        "name",
        "coach"
     ],
     "text":"Match the NHL Team with its Coach"
  },
  {
    "id":4,
    "text":"Match the NHL Team with its most recent Cap Year",
     "pairs":[
        "name",
        "cupRecent"
     ]
  },
  {
     "id":5,
     "text":"Match the NHL Team with the number of Cups Won",
     "pairs":[
        "name",
        "cupCount"
     ]
  },
  {
    "id":6,
    "pairs":[
        "name",
        "captain"
     ],
     "text":"Match the NHL Team with its Captain"
  },
  {
     "id":7,
     "text":"Match the NHL Captain with his Draft Position",
     "pairs":[
        "captain",
        "draftRoundPlace"
     ]
  },
  {
     "id":8,
     "text":"Match the NHL Captain with the NHL Team that drafted him",
     "pairs":[
        "captain",
        "draftBy"
     ]
  },
  {
     "id":9,
     "text":"Match the NHL Captain with his Jersey #",
     "pairs":[
        "captain",
        "captainNumber"
     ]
  }
];

//if (false)
data.forEach(obj => {
    Object.keys(obj).forEach(key => obj[key] === '' && delete obj[key]);
    db.collection("/trivia/nhl/team").add(obj).then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
});

//if (false)
qs.forEach((obj, i) => {
  db.collection("/trivia/nhl/question").doc(String(i+1)).set(obj).then(function(docRef) {
    console.log("Document written with ID: ", (i+1));
  })
  .catch(function(error) {
      console.error("Error adding document: ", error);
  });

});