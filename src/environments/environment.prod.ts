export const environment = {
  production: true,
  version: "2.0.6",
  build: "385",
  config: {
    apiKey: "AIzaSyALYE3vbv4585BzYEYI5zko35YH_iks0Ww",
    authDomain: "betting-game-hockey.firebaseapp.com",
    databaseURL: "https://betting-game-hockey.firebaseio.com",
    projectId: "betting-game-hockey",
    storageBucket: "betting-game-hockey.appspot.com",
    messagingSenderId: "954133284025",
    appId: "1:954133284025:web:33f31e6e87685d58240768",
    measurementId: "G-XCRZXMH703",
    experimentalForceLongPolling: true
  },
  admins: ['JTdh9Y9eO2MzzPJnEGboOIsdtxC3', 'lrDtAgYsqDgdMzb4c8r7zEVoVLU2'],
  nhl_legacy_api: 'https://statsapi.web.nhl.com/api/v1/',
  nhl_api: 'https://www.livecurrent.com/ai',
  nfl_legacy_api: 'https://site.api.espn.com/apis/site/v2/sports/football/nfl/',
  nba_legacy_api: 'https://site.api.espn.com/apis/site/v2/sports/basketball/nba/'
};
