// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: "AIzaSyALYE3vbv4585BzYEYI5zko35YH_iks0Ww",
  authDomain: "betting-game-hockey.firebaseapp.com",
  databaseURL: "https://betting-game-hockey.firebaseio.com",
  projectId: "betting-game-hockey",
  storageBucket: "betting-game-hockey.appspot.com",
  messagingSenderId: "954133284025",
  appId: "1:954133284025:web:41eacd872006ac26240768"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();