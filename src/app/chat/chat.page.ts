import { Component, OnInit, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Constants } from '../constants'
import { PopupMessageComponent } from '../popups/popup-message/popup-message.component';
import { PopoverController, LoadingController, IonItemSliding, AlertController, ModalController, IonIcon } from '@ionic/angular';
import { ActivityService } from '../services/activity.service';
import { ProfilePage } from '../pages/profile/profile.page';
import { Subject, Subscription } from 'rxjs';

import { debounceTime } from 'rxjs/operators';
import { LayoutService } from '../services/layout.service';
import { NotificationsPage } from '../notifications/notifications.page';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { SearchService } from '../services/search/search.service';
import { UserService } from '../service/user.service';
import { ModalChatCreateComponent } from '../popups/new/modal-chat-create/modal-chat-create.component';
import { ModalChatManageComponent } from '../popups/new/modal-chat-manage/modal-chat-manage.component';
import { ModalService } from '../service/modal.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage extends NotificationsPage implements OnInit {
  @ViewChild(IonItemSliding) slidingItem: IonItemSliding;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  private subject: Subject<string> = new Subject();
  private subscription: Subscription;

  user: any;
  messages = {};
  objectKeys = Object.keys;
  messageRefs: any[] = [];
  users = {};
  constants = Constants;
  search = '';
  sortedMessages: Array<any> = [];
  myUid;
  filteredRefs;
  chatLinks: Object;

  constructor(
    public popoverCtrl: PopoverController,
    protected afs: AngularFirestore,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public modalController: ModalController,
    public activityService: ActivityService,
    protected layoutService: LayoutService,
    protected router: Router,
    public myapp: AppComponent,
    protected searchService: SearchService,
    protected userService: UserService,
    public modalService: ModalService) {
    super(afs, loadingController, alertController, modalController, popoverCtrl, router, layoutService, userService, modalService)
    this.userService.user$.subscribe((user: any) => {
      if (user) {
        this.user = user;
        this.myUid = user.uid;
        if (user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
          this.myapp.loginAlert();
        }
        this.afs.collection('/user/' + this.user.uid + '/chat')
          .valueChanges({ idField: 'id' }).subscribe((messageRefs: Array<any>) => {
            const updatedRefs = [];
            let messageRefsFiltered = messageRefs.filter(item => item.challengeId || item.gameId ? null : item);
            // console.log("ChatPage -> messageRefsFiltered", messageRefsFiltered)
            messageRefsFiltered.forEach(message_ref => {
              if (!this.messages || typeof this.messages[message_ref.id] === 'undefined') { // Build Chat list once
                this.afs.collection('/chat/' + message_ref.id + '/message', ref => ref
                  .orderBy('created', 'desc'))
                  .valueChanges({ idField: 'id' }).subscribe((messages: Array<any>) => {
                    if (messages.length || message_ref) {
                      if (typeof this.messages[message_ref.id] === 'undefined') {
                        if (!message_ref.private) {
                          this.afs.doc('/chat/' + message_ref.id).valueChanges().subscribe((chat: any) => {
                            if (chat) {
                              if (chat.users.indexOf(this.user.uid) !== -1) {
                                chat.users.splice(chat.users.indexOf(this.user.uid), 1);
                              }
                              this.getUserProfiles(chat.users);
                              message_ref.users = chat.users;
                              const activeChat = this.messageRefs.find((item, i) => {
                                if (item.id === message_ref.id) {
                                  return message_ref
                                }
                              })
                              if (!activeChat) {
                                this.messageRefs.push(message_ref);
                                this.filterChatList();
                              }
                            }
                          });
                        } else {
                          this.messageRefs.push(message_ref);
                        }
                      }
                      this.messages[message_ref.id] = messages;
                      if (this.messageRefs && this.messageRefs.length > 0) {
                        this.filterChatList();
                      }
                    }
                  });
              } else {
                if (!message_ref.private) {
                  const old_ref = this.messageRefs.find(old_ref => {
                    if (old_ref.id === message_ref.id) {
                      old_ref.groupName = message_ref.groupName;
                      old_ref.groupPhoto = message_ref.groupPhoto;
                      old_ref.status = message_ref.status;
                      old_ref.users = message_ref.users
                      return old_ref
                    } else {
                      return null
                    }
                  });
                  if (old_ref) {
                    updatedRefs.push(old_ref);
                  }
                } else {
                  updatedRefs.push(message_ref);
                }
              }
              if (message_ref.private &&
                (!this.users || typeof this.users[message_ref.private] === 'undefined')) {
                this.getUserProfiles([message_ref.private]);
              }
              if (!message_ref.private) {
              }
              if (message_ref.users && message_ref.users.length) {
              }
              // if (this.messageRefs && this.messageRefs.length > 0) {
              //   console.log(this.messageRefs, 'C')
              //   this.filterChatList();
              // }
            });
            if (this.messageRefs && this.messageRefs.length > 0) {
              this.messageRefs = updatedRefs;
              this.filterChatList();
            }
          });
      }
    });
  }

  getUserProfiles(uids: Array<string>) {
    uids.forEach(uid => {
      if (uid) {
        this.afs.doc('/user/' + uid).get().subscribe(user => {
          if (user.data()) {
            this.users[uid] = user.data();
            this.users[uid].uid = uid;
          }
        });
      }
    });
  }

  getChatUsers(chatId) {
    const messageRef = this.messageRefs.find(item => item.id === chatId);
    if (!messageRef) return [];
    return (messageRef.private ? Array(messageRef.private) : messageRef.users) || [];
  }

  ngOnInit() {
    super.getUsers();
    this.filterChatList();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  async openPrivateChat(ev: any, user_uid: any, chatId?: any, chat?: any) {
    const modal = await this.modalController.create({
      component: PopupMessageComponent,
      componentProps: {
        recepient: this.users[user_uid],
        chatId: chatId,
        chat,
        popoverCtrl: this.popoverCtrl,
        afs: this.afs,
        deleteChat: this.deleteChat,
        alertController: this.alertController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-default',
      id: 'popupMessages'
    });
    return await modal.present();
  }

  async openChat(ev: any, chat: any) {
    if (chat && chat.private) {
      return this.openPrivateChat(ev, chat.private, chat.id, chat);
    } else if (chat && !chat.private) {
      const modal = await this.modalController.create({
        component: PopupMessageComponent,
        componentProps: {
          recepient: null,
          groupName: chat.groupName,
          groupPhoto: chat.groupPhoto,
          chatId: chat.id,
          chat,
          popoverCtrl: this.popoverCtrl,
          afs: this.afs,
          messageRefs: this.messageRefs,
          deleteChat: this.deleteChat,
          alertController: this.alertController
        },
        id: 'popupMessages',
        swipeToClose: true,
        showBackdrop: true,
        backdropDismiss: true,
        cssClass: 'popover-default',
      });
      return await modal.present();
    }
  }

  ngOnDestroy(): void {
    if (typeof this.subscription !== 'undefined') {
      this.subscription.unsubscribe();
    }
  }

  clearFilter() {
    this.filterChatList();
    this.subscription.unsubscribe();
  }

  filterChatList(event?) {
    if (event) {
      const search = event.target.value;
      if (this.users && search.length > 1) {
        this.subject.next(search);
        this.subscription = this.subject.pipe(
          debounceTime(1000))
          .subscribe(searchTextValue => {
            this.filteredRefs = [];
            this.messageRefs.forEach(message_ref => {
              if (message_ref.private && this.users[message_ref.private] &&
                this.users[message_ref.private].name.toLowerCase().indexOf(searchTextValue.toLowerCase()) !== -1) {
                const findRef = this.filteredRefs.find(ref => ref.id === message_ref.id ? ref : null);
                if (!findRef) {
                  this.filteredRefs.push(message_ref);
                }
              } else if (!message_ref.private) {
                this.getChatUsers(message_ref.id).forEach(user => {
                  if (this.users[user].name.toLowerCase().indexOf(searchTextValue.toLowerCase()) !== -1) {
                    const findRef = this.filteredRefs.find(ref => ref.id === message_ref.id ? ref : null);
                    if (!findRef) {
                      this.filteredRefs.push(message_ref);
                    }
                  }
                });
              };
              this.filterTextMessage(message_ref, searchTextValue);
            });
          });
      }
    } else {
      this.sorted()
      this.filteredRefs = this.messageRefs;
    }
  }

  filterTextMessage(message_ref, search) {
    this.afs.collection('/chat/' + message_ref.id + '/message', ref =>
      ref.where('text', ">=", search)
        .where('text', '<=', search + '\uf8ff'))
      .valueChanges({ idField: 'id' }).subscribe((messages: Array<any>) => {
        if (messages.length > 0) {

          const findRef = this.filteredRefs.find(ref => ref.id === message_ref.id ? ref : null);
          if (!findRef) {
            this.filteredRefs.push(message_ref);
          }
        }
      })
  }

  sorted() {
    this.messageRefs.sort((a, b) => (this.messages[a.id][0] && this.messages[b.id][0]) ? new Date(this.messages[b.id][0]['created'].seconds).getTime() - new Date(this.messages[a.id][0]['created'].seconds).getTime() : null);
    this.messageRefs.filter((item, i) => {
      if (!this.messages[item.id][0]) {
        this.messageRefs.splice(i, 1);
        this.messageRefs.unshift(item)
      }
    });
    // console.log(this.messageRefs, 'this.messageRefs')
    this.chatLinks = {};
    Object.entries(this.links).forEach(link => {
    Object.keys(link[1]).map((key) => {
        if(key === 'chatId') {
          delete this.links[link[0]]
        }
      })
    })
    this.messageRefs.forEach(chat => {
      this.chatLinks[chat.id] = chat;

      if (this.messages[chat.id] && this.messages[chat.id].length) {
        this.links[this.messages[chat.id][0].created.seconds] = { chatId: chat.id }
      } else if (chat.dateJoined) {
        this.links[chat.dateJoined.seconds] = { chatId: chat.id }
      }
    })
    // console.log("sorted -> this.chatLinks", this.chatLinks)
    // console.log("filterChatList -> this.links", this.links)
  }

  getSortedSchedule() {
    return Object.keys(this.links).sort((a: any, b: any) => { return b - a; });
  }

  async reportChat(chatId: any, recepientId?: any, adminChat?) {
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Report and Delete this Chat?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            if (this.slidingItem) {
              this.slidingItem.closeOpened();
            }
          }
        }, {
          text: 'Report',
          cssClass: 'danger',
          handler: () => {
            if (this.slidingItem) {
              this.slidingItem.closeOpened();
            }
            if (adminChat) {
              this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
              this.afs.collection('/chat/').doc(chatId).get().subscribe(resp => {
                let users = resp.data()['users'];
                users.forEach(user => {
                  this.afs.collection('/user/' + user + '/chat').doc(chatId).delete();
                })
                this.afs.collection('/chat/').doc(chatId).delete();
              })
            } else if (recepientId) {
              this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
            } else {
              this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  async deleteChat(chatId: any, recepientId?: any, adminChat?) {
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Delete this Chat?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            if (this.slidingItem) {
              this.slidingItem.closeOpened();
            }
          }
        }, {
          text: 'Delete',
          cssClass: 'danger',
          handler: () => {
            if (this.slidingItem) {
              this.slidingItem.closeOpened();
            }
            if (adminChat) {
              this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
              this.afs.collection('/chat/').doc(chatId).get().subscribe(resp => {
                let users = resp.data()['users'];
                users.forEach(user => {
                  this.afs.collection('/user/' + user + '/chat').doc(chatId).delete();
                })
                this.afs.collection('/chat/').doc(chatId).delete();
              })
            } else if (recepientId) {
              this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
            } else {
              this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  async leaveChat(ev, chatId, privateChat?) {
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Are you sure you want to leave this chat?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.slidingItem.closeOpened();
          }
        }, {
          text: 'Leave',
          cssClass: 'danger',
          handler: () => {
            this.slidingItem.closeOpened();
            this.afs.collection('/chat/').doc(chatId).get().subscribe(resp => {
              if (privateChat) {
                this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
              } else {
                let chatUsers = resp.data()['users'];
                const user_index = chatUsers.indexOf(this.user.uid);
                if (user_index !== -1) {
                  chatUsers.splice(user_index, 1);
                  this.afs.collection('/chat/').doc(chatId).set({ users: chatUsers });
                  this.afs.collection('/user/' + this.user.uid + '/chat').doc(chatId).delete();
                };
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async manageChat(chatId: any, privateChat?: boolean, globalManage?: boolean) {
    const popover = await this.popoverCtrl.create({
      component: ModalChatManageComponent,
      componentProps: {
        chatId: chatId,
        modalController: this.modalController,
        afs: this.afs,
        messageRefs: this.messageRefs,
        globalManage,
        privateChat,
        myUid: this.myUid,
        openPrivateChat: this.openPrivateChat,
        createGroup: this.createGeneralChatInfo,
        searchService: this.searchService
      },
      // swipeToClose: true,
      // showBackdrop: true,
      // backdropDismiss: true,
      cssClass: 'modal-wrap-default',
    });
    popover.onDidDismiss().then((data) => {
      if (this.slidingItem) {
        this.slidingItem.closeOpened();
      }
    });
    await popover.present();
  }

  async createGeneralChatInfo(globalManage?: boolean) {
    const popover = await this.popoverCtrl.create({
      component: ModalChatCreateComponent,
      componentProps: {
        afs: this.afs,
        messageRefs: this.messageRefs,
        globalManage,
        myUid: this.myUid,
        searchService: this.searchService
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const modal = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid,
        modalController: this.modalController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await modal.present();
  }

  openInviteGame(link, sender) {
    this.router.navigate([link+"/"+sender]);
  }

}
