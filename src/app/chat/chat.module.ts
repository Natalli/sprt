import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChatPage } from './chat.page';
import { UserModule } from '../components/user/user.module';
import { DirectivesModule } from '../directives/directives.module';
import { SearchServiceModule } from '../services/search/search.module';
import { MentionModule } from '../components/mention/mention.module';
import { PipesModule } from '../pipes/pipes.module';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';

const routes: Routes = [
  {
    path: '',
    component: ChatPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule.forRoot(),
        RouterModule.forChild(routes),
        UserModule,
        DirectivesModule,
        SearchServiceModule,
        MentionModule,
        PipesModule,
        HeaderModule,
        SubHeaderModule
    ],
    declarations: [ChatPage]
})
export class ChatPageModule { }
