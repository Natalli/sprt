export const SPORT_TABS = [
    { name: 'all', icon: '' },
    { name: 'nhl', icon: 'hockey' },
    { name: 'nfl', icon: 'american-football' },
    { name: 'nba', icon: 'basketball' }
]