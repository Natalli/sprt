import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      
      // {
      //   path: 'challenges',
      //   loadChildren: () => import('../challenges/challenges.module').then(m => m.ChallengesPageModule)
      // },
      {
        path: 'private-games',
        loadChildren: () => import('../pages/challenges/challenges.module').then(m => m.ChallengesPageModule)
      },
      {
        path: 'trivia',
        loadChildren: () => import('../trivia/trivia.module').then(m => m.TriviaPageModule)
      },
      {
        path: 'chat',
        loadChildren: () => import('../chat/chat.module').then(m => m.ChatPageModule)
      },
      {
        path: '',
        redirectTo: '/start',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
