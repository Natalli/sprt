import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
import { IonicModule } from '@ionic/angular';
import { DirectivesModule } from '../directives/directives.module';
import { TabsPageRoutingModule } from './tabs-routing.module';

@NgModule({
  declarations: [TabsPage],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    DirectivesModule,
    TabsPageRoutingModule
  ]
})
export class TabsPageModule { }
