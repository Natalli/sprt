import { Component, OnInit, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { ActivityService } from '../services/activity.service';
import { Router, NavigationEnd } from '@angular/router';
import { LayoutService } from '../services/layout.service';
import { IonIcon } from '@ionic/angular';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss']
})
export class TabsPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  tabsMenu: boolean;
  count: number = 0;
  constructor(
    public activityService: ActivityService,
    public router: Router,
    protected layoutService: LayoutService,
    public myapp: AppComponent) {
  }

  ngOnInit() {
    this.activityService.count$.subscribe(resp => {
      this.count = resp;
    })
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (e.url === '/login' ||
          e.url === '/signup' ||
          e.url === '/forgot' ||
          e.url.split('/')[1] === 'bid' ||
          e.url.split('/')[1] === 'detail' ||
          e.url === '/tutorial') {
          this.tabsMenu = false;
        } else {
          this.tabsMenu = true
        }
      }
    });
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

}
