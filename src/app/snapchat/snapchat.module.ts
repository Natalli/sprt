import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SnapchatComponent } from './snapchat.component';
import { MentionModule } from '../components/mention/mention.module';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [SnapchatComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    MentionModule,
    PipesModule
  ],
  exports: [SnapchatComponent]
})
export class SnapchatModule { }
