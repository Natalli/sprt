import { Component, OnInit, Input, ViewChild, ElementRef, ViewChildren, QueryList, HostListener } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ModalController, Platform, IonIcon } from '@ionic/angular';
import { ProfilePage } from '../pages/profile/profile.page';
import { Constants } from '../constants';
import { UserService } from '../service/user.service';
import { Subscription } from 'rxjs';
import { LayoutService } from '../services/layout.service';
import { UsersService } from '../service/users.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AppComponent } from '../app.component';
import { DatePipe } from '@angular/common';
import { AngularFireFunctions } from '@angular/fire/compat/functions';

@Component({
  selector: 'app-snapchat',
  templateUrl: './snapchat.component.html',
  styleUrls: ['./snapchat.component.scss'],
})

export class SnapchatComponent implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  @ViewChild('focusInput') focusInput;
  @ViewChild('scrollMe', { read: ElementRef }) private myScrollContainer: ElementRef;
  @Input() set setChallengeId(value) {
    if (value) {
      this.challengeId = value;
    }
  }

  @Input() set setSport(value) {
    if (value) {
      this.sport = value;
    }
  }

  @Input() set setGameId(value) {
    if (value) {
      this.gameId = value;
    }
  }

  @Input() set setInviteId(value) {
    if (value) {
      this.inviteId = value;
    }
  }

  @Input() set setMyUid(value) {
    if (value) {
      this.myUid = value;
    }
  }

  @Input() notLoggedUser: boolean;

  private subscription: Subscription;

  inviteId: any;
  gameId: any;
  challengeId: any;
  sport: any;
  myUid: string;

  message: string = '';
  chatId;
  chatRef;
  user;
  recepient;
  users = {};
  timer;
  messages;
  constants = Constants;
  showUserName; boolean;
  chatUsers;
  chat;
  expand: boolean;
  showSnapchat: boolean;
  mobile: boolean;
  chatSubscription: any;
  segmentSelected: string = 'all';
  loading: boolean;
  mentions: any;
  mentionName: string;
  userMentions: any[] = [];
  firstLoad: boolean = false;
  expandArea: boolean = false;

  constructor(
    private afs: AngularFirestore,
    public modalController: ModalController,
    protected userService: UserService,
    public platform: Platform,
    protected layoutService: LayoutService,
    private usersService: UsersService,
    public myapp: AppComponent,
    protected datePipe: DatePipe,
    protected elementRef: ElementRef,
    private fns: AngularFireFunctions,
    private router: Router) {
    this.users = this.usersService.users;
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        if (event.url.split('/').length > 2) {
          this.scrollToBottom();
        }
      }
    });

  }

  ngOnInit() {
    this.initChat();
    this.sizeWindowDetect();
  }

  @HostListener('click', ['$event'])
  onClick(e) {
    // let mentionBlock = e['target'].classList[0];
    if (e['target'].classList[0] === 'decorate') {
      let id = e['target'].classList[1];
      this.userInfo(id);
    }
  }

  addMention(e) {
    let txt = e.target.value;
    if (!txt.length && this.userMentions.length) {
      this.userMentions = [];
    }
    if (txt.includes('@')) {
      let names = txt.split('@');
      let name = names[names.length - 1].toLowerCase();
      if (name.length > 1) {
        this.mentions = Object.values(this.users).filter(user => user['keywords'].includes(name) ? user : null);
        if (this.mentions.length) {
          this.mentions = this.mentions.filter(function (e) { return this.indexOf(e) < 0 }, this.userMentions);
        }
      } else {
        this.mentions = [];
      }
    }
  }

  selectedMention(data) {
    this.mentionName = data.name;
    let mention = this.message.split('@').pop();
    this.message = this.message.replace(mention, this.mentionName)
    this.userMentions.push(data)
    this.mentions = null;
    this.focusInput.setFocus();
    this.message = this.message + ' ';
  }

  getUser() {
    if (this.myUid) {
      this.afs.collection("user").doc(this.myUid).get().subscribe(response => {
        this.user = response.data();
      })
    }
  }

  sizeWindowDetect() {
    if (this.platform.width() < 575) {
      this.mobile = true;
      this.showSnapchat = true;
      // this.initChat();
    } else {
      this.mobile = false;
    }
    this.platform.resize.subscribe(async () => {
      if (this.platform.width() < 575) {
        this.mobile = true;
        this.showSnapchat = true;
      } else {
        this.mobile = false;
      }
    })
  }

  ngAfterViewInit() {
    if (this.mobile) {
      this.scrollToBottom();
    }
    this.layoutService.fixIconEdge(this.icons);
  }

  ngOnDestroy(): void {
    if (typeof this.subscription !== 'undefined') {
      this.subscription.unsubscribe();
    }
    if (typeof this.chatSubscription !== 'undefined') {
      this.chatSubscription.unsubscribe();
    }
  }

  initChat() {
    if (this.inviteId) {
      this.subscription = this.afs.collection('/chat', ref => ref
        .where('inviteId', '==', this.inviteId))
        .valueChanges({ idField: 'id' })
        .subscribe(chat => {
          if (chat.length) {
            this.chatId = chat[0]['id'];
            this.chatUsers = chat[0]['users'];
            // this.getUserChat();
            this.getUserProfiles(this.chatUsers);
            this.checkUnseenMessages(chat[0])
          }
          this.initMessage();
          if (!this.notLoggedUser) {
            this.addToChat();
          }
        })
    } else {
      // this.afs.collection('/chat').doc('vsMRioaOkdaffI1LittH').delete()
      // console.log(this.challengeId, 'ch id')
      // console.log(this.gameId, 'g id')
      this.subscription = this.afs.collection('/chat', ref => ref
        .where('challengeId', '==', this.challengeId)
        .where('gameId', '==', this.gameId || null))
        .valueChanges({ idField: 'id' })
        .subscribe(chats => {
          // console.log(chats, 'chat');
          if (chats.length) {
            const minChat = chats.reduce(function (a, b) { return new Date(a['created']['seconds']).getTime() * 1000 < new Date(b['created']['seconds']).getTime() * 1000 ? a : b });
            this.chatId = minChat['id'];
            this.chatUsers = minChat['users'];
            // this.getUserChat();// TODO: freezes the app for 0.2s
            this.getUserProfiles(this.chatUsers);
            // this.checkUnseenMessages(chat[0])
          }
          this.initMessage();
          this.addToChat();// TODO: freezes the app for 0.3s
        })
    }
  }

  addToChat() {
    this.showUserName = true;
    if (this.chatId) {
      const user = this.chatUsers.find(item => item === this.myUid ? item : null);
      if (!user) {
        this.chatUsers.push(this.myUid);
        this.afs.collection('/chat/').doc(this.chatId).update({ users: this.chatUsers });
        this.afs.collection('/user/' + this.myUid + '/chat').doc(this.chatId).set({
          private: false,
          challengeId: this.challengeId || null,
          gameId: this.gameId || null,
          inviteId: this.inviteId || null,
          dateJoined: new Date(Date.now())
        })
      }
    } else {
      this.afs.collection('/chat').add({
        users: [this.myUid],
        challengeId: this.challengeId || null,
        gameId: this.gameId || null,
        inviteId: this.inviteId || null,
        created: new Date(Date.now())
      }
      )
        .then((docRef) => {
          this.afs.collection('/user/' + this.myUid + '/chat').doc(docRef.id).set({
            private: false,
            challengeId: this.challengeId || null,
            gameId: this.gameId || null,
            inviteId: this.inviteId || null,
            dateJoined: new Date(Date.now())
          });
          this.chatId = docRef.id;
        });
    }
  }

  // getUserChat() {
  //   this.afs.collection('/user/' + this.myUid + '/chat').doc(this.chatId)
  //     .valueChanges().subscribe(response => {
  //       this.chat = response;
  //     })
  // }

  initMessage() {
    // delete this.chatRef;
    if (this.myUid || this.notLoggedUser) {
      if (this.chatId) {
        this.loading = true;
        if (typeof this.chatSubscription !== 'undefined') {
          this.chatSubscription.unsubscribe();
        }
        this.chatSubscription = this.afs.collection('/chat/' + this.chatId + '/message', ref => ref
          .orderBy('created', 'desc')
          .limit(50)).snapshotChanges() // TODO: optimize perrformance, every 50 messages slow the app for 0.3s
          .subscribe({
            next: (response) => {
              let messages = [];
              if (response.length && !response[0].payload.doc.metadata.hasPendingWrites) {
                response.forEach((doc, index) => {
                  messages.push(Object.assign(doc.payload.doc.data(), { id: doc.payload.doc.id }));
                });
                if (this.userService.userData) {
                  const user = this.userService.userData;
                  const friends = user.friends || [];
                  if (this.segmentSelected === 'friends') {
                    // console.log('FR')
                    if (friends.length > 0) {
                      let filtered = messages.filter(v => {
                        return friends.find(v2 => v['sender'] === v2.uid || v['sender'] === this.myUid ? v : null);
                      });
                      this.messages = filtered.reverse();
                      if (this.messages && this.messages.length) {
                        this.loading = false;
                        this.scrollToBottom();
                      } else {
                        this.loading = false;
                      }
                    }
                  } else {
                    this.messageFiltered(messages);
                  }
                } else {
                  this.messageFiltered(messages);
                }
              } else {
                this.loading = false;
              }
            }, error: (err: HttpErrorResponse) => {

            }
          })
      }
    }
  }

  messageFiltered(messages) {
    messages = messages.reverse();
    if (!this.messages) {
      this.messages = messages;
    } else {
      if (this.messages.filter(e => e.id === messages[messages.length - 1].id).length === 0 && messages[messages.length - 1]) {
        this.messages.push(messages[messages.length - 1]);
      }
    }
    if (this.messages.length && this.showSnapchat) {
      this.loading = false;
      this.scrollToBottom();
    } else {
      this.loading = false;
    }
  }

  getUserProfiles(uids: Array<string>) {
    this.usersService.load(uids);
  }

  CheckSpaces = (str) => str.trim() !== '';

  scrollToBottom(): void {
    if (this.messages && this.messages.length && this.showSnapchat) {
      setTimeout(() => {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      }, 1000)
    }
  }

  sendMessage(ev) {
    if (ev && ev.keyCode === 13 || ev && ev.type === 'click') {
      if (this.myUid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
        this.myapp.loginAlert('chat');
        return;
      }
      const text = this.message || ev.target.value;
      if (this.CheckSpaces(text)) {
        if (this.chatId) {
          this.afs.collection('/chat/' + this.chatId + '/message').add({
            created: new Date(Date.now()),
            sender: this.myUid,
            receiver: this.recepient ? this.recepient.uid : null,
            text
          });
          if (this.mobile && !this.expand) {
            this.expand = true;
          }
          setTimeout(() => {
            this.scrollToBottom();
          }, 500)
          this.userMentions.forEach(user => {
            const notification = {
              title: "Mention in chat",
              privateChat: true,
              text: "mentioned you in chat",
              platforms: ['ios', 'android', 'web'],
              start: new Date(),
              scheduling: 'Now',
              receivers: [user.uid],
              created: new Date(),
              // chatRequest: true,
              sendRequest: true,
              chatId: this.chatId,
              sender: this.myUid
            };
            //this.afs.collection('/notification/').add(notification);
            const callable = this.fns.httpsCallable('sendNativeNotification');
            Object.values(user.tokens).forEach(token => {
              if (token) callable({ tokens: token, title: 'Mention in chat', body: this.users[this.myUid].name + " mentioned you in chat\n", gotoUrl: this.sport ? ('/' + this.sport + '/' + this.challengeId) : null })
                .toPromise().then((resp) => {
                  //console.log(resp);
                });
            });
          })
          this.mentions = null;
          this.userMentions = [];
          this.expandArea = false;
          // console.log(this.chatUsers, 'chatUsers')
          // this.chatUsers.forEach(user_uid => {
          //   if (user_uid !== this.myUid) { // Do not notify sender
          //     this.afs.collection('/user/' + user_uid + '/chat/').doc(this.chatId).get()
          //     .pipe(takeUntil(this.destroy$))
          //     .subscribe(response => {
          //       let status = response.data() && response.data().status || [];
          //       console.log(status, 'status')
          //       status.push(user_uid);
          //       if (!response.exists) {
          //         response.ref.set({ status });
          //       }
          //       else {
          //         response.ref.update({ status });
          //       }
          //     })
          //   }
          // })
        } else {
          console.log('chat_id is empty');
        }
        ev.target.value = '';
        this.message = '';
      }
    }
  }

  transformText(ev) {
    if (this.myUid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
      //this.message = '';
      ev.target.value = '';
      this.myapp.loginAlert('chat');
      return;
    }
    if (ev.target.value.length > 24) {
      this.expandArea = true;
    } else {
      this.expandArea = false;
    }
    this.message = ev.target.value.replace(/((?:(?:^|[.?!])\s*)+)(.)/g, function (m, tail, ch) {
      return tail + ch.toUpperCase();
    });
    this.addMention(ev);
  }

  checkUnseenMessages(data) {
    let unseenMessages = data.status || [];
    if (data && unseenMessages.length > 0) {
      this.afs.collection('/user/' + this.myUid + '/chat').doc(this.chatId).update({ status: [] });
    }
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const modal = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid,
        modalController: this.modalController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await modal.present();
  }

  expandSnapchat() {
    if (this.mobile) {
      this.firstLoad = false;
      const element = document.getElementById('scrollChat');
      if (this.expand) {
        this.expand = false;
        element.style.paddingBottom = '40px';
      } else {
        this.expand = true;
        element.style.paddingBottom = 'inherit';
        this.scrollToBottom();
      }
    } else {
      if (this.showSnapchat) {
        this.showSnapchat = false;
      } else {
        this.showSnapchat = true;
        setTimeout(() => {
          this.scrollToBottom();
          // this.checkUnseenMessages(this.chat);
        }, 300)
      }
    }
  }

  segmentChanged(event) {
    this.loading = true;
    this.segmentSelected = event.detail.value;
    this.initMessage()
  }

  uniqueDate(value, prevValue,) {
    if (!prevValue) {
      let date = this.datePipe.transform(value.created.toDate(), 'MMMM d');
      return date;
    }
    let date = this.datePipe.transform(value.created.toDate(), 'MMMM d');
    let prevDate = this.datePipe.transform(prevValue.created.toDate(), 'MMMM d');
    if (date !== prevDate) {
      return date;
    } else {
      return null;
    }
  }

  goToLogin() {
    this.router.navigate(['/login'])
  }

}
