import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IonContent } from '@ionic/angular';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  user: any;
  challenges: Array<Object> = [];
  date = Date;
  userChallenges: Array<Object> = [];
  invites: any[];
  tabsBySport = [{ name: 'all', icon: '' }, { name: 'nhl', icon: 'hockey' }, { name: 'nfl', icon: 'american-football' }, { name: 'nba', icon: 'basketball' }];
  sportSelected: string = 'all';
  optimizedChallenges: {};
  segmentSelected: string = 'all';
  userData: any;

  constructor(
    public router: Router,
    private afs: AngularFirestore,
    public modalController: ModalController,
    private userService: UserService) {
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.loadUserData();
  }

  loadUserData() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.userService.getUser$(user.uid).subscribe(resp => {
          this.userData = resp;
        })
        this.loadInvites();
        this.loadChallenges();
      }
    })
  }

  loadChallenges() {
    this.afs.collection("challenge", ref => ref
      .orderBy('start', 'desc')
      .where('start', '>', new Date('2019-11-01'))
      .where('start', '<', new Date(Date.now() + 10 * 24 * 60 * 60000))
      .where('disabled', '==', false)).valueChanges({ idField: 'id' }).subscribe(challenges => {
        if (this.segmentSelected === 'my') {
          this.userChallenges = challenges.filter(challenge => challenge['games'].find(item => this.userData.games[item] ? item : null))
          this.loadHistory();
        } else {
          this.userChallenges = challenges;
          this.loadHistory();
        }
      });
  }

  async loadInvites() {
    if (typeof this.invites === 'undefined') {
      this.afs.collection('invite', ref => ref
        .where('users', 'array-contains', this.user.uid)
        .orderBy('start', 'desc')
        .where('start', '>', new Date(Date.now() - 87 * 24 * 3600 * 1000)))
        .valueChanges({ idField: 'id' }).subscribe((invites) => {
          this.invites = invites;
          this.loadHistory();
        });
    }
  }

  loadHistory() {
    this.optimizedChallenges = {};
    if (this.userChallenges) {
      let nhl = this.userChallenges.filter(challenge => challenge['sport'] === 'nhl' ? challenge : null);
      let nfl = this.userChallenges.filter(challenge => challenge['sport'] === 'nfl' ? challenge : null);
      let nba = this.userChallenges.filter(challenge => challenge['sport'] === 'nba' ? challenge : null);
      this.optimizedChallenges['challenges'] = { all: this.userChallenges, nhl: nhl, nfl: nfl, nba: nba };
    }
    if (this.invites) {
      let nhl = this.invites.filter(challenge => challenge['sport'] === 'nhl' ? challenge : null);
      let nfl = this.invites.filter(challenge => challenge['sport'] === 'nfl' ? challenge : null);
      let nba = this.invites.filter(challenge => challenge['sport'] === 'nba' ? challenge : null);
      this.optimizedChallenges['invites'] = { all: this.invites, nhl: nhl, nfl: nfl, nba: nba };
    }
  }

  selectSport(sport) {
    this.sportSelected = sport;
  }

  segmentChanged(event) {
    this.segmentSelected = event.detail.value;
    this.loadChallenges();
  }

}
