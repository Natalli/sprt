import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { SearchServiceModule } from './search.module';

@Injectable({
  providedIn: SearchServiceModule
})
export class SearchService {
  constructor(
    protected afs: AngularFirestore
  ) {
  }

  searchUser(name): Observable<any> {
    return this.afs.collection("/user/", ref => ref
      .orderBy('keywords')
      .startAt(name.toLowerCase())
      .endAt(name.toLowerCase() + "\uf8ff")
    ).valueChanges({ idField: 'id' })
  }

}
