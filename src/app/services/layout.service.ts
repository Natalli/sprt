import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  constructor() {
  }

  fixIconEdge(icons) {
    const browserName = navigator.userAgent;
    if (browserName.indexOf('Edge') > -1) {
      setTimeout(() => {
        icons.forEach(icon => {
          // console.log(icon, 'icons')
          let attr = icon.nativeElement.getAttribute('name').split('-');
          if (attr.pop() === 'outline') {
            let newAttr = attr.join('-');
            icon.nativeElement.setAttribute('name', newAttr)
          }
        });
      }, 300)

      // console.log(icons, 'icons')
    }
  }

  public historyBack() {
    window.history.back();
  } 
}
