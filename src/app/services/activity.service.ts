import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { SubjectService } from '../service/subject.service';
import { UserService } from '../service/user.service';


@Injectable({
  providedIn: 'root'
})
export class ActivityService extends SubjectService {

  notifications: any[];
  unseenNotifications = 0;
  unseenMessages = 0;
  unseenCount = 0;
  user: any;
  protected subjects: any = {};
  constructor(
    private afs: AngularFirestore,
    private userService: UserService) {
    super();
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.checkNotifications();
        this.checkMessages();
      }
    })
  }

  public get count$(): Observable<any> {
    return this.getSubject$('count');
  }

  public set count(value: number) {
    this.getSubject('count').next(value);
  }

  checkNotifications() {
    this.afs.collection('/notification/', ref => ref
      .orderBy('start', 'desc')
      .where('start', '<=', new Date())
      .where('start', '>', new Date(Date.now() - 2 * 6048e5)) // expire in 4 weeks
      .orderBy('created', 'desc')
      .orderBy('receivers')
      .where('receivers', 'array-contains-any', ['all', this.user.uid]))
      .valueChanges({ idField: 'id' }).subscribe((notifications: Array<any>) => {
        this.notifications = [];
        this.unseenNotifications = 0;
        notifications.forEach((notification, key) => {
          if (!notification.status || typeof notification.status[this.user.uid] === 'undefined') {
            this.unseenNotifications++;
          }
        });

        this.unseenCount = this.unseenNotifications + this.unseenMessages;
        this.count = this.unseenCount;
      });
  }

  checkMessages() {
    this.afs.collection('/user/' + this.user.uid + '/chat')
      .valueChanges({ idField: 'id' }).subscribe((messageRefs: Array<any>) => {
        let messageRefsArr = messageRefs.filter(item => item.challengeId || item.gameId || item.inviteId ? null : item);
        this.unseenMessages = 0;
        messageRefsArr.map(item => {
          if (item.status && item.status.length > 0) {
            this.unseenMessages += 1
          }
        });

        this.unseenCount = this.unseenNotifications + this.unseenMessages;
        this.count = this.unseenCount;
      });
  }

}
