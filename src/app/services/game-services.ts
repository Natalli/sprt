import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable()
export class GameService {
    constructor(
        private http: HttpClient) {
    }

    getGames(): Observable<void> {
        let startDate = new Date(Date.now() - 10 * 12096e5); // 10*2 weeks -
        let endDate = new Date(Date.now() + 12096e5); // 2 weeks +
        return this.http.get(environment.nhl_api + '/schedule?startDate=' + startDate.toJSON().slice(0, 10) + '&endDate=' + endDate.toJSON().slice(0, 10))
            .pipe(map((response: any) => response)).pipe(map((games) => games.dates));
    }

}