import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import 'firebase/messaging';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  currentMessage = new BehaviorSubject(null);
  messages;

  constructor(private angularFireMessaging: AngularFireMessaging) { }

  requestPermission() {
    this.angularFireMessaging.requestToken
      .subscribe(
        (token) => { }, //console.log('Permission granted!', token);
        (error) => { console.error(error); }
      );
  }

  async getToken() {
    return new Promise((resolve, reject) => {
      this.angularFireMessaging.getToken.subscribe( (token) => {
        //console.log('token', token);
        resolve(token);
      },
        (error) => { console.error(error); resolve(null); }
      );
    });
  }

  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
    (messages) => {
      this.messages = messages;
      this.currentMessage.next(messages);
    })

  }

}
