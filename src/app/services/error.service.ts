import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  constructor() {
  }
  
  codeTransform(code) {
    return code.split('/')[1]
  }
}
