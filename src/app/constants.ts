export class Constants {
  public static readonly EMAIL_REGEXP: RegExp = /^(?=.{0,255}$)([a-zA-Z](\.?[a-zA-Z0-9_+-])*@[a-zA-Z0-9-]+\.([a-zA-Z]{1,6}\.)?[a-zA-Z]{2,15}$)/;
  public static readonly PASSWORD_REGEXP: RegExp = /^(?=.*\d)(?=.*[a-z|а-я])(?=.*[A-Z|А-Я]).{6,32}$/;
  public static readonly NAME_REGEXP: RegExp = /^[a-zA-Z]/;
  public static readonly FULL_NAME_REGEXP: RegExp = /^[a-zA-Z0-9]/;
  public static readonly AVATAR_DEMO: string = '/assets/img_avatar.png';
  public static readonly GROUP_AVATAR_DEMO: string = '/assets/img_group.png';
  public static readonly ICONS: string = 'assets/icons/icons.svg#';
  public static readonly NUMERIC = [
    { id: 'Seven', value: '7' },
    { id: 'Eight', value: '8' },
    { id: 'Nine', value: '9' },
    { id: 'Four', value: '4' },
    { id: 'Five', value: '5' },
    { id: 'Six', value: '6' },
    { id: 'One', value: '1' },
    { id: 'Two', value: '2' },
    { id: 'Three', value: '3' },
    { id: 'Random', value: 'random' },
    { id: 'Zero', value: '0' },
    { id: 'Point', value: '.' },
    { id: 'Clear', value: 'clear' },
    { id: 'Check', value: 'check' },
  ];
  public static readonly SRC = {
    SPORT: '/assets/icons/sports.svg#',
    ICONS: '/assets/icons/icons.svg#',
    LOGO: '/assets/imgs/logo.svg#',
    NBA: '/assets/icons/nba.svg#',
    NFL: '/assets/icons/nfl.svg#',
    NHL: '/assets/icons/nhl.svg#'
  };
  public static readonly DEFAULT_LEAGUES = {
    nba: [],
    nfl: [],
    nhl: []
  }
  public static readonly DEFAULT_GAME = {
    id: 0, 
    bid_amount: 10, 
    max_users: 8, 
    users: [], 
    winners: []
  };
  public static readonly GRID_PREFS = {
    nba: {
      scoreMultiplier: 7,
      scoreShift: 83,
      count: 7,
      max: 130,
      balance: 9.52
    },
    nfl: {
      scoreMultiplier: 7,
      scoreShift: 0,
      count: 7,
      max: 70,
      balance: 9.52
    },
    nhl: {
      scoreMultiplier: 1,
      scoreShift: 0,
      count: 7,
      max: 10,
      balance: 9.58
    }
  };
  public static readonly BIDS = {
    SINGLE: 'single',
    MATRIX: 'matrix'
  }
}
