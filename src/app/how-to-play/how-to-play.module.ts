import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SnapchatModule } from '../snapchat/snapchat.module';
import { DirectivesModule } from '../directives/directives.module';
import { HowToPlayPage } from './how-to-play.page';

const routes: Routes = [
  {
    path: '',
    component: HowToPlayPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SnapchatModule,
    DirectivesModule
  ],
  declarations: [HowToPlayPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class HowToPlayPageModule {}
