import { Component, OnInit, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Constants } from '../constants';
import { PopoverController, LoadingController, IonItemSliding, AlertController, ModalController, IonIcon } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProfilePage } from '../pages/profile/profile.page';
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import { LayoutService } from '../services/layout.service';
import { UserService } from '../service/user.service';
import { ModalConfirmComponent } from '../popups/new/modal-confirm/modal-confirm.component';
import { ModalService } from '../service/modal.service';

@Component({
  template: '',
})
export class NotificationsPage implements OnInit {
  @ViewChild(IonItemSliding) slidingItem: IonItemSliding;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  user: any;
  userData: Object;
  messages = {};
  objectKeys = Object.keys;
  objectValues = Object.values;
  messageRefs: any[] = [];
  notificationUsers: Array<Object>;
  constants = Constants;
  search = '';
  notifications: any[];
  unseen: any[];
  notificationSub: any;
  chat;
  links = {};
  notificationsLinks: Object;
  constructor(
    protected afs: AngularFirestore,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public modalController: ModalController,
    public popoverCtrl: PopoverController,
    protected router: Router,
    protected layoutService: LayoutService,
    protected userService: UserService,
    public modalService: ModalService) {
    this.userService.user$.subscribe((user: any) => {
      if (user) {
        this.user = user;
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(user => {
          this.userData = user;
          //this.user = {user, ...this.user}; // Do not overwrite FireAuth
        });
        this.loadNotifications();
      }
    });
  }

  ngOnInit() {
    // this.getUsers();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  seeAllNotifications() {
    if (this.unseen && this.user.uid) {
      this.unseen.forEach(notificationId => {
        this.afs.collection('/notification/').doc(notificationId).update({ ['status.' + this.user.uid]: 'seen' });
      })
    }
  }

  getFriendName(id: string) {
    let friend = this.notificationUsers.filter((item) => (item['id'] || item['uid']) === id ? item : '')
    return friend[0]['name'];
  }

  async confirmGameInvite(invite, startKey?) {
    // console.log("NotificationsPage -> confirmGameInvite -> invite", invite)
    this.loadUser().then(async () => {
      const friendIndex = this.getFriendIndex(invite.sender);
      if (typeof friendIndex === 'undefined' ||
        (typeof this.user.friends[friendIndex].sent !== 'undefined' && this.user.friends[friendIndex].sent)) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Confirm Invite Game',
          message: this.getFriendName(invite.sender) + ' is not your Friend',
          buttons: [
            {
              text: 'Accept + Friend',
              cssClass: 'success bold',
              handler: () => {
                if (typeof friendIndex === 'undefined' ||
                  (typeof this.user.friends[friendIndex].sent === 'undefined' || !this.user.friends[friendIndex].sent)) {
                  this.updateFriends(invite.sender, friendIndex, { uid: invite.sender, sent: false });
                  const notification = {
                    title: "Friend request confirmed",
                    text: "accepted your friend request",
                    platforms: ['ios', 'android', 'web'],
                    start: new Date(),
                    scheduling: 'Now',
                    receivers: [invite.sender],
                    sender: this.user.uid,
                    sendRequest: true,
                    created: new Date(),
                  };
                  this.afs.collection('/notification/').add(notification);
                }
                this.loadInviteGame(invite.inviteRequest).then((inviteGameRef: any) => {
                  const inviteGame = inviteGameRef.data();
                  const notification = {
                    title: 'Game Invite Accepted',
                    text: 'accepted your Invite for ' + (inviteGame ? ('"' + inviteGame.name + '"') : 'a') + ' Game',
                    platforms: ['ios', 'android', 'web'],
                    start: new Date(),
                    scheduling: 'Now',
                    receivers: [invite.sender],
                    sender: this.user.uid,
                    friendRequest: false,
                    sendRequest: true,
                    created: new Date()
                  }
                  // console.log(notification);
                  this.afs.collection('/notification/').add(notification)
                    .then((docRef) => { });
                });
                this.dismissNotification(invite.id, startKey);
                this.router.navigate([invite.inviteLink || '/challenge']);
              }
            },
            {
              text: 'Decline',
              cssClass: 'warning',
              handler: () => {
                this.loadInviteGame(invite.inviteRequest).then((inviteGameRef: any) => {
                  const inviteGame = inviteGameRef.data();
                  inviteGameRef.ref.update({ users: firebase.firestore.FieldValue.arrayRemove(this.user.uid) });
                  const notification = {
                    title: 'Game Invite Declined',
                    text: 'declined your Invite for ' + (inviteGame ? ('"' + inviteGame.name + '"') : 'a') + ' Game',
                    platforms: ['ios', 'android', 'web'],
                    start: new Date(),
                    scheduling: 'Now',
                    receivers: [invite.sender],
                    sender: this.user.uid,
                    friendRequest: false,
                    sendRequest: true,
                    created: new Date()
                  }
                  this.afs.collection('/notification/').add(notification)
                    .then((docRef) => { });
                });
                this.dismissNotification(invite.id, startKey);
              }
            }, {
              text: 'Decline + Block',
              cssClass: 'danger',
              handler: () => {
                if (typeof friendIndex === 'undefined' || !this.user.friends[friendIndex].blocked) {
                  this.updateFriends(invite.sender, friendIndex, { uid: invite.sender, blocked: true });
                }
                this.loadInviteGame(invite.inviteRequest).then((inviteGameRef: any) => {
                  inviteGameRef.ref.update({ users: firebase.firestore.FieldValue.arrayRemove(this.user.uid) });
                });
                this.dismissNotification(invite.id, startKey);
              }
            }
          ]
        });
        await alert.present();
      }
      else {
        this.loadInviteGame(invite.inviteRequest).then((inviteGameRef: any) => {
          const inviteGame = inviteGameRef.data();
          const notification = {
            title: 'Game Invite Accepted',
            text: 'accepted your Invite for ' + (inviteGame ? ('"' + inviteGame.name + '"') : 'a') + ' Game',
            platforms: ['ios', 'android', 'web'],
            start: new Date(),
            scheduling: 'Now',
            receivers: [invite.sender],
            sender: this.user.uid,
            friendRequest: false,
            sendRequest: true,
            created: new Date()
          }
          // console.log(notification);
          this.afs.collection('/notification/').add(notification)
            .then((docRef) => { });
        });
        this.dismissNotification(invite.id, startKey);
        this.router.navigate([invite.inviteLink || '/challenge']);
      }
    });
  }

  getFriendIndex(friendUid) {
    let friendIndex;
    this.user.friends.forEach((friend: any, index: number) => {
      if (friend.uid && friend.uid === friendUid) {
        friendIndex = index;
      }
    });
    return friendIndex;
  }

  updateFriends(inviteSenderUid, friendIndex, status) {
    let friends = this.user.friends;
    if (friendIndex) {
      friends[friendIndex] = status;
    }
    else {
      friends.push(status);
    }
    this.afs.doc('user/' + this.user.uid).update({ friends: friends });
  }

  loadUser() {
    return new Promise((resolve) => {
      if (!this.user || !this.user.friends) {
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe((data: object) => {
          if (data && Object.values(data).length > 0) {
            this.user = { ...data, ...this.user }
          }
          resolve(this.user);
        });
      }
      else {
        resolve(this.user);
      }
    });
  }

  loadInviteGame(inviteId: string) {
    return new Promise((resolve) => {
      this.afs.collection("invite").doc(inviteId).get().subscribe((data) => {
        resolve(data);
      });
    });
  }

  loadNotifications() {
    if (typeof this.notificationSub === 'undefined')
      this.notificationSub = this.afs.collection('/notification/', ref => ref
        .orderBy('start', 'desc')
        .where('start', '<=', new Date())
        .where('start', '>', new Date(Date.now() - 4 * 6048e5)) // expire in 4 weeks
        .orderBy('created', 'desc')
        .orderBy('receivers')
        .where('receivers', 'array-contains-any', ['all', this.user.uid]))
        .valueChanges({ idField: 'id' }).subscribe((notifications: Array<any>) => {
          this.notifications = [];
          const unseen = []
          notifications.forEach((notification, key) => {
            if (notification.inviteRequest) {
              this.afs.collection('invite').doc(notification.inviteRequest).valueChanges().subscribe((invite: any) => {
                if (invite && invite.games) {
                  notification.inviteLink = '/detail/0/' + invite.games[0] + '/invite/' + notification.inviteRequest;
                }
              });
            }
            if (!notification.status || typeof notification.status[this.user.uid] === 'undefined') {
              if (true || this.router.url === '/notifications') {
                this.afs.collection('/notification/').doc(notification.id).update({ ['status.' + this.user.uid]: 'seen' });
              }
              this.notifications.push(notification);
              unseen.push(notification.id);
            }
            else if (notification.status[this.user.uid] !== 'dismissed') {
              this.notifications.push(notification);
            }
            
            if (this.userData && this.userData['friends']) {
              let removeFriendRequest = this.userData['friends'].find(user => (user.uid === notification.sender) ? user : null);
              if (!removeFriendRequest && notification.friendRequest) {
                this.deleteNotification(notification.id);
              }
              this.userData['friends'].forEach(user => {
                if (notification.friendRequest &&
                  user.uid === notification.sender &&
                  user.sent === false) {
                  this.deleteNotification(notification.id);
                }
              });
            }
          });
          // console.log(this.notifications, 'NNNN')
          this.notificationsLinks = {};
          // console.log(this.links, 'l 1')
          this.notifications.forEach(notification => {
            this.notificationsLinks[notification.id] = notification;
            this.links[notification.created.seconds] = { notificationId: notification.id }
          });
          // console.log(this.links, 'l 2')
          // console.log("NotificationsPage -> loadNotifications -> this.links", this.notificationsLinks)
          if (!this.unseen && unseen) {
            this.unseen = unseen;
          };
        });
  }

  async readNotification(id) {
    if (this.unseen.indexOf(id) !== -1) {
      this.unseen.splice(this.unseen.indexOf(id), 1);
      this.afs.collection('/notification/').doc(id).update({ ['status.' + this.user.uid]: 'seen' });
    }
  }

  async dismissNotification(id, startKey) {
    delete this.links[startKey];
    this.afs.collection('/notification/').doc(id).update({ ['status.' + this.user.uid]: 'dismissed' });
    await document.querySelector('ion-item-sliding').closeOpened();
  }

  getUsers() {
    this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(users => {
      this.notificationUsers = users;
    })
  }

  getUser(id) {
    let user = this.notificationUsers.filter((item) => item['id'] === id ? item : '')
    return user[0]
  }

  isTextOverflow(elementId: string): boolean {
    const elem = document.getElementById(elementId);
    if (elem) {
      return (elem.clientHeight < elem.scrollHeight);
    }
    else {
      return false;
    }
  }

  acceptInvite(user, sent, id?, startKey?) {
    this.afs.collection("user").doc(user).get().subscribe(resp => {
      let userData = resp.data();
      userData['friends'].find((u, i) => {
        if (u['uid'] === this.user.uid) {
          let friends = userData['friends'];
          userData['friends'][i] = {
            uid: this.user.uid,
            sent
          }
          this.saveData(user, friends, null, null, null)
        }
      })
      this.userData['friends'].find((u, i) => {
        if (u['uid'] === user) {
          let vFriends;
          this.userData['friends'][i] = {
            uid: user,
            sent
          }
          vFriends = this.userData['friends'];
          this.saveData(null, null, this.user.uid, vFriends, 'You accepted the invitation', sent);
          const notification = {
            title: "Friend request confirmed",
            text: "accepted your friend request",
            platforms: ['ios', 'android', 'web'],
            start: new Date(),
            scheduling: 'Now',
            receivers: [user],
            created: new Date(),
            sendRequest: true,
            sender: this.user.uid
            // name: this.userData['name'],
            // photoURL: this.user.photoURL || Constants.AVATAR_DEMO
          };
          // console.log(notification)
          this.dismissNotification(id, startKey);
          this.afs.collection('/notification/').add(notification);
        }
      })
    })
  }

  acceptFriend(user_uid) {
    var friends = this.userData['friends'] || [];
    friends.push({ uid: user_uid, sent: false });
    this.afs.collection("user").doc(user_uid).get().subscribe(user => {
      var vFriends = user.data()['friends'] || [];
      vFriends.push({ uid: this.user.uid, sent: false });
      this.saveData(this.user.uid, friends, user_uid, vFriends, null);
    })
  }

  blockedUser(user, blocked, id?, startKey?) {
    let vFriends = this.userData['friends'];
    vFriends.push({
      uid: user,
      blocked
    });
    this.saveData(null, null, this.user.uid, vFriends, 'The user is blocked', blocked);
    this.deleteNotification(id, startKey);
  }

  deleteFriend(index, uid, nameArray, id) {
    this.afs.collection("user").doc(uid).get().subscribe(user => {
      let friends = user.data()['friends'];
      let removeUser = friends.find(user => user.uid === this.user.uid);
      let i = friends.indexOf(removeUser);
      if (i != -1) {
        friends.splice(i, 1);
      }
      this.saveData(uid, friends, null, null, null)
    })
    let vFriends = this.userData['friends'];
    let removeVUser = vFriends.find(user => user.uid === uid);
    let vI = vFriends.indexOf(removeVUser);
    if (vI != -1) {
      vFriends.splice(vI, 1);
    }
    this.saveData(null, null, this.user.uid, vFriends, 'Successfully deleted', null);
    this.deleteNotification(id);
  }

  async confirmDelete(index, uid, text?, nameArray?, id?) {
    let confirmDeletePopup: boolean = true;
    const popover = await this.popoverCtrl.create({
      component: ModalConfirmComponent,
      componentProps: {
        confirmDeletePopup,
        confirmDeleteBtnName: 'Ok',
        deleteFriend: this.deleteFriend,
        saveData: this.saveData,
        uid, id, index, nameArray,
        afs: this.afs,
        user: this.user,
        userData: this.userData,
        loading: this.loading,
        loadingController: this.loadingController,
        text,
        dismissNotification: this.dismissNotification
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  async acceptChat(user_uid, notification, startKey) {
    if (this.userData && this.userData['friends']) {
      const friendExist = this.userData['friends'].find(user => user.uid === notification.sender ? user : null);
      var btnName = '';
      if (friendExist) {
        btnName = 'Accept'
      } else {
        btnName = 'Accept + Friend';
        var message = this.getFriendName(notification.sender) + ' is not your Friend'
      };
    }
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Сonfirm the chat invitation',
      message,
      buttons: [
        {
          text: btnName,
          cssClass: 'success bold',
          handler: () => {
            this.afs.collection('/chat/').doc(notification.chatId).get().subscribe((data) => {
              this.chat = data.data();
              this.chat['users'].push(user_uid);
              if (notification.privateChat) {
                this.afs.collection('/user/' + user_uid + '/chat').doc(notification.chatId).set({ private: notification.sender });
              } else {
                this.afs.doc('/chat/' + notification.chatId).update({ users: this.chat['users'] });
                this.afs.collection('/user/' + this.user.uid + '/chat').doc(notification.chatId).set({
                  private: false,
                  dateJoined: new Date(Date.now()),
                  groupName: notification.groupName,
                  groupPhoto: notification.groupPhoto
                });
              }

              this.afs.collection('/chat/' + notification.chatId + '/message', ref => ref.orderBy('created')).add({
                created: new Date(Date.now()),
                sender: this.user.uid,
                joined: true
              });

              if (this.userData && this.userData['friends']) {
                const friendExist = this.userData['friends'].find(user => user.uid === notification.sender ? user : null)
                if (!friendExist) {
                  this.acceptFriend(notification.sender);
                }
              }
              this.dismissNotification(notification.id, startKey);
              this.router.navigate(['/chat']);
            })
          }
        }, {
          text: 'Dismiss',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.dismissNotification(notification.id, startKey);
          }
        }
      ]
    });
    delete this.links[startKey]
    await alert.present();
  }

  declineChat(id, startKey) {
    this.modalService.presentLoading('You have refused to join the chat');
    this.deleteNotification(id, startKey);
  }

  saveData(myUid, friends, userUid, vFriends, text, sent?) {
    if (myUid && friends) {
      this.afs.collection("user").doc(myUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends });
        }
        else {
          item.ref.update({ friends });
        }
      });
    }
    if (userUid && vFriends) {
      this.afs.collection("user").doc(userUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends: vFriends });
        }
        else {
          item.ref.update({ friends: vFriends });
          if (text) {
            this.modalService.presentLoading(text);
            setTimeout(() => {
              this.loadingController.dismiss();
            }, 3000)
          }
        }
      });
    }
  }

  deleteNotification(id, startKey?) {
    delete this.links[startKey];
    this.afs.collection('/notification/').doc(id).delete();
  }

  async loading(text) {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: text,
      translucent: true
    });
    return await loading.present();
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const popover = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid,
        modalController: this.modalController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await popover.present();
  }
}

