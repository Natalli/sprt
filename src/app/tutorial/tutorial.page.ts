import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})

export class TutorialPage implements OnInit {
  @ViewChild('slider', { static: true }) slides: IonSlides;
  currentSlide: number = 0;
  showSkipBtn: boolean;
  lastSlide: boolean;
  data: any;
  timer: number = 0;
  url: string;
  firstStart: boolean;
  intervalTimer: any;
  src: string = '';
  loading: boolean;
  showSignUpBtn: boolean;
  user: any;
  stopped: boolean;
  slideOpts = {
    initialSlide: 0,
    speed: 400,
  };
  constructor(
    protected afs: AngularFirestore,
    protected route: Router,
    protected userService: UserService
  ) {

  }

  getUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
      }
    })
  }

  ngOnInit() {
    this.getUser();
    this.loading = true;
    this.route.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (['/tutorial', '/howto'].includes(e.url)) {
          this.currentSlide = 0;
          this.slides.slideTo(0);
          if (this.data) {
            this.slideTimer(this.currentSlide);
          } else {
            this.getTutorial();
          }
        } else if (e instanceof NavigationStart) {
          clearInterval(this.intervalTimer);
          this.src = null;
        }
      }
    });

    this.url = this.route.url;
    if (this.url === '/tutorial') {
      this.showSkipBtn = true;
      this.firstStart = true;
    } else {
      this.firstStart = false;
    }
  }

  getTutorial() {
    this.afs.doc('/info/tutorial').get().subscribe(resp => {
      this.data = resp.data();
      if (this.currentSlide === 0) {
        this.slideTimer(this.currentSlide);
      }
    })
  }

  slideChanged() {
    this.slides.getActiveIndex().then(index => {
      this.currentSlide = index;
      this.src = '';
      setTimeout(() => {
        this.slideTimer(this.currentSlide);
      })
      this.lastSlide = false;
      this.slides.lockSwipeToPrev(false);
      if (this.url === '/tutorial') {
        this.showSkipBtn = true;
      }
      if (this.currentSlide === 0) {
        this.slides.lockSwipeToPrev(true);
      } else if (this.currentSlide === Object.values(this.data).length - 1) {
        if (this.user && this.user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2' && this.url === '/howto') {
          this.showSignUpBtn = true;
        }
        this.lastSlide = true;
        this.showSkipBtn = false;
      }
    });

  }

  slideTimer(currentSlide) {
    clearInterval(this.intervalTimer);
    let i = +currentSlide + 1;
    let next = +currentSlide + 2;
    let slide = this.data['slide' + i];
    let nextSlide = this.data['slide' + next];
    this.src = slide.src;
    let nextSrc;
    if (nextSlide) {
      nextSrc = nextSlide.src;
    }

    this.timer = slide.duration;
    this.intervalTimer = setInterval(() => {
      this.timer--;
      if (this.timer === 0) {
        clearInterval(this.intervalTimer);
        if (this.lastSlide && this.url === '/tutorial') {
          this.route.navigate(['/login']);
        } else {
          if (!this.stopped && !this.lastSlide) {
            this.slides.update().then(() => {
              this.src = nextSrc;
              this.slides.slideTo(i);
            })
          }
        }
      }
    }, 1000);
  }

}
