import { Component } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { Platform, LoadingController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { PushNotifications, PermissionStatus, Token, PushNotificationSchema, ActionPerformed } from '@capacitor/push-notifications'
import firebase from "firebase/compat/app";
import 'firebase/compat/auth';
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { environment } from './../environments/environment';
import { MessagingService } from './services/messaging.service';
import { ActivityService } from './services/activity.service';
import { LayoutService } from './services/layout.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { AlertController } from '@ionic/angular';
import { Location } from '@angular/common';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { Share } from '@capacitor/share';
import { UserService } from './service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})

export class AppComponent {

  authState: Observable<firebase.User>;
  isAdmin: boolean = false;
  public appVersion: string = environment.version;
  public appBuild: string = environment.build;
  SmartBanner: any;
  user: any;
  messageRefs: any[] = [];
  notificationRefs: any[] = [];
  message;
  window = window;
  settings = false;
  capacitor = Capacitor;
  constructor(
    protected platform: Platform,
    protected splashScreen: SplashScreen,
    protected statusBar: StatusBar,
    protected afs: AngularFirestore,
    //    protected firestore: Firestore,
    public router: Router,
    public loadingController: LoadingController,
    public activityService: ActivityService,
    protected messagingService: MessagingService,
    protected menu: MenuController,
    protected layoutService: LayoutService,
    protected fns: AngularFireFunctions,
    protected keyboard: Keyboard,
    public alertController: AlertController,
    public location: Location,
    public screenshot: Screenshot,
    public openNativeSettings: OpenNativeSettings,
    protected userService: UserService,
    protected menuCtrl: MenuController) {
    this.checkBrowser();
    this.initApp();
    this.userService.getAuth();
  }

  get service() {
    return this.userService;
  }

  initFirebaseFunctions() {
    this.fns.useFunctionsEmulator('https://sprtmtrx.com');
  }

  initWebNotifications() {
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;
    this.messagingService.getToken().then((token => {
      this.afs.doc('user/' + this.user.uid).update({ [`tokens.${Capacitor.getPlatform()}`]: token });
    }));
  }

  handleMenu(ev) {
    if (ev.target && ['ION-IMG', 'ION-TOOLBAR', 'ION-TITLE', 'ION-CONTENT'].includes(ev.target.tagName)) {
      this.menu.close();
    }
    if (ev.target && ['ION-IMG'].includes(ev.target.tagName)) {
      this.router.navigate(['/home']);
    }
  }

  initNativeNotifications(settings?) {
    this.settings = settings;
    console.log('Initializing Native Notifications');

    PushNotifications.removeAllListeners();

    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermissions().then((result: PermissionStatus) => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
        if (settings) {
          if (typeof result.receive !== 'undefined') {
            alert('Enable Notifications for SPRT MTRX app in Settings and try again');
            this.openNativeSettings.open('notification_id').then(
              (val) => {
                //console.log('Notifications settings triggered', val);
              },
              (val) => {
                console.log('Notifications settings failed', val);
              }
            );
          }
        }
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: Token) => {
        //console.log('updated', { [`tokens.${Capacitor.getPlatform()}`]: token.value });
        this.afs.doc('user/' + this.user.uid).update({ [`tokens.${Capacitor.getPlatform()}`]: token.value })
          .then(() => {
            if (this.settings) {
              alert('Notifications are enabled now');
            }
            console.log('Push registration success, token: ' + token.value);
          });
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        console.log('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        if (notification && notification.body) {
          let message = notification.body;
          message = (notification.subtitle || '') + "\n" + message;
          if (notification.title) {
            message = notification.title + "\n" + message;
          }
          alert(message);
        }
        if (notification && notification.data && notification.data.gotoUrl) {
          this.router.navigate([notification.data.gotoUrl]);
        }
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (pl: ActionPerformed) => {
        if (pl && pl.notification && pl.notification.data && pl.notification.data.gotoUrl) {
          this.router.navigate([pl.notification.data.gotoUrl]);
        }
      }
    );
  }

  async checkBrowser() {
    const isIE = /msie\s|trident\//i.test(window.navigator.userAgent)
    if (isIE) {
      const loading = await this.loadingController.create({
        spinner: null,
        duration: 60 * 60 * 1000,
        message: 'You are using unsupported browser. Please download <a href="https://www.microsoft.com/en-us/edge" target="_blank">IE Edge</a>, <a href="https://www.google.ca/chrome/" target="_blank">Google Chrome</a> or <a href="https://www.apple.com/safari/" target="_blank">Safari</a>',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
    }
  }

  checkAuthState() {
    if (window.location.href.match(/tutorial|login|signup|forgot|contact|privacy|rules/g) === null) {
      this.userService.auth = getAuth();
      onAuthStateChanged(this.userService.auth, (user) => {
        if (user) {
          this.user = user;
          this.isAdmin = environment.admins.includes(user['uid']);
          if (window.location.href.match(/admin/g) !== null && !this.isAdmin) {
            this.router.navigate(['challenges']);
          }
          this.notificationPermission();
        } else {
          if (!JSON.parse(localStorage.getItem('tutorial'))) {
            //localStorage.setItem('tutorial', JSON.stringify(new Date()));
            //this.router.navigate(['tutorial']);
          }
        }
      }, err => {
        console.log('Please try again: ' + `${err['code']} (${err.message})`);
      });
    }
  }

  notificationPermission(settings?) {
    try {
      if (Capacitor.getPlatform() === 'android' || Capacitor.getPlatform() === 'ios') {
        this.initNativeNotifications(settings);
      } else if (Capacitor.getPlatform() === 'web') {
        this.initWebNotifications();
      } else {
        console.log('Platform ' + Capacitor.getPlatform() + ' does not have PushNotifications implementation.');
      }
    } catch (e) {
      console.log('error', e);
    }
  }

  initApp() {
    this.platform.ready().then(() => {
      if (!Capacitor || Capacitor.getPlatform() !== 'web') {
        //this.keyboard.hideFormAccessoryBar(false);
        //this.splashScreen.hide();
      }
      this.afs.persistenceEnabled$.subscribe((data) => {
        //console.log('persistenceEnabled: ' + data);
      })
      this.statusBar.hide();
      this.checkAuthState();
    });
  }

  async loginAlert(page?) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'You Are Not Logged In',
      mode: 'ios',
      buttons: [
        {
          text: 'Keep Browsing',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'Signup',
          role: 'signup',
        },
        {
          text: 'Login',
          role: 'login',
        }
      ]
    });
    alert.onDidDismiss().then((data) => {
      const action = (data || { role: 'login' }).role;
      switch (action) {
        case 'cancel':
        case 'backdrop':
          if (!['detail', 'chat'].includes(page)) {
            this.location.back();
          }
          break;
        case 'signup':
          this.router.navigate(['signup']);
          break;
        default:
          this.router.navigate(['login']);
      }
    });
    alert.present();
  }

  async addScreenshot() {
    this.platform
    try {
      this.screenshot.save('jpg', 80, 'sprtmtrx').then(res => {
        this.shareFile(res.filePath);
      });
    }
    catch (e) {
      console.log("error", e)
    }
  }

  async shareFile(file) {
    await Share.share({
      title: 'Share this',
      text: 'http://SPRTMTRX.com',
      url: 'file://' + file,
      dialogTitle: 'Share with buddies'
    });
  }

  async shareAppLink() {
    await Share.share({
      title: 'Share with your friends',
      url: 'http://SPRTMTRX.com',
      dialogTitle: 'Share App'
    });
  }

  closeMenu(ev) {
    this.menuCtrl.close();
  }

}
