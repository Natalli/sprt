import { Directive, HostListener } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Directive({
    selector: '[tap]'
})
export class TapDirective {

    static nativeAudio: NativeAudio;
    @HostListener("click", ["$event.target"]) onClick(event) {
        //TapDirective.nativeAudio.play('ping').catch(e => { console.log('no play', e); })
    }

    constructor() {
        if (!TapDirective.nativeAudio) {
            TapDirective.nativeAudio = new NativeAudio();
            TapDirective.nativeAudio.preloadComplex('ping', 'assets/audio/ping.m4a', 0.3, 1, 0);
        }
    }
}
