import { Directive, ElementRef, HostListener } from '@angular/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Directive({
  selector: '[click-nav]'
})
export class ClickDirective {
  static counter = 0;
  static nativeAudio: NativeAudio;
  @HostListener("click", ["$event"]) onClick(event: KeyboardEvent) {
    ClickDirective.counter++;
    //ClickDirective.nativeAudio.play('ping').catch(e=>{console.log('no play',e);})
  }

  constructor(el: ElementRef) {
      if( !ClickDirective.nativeAudio ) {
        ClickDirective.nativeAudio = new NativeAudio();
        ClickDirective.nativeAudio.preloadComplex('ping', 'assets/audio/ping.m4a', 0.3, 1, 0);
      }
  }
}



