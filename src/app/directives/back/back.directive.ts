import { AfterViewInit, Directive, HostListener, Input } from '@angular/core';
import { Router, Event } from '@angular/router';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { NavController, Platform } from '@ionic/angular';

@Directive({
    selector: 'back, [back]'
})
export class BackDirective implements AfterViewInit {
    @Input() back: string;
    @Input() defaultHref: string;
    static nativeAudio: NativeAudio;
    constructor(
        protected navCtrl: NavController,
        protected router: Router,
        protected platform: Platform) {
        this.backButton();
        if( !BackDirective.nativeAudio ) {
            BackDirective.nativeAudio = new NativeAudio();
            BackDirective.nativeAudio.preloadComplex('ping', 'assets/audio/ping.m4a', 0.3, 1, 0);
        }
    }

    @HostListener('click', ['$event']) click(e: Event) {
        if (this.back) {
            this.router.navigate([this.back]);
        } else {
            if (window.history.length < 3) {
                this.router.navigate([this.defaultHref]);
            } else {
                this.navCtrl.back({ animated: true });
            }
        };
        //BackDirective.nativeAudio.play('ping').catch(e=>{console.log('no play',e);})
    }

    backButton() {
        this.platform.backButton.subscribe(() => {
            if (this.back) {
                this.router.navigate([this.back]);
            } else {
                if (window.history.length < 3) {
                    this.router.navigate([this.defaultHref]);
                } else {
                    this.navCtrl.back({ animated: true });
                }
            };
        });
    }

    ngAfterViewInit(): void { }
}
