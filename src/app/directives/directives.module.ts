import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackDirectiveModule } from './back/back.module';
import { TapDirectiveModule } from './tap/tap.module';
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        BackDirectiveModule,
        TapDirectiveModule
    ],
    exports: [
        BackDirectiveModule,
        TapDirectiveModule
    ]
})
export class DirectivesModule {
}
