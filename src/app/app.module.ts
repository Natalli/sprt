import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { DatePipe } from '@angular/common'
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireFunctionsModule, REGION } from '@angular/fire/compat/functions';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import 'firebase/storage';

import { GameService } from './services/game-services'
import { MessagingService } from './services/messaging.service';
import { ImageCropperModule } from 'ngx-image-cropper';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { PopupTriviaComponent } from './popups/popup-trivia/popup-trivia.component';
import { UserModule } from './components/user/user.module';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { BannerModule } from './components/banner/banner.module';
import { PipesModule } from './pipes/pipes.module';
import { PopupMessageComponent } from './popups/popup-message/popup-message.component';
import { PopupShareComponent } from './popups/popup-share/popup-share.component';
import { HeaderModule } from './components/header/header.module';
import { ModalGameProgressComponent } from './popups/new/modal-game-progress/modal-game-progress.component';
import { IcoSvgModule } from './components/ico-svg/ico-svg.module';
import { ModalWinnerComponent } from './popups/new/modal-winner/modal-winner.component';
import { ModalUserGamesComponent } from './popups/new/modal-user-games/modal-user-games.component';
import { ModalCropComponent } from './popups/new/modal-crop/modal-crop.component';
import { ModalConfirmComponent } from './popups/new/modal-confirm/modal-confirm.component';
import { ModalChatCreateComponent } from './popups/new/modal-chat-create/modal-chat-create.component';
import { ModalChatManageComponent } from './popups/new/modal-chat-manage/modal-chat-manage.component';
import { ModalBidComponent } from './popups/new/modal-bid/modal-bid.component';
import { ModalUpdateAppComponent } from './popups/new/modal-update-app/modal-update-app.component';


@NgModule({
    declarations: [
        AppComponent,
        PopupTriviaComponent,
        PopupMessageComponent,
        PopupShareComponent,
        ModalGameProgressComponent,
        ModalWinnerComponent,
        ModalUserGamesComponent,
        ModalCropComponent,
        ModalConfirmComponent,
        ModalChatCreateComponent,
        ModalChatManageComponent,
        ModalBidComponent,
        ModalUpdateAppComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot({
            scrollAssist: false,
            scrollPadding: false
        }),
        AppRoutingModule,
        AngularFireMessagingModule,
        AngularFireModule.initializeApp(environment.config),
        HttpClientModule,
        AngularFireFunctionsModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        FormsModule,
        ImageCropperModule,
        RoundProgressModule,
        UserModule,
        BannerModule,
        PipesModule,
        HeaderModule,
        IcoSvgModule
    ],
    providers: [
        AngularFirestoreModule,
        StatusBar,
        SplashScreen,
        DatePipe,
        Location,
        GameService,
        MessagingService,
        Keyboard,
        OpenNativeSettings,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: REGION, useValue: 'us-central1' },
        Screenshot,
        NativeAudio
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
