import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { TriviaPage } from './trivia.page';
import { BannerModule } from '../components/banner/banner.module';
import { DirectivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: TriviaPage
      }
    ]),
    DirectivesModule,
    BannerModule,
    PipesModule,
    HeaderModule,
    SubHeaderModule
  ],
  declarations: [TriviaPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class TriviaPageModule { }
