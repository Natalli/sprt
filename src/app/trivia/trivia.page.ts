import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController, ModalController } from '@ionic/angular';
import { Capacitor } from '@capacitor/core';
import { ActivityService } from '../services/activity.service';
import { LayoutService } from '../services/layout.service';
import { AppComponent } from '../app.component';
import { PopupTriviaComponent } from '../popups/popup-trivia/popup-trivia.component';
import { AngularFirestore } from '@angular/fire/compat/firestore';


@Component({
  selector: 'app-trivia',
  templateUrl: 'trivia.page.html',
  styleUrls: ['trivia.page.scss'],
})
export class TriviaPage {
  sports: Array<any> = [{ name: 'NHL', icon: 'hockey', url: 'nhl' }, { name: 'NBA', icon: 'basketball', url: 'nba' }];
  selectedSport;
  triviaMessage = 'Pick Your Sport';
  quote: any;

  constructor(
    public router: Router,
    public activityService: ActivityService,
    public popoverCtrl: PopoverController,
    protected layoutService: LayoutService,
    public myapp: AppComponent,
    private afs: AngularFirestore,
    protected modalController: ModalController) {
    if (!Capacitor || Capacitor.getPlatform() !== 'ios') {
      this.sports = [{ name: 'NHL', icon: 'hockey', url: 'nhl' }, { name: 'NBA', icon: 'basketball', url: 'nba' }, { name: 'NFL', icon: 'american-football' }, { name: 'MLB', icon: 'baseball' }, { name: 'EPL', icon: 'football' }];
    }
  }

  ionViewDidEnter() {
    this.randomQuote()
  }


  async openTrivia(sport) {
    const modal = await this.modalController.create({
        component: PopupTriviaComponent,
        componentProps: {
          modalController: this.modalController,
          user: this.myapp.user,
          sport: sport
        },
        swipeToClose: true,
        showBackdrop: true,
        backdropDismiss: true,
        cssClass: 'popover-trivia', //popover-default
    });
    return await modal.present();
  }

  randomQuote() {
    this.afs.collection("quotes").valueChanges({ idField: 'id' }).subscribe(quotes => {
      this.quote = quotes[Math.floor(Math.random() * quotes.length)];
    });
  }

}
