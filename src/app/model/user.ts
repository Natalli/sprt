export class User {
    uid: string;
    username: string;
    publicName: string;
    email: string;
    color: string;
    legalName?: string;
    url: string;
}