export class NflEvent {
    date: string;
    events = [];
    games = []
    matches = [];
    totalEvents = 0;
    totalMatches = 0;
    sport = 'nfl';

    constructor() {
    }

    get totalGames() {
        return this.games.length;
    }

    get totalItems() {
        return this.games.length;
    }

    static startDate(start) {
        if ( start && start.seconds) {
            return (new Date(start.seconds*1000)).toJSON().substr(0,10).replace(/-/g,'');
        } else {
            return start.replace(/-/g,'');
        }
    }





}
