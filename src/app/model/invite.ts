import { timestamp } from 'rxjs/operators';

export class Invite {
    id?: string;
    name: string = '';
    text: string = '';
    amount: number = 10;
    start: any = new Date();
    disabled: boolean;
    games: Array<string> = [];
    admins: Array<string> = [];
    users: Array<string> = [];
    winners: Array<string> = [];

    constructor(admin?: string, date?: Date, name?: string, amount?: number) {
        if ( typeof admin !== 'undefined') {
            this.admins.push(admin);
            this.users.push(admin);
        }
        if ( typeof date !== 'undefined') {
            this.start = date;
        }
        if ( typeof name !== 'undefined') {
            this.name = name;
        }
        if ( typeof amount !== 'undefined') {
            this.amount = amount;
        }
    }

    static startDate(start) {
        if ( start && start.seconds) {
            return (new Date(start.seconds*1000)).toJSON().substr(0,10).replace(/-/g,'');
        } else {
            return start.replace(/-/g,'');
        }
    }
}
