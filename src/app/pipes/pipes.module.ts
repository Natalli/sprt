import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharactersPipe } from './characters.pipe';
import { MentionPipe } from './mention.pipe';
import { BoldPipe } from './bold.pipe';
@NgModule({
    declarations: [
        CharactersPipe,
        MentionPipe,
        BoldPipe
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        CharactersPipe,
        MentionPipe,
        BoldPipe
    ]
})
export class PipesModule {
}
