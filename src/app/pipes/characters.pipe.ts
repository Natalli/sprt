import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'characters',
    pure: false
})
export class CharactersPipe implements PipeTransform {

    constructor() {
    }

    transform(value: any) {
        let elems;
        if(value === '$0' || value === '$undefined') {
            elems = '$000'.split('');
        } else {
            elems = value.toString().split('');
        }
        
        let str = '';
        elems.forEach(element => {
            let letter = '<span>' + element + '</span>';
            str = str + letter
        });
        return str;
    }

}
