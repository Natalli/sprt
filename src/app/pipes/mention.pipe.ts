import { Pipe, PipeTransform } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { UserService } from '../service/user.service';

@Pipe({
    name: 'mention',
    pure: false
})
export class MentionPipe implements PipeTransform {
    constructor(
        protected afs: AngularFirestore,
        protected userService: UserService
    ) {
    }

    transform(value: any, users: any) {
        if (value && value.includes('@') && Object.values(users).length) {
            let results = value.match(/@[a-zA-z]\S+/g);
            if (results && results.length) {
                let str = '';
                for (let i = 0; i < results.length; i++) {
                    results[i] = results[i].replace(/[.,\/#!$%\^&\*;:{}=\-`~()]/g, "");
                    let mention = results[i].split('@')[1];
                    let user = Object.values(users).find(user => user['name'].includes(mention) ? user : null);
                    if (user) {
                        let name = user['name'].replace(/((\s*\S+)*)\s*/, "$1");
                        let elem = `<span class="decorate ${user['uid']}">@${user['name']}</span>`;
                        if(str) {
                            str = str.replace(`@${name}`, elem);
                        } else {
                            str = value.replace(`@${name}`, elem);
                        }
                        // return value;
                    } else {
                        return value;
                    }
                } 
                return str;
            }
        } else {
            return value;
        }
    }

}
