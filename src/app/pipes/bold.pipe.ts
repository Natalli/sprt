import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'bold',
    pure: false
})
export class BoldPipe implements PipeTransform {

    constructor() {
    }

    transform(str: any) {
        let headwords = str.match(/\b([A-Z])([a-z]+)?\b/gm);
        let newStr = '';
        for (let i = 0; i < headwords.length; i++) {
            if (i > 0) {
                let element = '<strong>' + headwords[i] + '</strong>';
                if (newStr) {
                    newStr = newStr.replace(headwords[i], element);
                } else {
                    newStr = str.replace(headwords[i], element);
                }

            }
        }
        return newStr + ':';
    }

}
