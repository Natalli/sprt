import { UsersService } from './../service/users.service';
import { Component, OnInit, ViewChildren, ElementRef, QueryList, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../common.service';
import { EngineService } from '../engine.service';

import { PopoverController, ModalController, IonIcon, Platform, NavController } from '@ionic/angular';
import { Constants } from '../constants';
import { Invite } from '../model/invite';
import { LayoutService } from '../services/layout.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { UserService } from '../service/user.service';
import { ISettings } from '../interfaces/settings';
import { SettingsNotification } from '../interfaces/enum/settings.enum';
import { AppComponent } from '../app.component';
import { Capacitor } from '@capacitor/core';
import *  as deepEqual from 'fast-deep-equal';
import { ProfilePage } from '../pages/profile/profile.page';
import { AdLoadInfo, AdMob, AdMobBannerSize, AdMobRewardItem, AdOptions, BannerAdOptions, BannerAdPluginEvents, BannerAdPosition, BannerAdSize, InterstitialAdPluginEvents, RewardAdOptions, RewardAdPluginEvents } from '@capacitor-community/admob';
import { environment } from 'src/environments/environment';
import { ClickDirective } from '../directives/click/click.directive';
import { PopupShareComponent } from '../popups/popup-share/popup-share.component';
import { SearchService } from '../services/search/search.service';
import { ModalWinnerComponent } from '../popups/new/modal-winner/modal-winner.component';
import { GameComponent } from '../components/game/game.component';

@Component({
  selector: 'app-challenge',
  templateUrl: './new-challenge.page.html',
  styleUrls: ['./new-challenge.page.scss'],
})
export class ChallengePage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  @ViewChild(GameComponent) activeGame: GameComponent;
  userBids: Object = null;
  user: any;
  moreGames;
  challenge_id: string;
  challenges: Array<Object> = [];
  challenge: any = {};
  history: Array<Object> = [];
  games: Array<Object> = [];
  users: Object = {};
  stats: Object;
  challengeWinners: Object = {};
  teams2: Object = {};
  bids: Object = {};

  constants = Constants;
  objectKeys = Object.keys;
  capacitor = Capacitor;
  date = Date;
  today = '';
  intervalId: any = '';
  winner: Array<any>;
  isShowPopup: boolean;
  isLastGamePlayed: boolean;
  userData: any;
  leaders: any;
  hiddenTabs: boolean;
  quote: Object = {};
  userFriends: any = [];
  segmentSelected: string = 'all';
  showPopup: boolean;
  noFriend: boolean = true;
  invites: object;
  links = {};
  scoreMultiplier = 1;
  leaderboard: any;
  challengeGames: any;
  settings: ISettings;
  notificationLabel: typeof SettingsNotification = SettingsNotification;
  sport: string;
  league: string;
  leadersCount: number = 9;
  maxWin: number;
  countWin: number;
  math = Math;
  challengeUsers: any[];
  usersLeaderboard: any[];
  tabWinners: any[];
  currentHidden = true;
  challengeStarted: boolean;
  isBidsMissing: boolean;
  currentLeaders: any[];
  bid = [];
  winningUser: {};
  grid = { count: 7, amount: 0, total: 0, max_users: 8, team_id: 0 };
  finalGame: any;
  finalistBids: {};
  bidSelected = 'winning';
  scoreShift: number;
  winningBid = [];
  finalGameLeaders: any[];
  gamesIntervalId: any;
  gamesSubs: any;
  challengeSubs: any;
  teamSubs: any;
  challengesSubs: any;
  linksSorted: string[];
  finalists: any;
  fakeFinalist = [];
  firstFinalist = [];
  goals: any;
  finalGameScoringPlays: any;
  timer = '';
  Constants = Constants;
  leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
  standings: Object = {};
  baseScore: number = 0;
  resetSlides: boolean = false;
  gamePkSelected: any = {};
  gameSelected: any = {};
  showBidsGamePopup: boolean = false;
  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private afs: AngularFirestore,
    private datePipe: DatePipe,
    private http: HttpClient,
    public commonService: CommonService,
    public engineService: EngineService,
    private popoverCtrl: PopoverController,
    public modalController: ModalController,
    protected layoutService: LayoutService,
    private fns: AngularFireFunctions,
    public usersService: UsersService,
    protected userService: UserService,
    public myapp: AppComponent,
    public platform: Platform,
    private navCtrl: NavController,
    private searchService: SearchService
  ) {
    this.backButton();
    this.users = this.usersService.users;
    this.settings = this.userService.settings;
  }

  ngOnInit() {
    this.sport = this.league = this.router.url.split('/')[1];
    this.loadUser();
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (['/challenge', '/nfl', '/nhl', '/nba'].includes(e.url)) {
          this.randomQuote();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  async ionViewDidEnter() {
    //this.init();
  }

  async ionViewWillLeave() {
    if (this.intervalId) {
      clearTimeout(this.intervalId);
    }
    if (![/bid/, /detail/].some(rx => rx.test(this.router.url)) && this.gamesSubs) {
      this.gamesSubs.unsubscribe();
      delete this.gamesSubs;
    }
    if (![/bid/, /detail/].some(rx => rx.test(this.router.url)) && this.challengeSubs) {
      this.challengeSubs.unsubscribe();
      delete this.challengeSubs;
    }
    if (this.challengesSubs) {
      this.challengesSubs.unsubscribe();
    }
    if (![/bid/, /detail/].some(rx => rx.test(this.router.url)) && this.teamSubs && Object.keys(this.teamSubs).length) {
      Object.keys(this.teamSubs).forEach(gamePk => {
        this.teamSubs[gamePk].unsubscribe();
        delete this.teamSubs[gamePk];
      });
    }
  }

  async init() {
    this.route.params.subscribe(params => {
      if (params.id) {
        if (params.id !== 'invite') {
          this.challenge_id = params.id;
          this.loadChallenges(params.id);
        }
        setTimeout(() => {
          this.interstitial();
        }, 5000);
      }
      else {
        this.loadChallenges();
      }
    });
    this.initAdmob();
  }

  async initAdmob(): Promise<void> {
    if (['ios'].includes(Capacitor.getPlatform())) {
      const { status } = await AdMob.trackingAuthorizationStatus();
    }
    AdMob.initialize({
      requestTrackingAuthorization: true,
      //testingDevices: ['cf2ec1b1db89a991ea3b9dbdd91def52','81CB620AC914C57361FDB9E782545363'],
      initializeForTesting: false,
    }).then(() => {
      setTimeout(() => {
        //this.interstitial()
      }, 3000);
    }).catch((err) => { console.log('AdMob error', err) });
  }

  async banner(): Promise<void> {
    AdMob.addListener(BannerAdPluginEvents.Loaded, () => {
      // Subscribe Banner Event Listener
    });

    AdMob.addListener(BannerAdPluginEvents.SizeChanged, (size: AdMobBannerSize) => {
      // Subscribe Change Banner Size
    });

    const options: BannerAdOptions = {
      adId: (Capacitor.getPlatform() === 'android' ? 'ca-app-pub-1812555417103391/7583361411' : 'ca-app-pub-1812555417103391/4705916632'),
      adSize: BannerAdSize.BANNER,
      position: BannerAdPosition.BOTTOM_CENTER,
      margin: 0,
      // isTesting: true
      // npa: true
    };
    AdMob.showBanner(options);
  }

  async rewardVideo(): Promise<void> {
    AdMob.addListener(RewardAdPluginEvents.Loaded, (info: AdLoadInfo) => {
      // Subscribe prepared rewardVideo
    });

    AdMob.addListener(RewardAdPluginEvents.Rewarded, (rewardItem: AdMobRewardItem) => {
      // Subscribe user rewarded
      console.log(rewardItem);
    });

    const options: RewardAdOptions = {
      adId: (Capacitor.getPlatform() === 'android' ? 'ca-app-pub-1812555417103391/7583361411' : 'ca-app-pub-1812555417103391/4705916632'),
      isTesting: true
      // npa: true
      // ssv: {
      //   userId: "A user ID to send to your SSV"
      //   customData: JSON.stringify({ ...MyCustomData })
      //}
    };
    await AdMob.prepareRewardVideoAd(options);
    const rewardItem = await AdMob.showRewardVideoAd();
  }

  async interstitial(): Promise<void> {
    if (!ClickDirective.counter || ClickDirective.counter % 6 !== 0) {
      return;
    }

    AdMob.addListener(InterstitialAdPluginEvents.Loaded, (info: AdLoadInfo) => {
      // Subscribe prepared interstitial
    });

    const options: AdOptions = {
      adId: (Capacitor.getPlatform() === 'android' ? 'ca-app-pub-1812555417103391/7583361411' : 'ca-app-pub-1812555417103391/4705916632'),
      // isTesting: true
      // npa: true
    };

    await AdMob.prepareInterstitial(options).then(ret => { })
      .catch(ret => {
        console.log('prepareInterstitial catch', ret);
      });

    await AdMob.showInterstitial().then(ret => { })
      .catch(ret => {
        console.log('showInterstitial catch', ret);
      });

  }

  async loadGames(date?) {
    if (this.games && this.games.length) {
      return;
    }
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    let startDate;
    let endDate;
    if (!date) {
      if (this.challenge.id) {
        date = this.datePipe.transform(new Date(this.challenge['start']['seconds'] * 1000), 'yyyy-MM-dd');
      }
      else {
        date = '2019-08-01';
      }
    }
    if (date) {
      startDate = new Date((new Date(date)).getTime() - 1000 * 60 * 60 * 12);
      endDate = new Date((new Date(date)).getTime() + 1000 * 60 * 60 * 36);
    }
    if (typeof this.gamesSubs !== 'undefined') {
      this.gamesSubs.unsubscribe();
    }
    this.gamesSubs = this.afs.collection(this.sport, ref => ref
      .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
      .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
      .valueChanges().subscribe((games: Array<any>) => {
        this.resetLeaders();
        this.games = games;
        if (this.finalGame && this.finalGame.gamePk) {
          this.games.forEach(event => {
            event['games'].forEach(game => {
              if (game.gamePk === this.finalGame.gamePk) {
                this.currentHidden = false;
                this.finalGame = game;
                this.finalGame.challenge_id = this.challenge_id;
                this.baseScore = Constants.GRID_PREFS[this.challenge.sport].scoreShift || 0;
                this.scoreMultiplier = this.challenge.sport !== 'nhl' ? 7 : 1; // NBA+NFL
                this.scoreShift = this.challenge.sport === 'nba' ? 83 : 0; // NBA
                this.finalGameLeaders = this.engineService.calcCurrentWinner(game, this.teams2[game.gamePk], this.bids[game.gamePk], this.scoreMultiplier, this.scoreShift);
              }
            });
          });
        }
        this.calcCurrentLeaders();
        this.processNotifications();
        this.getChallengeGames();
        setTimeout(() => {
          this.getChallengeGames();
        }, 3000);
      });
  }

  resetLeaders() {
    if (this.teams2 && Object.keys(this.teams2).length) {
      Object.keys(this.teams2).forEach((gamePk: string) => {
        if (Object.keys(this.teams2[gamePk]).length) {
          Object.keys(this.teams2[gamePk]).forEach((team_id: any) => {
            delete this.teams2[gamePk][team_id].leaders;
          })
        }
      });
    }
  }

  async loadUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.init();
        this.loadInvites();
        this.showBidsGamePopup = false;
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(item => {
          this.userData = item;
          this.users[user.uid] = item;
          this.users[user.uid].id = user.uid;
          if (this.userData && this.userData['friends']) {
            (this.userData['friends'] || []).forEach(friend => {
              if (!friend.sent && !friend.blocked) {
                this.userFriends.push(friend.uid)
              }
            })
          }
          this.user.games = item['games'];
          if (item && Object.values(item).length > 0) {
            var userBids = this.userBids || {};
            this.userBids = {};
            Object.keys(item['games'] || {}).forEach(game_id => {
              this.userBids[game_id] = userBids[game_id] || { [user.uid]: item['games'][game_id] };
            });
            let userBidsArr = Object.keys(this.userBids || {});
            let bids = (this.challenge['games'] || []).filter(function (n) {
              return userBidsArr.indexOf(n) !== -1;
            });
            if (this.challenge.games && this.challenge.games.length && this.challenge.games.length === bids.length) {
              this.isBidsMissing = false;
              this.missingBids(bids);
              if (((new Date(this.challenge['start']['seconds']).getTime() + 1 * 3600 * 24) * 1000 > new Date().getTime()) && !this.showBidsGamePopup) {
                this.openWinnerPopup(this.challenge_id, this.user.uid, 'bidsGame');
                this.showBidsGamePopup = true;
              }
            } else {
              this.isBidsMissing = true;
            }
          }
        });
      }
    });
    
  }

  missingBids(userBids) {
    if (!userBids || userBids.length !== this.challenge.games.length) {
      //this.sendNotification(this.notificationLabel.missingBids, this.challenge)
    }
  }

  loadUsers(users) {
    this.usersService.load(users);
  }


  getUser(uid) {
    if (!this.users || !this.users[uid]) {
      this.loadUsers([uid]);
    }
    return this.users[uid] || {};
  }

  getFinalistBids(gamePk: string, finalists: string[]) {
    finalists = finalists || ['JTdh9Y9eO2MzzPJnEGboOIsdtxC3', 'lrDtAgYsqDgdMzb4c8r7zEVoVLU2'];
    if (!this.bids || !this.bids[gamePk]) {
      this.loadBids(gamePk)
      return {};
    }
    // console.log(Object.keys(this.bids[gamePk] || {}).filter(uid => finalists.includes(uid)));
    const finalistBids = {};
    finalists.forEach(uid => {
      if (this.bids[gamePk][uid]) {
        finalistBids[uid] = this.bids[gamePk][uid];
      }
    });
    // console.log(finalistBids);

    return finalistBids;
  }

  calcWinRangeMiltiplied(cellIndex, sport) {
    const scoreMultiplier = sport !== 'nhl' ? 7 : 1; // NBA+NFL
    const scoreShift = sport === 'nba' ? 83 : 0; // NBA
    switch (sport) {
      case 'nfl':
      case 'nba':
        switch (cellIndex) {
          case 6:
            cellIndex = scoreShift + ((cellIndex) * scoreMultiplier + 1);
            break;
          default:
            cellIndex = '<span>→</span>' + (scoreShift + (cellIndex + 1) * scoreMultiplier);
        }
        break;
      case 'nhl':
        break;
      default:
    }
    return cellIndex + (cellIndex >= 6 ? '+' : '');
  }

  selectFillClass(row, col, arr?) {
    return '';
  }

  initFinalists() {
    this.resetBid(0);
  }

  getUserColor(user_uid, winTab) {
    let color = 'inherit';
    if (this.users[user_uid] && this.users[user_uid].color) {
      color = this.users[user_uid].color;
    }
    if (winTab) {
      return { 'color': color }
    } else {
      return { 'background': '#fff', 'color': '#333', 'box-shadow': '1px 1px 2px' + color };
    }
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const modal = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid,
        modalController: this.modalController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await modal.present();
  }

  resetBid(val) {

    for (let r = 0; r < this.grid.count; r++) {
      this.bid[r] = this.bid[r] || [];
      for (let c = 0; c < this.grid.count; c++) {
        if (c != r || c == 6 || r == 6 || !this.challenge.sport || this.challenge.sport !== 'nhl') {
          this.bid[r][c] = val;
        } else { // tied score
          this.bid[r][c] = 0;
        }
      }
    }
    //console.log(this.bid);

    return true;
  }

  openFinalistGame(uid) {
    this.router.navigate(['/bid/' + (this.users[uid].games[this.finalGame.gamePk] || 0) + '/' + this.finalGame.gamePk + '/' + this.challenge_id], { replaceUrl: true, state: { user_uid: uid } });
  }

  async openTab(user_uid, showProfile?) {
    if (showProfile && this.bidSelected !== 'winning' && this.bidSelected === user_uid) {
      this.openFinalistGame(user_uid);
    }
    this.bidSelected = user_uid;
  }

  async loadTeams(challengeGames?) {
    if (this.teams2 && Object.keys(this.teams2).length) {
      return;
    }
    if (challengeGames && challengeGames.length) {
      challengeGames.forEach(gamePk => {
        if (!this.teams2 || !this.teams2[gamePk]) {
          this.teamSubs = this.teamSubs || {};
          if (typeof this.teamSubs[gamePk] !== 'undefined') {
            this.teamSubs[gamePk].unsubscribe();
          }
          this.teamSubs[gamePk] = this.afs.collection("team").doc(gamePk).valueChanges().subscribe((game: any) => {
            if (!game) {
              this.teams2[gamePk] = { [0]: { locked: false } };
            }
            else {
              Object.keys(game || {}).forEach(team_id => {
                this.teams2[gamePk] = this.teams2[gamePk] || {};
                this.teams2[gamePk][team_id] = game[team_id];
              });
              if (!this.challengeStarted && this.teams2[gamePk][0].locked) {
                this.challengeStarted = true;
              }
            }
            if (Object.keys(this.teams2).length === challengeGames.length ||
              challengeGames[challengeGames.length - 1] === gamePk) {
              setTimeout(() => {
                this.calcChallengeWinners(); // TODO: investigate click delay
              }, (!this.challenge_id || !this.games || !this.games.length) ? 500 : 0);
            }
          });
        }
      });
    }
    else if (false) {
      this.teamSubs['all'] = this.afs.collection("team").valueChanges({ idField: 'id' }).subscribe(games => {
        games.forEach(game => {
          Object.keys(game).forEach(team_id => {
            var team = game[team_id];
            this.teams2[game.id] = this.teams2[game.id] || {};
            this.teams2[game.id][team_id] = team;
          });
        });
        setTimeout(() => {
          this.calcChallengeWinners();
        }, (!this.challenge_id || !this.games) ? 0 : 1500);
      });
    }
  }

  getSortedSchedule() {
    this.linksSorted = Object.keys(this.links).sort((a: any, b: any) => { return a - b; });
    return this.linksSorted;
  }

  calcChallengeWinners() {
    if (!this.challenge_id) return;
    this.challengeWinners = {};
    this.challengeUsers = [];
    let gamesEnd = [];
    if (this.games instanceof Array && Object.keys(this.teams2).length) {
      if ((this.challenge_id && (this.challenges[this.challenge_id] || { games: [] })['games'].length)) {
        let count = 0;
        this.challenges[this.challenge_id]['games'].forEach(gamePk => {
          if (this.teams2[gamePk] && this.games) {
            this.teams2[gamePk][0]['winners'] && this.teams2[gamePk][0]['winners'].length ? count++ : null;
            this.games.forEach(chGames => {
              chGames['games'].forEach(game => {
                if (game.gamePk === gamePk && !['1', '6', '7'].includes(game.status.statusCode)) {
                  this.currentHidden = false;
                }
              });
            });
            Object.keys(this.teams2[gamePk]).forEach(team_id => {
              this.challengeWinners[gamePk] = Array.from(new Set((this.challengeWinners[gamePk] || []).concat(this.teams2[gamePk][team_id]['winners'] || [])));
              this.challengeUsers = Array.from(new Set((this.challengeUsers || []).concat(this.teams2[gamePk][team_id]['users'] || [])));
            });
          }
        });
      }
    }
    this.calcWinnersStats();
    this.openWinnerPopup(this.challenge_id, this.winner, 'challengeShowPopups');
    return this.challengeWinners;
  }

  calcWinnersStats() {
    if (!this.user) { // do not calc until user data loaded
      return;
    }
    this.stats = { [this.user.uid]: 0 };
    let finalistStats = { [this.user.uid]: 0 };
    Object.keys(this.challengeWinners || {}).forEach(gamePk => {
      if (this.challengeWinners[gamePk]) {
        this.challengeWinners[gamePk].forEach(winner_uid => {
          this.stats[winner_uid] = (this.stats[winner_uid] || 0) + 1;
          if (gamePk !== this.challenge.finalGame) {
            finalistStats[winner_uid] = (finalistStats[winner_uid] || 0) + 1;
          }
        });
      }
    });
    if (Object.keys(this.stats).length === 1 && this.stats[this.user.uid] === 0) {
      this.stats = {};
    }
    this.leaderboard = Object.entries(this.stats).sort((a, b) => a[1] > b[1] ? -1 : a[1] === b[1] ? 0 : 1);
    this.finalists = Object.entries(finalistStats).sort((a, b) => a[1] > b[1] ? -1 : a[1] === b[1] ? 0 : 1);
    let winners = this.leaderboard.filter(lead => this.leaderboard[0][1] === lead[1] ? lead : null);
    let prize = Math.round((+this.challenges[this.challenge_id].amount || 100) / winners.length);
    if (this.isLastGamePlayed) { // Refactor!
      let winPrizes = [];
      let challengePrizes = {};
      winners.forEach(item => {
        let win = {};
        win[item[0]] = prize;
        winPrizes.push(win);
        challengePrizes[item[0]] = prize;
      });
      if (!this.challenges[this.challenge_id]['winners'] || !this.challenges[this.challenge_id]['challengeWinners'] ||
        !deepEqual(this.challenges[this.challenge_id]['winners'], winPrizes)) {
        //this.afs.collection('challenge').doc(this.challenge_id).update({ winners: winPrizes, challengeWinners: challengePrizes });
      }
      this.openWinnerPopup(this.challenge_id, this.leaderboard[0], 'challengeShowPopups');
    }
    this.winner = this.leaderboard[0];
    if (this.winner && this.winner.length) {
      if (!this.challenge.leader || this.challenge.leader !== this.winner[0]) {
        console.log('new leader');
        //this.sendNotification(this.notificationLabel.challengeCurrentWinner, this.challenge, {}, this.challenge.leader); // previous leader
        //this.sendNotification(this.notificationLabel.challengeCurrentWinner, this.challenge, {}, this.winner[0]); // new leader
        this.challenge.notifications = this.challenge.notifications || {};
        this.challenge.notifications.challengeCurrentWinner = { sent: new Date(), recepients: [this.winner[0]] };
        this.afs.collection("challenge").doc(this.challenge.id).update({ leader: this.winner[0], notifications: this.challenge.notifications });
      }
    }
    let friendLeaders = [this.leaderboard.find(user => user[0] === this.user.uid ? user : null)];
    if (this.userData && this.userData['friends']) {
      this.userData['friends'].forEach(friend => {
        let leader = this.leaderboard.find(user => user[0] === friend.uid ? user : null);
        if (leader) {
          friendLeaders.push(leader)
        }
      });
    };
    this.leaders = this.leaderboard;
    if (this.leaders.length) {
      this.maxWin = this.leaders[0][1];
      this.countWin = 0;
      this.leaders.find(lead => {
        if (lead[1] === this.maxWin) {
          this.countWin += 1;
        }
      })
    };
    this.usersLeaderboard = this.zeroUsersConcat(this.challengeUsers, this.leaders);
    this.tabWinners = this.usersLeaderboard;
    this.calcCurrentLeaders();
  }

  calcCurrentWinner() {
    let leaders = [];
    this.games.forEach(challengeGames => {
      //leaders = []; // Seriously?
      if (!challengeGames) return;
      let currentLeaders = [];
      let games = challengeGames['games'];
      if (games) games.forEach(game => {
        if (this.teams2[game.gamePk] && this.teams2[game.gamePk][0] && (!this.teams2[game.gamePk][0]['winners'] || this.teams2[game.gamePk][Object.values(this.teams2[game.gamePk]).length - 1]['winners'].length === 0)) {
          let leader = this.getLeaders(game);
          leader.forEach(lead => {
            currentLeaders.push(lead);
          });
          this.currentHidden = currentLeaders.length < 2;
          let arr = [];
          currentLeaders.forEach(item => {
            let repeatEl = arr.find(a => a[0] === item ? a : null);
            repeatEl ? ++repeatEl[1] : arr.push([item, 1]);
          });
          leaders = arr.sort((a, b) => a[1] > b[1] ? -1 : a[1] === b[1] ? 0 : 1);
        }
      });
    })
    return leaders;
  }

  zeroUsersConcat(users, leaders) {
    let zeroUsers = [];
    let leaderUsers = [];
    leaders.forEach(leader => leaderUsers.push(leader[0]));
    users.filter(user => leaderUsers.indexOf(user) === -1 ? zeroUsers.push([user, 0]) : null);
    return leaders.concat(zeroUsers);
  }

  openWinnerPopup(id, winner, localStorageKey) {
    const showPopups = JSON.parse(localStorage.getItem(localStorageKey)) || [];
    if (showPopups.indexOf(id) === -1 && winner) {
      if (!this.showPopup) {
        this.winnerPopup(id, winner, localStorageKey)
      }
    }
  }

  async winnerPopup(id, winner: any, localStorageKey, event?) {
    let title = '';
    let text = '';
    let isWinner = false;
    let userWinner;
    let url;
    if (localStorageKey === 'challengeShowPopups' && winner && this.isLastGamePlayed && !this.showPopup) {
      if (this.challenge.sport !== 'nba' || this.challenge.games.length === winner[1]) {
        const users = await this.usersService.load([winner[0]]);
        userWinner = users ? users[winner[0]] : {};
        if (Object.keys(userWinner||{}).length && !this.showPopup) {
          this.showPopup = true;
          url = userWinner.photoURL || Constants.AVATAR_DEMO;
          winner[0] === this.user.uid ? (text = 'You Won!', isWinner = true) : text = 'Won!';
        }
        else {
          setTimeout(() => {
            this.winnerPopup(id, winner, localStorageKey, event);
          }, 1500);
          return;
        }
      }
      else {
        let isWinner = false;
        title = 'Ohhh Noooo...'
        text = "Nobody won all 3 games.\n\nThe $200 prize will be added to tomorrow’s Win It All NBA Challenge!";
        url = '/assets/app_logo.png';
      }
      const popover = await this.popoverCtrl.create({
        component: ModalWinnerComponent,
        event: event,
        componentProps: {
          winner: userWinner,
          url,
          text,
          title,
          isWinner
        },
        cssClass: 'modal-wrap-default'
      });

      popover.onDidDismiss()
        .then((result) => {
          this.isShowPopup = true;
          if (this.isShowPopup) {
            let showPopups = JSON.parse(localStorage.getItem(localStorageKey)) || [];
            if (showPopups.indexOf(id) === -1) {
              showPopups.push(id);
              localStorage.setItem(localStorageKey, JSON.stringify(showPopups));
            }
          }
        });
      return await popover.present();
    } else if (localStorageKey === 'bidsGame') {
      text = "You're in business";
      userWinner = this.getUser(winner) || null;
      url = this.getUser(winner).photoURL || Constants.AVATAR_DEMO;
      const popover = await this.popoverCtrl.create({
        component: ModalWinnerComponent,
        event: event,
        componentProps: {
          winner: userWinner,
          url,
          text, isWinner
        },
        cssClass: 'modal-wrap-default'
      });

      popover.onDidDismiss()
        .then((result) => {
          this.isShowPopup = true;
          if (this.isShowPopup) {
            let showPopups = JSON.parse(localStorage.getItem(localStorageKey)) || [];
            if (showPopups.indexOf(id) === -1) {
              showPopups.push(id);
              localStorage.setItem(localStorageKey, JSON.stringify(showPopups));
            }
          }
        });
      return await popover.present();
    }


  }

  checkFinalist(uid, row, col) {
    return this.fakeFinalist && !this.fakeFinalist.includes(uid); // TODO: Check if required
  }

  checkNFLNHLFinalistBids(uid, row, col) {
    return true; // TODO: Check if required
    if (this.finalistBids[this.finalists[0][0]][row][col] === 0) return true;
    const user = this.finalists.find(user => user[0] === uid);
    return user[1] >= this.finalists[0][1];
  }

  getChallengeGames(challenge_id?) {
    challenge_id = challenge_id || this.challenge_id;
    let challenge = this.challenges[challenge_id] || this.challenge || { games: [] };
    if (!challenge['start']) return;
    let games = [];
    let date = new Date(challenge['start']['seconds'] * 1000);
    let endedGamesCount = 0;
    let gameCount = 0;
    let game = {};
    for (var i = 0; i < this.games.length; i++) {
      if (this.games[i]['date'] == date.toJSON().slice(0, 10) || true) { // Temp allow multi day Challenges
        game = Object.assign({}, this.games[i], { games: [] });
        let localFinalGame;
        for (var j = 0; j < this.games[i]['games'].length; j++) {
          if (challenge['games'].includes(String(this.games[i]['games'][j]['gamePk']))) {
            game['games'].push(this.games[i]['games'][j]);
            if (this.games[i]['games'][j]['status']['statusCode'] === '6' || this.games[i]['games'][j]['status']['statusCode'] === '7') {
              endedGamesCount++;
              this.games[i]['games'][j]['isEnd'] = true;
            }
            if (this.challenge.finalGame == this.games[i]['games'][j].gamePk) {
              localFinalGame = this.games[i]['games'][j];
            }
            gameCount++;

            // 1. Determine the Finalist Game
            if (this.challenge.games.length === gameCount && this.challenge.games.length - 1 <= endedGamesCount &&
              localFinalGame && (!this.finalGame || !this.finalistBids) && this.challenge.finalGame) {  // TODO: Show only on last Game
              this.finalGame = localFinalGame;
              this.finalGame.challenge_id = this.challenge_id;
              this.scoreMultiplier = this.challenge.sport !== 'nhl' ? 7 : 1; // NBA+NFL
              this.scoreShift = this.challenge.sport === 'nba' ? 83 : 0; // NBA
              this.getGameGoals(this.finalGame.gamePk);
              this.finalGameLeaders = this.engineService.calcCurrentWinner(this.finalGame, this.teams2[this.finalGame.gamePk], this.bids[this.finalGame.gamePk], this.scoreMultiplier, this.scoreShift);

              // 2. Determine Finalists
              let finalists = [];
              if (this.usersLeaderboard && this.usersLeaderboard[0] && (this.usersLeaderboard[0][1] > (gameCount - 2) || this.challenge.sport !== 'nba')
                && this.bids[this.finalGame.gamePk] && Object.keys(this.bids[this.finalGame.gamePk]).length) {
                this.finalistBids = {};
                // We have Finalists
                if (this.challenge.sport === 'nba') { // NBA
                  this.finalists.forEach(user => {
                    if (user[1] >= 2) { // NBA
                      finalists.push(user[0]);
                    }
                  });
                }
                else if (this.challenge.sport !== 'nba') { // NHL/NFL
                  this.finalists.forEach(user => {
                    if (user[1] === this.finalists[0][1]) {
                      finalists.push(user[0]);
                      this.firstFinalist.push(user[0]);
                    }
                  });

                  this.finalists.forEach(user => {
                    const uid = user[0];
                    if (user[1] === this.finalists[0][1] - 1) {
                      finalists.push(user[0]);
                    }
                  });
                }

                // 3. Get Finalist Winning bids
                Object.keys(this.teams2[this.finalGame.gamePk]).forEach(team_id => {
                  //console.log(team_id, this.teams2[this.finalGame.gamePk][team_id]);
                  finalists.forEach(uid => {
                    if (this.teams2[this.finalGame.gamePk][team_id].users.includes(uid)) {
                      //console.log('Team: '+team_id, uid);
                      //console.log(this.teams2[this.finalGame.gamePk][team_id].users);
                      let teamBids = this.commonService.getObjectOrderedKeys(this.bids[this.finalGame.gamePk] || {}, this.teams2[this.finalGame.gamePk][team_id].users);
                      let winningBid = [];
                      this.winningUser = [];
                      let winners = this.engineService.calcBidWinner(this.finalGame, { [this.finalGame.gamePk]: teamBids }, this.scoreMultiplier, this.scoreShift);
                      this.engineService.calcWinningBids(this.finalGame, { [this.finalGame.gamePk]: teamBids }, winners, this.winningUser, winningBid);
                      this.finalistBids[uid] = [];
                      this.engineService.winningBid.forEach((row, x) => {
                        this.finalistBids[uid][x] = [];
                        row.forEach((cell, y) => {
                          if (this.engineService.winningUser[x][y] === uid) {
                            this.finalistBids[uid][x][y] = cell;
                          }
                          else {
                            this.finalistBids[uid][x][y] = 0;
                          }
                        });
                      });
                      //this.finalistBids[uid] = this.engineService.winningBid;
                    }
                  })
                  //console.log(this.finalistBids);
                });

                // 4. Check if second place Finalist has at least one square not shared with first place Finalists
                if (this.challenge.sport !== 'nba') { // NHL/NFL
                  this.fakeFinalist = [];
                  this.finalists.forEach(user => {
                    const uid = user[0];
                    if (user[1] === this.finalists[0][1] - 1) {
                      //console.log('found second place Finalist', uid, this.users[uid].publicName);
                      let isSecondFinalist = false;
                      this.finalistBids[uid].forEach((row, x) => {
                        row.forEach((cell, y) => {
                          if (cell) {
                            //console.log('found second place Finalist winnig bid ', x, y, cell);
                            let isBidUnique = true;
                            this.firstFinalist.forEach(uid2 => {
                              if (uid !== uid2) {
                                if (this.finalistBids[uid2] && this.finalistBids[uid2][x][y]) {
                                  //console.log('user ' + uid2+ '(', this.users[uid2].publicName + ') has a competing bid ' + this.finalistBids[uid2][x][y]);
                                  isBidUnique = false;
                                }
                              }
                            });
                            if (isBidUnique) {
                              //console.log('found second place Finalist UNIQUE bid ', x, y, cell);
                              isSecondFinalist = true;
                            }
                            else {
                              //console.log('couldnt found second place Finalist UNIQUE bid ', x, y, cell);
                              this.finalistBids[uid][x][y] = 0; // reseting
                            }
                          }
                        });
                      });
                      if (!isSecondFinalist) {
                        //console.log('RESULT: user has to be removed from Finalist Grid', uid, this.users[uid].publicName);
                        this.fakeFinalist.push(uid);
                      }
                      else {
                        //console.log('RESULT: user will remain on Finalist Grid', uid, this.users[uid].publicName);
                      }
                    }
                  });
                  if (this.fakeFinalist.length) {
                    console.log('hiding fake Finalists', this.fakeFinalist);
                  }
                }
              }
              else if (!this.bids[this.finalGame.gamePk]) {
                this.bids[this.finalGame.gamePk] = {};
                this.loadBids(this.finalGame.gamePk);
              }
            }
          }
        }
        if (game['games'].length > 0) {
          games.push(game);
        }
      }
    }

    if (typeof games[0] === 'undefined') return [];

    this.isLastGamePlayed = (endedGamesCount && endedGamesCount === this.challenge.games.length);
    if (endedGamesCount === 0 || this.isLastGamePlayed) {
      games[0].games.sort((a, b) => new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime());
      this.challengeGames = games;
    } else {
      games[0].games.sort((a, b) => {
        if (!a.isEnd) {
          return new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime()
        }
      })
      let startGames = [];
      let endGames = [];
      games[0].games.forEach(item => item.isEnd ? endGames.push(item) : startGames.push(item)) 
      if(startGames.length) {
        startGames.sort((a, b) => new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime());
      } 
      if(endGames.length) {
        endGames.sort((a, b) => new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime());
      } 
      games[0].games = startGames.concat(endGames)
      this.challengeGames = games;
    }
    return games;
  }

  async loadBids(gamePk, userFiltered?, inviteId?) {

    if (gamePk && (!this.bids || !this.bids[gamePk] || !Object.keys(this.bids[gamePk]).length)) {
      this.bids[gamePk] = {};
      const legacyBid = (parseInt(gamePk) > 2019020402 && parseInt(gamePk) < 2019020565);
      this.afs.collection("bid/").doc(String(gamePk)).valueChanges().subscribe((bids: object) => {
        if (legacyBid) {
          Object.values(bids).forEach(team => {
            this.bids[gamePk] = Object.assign({}, this.bids[gamePk], team);
          });
        } else {
          this.bids[gamePk] = bids;
        }
        if (!userFiltered && inviteId && bids) {
          this.afs.collection('invite').doc(inviteId).update({ bids: { [gamePk]: bids } });
        }
        this.initFinalists();
      });
    }
    return this.bids[gamePk];
  }

  loadBidsFiltered(gamePk, userFiltered?) {
    if (!this.bids || !this.bids[gamePk]) {
      const legacyBid = (parseInt(gamePk) > 2019020402 && parseInt(gamePk) < 2019020565);
      this.afs.collection("bid/").doc(String(gamePk)).valueChanges().subscribe((item: object) => {
        if (legacyBid) {
          Object.values(item).forEach(team => {
            this.bids[gamePk] = Object.assign({}, this.bids[gamePk], team);
          });
        } else {
          if (typeof this.teams2[gamePk] !== 'undefined' && typeof this.user.games[gamePk] !== 'undefined'
            && typeof this.teams2[gamePk][this.user.games[gamePk]] !== 'undefined') {
            let users = this.teams2[gamePk][this.user.games[gamePk]].users;
            this.bids[gamePk] = {}
            users.forEach(user_uid => {
              if (typeof item[user_uid] !== 'undefined') {
                this.bids[gamePk][user_uid] = item[user_uid];
              }
            });
            //this.bids[gamePk] = item;
          } else {
            this.bids[gamePk] = item;
          }
        }
      });
    }
    return this.bids[gamePk];
  }

  loadBidsCollection(gamePk, userFiltered?) {
    if (!this.bids || !this.bids[gamePk]) {
      const legacyBid = (parseInt(gamePk) > 2019020402 && parseInt(gamePk) < 2019020565);
      if (userFiltered && typeof this.teams2[gamePk] !== 'undefined'
        && typeof this.user.games[gamePk] !== 'undefined'
        && typeof this.teams2[gamePk][this.user.games[gamePk]] !== 'undefined') {
        this.bids[gamePk] = this.bids[gamePk] || {};
        (this.teams2[gamePk][this.user.games[gamePk]].users).forEach(user_uid => {
          this.afs.collection("bid/" + gamePk + "/" + user_uid).valueChanges().subscribe(item => {
            this.bids[gamePk][user_uid] = item;
          });
        });
      } else {
        this.afs.collection("bid/").doc(String(gamePk)).valueChanges().subscribe((item: object) => {
          if (legacyBid) {
            Object.values(item).forEach(team => {
              this.bids[gamePk] = Object.assign({}, this.bids[gamePk], team);
            });
          } else {
            this.bids[gamePk] = item;
          }
        });
      }
    }
    return this.bids[gamePk];
  }

  getChallengeGamesSubmitted(challenge_id?) {
    challenge_id = challenge_id || this.challenge_id;
    let userBids = Object.keys(this.userBids || {});
    if (!userBids || !this.challenge || !this.challenge['games']) return [];
    return this.challenge['games'].filter(function (n) {
      return userBids.indexOf(n) !== -1;
    });
  }

  async loadChallenges(challenge_id?) {
    if (challenge_id) {
      if (typeof this.challengeSubs !== 'undefined') {
        this.challengeSubs.unsubscribe();
      }
      this.challengeSubs = this.afs.collection('challenge').doc(challenge_id).valueChanges().subscribe((challenge: any) => {
        //console.log('challenge updated');
        if (Object.keys(this.challenge).length) return;
        this.challenge = challenge;
        this.loadLeague(this.challenge.sport);
        this.loadLeagueStandings(this.challenge.sport);
        this.countdown(this.challenge.start);
        this.loadTeams(challenge.games || []);
        const leader = challenge.leader || '';
        this.challenge.id = challenge_id;
        this.challenges[challenge_id] = Object.assign({ name: '', text: '', games: [] }, challenge);
        this.loadGames();
        setTimeout(() => {
          this.calcChallengeWinners();
          this.calcCurrentLeaders();
        }, (typeof this.stats === 'undefined' || !this.users) ? 100 : 0);

        setTimeout(() => {
          this.challenges[challenge_id]['games'].forEach((gamePk, index) => {
            //this.loadBids(gamePk, true);
          });
        }, 200);
      });
    } else {
      this.challengesSubs = this.afs.collection('challenge', ref => ref
        .orderBy('start', 'desc')
        .where('start', '>', new Date('2020-04-01'))
        .where('start', '<', new Date(Date.now() + 120 * 24 * 60 * 60000))
        .where('sport', '==', this.sport || '')
        .where('disabled', '==', false)
        .limit(6))
        .valueChanges({ idField: 'id' }).subscribe((challenges) => {
          challenges.forEach((challenge: any, index) => {
            if (index < 8 && challenge['start']['seconds'] * 1000 < Date.now()
              && challenge.games && Object.keys(this.users).length < 2) { // Preload users to optimize Challenge page load
              const date = this.datePipe.transform(new Date(challenge['start']['seconds'] * 1000), 'yyyy-MM-dd');
              //this.loadTeams(challenge.games || []);
              setTimeout(() => {
                const users = this.getChallengeUsers(challenge.id, true);
                this.loadUsers(users);
              }, 500);
              if (!Object.keys(this.teams2).length || !Object.keys(this.users).length) {
                setTimeout(() => {
                  const users = this.getChallengeUsers(challenge.id, true);
                  this.loadUsers(users);
                }, 2000);
              }
            }
            if (challenge['start']['seconds'] * 1000 < (Date.now() - 4 * 30 * 24 * 60 * 60000)) {
              this.history = this.history || [];
              this.history.push(Object.assign({ name: '', text: '', games: [] }, challenge));
            }
            else {
              this.challenges = this.challenges || [];
              this.challenges[challenge.id] = Object.assign({ name: '', text: '', games: [] }, challenge);
              this.links[challenge.start.seconds] = { challengeId: challenge.id };
            }
            if (challenge['id'] === this.challenge_id) {
              this.challenge = challenge;
              if (this.challenges[challenge.id] && this.challenges[challenge.id]['games']) {
                this.challenges[challenge.id]['games'].forEach((gamePk, index) => {
                  this.loadBids(gamePk, true);
                });
              }
            }
          });
          this.getSortedSchedule();
        });
    }
  }

  getChallengeUsers(challenge_id, winners?) {
    let users = [];
    if (this.challenges[challenge_id] && this.challenges[challenge_id]['games']) {
      this.challenges[challenge_id]['games'].forEach(game => {
        if (this.teams2[game]) {
          Object.keys(this.teams2[game]).forEach(team_id => {
            (this.teams2[game][team_id][(winners ? 'winners' : 'users')] || []).forEach(user => {
              if (users.indexOf(user) === -1) {
                users.push(user);
              }
            })
          });
        }
      });
    }
    return users;
  }

  isInviteAdmin(invite: any) {
    return invite && invite.admins.indexOf(this.user.uid) !== -1;
  }

  isWinning(invite: any) {
    return invite && invite.winners.indexOf(this.user.uid) !== -1;
  }

  async loadInvites() {
    if (typeof this.invites === 'undefined') {
      this.afs.collection('invite', ref => ref
        .where('users', 'array-contains', this.user.uid)
        .orderBy('start', 'desc')
        .where('sport', '==', this.sport)
        .where('start', '>', new Date(Date.now() - 3 * 24 * 3600 * 1000)))
        .valueChanges({ idField: 'id' }).subscribe((invites: Array<any>) => {
          this.invites = {};
          invites.forEach((invite: Invite) => {
            this.invites[invite.id] = invite;
            while (typeof this.links[invite.start.seconds] !== 'undefined' &&
              this.links[invite.start.seconds].inviteId && this.links[invite.start.seconds].inviteId !== invite.id) {
              invite.start.seconds++;
            }
            this.links[invite.start.seconds] = { inviteId: invite.id };
          });
          // if ((!this.linksSorted || !this.linksSorted.length) && Object.keys(this.links).length) {
            this.getSortedSchedule();
          // }
        });
    }
  }

  doRefreshChallenge(event: any) {
    let self = this;
    setTimeout(() => {
      self.loadGames();
      event.target.complete();
    }, 2000);
  }

  getUrl(index) {
    return this.getUser(index).photoURL || Constants.AVATAR_DEMO;
  }

  calcCurrentLeaders() {
    if (this.usersLeaderboard) {
      // let currentLeaders = this.usersLeaderboard;
      let currentLeaders = JSON.parse(JSON.stringify(this.usersLeaderboard));
      let leaders = this.calcCurrentWinner();
      currentLeaders.forEach(win => {
        leaders.find(lead => {
          if (win[0] === lead[0]) {
            win[1] += lead[1]
          }
        })
      })
      this.currentLeaders = currentLeaders.sort((a, b) => a[1] > b[1] ? -1 : a[1] === b[1] ? 0 : 1);
      if (this.segmentSelected === 'current') {
        this.tabWinners = this.currentLeaders;
      }
    }
  }

  segmentChanged(event) {
    this.segmentSelected = event.detail.value;
    if (this.segmentSelected === 'current') {
      this.leadersCount = 9;
      this.calcCurrentLeaders();
    } else {
      this.leadersCount = 9;
      this.tabWinners = this.usersLeaderboard;
    }
  }

  isFriend(uid: string) {
    return this.userFriends.includes(uid);
  }

  friendExist() {
    return this.userFriends && this.userFriends.length;
  }

  randomQuote() {
    this.afs.collection("quotes").valueChanges({ idField: 'id' }).subscribe(quotes => {
      this.quote = quotes[Math.floor(Math.random() * quotes.length)];
    });
  }

  getLeaders(game) {
    let leaders = [];
    if (!game.teams || !this.teams2
      || (!game.teams.away.score && !game.teams.home.score)
      || !this.teams2[game.gamePk]) {
      return leaders;
    }
    if (this.user.games && this.user.games[game.gamePk]
      && this.teams2[game.gamePk][this.user.games[game.gamePk]]) {
      leaders = this.teams2[game.gamePk][this.user.games[game.gamePk]].winners || this.teams2[game.gamePk][this.user.games[game.gamePk]].leaders || [];
      if (!leaders.length && this.teams2[game.gamePk][this.user.games[game.gamePk]].leaders
        && this.teams2[game.gamePk][this.user.games[game.gamePk]].leaders.length) {
        leaders = this.teams2[game.gamePk][this.user.games[game.gamePk]].leaders;
      }
    }
    if (!leaders || !leaders.length) {
      if (Object.keys(this.bids).length && Object.keys(this.bids[game.gamePk] || {}).length
        && this.teams2[game.gamePk][this.user.games[game.gamePk]]) {
        leaders = this.engineService.calcCurrentWinner(game, this.teams2[game.gamePk], this.bids[game.gamePk], this.scoreMultiplier, undefined);
        this.teams2[game.gamePk][this.user.games[game.gamePk]].leaders = leaders;
        //this.setGameLeaders(game, leaders); // disabled
      }
      else {
        this.loadBids(game.gamePk, true);
      }
    }
    this.loadUsers(leaders);
    return leaders;
  }

  processNotifications() {
    if (this.user.uid !== '-JTdh9Y9eO2MzzPJnEGboOIsdtxC3') return;
    console.log('processNotifications');
    if (Object.keys(this.users).length > 1 && this.games instanceof Array && Object.keys(this.teams2).length && this.challenge) {
      let event;
      for (var i = 0; i < this.games.length; i++) {
        event = this.games[i];
        if (event.games) {
          event.games.forEach(game => {
            if (game.gamePk && this.challenge.games.indexOf(game.gamePk) !== -1) {
              if (['6', '7'].includes(game.status.statusCode)) {
                if (this.teams2[game.gamePk] && this.teams2[game.gamePk][0].locked &&
                  (!this.challenge.notifications || !this.challenge.notifications.gameWinner || !this.challenge.notifications.gameWinner[game.gamePk])) {
                  // console.log(this.teams2[game.gamePk]);
                  game.winners = game.winners || [];
                  Object.values(this.teams2[game.gamePk]).forEach((team: any) => {
                    game.winners = game.winners.concat(team.winners);
                  });
                  if (!game.winners.length) return; // Winners are not calculated yet
                  this.challenge.notifications = this.challenge.notifications || {}; // Troubleshoot delayed Notifcation flag
                  this.challenge.notifications.gameWinner = (this.challenge.notifications || { gameWinner: {} }).gameWinner || {};
                  this.challenge.notifications.gameWinner[game.gamePk] = { sent: new Date(), recepients: game.winners };
                  // console.log(game);
                  this.afs.collection("challenge").doc(this.challenge.id).update({ notifications: this.challenge.notifications })
                    .then(() => {
                      // console.log('count GameWon', Object.keys(this.challenge.notifications.gameWinner).length);
                      // console.log('count Challenge Games', this.challenge.games.length);
                      if (this.challenge.games.length === Object.keys(this.challenge.notifications.gameWinner).length
                        && !this.challenge.notifications.challengeWon) {
                        if (this.leaderboard.length) {
                          this.challenge.notifications.challengeWon = { sent: new Date(), recepients: [this.leaderboard[0][0]] };
                          console.log('challengeWon');
                          this.sendNotification(this.notificationLabel.challengeWon, this.challenges[this.challenge_id]);
                          this.afs.collection("challenge").doc(this.challenge.id).update({ notifications: this.challenge.notifications });
                        }
                      }
                    });
                  this.sendNotification(this.notificationLabel.gameWinner, this.challenge, game);

                }
              }
              else if (!['1'].includes(game.status.statusCode)) {
                if (this.teams2[game.gamePk] && this.teams2[game.gamePk][0].locked && (!this.challenge.notifications || !this.challenge.notifications.challengeStarted)) {
                  // console.log('challengeStarted', this.challenge.notifications);
                  // console.log(this.teams2[game.gamePk][0]);
                  this.challenge.users = this.challenge.users || this.getChallengeUsers(this.challenge.id);
                  this.challenge.notifications = { ...this.challenge.notifications, challengeStarted: { sent: new Date(), recepients: this.challenge.users } };
                  // console.log(this.challenge);
                  this.afs.collection("challenge").doc(this.challenge.id).update({ notifications: this.challenge.notifications });
                  this.sendNotification(this.notificationLabel.challengeStarted, this.challenge);
                }
              }
              else { }
            }
          });
        }
      }
    }

    return;
  }

  setGameLeaders(game, leaders) {
    if (game && game.status.statusCode !== '1' && this.user.games && typeof this.user.games[game.gamePk] !== 'undefined'
      && this.teams2[game.gamePk][this.user.games[game.gamePk]]) {
      const team_id = this.user.games[game.gamePk];
      const team = this.teams2[game.gamePk][this.user.games[game.gamePk]];
      team.leaders = leaders || [];
      //this.afs.collection("team").doc(game.gamePk).update({ [team_id]: Object.assign({}, team) });
    }
  }

  showMore() {
    const step = 50;
    this.leadersCount += step;
  }

  async sendNotification(type: string, challenge: any, game?: any, leader?: any) {
    if (this.user.uid !== '-JTdh9Y9eO2MzzPJnEGboOIsdtxC3') return;
    let recipients = [];
    let title = '';
    let body = '';
    if (type === this.notificationLabel.challengeWon) {
      // console.log(this.leaderboard);
      if (this.leaderboard.length) {
        recipients.push(this.leaderboard[0][0]);
      }
      // console.log(recipients);
      title = this.user.uid === this.leaderboard[0][0] ? 'CONGRATULATIONS!' : 'Challenge Won';
      body = (this.user.uid === this.leaderboard[0][0] ? 'You' : this.getUser(this.leaderboard[0][0]).name) + ' won challenge';
    } else if (type === this.notificationLabel.challengeStarted) {
      recipients = challenge.users;
      title = 'Challenge Started';
      body = challenge.name + ' is underway';
    } else if (type === this.notificationLabel.gameWinner) {
      recipients = game.winners;
      title = 'Game Won';
      body = 'You Won ' + game.teams.away.team.name + ' @ ' + game.teams.home.team.name + ' Game';
    } else if (this.notificationLabel.challengeCurrentWinner) {
      recipients = [leader];
      title = 'Challenge Leader';
      body = challenge.leader !== leader ? 'You are the current Leader of the ' + challenge.name : 'You are no longer Leader of the ' + challenge.name;
    } else if (type === this.notificationLabel.missingBids) {
      recipients = [this.user.uid];
      title = this.notificationLabel.missingBids;
      body = "Hey " + this.user.displayName + ", You have not placed bids for # Games for today's " + challenge.name
    }

    recipients.forEach(uid => {
      if (this.getUser(uid)) {
        // console.log(this.users[uid], 'user token')
        if (this.getUser(uid).tokens) {
          let tokens = Object.values(this.getUser(uid).tokens).filter(uid => uid);
          // console.log('SENDING TO ', tokens);
          // console.log({ tokens: tokens, title: title, body: body });
          if (tokens) {
            const callable = this.fns.httpsCallable('sendNativeNotification');
            tokens.forEach(token => {
              if (false && type !== this.notificationLabel.challengeMissingBids && token) {
                callable({ tokens: token, title: title, body: body })
                  .toPromise().then((resp) => {
                    console.log('SENT', token);
                  });
              }
              else {
                console.log('SENT disabled');
              }
            });
          }
        }
      }
    });
    return;
  }

  handleBid(data) {
    this.winningBid = this.engineService.winningBid;
    var msg = '';
    if (this.bidSelected == 'winning' && data.ev.type === 'click' && typeof this.users[this.winningUser[data.row][data.col]] !== 'undefined') {
      Object.keys(this.finalistBids).forEach(user_uid => {
        let bid = Object.values(this.finalistBids[user_uid]);
        if (bid[data.row][data.col] && this.checkFinalist(user_uid, data.row, data.col)) {
          msg += "\n" + this.users[user_uid].name + ': ' + bid[data.row][data.col] + '$M';
        }
      });
      if (msg) {
        msg = "Finalist bids: \n" + msg;
        alert(msg)
      }
      return;
    }
  }

  isUserBids(challenge) {
    if (this.user && this.user.games) {
      if (Object.keys(this.user.games).indexOf(challenge['games'][0]) !== -1) {
        return true;
      } else {
        return false;
      }
    }
  }

  updatePlayStats(gamePk) {
    this.http.get(environment[this.sport + '_legacy_api'] + 'summary?event=' + gamePk)
      .subscribe((data: any) => {
        if ((data.scoringPlays && data.scoringPlays.length)) { // NFL
          const updateData: any = { currentDrive: '' };
          if (data.scoringPlays && data.scoringPlays.length) {
            data.scoringPlays.reverse()
            updateData.scoringPlays = data.scoringPlays;
          }
          if (!deepEqual(this.finalGameScoringPlays, data.scoringPlays)) {
            this.afs.doc('live/' + gamePk).update(updateData);
          }
        }
        else if (data.plays && data.plays.length) { // NBA
          const updateData: any = { currentDrive: '' };
          const plays = data.plays.filter(play => play.scoreValue > 0);
          plays.reverse();
          updateData.scoringPlays = plays;
          if (!deepEqual(this.finalGameScoringPlays, plays)) {
            this.afs.doc('live/' + gamePk).update(updateData);
          }
        }
      })
  }

  getGameGoals(gamePk) {

    this.afs.doc('live/' + gamePk).get()
      .subscribe((data: any) => {
        if (data.data().scoringPlays && data.data().scoringPlays.length) {
          this.finalGameScoringPlays = data.data().scoringPlays;
          let goals = this.finalGameScoringPlays.map(item => {
            return {
              row: this.engineService.calcWinScoreMiltiplied(item.awayScore, this.scoreMultiplier, this.scoreShift),
              col: this.engineService.calcWinScoreMiltiplied(item.homeScore, this.scoreMultiplier, this.scoreShift)
            }
          })
          this.goals = goals.filter((goal, index) => {
            return (!index || goals[index - 1].row !== goal.row || goals[index - 1].col !== goal.col)
          }).reverse();
        } else {
          this.updatePlayStats(gamePk);
        }

      })
  }

  getGoals(row, col) {
    if (this.goals) {
      if (JSON.stringify(this.goals).indexOf(JSON.stringify({ row, col })) !== -1) {
        const index = this.goals.findIndex(item => item.row == row && item.col == col);
        if (index > 0) {
          const prevElem = this.goals[index - 1];
          if (row > prevElem.row && col == prevElem.col) {
            return 'goal top'
          } else if (row == prevElem.row && col > prevElem.col) {
            return 'goal left'
          }
        } else {
          return 'goal'
        }
      }
    }
  }

  backButton() {
    this.platform.backButton.subscribe(() => {
      this.navCtrl.back({ animated: true });
    })
  }

  countdown(start) {
    let timer = setInterval(() => {
      let nowDate: any = new Date();
      let achiveDate: any = new Date(start['seconds'] * 1000);
      let result = (achiveDate - nowDate) + 1000;
      if (result < 0) {
        this.timer = '';
        clearInterval(timer);
      } else {
        let days = Math.floor(result / 1000 / 60 / 60 / 24);
        if (days < 1) {
          let seconds: any = Math.floor((result / 1000) % 60);
          let minutes: any = Math.floor((result / 1000 / 60) % 60);
          let hours: any = Math.floor((result / 1000 / 60 / 60) % 24);
          if (seconds < 10) seconds = '0' + seconds;
          if (minutes < 10) minutes = '0' + minutes;
          if (hours < 10) hours = '0' + hours;
          this.timer = hours + ':' + minutes + ':' + seconds;
        }
        else {
          this.timer = `${days} days`;
        }
      }
    }, 1000);
  }

  isPastGame(game) {
    return game && (game.status?.statusCode == '7' ||
      (new Date(game.gameTime).getTime()) - (new Date().getTime()) > 0);
  }

  loadLeague(league) {
    const lleague = league.toLowerCase();
    if (!Object.keys(this.leagues[lleague]).length) {
      this.afs.collection(`${lleague}-team`, ref => ref
        .where('isActive', '==', true)).get().subscribe(snapshot => {
          snapshot.docs.forEach(doc => {
            this.leagues[lleague][doc.data()['oid'] || doc.id] = doc.data();
          })
        });
    }
  }

  loadLeagueStandings(league) {
    this.afs.collection(league + '-stats').valueChanges({ idField: 'id' }).subscribe(standings => {
      this.standings[league] = {};
      standings.forEach(stat => {
        this.standings[league][stat.id] = stat;
        delete this.standings[league][stat.id].id;
      });
      console.log(this.standings);
      
    });
  }

  clickGame(event, game) { 
    if (game?.status && !['trophy-box', 'ico-trophy'].includes(event.target.classList?.value) &&
        !['trophy-box', 'ico-trophy'].includes(event.target.parentNode?.classList?.value)) {
      switch (isNaN(parseInt(game?.status.statusCode)) || parseInt(game?.status.statusCode)) {
        case true: // not a number
          break;
        case 1: // Scheduled
          if (this.isPastGame(game)) { // No Bids allowed
            // this.router.navigate([`/bid/0/${game.gamePk}/${this.gameChallengeMap[game.gamePk] || 0}`]);
            this.router.navigate([( ((!game||game?.status.statusCode==1||game?.status.statusCode==2) && (!this.teams2[game.gamePk] || !this.teams2[game.gamePk][0].locked) ) ? '/detail/':'/bid/' ) +  ( (!this.userBids || !this.userBids[game.gamePk]) ? '0': this.userBids[game.gamePk][this.user.uid] ) +'/'+ game.gamePk+'/'+this.challenge_id])
          }
          else { //
            this.gameSelected[this.league] = { ...{ sport: 'nhl' }, ...game };
            this.gamePkSelected[this.league] = game.gamePk;
            this.resetActiveGame(true);
          }
          break;
        case 2: // Pre-Game
        case 3: // 
        case 4: // 
        case 5: // 
        case 6: // Pre-Final
        case 7: // Final
        default:
          // this.router.navigate([`/bid/0/${game.gamePk}/${this.gameChallengeMap[game.gamePk] || 0}`]);
          this.router.navigate([( ((!game||game?.status.statusCode==1||game?.status.statusCode==2) && (!this.teams2[game.gamePk] || !this.teams2[game.gamePk][0].locked) ) ? '/detail/':'/bid/' ) +  ( (!this.userBids || !this.userBids[game.gamePk]) ? '0': this.userBids[game.gamePk][this.user.uid] ) +'/'+ game.gamePk+'/'+this.challenge_id])
      }
    }
  }

  resetActiveGame(resetSlides?) {
    if (this.activeGame) {
      this.activeGame.showGameInfo = false;
      this.activeGame.showMoreInfoAway = false;
      this.activeGame.resetCountdown();
      this.activeGame.resetBid();
      this.resetSlides = resetSlides;
    }
  }

  showTeamStats(val) {
  }

  isBidPlaced(gamePk) {
    return this.user && this.user.games &&
      this.user.games[gamePk];
  }

  countBids(gamePk) {
    return this.isBidPlaced(gamePk) ? 1 : 0;
  }

  async shareInviteGame(link) {
    const modal = await this.modalController.create({
      component: PopupShareComponent,
      componentProps: {
        searchService: this.searchService,
        afs: this.afs,
        link: link
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-default',
    });
    await modal.present();
  }
}
