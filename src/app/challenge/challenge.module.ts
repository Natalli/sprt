import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChallengePage } from './challenge.page';
import { UserModule} from '../components/user/user.module';
import { SnapchatModule } from '../snapchat/snapchat.module';
import { DirectivesModule } from '../directives/directives.module';
import { ClickDirective } from '../directives/click/click.directive';
import { SearchServiceModule } from '../services/search/search.module';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';
import { GameModule } from '../components/game/game.module';
import { IcoSvgModule } from '../components/ico-svg/ico-svg.module';
import { GridModule } from '../components/grid/grid.module';
import { DateModule } from '../components/date/date.module';

const routes: Routes = [
  {
    path: '',
    component: ChallengePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    UserModule,
    SnapchatModule,
    DirectivesModule,
    SearchServiceModule,
    HeaderModule,
    SubHeaderModule,
    GameModule,
    GridModule,
    DateModule,
    IcoSvgModule
  ],
  declarations: [ChallengePage, ClickDirective],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ChallengePageModule {}
