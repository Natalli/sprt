import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-default',
  templateUrl: './modal-default.component.html',
  styleUrls: ['./modal-default.component.scss'],
})
export class ModalDefaultComponent implements OnInit {
  title;
  popoverCtrl;

  constructor(
  ) { }

  ngOnInit() { }

  check(ev) {
    this.popoverCtrl.dismiss(ev === 'yes');
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
