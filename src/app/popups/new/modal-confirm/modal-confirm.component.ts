import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss'],
})
export class ModalConfirmComponent implements OnInit {
  text;
  btnName;
  btnNameConfirm;
  submit;
  submitConfirm;
  user;
  myapp;
  isDisabledSubmit;
  invite_id;
  game;
  afs;
  bid;
  sendNotification;
  notificationLabel;
  invite;
  getOpenGame;
  grid;
  loadingController;
  nextGameBids;
  sendBidEmail;
  router;

  // friends page
  
  confirmDeletePopup;
  confirmDeleteBtnName;
  deleteFriend;
  saveData;
  index;
  uid;
  nameArray;
  id;
  userData;
  friends;
  myUid;
  loading;
  invitations;
  pendingFriends;
  blockedFriends;
  
  popup;
  dismissNotification;

  ProfileForm;
  url;
  cancelEvent;
  userService;

  constructor(
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverCtrl.dismiss();
  }
}
