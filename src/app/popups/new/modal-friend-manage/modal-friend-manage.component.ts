import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonSearchbar, ModalController, PopoverController } from '@ionic/angular';
import { Constants } from 'src/app/constants';
import { UserService } from 'src/app/service/user.service';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-modal-friend-manage',
  templateUrl: './modal-friend-manage.component.html',
  styleUrls: ['./modal-friend-manage.component.scss'],
})
export class ModalFriendManageComponent implements OnInit {

  @ViewChild(IonSearchbar) searchbar: IonSearchbar;
  @Input() checkFriendQ: boolean;
  @ViewChild('focusInput') focusInput;
  user: any;

  afs: any;
  chat: any;
  users = {};
  search;
  searchUsers = {};

  addFriend;
  friends;
  saveData;
  loading;
  loadingController;
  selfUser;
  popoverCtrl;
  checkFriend;
  constants = Constants;
  objectKeys = Object.keys;
 
  searchService;

  constructor(
    
    public modalController: ModalController,
    public alertController: AlertController,
    protected layoutService: LayoutService,
    private userService: UserService) { }

  ngOnInit() {
    this.user = this.userService.user;
  }

  ionViewDidEnter() {
    this.focusInput.setFocus();
  }

  searchUser(ev) {
    this.search = (ev.target.value.length > 1) ? ev.target.value : '';
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.searchService.searchUser(this.search).subscribe(data => {
        this.searchUsers = this.filterUsers(data, ({ users: [this.user.uid] }).users);
        this.friends.forEach(friend => {
          let a = Object.values(this.searchUsers).map(user => {
            user['id'] === friend.uid ? user : null
          })
        })
      })
    } else {
      this.searchUsers = {}
    }
  }

  filterUsers(users, excluded) {
    if (users && excluded) {
      return users.filter(user => excluded.indexOf(user.id) === -1);
    }
    return users;
  }

  isFriend(uid) {
    return this.friends.find(user => user.uid === uid);
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
