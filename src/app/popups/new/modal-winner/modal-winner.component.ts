import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-winner',
  templateUrl: './modal-winner.component.html',
  styleUrls: ['./modal-winner.component.scss'],
})
export class ModalWinnerComponent implements OnInit {

  @Input() winner: { name: string, color: string };
  url: string;
  title;
  text;
  isWinner;

  constructor(
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
