import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-user-games',
  templateUrl: './modal-user-games.component.html',
  styleUrls: ['./modal-user-games.component.scss'],
})
export class ModalUserGamesComponent implements OnInit {

  name;
  challengeGames;
  challenge_id;
  userId;
  users;

  constructor(
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
