import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/compat/storage';
import { PopoverController } from '@ionic/angular';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { BehaviorSubject, finalize, Observable, tap } from 'rxjs';
import { Constants } from 'src/app/constants';
import { ModalChatManageComponent } from '../modal-chat-manage/modal-chat-manage.component';
import { ModalConfirmComponent } from '../modal-confirm/modal-confirm.component';
import { ModalCropComponent } from '../modal-crop/modal-crop.component';

@Component({
  selector: 'app-modal-chat-create',
  templateUrl: './modal-chat-create.component.html',
  styleUrls: ['./modal-chat-create.component.scss'],
})
export class ModalChatCreateComponent implements OnInit {

  afs;
  messageRefs;
  globalManage;
  myUid;
  searchService;

  chatId;
  user;
  modalController;
  groupName;


  chatUsers;
  constants = Constants;
  isShowProgress: boolean = false;
  ShowProgressEmitter = new BehaviorSubject<boolean>(this.isShowProgress);

  url: string = '';
  UrlEmitter = new BehaviorSubject<string>(this.url);

  imageChangedEvent: any = '';
  croppedImage: any = '';
  file: string;
  isCropFailed: boolean;
  percentage: Observable<number>;
  percent: number = 0;
  PercentageEmitter = new BehaviorSubject<number>(this.percent);

  task: AngularFireUploadTask;
  snapshot: Observable<any>;
  snap: any = {};
  SnapshotEmitter = new BehaviorSubject<any>(this.snap);
  popup;
  changedValue: boolean;
  chat;

  constructor(
    public popoverCtrl: PopoverController,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.url = this.chat && this.chat.groupPhoto || Constants.GROUP_AVATAR_DEMO;
    this.groupName = this.chat && this.chat.groupName || 'New group';
    this.UrlEmitter.next(this.url);
    this.ShowProgressEmitter.next(this.isShowProgress = false);
  }

  async addAvatar(event) {
    const fileType = event.target.files[0].type;
    if (fileType === 'image/png' ||
      fileType === 'image/jpg' ||
      fileType === 'image/jpeg' ||
      fileType === 'image/gif') {
      this.isCropFailed = false;
      this.imageChangedEvent = event;
      this.file = event.target.files[0];
      this.popup = await this.popoverCtrl.create({
        component: ModalCropComponent,
        componentProps: {
          imageChangedEvent: this.imageChangedEvent,
          imageCropped: this.imageCropped,
          imageLoaded: this.imageLoaded,
          loadImageFailed: this.loadImageFailed,
          confirmCropCancel: this.confirmCropCancel,
          imageCrop: this.imageCrop,
          file: this.file,
          cancelCrop: this.cancelCrop,
          popoverCtrl: this.popoverCtrl,
          storage: this.storage,
          user: this.user,
          croppedImage: this.croppedImage,
          task: this.task,
          percentage: this.percentage,
          snapshot: this.snapshot,
          url: this.url,
          changedValue: this.changedValue,
          isShowProgress: this.isShowProgress,
          ShowProgressEmitter: this.ShowProgressEmitter,
          UrlEmitter: this.UrlEmitter,
          PercentageEmitter: this.PercentageEmitter,
          SnapshotEmitter: this.SnapshotEmitter,
          chatId: this.chatId
        },
        cssClass: 'modal-wrap-default',
        id: "cropPopup"
      });
      return await this.popup.present();
    } else {
      this.isCropFailed = true;
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  async imageLoaded() {
    this.isCropFailed = false;
    // show cropper
  }

  loadImageFailed() {
    // img failed
    this.isCropFailed = true;
  }

  async imageCrop() {
    try {
      const imgBase = this.croppedImage.split(',')[1];
      this.changedValue = true;
      const path = `images/${this.chatId}/${this.file['name']}`;
      const fireRef = this.storage.ref(path);
      if (this.croppedImage) {
        this.task = fireRef.putString(imgBase, 'base64', { contentType: 'image/jpg' });
        this.task.then(resp => {
          if (resp.state === 'success') {
            this.popoverCtrl.dismiss();
            this.ShowProgressEmitter.next(this.isShowProgress = true);
          }
        }).catch(err => {
          console.log(err.message)
        })
        this.percentage = this.task.percentageChanges();
        this.percentage.subscribe(item => {
          this.PercentageEmitter.next(item);
        });

        this.snapshot = this.task.snapshotChanges().pipe(
          tap(console.log),
          // The file's download URL
          finalize(async () => {
            await this.storage.ref(path).getDownloadURL()
              .subscribe(resp => {
                this.url = resp;
                this.UrlEmitter.next(resp);
                setTimeout(() => {
                  this.ShowProgressEmitter.next(this.isShowProgress = false);
                }, 1000)

              }, error => {
                console.log(error, 'ERROR')
              });
          }),
        );
        this.snapshot.subscribe(snap => {
          this.SnapshotEmitter.next(snap);
        })
      }
    } catch (err) {
      alert(err)
    }
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  async cancelCrop() {
    let popupCrop = document.getElementById('cropPopup') as any;
    popupCrop.dismiss();
  }

  async confirmCropCancel(ev) {
    const popover = await this.popoverCtrl.create({
      component: ModalConfirmComponent,
      event: ev,
      componentProps: {
        text: 'Are you sure you want to cancel editing?',
        submitConfirm: this.cancelCrop,
        popup: this.popup,
        btnNameConfirm: 'Ok'
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  async createGroup() {
    if (this.chatId) {
      const url = this.UrlEmitter.getValue();
      if (this.groupName != this.chat['groupName'] || url != this.chat['groupPhoto']) {
        this.afs.collection('/user/' + this.myUid + '/chat').doc(this.chatId).get().subscribe(item => {
          if (!item.exists) {
            item.ref.set({ groupName: this.groupName, groupPhoto: url })
          } else {
            item.ref.update({ groupName: this.groupName, groupPhoto: url })
          }
        })
        this.chatUsers.forEach(item => {
          this.afs.collection('/user/' + item + '/chat').doc(this.chatId).get().subscribe(item => {
            if (!item.exists) {
              item.ref.set({ groupName: this.groupName, groupPhoto: url })
            } else {
              item.ref.update({ groupName: this.groupName, groupPhoto: url })
            }
          })
        })
        this.popoverCtrl.dismiss();
      }
    } else {
      this.afs.collection('/chat').add(
        { users: [this.myUid] }
      )
        .then((docRef) => {
          this.afs.collection('/user/' + this.myUid + '/chat').doc(docRef.id).set({
            groupName: this.groupName,
            groupPhoto: this.UrlEmitter.getValue(),
            private: false,
            admin: true,
            dateJoined: new Date(Date.now())
          });
          this.chatId = docRef.id;
          this.popoverCtrl.dismiss();
          this.manageChat(this.chatId, this.groupName, this.UrlEmitter.getValue())
        });
    }
  }

  async manageChat(chatId, groupName, groupPhoto) {
    // console.log(chatId, 'chat id');
    // console.log(groupName, 'name')
    // console.log(groupPhoto)
    const popover = await this.popoverCtrl.create({
      component: ModalChatManageComponent,
      componentProps: {
        chatId,
        modalController: this.modalController,
        afs: this.afs,
        messageRefs: this.messageRefs,
        globalManage: this.globalManage,
        myUid: this.myUid,
        groupName, groupPhoto,
        searchService: this.searchService
      },
      // swipeToClose: true,
      // showBackdrop: true,
      // backdropDismiss: true,
      cssClass: 'modal-wrap-default',
    });
    await popover.present();
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
