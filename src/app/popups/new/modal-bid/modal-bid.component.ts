import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';

@Component({
  selector: 'app-modal-bid',
  templateUrl: './modal-bid.component.html',
  styleUrls: ['./modal-bid.component.scss'],
})
export class ModalBidComponent implements OnInit {

  page;
  console = console;
  amount;
  items;
  resetBid;
  game;
  grid;
  touchedBid;
  selected;
  applyBid;
  user;
  afs;
  mode = 'custom';
  objectKeys = Object.keys;
  templates;
  constructor(
    public popoverCtrl: PopoverController,
    private navParams: NavParams
  ) { }

  ngOnInit() { 
    this.page = this.navParams.get('data');
  }

  segmentChanged(event) {
    this.mode = event.detail.value;
  }

  eventFromPopover() {
    this.popoverCtrl.dismiss();
  }

  deleteFavorite(template) {
    if ( this.templates[template] && this.game.sport ) {
      this.afs.doc('/user/' + this.user.uid + '/template/' + this.game.sport).update({[template]: firebase.firestore.FieldValue.delete()})
      .then(ret => {
        delete this.templates[template];
      })
    }
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
