import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-error',
  templateUrl: './modal-error.component.html',
  styleUrls: ['./modal-error.component.scss'],
})
export class ModalErrorComponent implements OnInit {
  title;
  text;
  cssClass;
  
  constructor(
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverCtrl.dismiss();
  }
}
