import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-crop',
  templateUrl: './modal-crop.component.html',
  styleUrls: ['./modal-crop.component.scss'],
})
export class ModalCropComponent implements OnInit {
  imageChangedEvent;
  imageCropped;
  imageLoaded;
  loadImageFailed;
  confirmCropCancel;
  imageCrop;
  cancelCrop;
  constructor(
    public popoverController: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverController.dismiss();
  }
}
