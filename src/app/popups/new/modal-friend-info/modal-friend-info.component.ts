import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-friend-info',
  templateUrl: './modal-friend-info.component.html',
  styleUrls: ['./modal-friend-info.component.scss'],
})
export class ModalFriendInfoComponent implements OnInit {


  constructor(
    public popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
