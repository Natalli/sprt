import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalService } from 'src/app/service/modal.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-modal-batch-nba',
  templateUrl: './modal-batch-nba.component.html',
  styleUrls: ['./modal-batch-nba.component.scss'],
})
export class ModalBatchNbaComponent implements OnInit {

  start;
  max;
  afs;
  toastController;

  constructor(
    public popoverCtrl: PopoverController,
    private datePipe: DatePipe,
    protected modalService: ModalService
  ) { }

  ngOnInit() { }

  create(startOnMonday?) {
    let weekday = new Array(5);
    weekday[0] = "Monday";
    weekday[1] = "Tuesday";
    weekday[2] = "Wednesday";
    weekday[3] = "Thursday";
    weekday[4] = "Friday";
    // let ch = []
    weekday.forEach((day, i) => {
      let date = new Date();
      if (startOnMonday) {
        date.setDate(date.getDate() + (1 + 7 - date.getDay()) % 7);
      } else {
        date = new Date(this.start);
      }
      date.setUTCHours(10, 0, 0);
      date.setDate(date.getDate() + i)
      let startDate = new Date(new Date(new Date(date.setUTCHours(10, 0, 0)).getTime() + (480 * 60 * 1000)).toISOString());
      let challenge = {
        id: (this.max + i + 1).toString(),
        name: day + ' NBA Challenge',
        amount: 200,
        games: [],
        sport: 'nba',
        text: '$200 Prize!',
        disabled: true,
        start: startDate
      }
      // ch.push(challenge)

      this.afs.collection('challenge').doc(challenge['id']).set(challenge);
      this.modalService.confirmModal('Success', 'NBA Batch was created successfully');
    })
  }

  selectDate(value) {
    this.start = this.datePipe.transform(value, 'yyyy-MM-dd');
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
