import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Constants } from 'src/app/constants';
import firebase from 'firebase/compat/app';
import { UserService } from 'src/app/service/user.service';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-modal-team',
  templateUrl: './modal-team.component.html',
  styleUrls: ['./modal-team.component.scss'],
})
export class ModalTeamComponent implements OnInit {
  
  users;
  team;
  afs;
  gamePk;
  team_id;
  toastController;
  Constants = Constants;

  constructor(
    public popoverCtrl: PopoverController,
    protected modalService: ModalService
  ) { }

  ngOnInit() { }

  deleteUser(uid) {
    this.afs.collection("team/").doc(String(this.gamePk))
    .update({ [`${this.team_id}.users`]: firebase.firestore.FieldValue.arrayRemove(uid) }).then(() => {
      this.team = this.team.filter(u => u !== uid);
      this.modalService.confirmModal('Confirm', 'The user has been removed from the team');
    });
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
