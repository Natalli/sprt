import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-game-progress',
  templateUrl: './modal-game-progress.component.html',
  styleUrls: ['./modal-game-progress.component.scss'],
})
export class ModalGameProgressComponent implements OnInit {
  bidsGame;
  challengeGames;
  game_id;
  next_game_id;
  router;
  location;
  nextGame;
  cancelNavigate;
  
  constructor(
    public popoverController: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverController.dismiss();
  }
}
