import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-modal-update-app',
  templateUrl: './modal-update-app.component.html',
  styleUrls: ['./modal-update-app.component.scss'],
})
export class ModalUpdateAppComponent implements OnInit {
  
  text;
  platform;
  
  constructor(
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  cancel() {
    this.popoverCtrl.dismiss();
  }
}
