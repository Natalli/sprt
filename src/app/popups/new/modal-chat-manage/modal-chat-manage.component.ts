import { Component, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AlertController, IonIcon, IonSearchbar, LoadingController, ModalController, Platform, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Constants } from 'src/app/constants';
import { ModalService } from 'src/app/service/modal.service';
import { UserService } from 'src/app/service/user.service';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-modal-chat-manage',
  templateUrl: './modal-chat-manage.component.html',
  styleUrls: ['./modal-chat-manage.component.scss'],
})
export class ModalChatManageComponent implements OnInit {

  @ViewChild(IonSearchbar) searchbar: IonSearchbar;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;

  @Input() checkFriendQ: boolean;
  @Input() globalManage: boolean;
  @ViewChild('focusInput') focusInput;
  private subscr: Subscription;

  user: any;
  chatRef: any;
  recepient: any;
  afs: any;
  chatId: any;
  messages: any[];
  chat: any;
  users = {};
  search;
  searchUsers = {};
  messageRefs;
  addFriend;
  friends;
  saveData;
  checkFriend;
  constants = Constants;
  objectKeys = Object.keys;
  myUid: string;
  searchFriendsUsers: any;
  isAdmin;
  manageChat: any;
  groupName;
  groupPhoto;
  privateChat: boolean;
  checkChat: boolean;
  openPrivateChat;
  createGroup;
  searchService;

  constructor(
    public popoverCtrl: PopoverController,
    public modalController: ModalController,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private platform: Platform,
    protected layoutService: LayoutService,
    private userService: UserService,
    protected modalService: ModalService
  ) { }

  ngOnInit() {
    this.user = this.userService.user;
    if (this.user) {
      this.myUid = this.user.uid
      if (this.chatId) {
        this.initChat();
        this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId).get().subscribe(data => {
          this.manageChat = data.data();
        })
      }
      this.afs.doc('/user/' + this.myUid).get().subscribe(response => {
        this.friends = [];
        response.data().friends.forEach(friend => {
          this.friends[friend.uid] = friend;
        })
        this.afs.collection("/user/").valueChanges({ idField: 'id' }).subscribe(data => {
          const userFriends = [];
          response.data().friends.forEach(friend => {
            const userFriend = data.find(user => user.id === friend.uid ? user : null);
            userFriends.push(userFriend);
            this.searchFriendsUsers = userFriends;
            this.searchUsers = userFriends;
          })
        })
      });
    }

    if (this.checkFriendQ) {
      return true
    } else {
      return false
    }
  }

  ionViewDidEnter() {
    if (this.globalManage || this.manageChat.admin) {
      this.focusInput.setFocus();
    }
  }

  initChat() {
    this.afs.collection('/chat/').doc(this.chatId).valueChanges({ idField: 'id' }).subscribe((chat: object) => {
      this.chat = chat;
      this.users = {};
      if (this.chat && this.chat.users) {
        this.chat.users.forEach(user_uid => {
          this.afs.doc('/user/' + user_uid).valueChanges().subscribe(user => {
            this.users[user_uid] = user;
            this.users[user_uid].uid = user_uid;
          });
        });
      }
    });
    this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId).valueChanges().subscribe(data => {
      this.manageChat = data;
    })
  }

  async deleteChatUser(user_uid: any) {
    const alert = await this.alertController.create({
      header: 'Confirm',
      message: 'Remove user ' + (this.users[user_uid].name || '') + ' from this Chat?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Delete',
          cssClass: 'danger',
          handler: () => {
            if (this.chat && this.chat.users) {
              const user_index = this.chat.users.indexOf(user_uid);
              if (user_index !== -1) {
                this.chat.users.splice(user_index, 1);
                if (this.chat.users.length < 1) {
                  this.confirmChatRemove(user_uid);
                } else {
                  this.afs.collection('/chat/').doc(this.chatId).set({ users: this.chat.users });
                  this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).delete();
                  this.modalController.dismiss();
                  let popup = document.getElementById('popupMessages') as any;
                  if (popup) {
                    popup.dismiss();
                  }
                }
              }
            } else {
              this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).delete();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  async confirmChatRemove(user_uid) {
    const alert = await this.alertController.create({
      header: 'Confirm delete chat',
      message: 'Are you sure you want to delete this chat?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Delete',
          cssClass: 'danger',
          handler: () => {

            // this.afs.collection('/chat/').doc(this.chatId).update({ users: this.chat.users });
            // this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).delete();
            this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId).delete();
            this.afs.collection('/chat/').doc(this.chatId).delete();
            this.alertController.dismiss();
            this.modalController.dismiss();
            let popup = document.getElementById('popupMessages') as any;
            if (popup) {
              popup.dismiss();
            }
          }
        }
      ]
    });
    await alert.present();
  }

  searchUser(ev) {
    this.search = (ev.target.value.length > 1) ? ev.target.value : '';
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.searchService.searchUser(this.search).subscribe(data => {
        this.searchUsers = this.filterUsers(data, (this.chat || { users: [this.user.uid] }).users);
        this.friends.forEach(friend => {
          let a = Object.values(this.searchUsers).map(user => {
            user['id'] === friend.uid ? user : null
          })
        })
      })
    } else {
      if (this.friends.length) {
        this.searchUsers = this.searchFriendsUsers;
      } else {
        this.searchUsers = {}
      }

    }
  }

  filterUsers(users, excluded) {
    if (users && excluded) {
      return users.filter(user => excluded.indexOf(user.id) === -1);
    }
    return users;
  }

  checkPrivateChat(user_uid) {
    if (this.privateChat) {
      const userExist = this.messageRefs.find(item => item.private === user_uid ? user_uid : null);
      if (userExist) {
        this.checkChat = true;
      } else {
        this.checkChat = false;
      }
    }
  }

  ngOnDestroy(): void {
    if (typeof this.subscr !== 'undefined') {
      this.subscr.unsubscribe();
    }
  }

  async addChatUser(user_uid: any) {
    if (!this.chatId) {
      if (this.privateChat) {
        this.subscr = this.afs.collection('/user/' + user_uid + '/chat', ref => ref
          .where('private', '==', this.myUid)).valueChanges({ idField: 'id' }).subscribe(data => {
            if (data.length) {
              this.afs.collection('/chat/').doc(data[0].id).update({ users: [this.myUid, user_uid] });
              this.afs.collection('/user/' + this.myUid + '/chat').doc(data[0].id).set({
                private: user_uid
              });
            } else {
              this.afs.collection('/chat').add(
                { users: [this.myUid, user_uid] }
              )
                .then((docRef) => {
                  this.afs.collection('/user/' + this.myUid + '/chat').doc(docRef.id).set({ private: user_uid });
                  this.afs.collection('/user/' + user_uid + '/chat').doc(docRef.id).set({ private: this.myUid });
                  this.chatId = docRef.id;
                  this.initChat();
                  const notification = {
                    title: "Invitation to chat",
                    privateChat: true,
                    text: "has invited you to private chat",
                    platforms: ['ios', 'android', 'web'],
                    start: new Date(),
                    scheduling: 'Now',
                    receivers: [user_uid],
                    created: new Date(),
                    chatRequest: true,
                    sendRequest: true,
                    chatId: docRef.id,
                    sender: this.user.uid
                  };
                  this.afs.collection('/notification/').add(notification);
                  this.modalService.presentLoading('You sent an invitation to chat');
                  this.modalController.dismiss();
                });
            }
          });
      } else {
        // if (this.friends[user_uid]) {
        //   this.afs.collection('/chat').add(
        //     { users: [this.myUid, user_uid] }
        //   )
        //     .then((docRef) => {
        //       this.afs.collection('/user/' + this.user.uid + '/chat').doc(docRef.id).set({ private: false, admin: true, dateJoined: new Date(Date.now()) });
        //       this.afs.collection('/user/' + user_uid + '/chat').doc(docRef.id).set({ private: false, dateJoined: new Date(Date.now()) });
        //       this.afs.collection('/chat/' + docRef.id + '/message', ref => ref.orderBy('created')).add({
        //         created: new Date(Date.now()),
        //         sender: this.user.uid,
        //         joined: true
        //       });
        //       this.chatId = docRef.id;
        //       this.searchUsers = this.searchFriendsUsers;
        //       this.search = '';
        //       this.initChat();
        //     });
        //   return;
        // } else {
        // console.log('NO CHAT - group')
        this.afs.collection('/chat').add(
          { users: [this.myUid] }
        )
          .then((docRef) => {
            this.afs.collection('/user/' + this.myUid + '/chat').doc(docRef.id).set({ private: false, admin: true, sentRequest: [] });
            this.chatId = docRef.id;
            this.searchUsers = this.searchFriendsUsers;
            this.search = '';
            this.initChat();
            const notification = {
              title: "Invitation to new Group",
              text: "has invited you to join a Group",
              platforms: ['ios', 'android', 'web'],
              start: new Date(),
              scheduling: 'Now',
              receivers: [user_uid],
              created: new Date(),
              chatRequest: true,
              sendRequest: true,
              chatId: docRef.id,
              sender: this.user.uid
            };
            this.afs.collection('/notification/').add(notification);
            this.modalService.presentLoading('You sent an invitation to chat');
          })
        return;
      }
    } else {
      // console.log('CHAT')
      const message = this.messageRefs.find(item => item.id === this.chatId);
      // if (this.friends[user_uid]) { // add friend in chat
      //   console.log('CHAT add friend')
      //   this.chat.users.push(user_uid);
      //   if (!message || message.private) { // create a new public chat from private one
      //     console.log('!message chat (new chat)')
      //     this.afs.collection('/chat').add(
      //       { users: this.chat.users }
      //     )
      //       .then((docRef) => {
      //         this.chat.users.forEach((uid: string) => {
      //           this.afs.collection('/user/' + uid + '/chat').doc(docRef.id).set({
      //             private: false,
      //             groupName: this.groupName || message.groupName,
      //             groupPhoto: this.groupPhoto || message.groupPhoto
      //           });
      //         });
      //         this.searchUsers = this.searchFriendsUsers;
      //         this.search = '';
      //       });
      //   } else { // add user to an existing public chat
      //     console.log(message.groupName, message.groupPhoto)
      //     this.afs.doc('/chat/' + this.chatId).update({ users: this.chat.users });
      //     this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).set({
      //       private: false,
      //       groupName: this.groupName || message.groupName,
      //       groupPhoto: this.groupPhoto || message.groupPhoto,
      //       dateJoined: new Date(Date.now())
      //     });
      //     this.afs.collection('/chat/' + this.chatId + '/message', ref => ref.orderBy('created')).add({
      //       created: new Date(Date.now()),
      //       sender: this.user.uid,
      //       joined: true
      //     });
      //     this.searchUsers = this.searchFriendsUsers;
      //     this.search = '';
      //   }
      // } else {
      const notification = {
        title: "Invite to New Group",
        text: "has invited you to a new Group",
        platforms: ['ios', 'android', 'web'],
        start: new Date(),
        scheduling: 'Now',
        receivers: [user_uid],
        created: new Date(),
        chatRequest: true,
        chatId: this.chatId,
        sender: this.user.uid,
        sendRequest: true,
        groupName: this.groupName || message.groupName,
        groupPhoto: this.groupPhoto || message.groupPhoto
      };
      this.afs.collection('/notification/').add(notification);
      this.modalService.presentLoading('You sent an invitation to chat');
      // }
    }
  }

  capitalize(text) {
    return text.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
  }

  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      this.modalController.dismiss();
    })
  }

  async addAdmin(user_uid, admin) {
    if (admin) {
      this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).update({ admin: false });
      const alert = await this.alertController.create({
        header: 'Remove admin',
        message: 'You removed ' + this.users[user_uid].name + ' is not an admin',
      });
      await alert.present();
    } else {
      this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).update({ admin: true });
      const alert = await this.alertController.create({
        header: 'Add admin',
        message: 'You added ' + this.users[user_uid].name + ' to admins',
      });
      await alert.present();
    }

  }

  adminDetect(user_uid) {
    this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).valueChanges().subscribe(data => {
      // return data.admin
      if (data.admin) {
        return true
      } else {
        return false
      }
    });
  }

  async loading(text) {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: text,
      translucent: true
    });
    return await loading.present();
  }

  openChat(ev, user_uid) {
    const chat = this.messageRefs.find(item => item.private === user_uid ? item : null);
    this.openPrivateChat(ev, user_uid, chat.id, chat);
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
