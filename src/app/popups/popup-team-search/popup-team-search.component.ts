import { Component, OnInit, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { ModalController, AlertController, IonSearchbar, Platform, IonIcon } from '@ionic/angular';
import { Router } from '@angular/router';

import { Constants } from 'src/app/constants';
import { LayoutService } from 'src/app/services/layout.service';
import { UsersService } from 'src/app/service/users.service';
import { SearchService } from 'src/app/services/search/search.service';

@Component({
  selector: 'app-popup-team-search',
  templateUrl: './popup-team-search.component.html',
  styleUrls: ['./popup-team-search.component.scss'],
})
export class PopupTeamSearchComponent implements OnInit {

  @ViewChild(IonSearchbar) searchbar: IonSearchbar;
  @ViewChild('focusInput') focusInput;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  user: any;
  afs: any;
  users = {};
  teams = {};
  search;
  popoverCtrl;
  objectKeys = Object.keys;
  eventCollapsed = [];
  teamCount = 10;
  team_id;
  searchUsers = {};
  friends;
  teamUsers;
  searchFriendsUsers: any;
  constants = Constants;
  constructor(
    private router: Router,
    public modalController: ModalController,
    public alertController: AlertController,
    private platform: Platform,
    public usersService: UsersService,
    protected searchService: SearchService,
    protected layoutService: LayoutService) {
    this.backButtonEvent();
  }

  ngOnInit() {
    if (this.user && this.user.friends && this.user.friends.length) {
      this.friends = [];
      this.user.friends.forEach(friend => {
        this.friends[friend.uid] = friend;
      })
    }

    Object.keys(this.teams).forEach((team_id) => {
      this.usersService.load(this.teams[team_id].users);
    });
    let teamUsers = [];
    Object.keys(this.teams).forEach(team_id => {
      if (this.teams[team_id].users && this.teams[team_id].users.length) {
        this.teams[team_id].users.forEach(user_uid => {
          teamUsers.push(user_uid);
        });
      }
    });

    this.afs.collection("/user/").valueChanges({ idField: 'id' }).subscribe(data => {
      const userFriends = [];
      this.teamUsers = [];
      this.teamUsers = data.filter(d => teamUsers.find(u => u === d.id ? d : null))
      this.searchUsers = this.teamUsers.filter(d => this.user.friends.find(f => f.uid === d.id ? d : null))
      this.searchFriendsUsers = this.searchUsers;
    })
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  ionViewDidEnter() {
    this.focusInput.setFocus();
  }

  searchUser(ev) {
    this.search = (ev.target.value.length > 1) ? ev.target.value.toLowerCase() : '';
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.searchUsers = this.teamUsers.filter(user => user.keywords && user.keywords.includes(this.search) ? user : null);
    } else {
      if (this.user.friends.length) {
        this.searchUsers = this.searchFriendsUsers;
      } else {
        this.searchUsers = {}
      }
    }
  }

  switchTeam(user) {
    let team;
    Object.keys(this.teams).forEach(team_id => {
      this.teams[team_id].users.find(user_uid => user === user_uid ? team = team_id : team = null);
      if (team && this.team_id !== team) {
        this.router.navigate([this.router.url.replace('bid/' + this.team_id, 'bid/' + team)], { replaceUrl: true, state: { user_uid: user } });
        this.modalController.dismiss();
      } else {
        this.modalController.dismiss(user);
      }
    })
  }

  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      this.modalController.dismiss();
    })
  }
}
