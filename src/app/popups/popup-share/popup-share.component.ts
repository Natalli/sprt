import { Component, OnInit, ViewChild, Input, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { ModalController, AlertController, IonSearchbar, Platform, IonIcon, LoadingController, ToastController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

import { Constants } from 'src/app/constants';
import { LayoutService } from 'src/app/services/layout.service';
import { Share } from '@capacitor/share';
import { Capacitor } from '@capacitor/core';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { UserService } from 'src/app/service/user.service';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-popup-share',
  templateUrl: './popup-share.component.html',
  styleUrls: ['./popup-share.component.scss'],
})
export class PopupShareComponent implements OnInit {

  @ViewChild(IonSearchbar) searchbar: IonSearchbar;
  @Input() checkFriendQ: boolean;
  @ViewChild('focusInput') focusInput;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;

  user: any;
  afs: any;

  users = {};
  search;
  searchUsers = [];

  friends;
  saveData;
  checkFriend;
  constants = Constants;
  objectKeys = Object.keys;
  myUid: string;
  searchService;
  searchFriendsUsers: any;
  link;
  linkSharing;
  capacitor = Capacitor;

  constructor(
    private router: Router,
    public modalController: ModalController,
    public alertController: AlertController,
    private platform: Platform,
    protected layoutService: LayoutService,
    private fns: AngularFireFunctions,
    private userService: UserService,
    protected popoverCtrl: PopoverController,
    protected modalService: ModalService) {
    this.backButtonEvent();
  }

  ngOnInit() {
    this.user = this.userService.user;
    this.linkSharing = window.location.origin + this.link + '#' + this.user.uid;
    this.myUid = this.user.uid;
    this.afs.doc('/user/' + this.myUid).get().subscribe(response => {
      this.friends = [];
      response.data().friends.forEach(friend => {
        this.friends[friend.uid] = friend;
      })
      this.afs.collection("/user/").valueChanges({ idField: 'id' }).subscribe(data => {
        const userFriends = [];
        response.data().friends.forEach(friend => {
          const userFriend = data.find(user => user.id === friend.uid ? user : null);
          userFriends.push(userFriend);
          this.searchFriendsUsers = userFriends;
          this.searchUsers = userFriends;
        })
      })
    });
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  ionViewDidEnter() {
    this.focusInput.setFocus();
  }

  searchUser(ev) {
    this.search = (ev.target.value.length > 1) ? ev.target.value : '';
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.searchService.searchUser(this.search).subscribe(data => {
        this.searchUsers = data;
      })
    } else {
      this.searchUsers = [];
    }
  }

  openProfile(uid) {
    this.popoverCtrl.dismiss();
    this.modalController.dismiss();
    this.router.navigate(['/profile/' + uid]);
  }

  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      this.modalController.dismiss();
    })
  }

  isFriend(uid) {
    return Object.keys(this.friends).includes(uid);
  }

  send(uid) {
    let user = this.searchUsers.find(user => user.id === uid);
    const notification = {
      title: 'Link Sharing',
      text: "shared with you a link to the invite game.",
      platforms: ['ios', 'android', 'web'],
      start: new Date(),
      scheduling: 'Now',
      receivers: [uid],
      created: new Date(),
      friendRequest: false,
      sendRequest: true,
      sender: this.user.uid,
      linkSharing: this.link
    }
    // console.log(notification, 'nnn')
    this.afs.collection('/notification/').add(notification)
      .then(resp => {
        if (resp) {
          this.modalService.confirmModal('Success', 'Notification sent');
        }
      })
      .then(() => {
        if (user.tokens) {
          const callable = this.fns.httpsCallable('sendNativeNotification');
          Object.values(user.tokens).forEach(token => {
            callable({
              tokens: token,
              title: 'Link Sharing',
              body: this.user.displayName + " shared with you a link to the invite game. ",
              gotoUrl: (this.link + '/' + this.user.uid)
            })
              .toPromise().then((resp) => {
                //console.log(resp);
              });
          });
        }
      });
  }

  copyLink(event) {
    navigator.clipboard.writeText('https://sprtmtrx.com' + this.link + '/' + this.user.uid);
    this.modalService.confirmModal('Success', 'Link copied to clipboard');
    // event.clipboardData.setData("text/plain", span.textContent);
  }

  async share() {
    await Share.share({
      title: 'Share this',
      text: 'Share a link to a private game https://sprtmtrx.com' + this.link + '/' + this.user.uid,
      url: 'https://sprtmtrx.com' + this.link + '/' + this.user.uid,
      dialogTitle: 'Share with buddies'
    });
  }

}
