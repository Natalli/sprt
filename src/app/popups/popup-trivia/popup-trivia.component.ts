import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { UsersService } from 'src/app/service/users.service';
import { DatePipe } from '@angular/common';
import { Constants } from 'src/app/constants';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Capacitor } from '@capacitor/core';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-popup-trivia',
  templateUrl: './popup-trivia.component.html',
  styleUrls: ['./popup-trivia.component.scss'],
})

export class PopupTriviaComponent implements OnInit {
  time: string;
  timeProgress: number = 60;
  countdownProgress: number = 0;
  intervalTimer: any;
  intervalCountdown: any;
  timerEnabled = false;
  width;
  items: any[];
  teams: { id: string; }[] = [];
  teams2: { id: string; }[] = [];
  rowCount = 4;
  columnCount = 4;
  qs = [];
  modalController;
  selected: string;
  successCounter = 0;
  failCounter = 0;
  intervalStopwatch: any;
  stopwatchProgress: Date;
  stopwatchTime: Date;
  user: any;
  leaders: any;
  selectedTeams2: boolean;
  currentQId = 1;
  currentTime: any;
  currentBest: boolean;
  quote: { id: string; };
  sport: string;
  constants = Constants;
  segmentSelected: string = 'all';
  userLimit = 10;
  userFriends: string[] = [];
  lead: any;
  question: any;
  constructor(
    protected afs: AngularFirestore,
    public usersService: UsersService,
    protected datePipe: DatePipe,
    private nativeAudio: NativeAudio,
    private userService: UserService) {
    if (Capacitor && ['ios', 'android'].includes(Capacitor.getPlatform())) {
      //this.nativeAudio.preloadComplex('ping', 'assets/audio/ping.m4a', 0.3, 1, 0);
    }
  }

  ngOnInit() {
    this.loadData();
    this.loadUserData();
  }

  loadLeaders(qid) {
    this.afs.doc('/trivia/' + this.sport + '/users/' + qid).valueChanges().subscribe(users => {
      this.leaders = users;
      this.usersService.load(Object.keys(this.leaders || {})).then(users => {
        //console.log(users);
      });
    });
  }

  countdown() {
    clearInterval(this.intervalCountdown);
    clearInterval(this.intervalStopwatch);
    this.stopwatchTime = undefined;
    this.countdownProgress = 6;
    this.intervalCountdown = setInterval(() => {
      this.countdownProgress--;
      if (this.countdownProgress <= 1) {
        clearInterval(this.intervalCountdown);
        this.stopwatch();
      }
    }, 1000);
  }

  skip() {
    this.countdownProgress = 1;
    clearInterval(this.intervalCountdown);
    this.stopwatch();
  }

  stopwatch() {
    clearInterval(this.intervalStopwatch);
    this.stopwatchProgress = new Date();
    this.stopwatchTime = new Date(new Date().getTime() - this.stopwatchProgress.getTime());
    this.intervalStopwatch = setInterval(() => {
      this.stopwatchTime = new Date(new Date().getTime() - this.stopwatchProgress.getTime());
    }, 1000);
    setTimeout(() => {
      let answers = document.querySelector('.answers');
      setTimeout(() => {
        answers = answers || document.querySelector('.answers');
        if (answers && Object.values(answers).length) {
          for (let i = answers.children.length; i >= 0; i--) {
            answers.appendChild(answers.children[Math.random() * i | 0]);
          }
        }
      }, (answers && Object.values(answers).length) ? 0 : 1000);
    }, 1);
  }

  timer() {
    if (!this.timerEnabled) return;
    clearInterval(this.intervalTimer);
    this.timeProgress = 60;
    if (this.timeProgress === 60) {
      this.time = '1:00';
    }
    this.width = 100;
    this.intervalTimer = setInterval(() => {
      this.timeProgress--;
      this.time = '0:' + this.timeProgress;
      if (this.width >= 10) {
        this.width -= 100 / 60;
      }
      if (this.timeProgress === 0) {
        clearInterval(this.intervalTimer);
      }
    }, 1000)
  }

  pick(i, team, teams) {
    if (Capacitor && ['ios', 'android'].includes(Capacitor.getPlatform())) {
      //this.nativeAudio.play('ping');
    }
    if (team.selected === 'success') {
      team.selected = undefined;
      this.selected = undefined;
      this.selectedTeams2 = undefined;
      return;
    }
    if (team.selected === 'success fade') {
      return;
    }
    if (!this.selected) {
      this.selected = team.id;
      this.selectedTeams2 = this.teams2 === teams;
      team['selected'] = 'success';
    }
    else {
      let team2 = teams.find(t => t.selected === 'success' && t !== team);
      if (!team2) {
        team2 = (this.teams === teams ? this.teams2 : this.teams).find(t => t['selected'] === 'success' && t !== team);
      }
      let cls = 'success fade';

      if ((this.selectedTeams2 === (this.teams2 === teams)) || !team2 ||
        (team[this.qs[this.currentQId].pairs[1]] !== team2[this.qs[this.currentQId].pairs[1]])) {
        cls = 'danger shake';
        this.failCounter++;
        setTimeout(() => {
          team['selected'] = undefined;
          (team2 || {})['selected'] = undefined;
        }, 500);
        if (this.failCounter >= 3) {
          clearInterval(this.intervalTimer);
          clearInterval(this.intervalStopwatch);
          setTimeout(() => {
            this.qs = [];
            this.teams = [];
            this.teams2 = [];
          }, 1200);
        }
      }
      else {
        this.successCounter++;
        if (this.successCounter === this.teams.length) {
          let time = new Date().getTime() - this.stopwatchProgress.getTime();
          this.currentTime = this.datePipe.transform(new Date(time), 'mm:ss');
          this.currentBest = false;
          this.saveSuccess(new Date().getTime() - this.stopwatchProgress.getTime())
          clearInterval(this.intervalTimer);
          clearInterval(this.intervalStopwatch);
        }
      }
      team['selected'] = cls;
      (team2 || {})['selected'] = cls;
      this.selected = undefined;
      this.selectedTeams2 = undefined;
    }
  }

  randomQuote() {
    this.afs.collection("quotes").valueChanges({ idField: 'id' }).subscribe(quotes => {
      this.quote = quotes[Math.floor(Math.random() * quotes.length)];
    });
  }

  loadData(qid?) {
    this.sport = this.sport || 'nba';
    this.selected = undefined;
    this.selectedTeams2 = undefined;
    this.successCounter = 0;
    this.failCounter = 0;
    this.qs = [];
    this.teams = [];
    this.teams2 = [];
    this.leaders = undefined;
    this.countdown();
    this.randomQuote();

    this.afs.collection('/trivia/' + this.sport + '/question/', ref => ref
      .limit(100))
      .valueChanges().subscribe((qs) => {
        this.qs = [];
        qs.forEach(q => {
          this.qs[q['id']] = q;
        })
        this.getRandomQs();
        if (this.question) {
          this.currentQId = qid || this.question.id;
        }
        // this.currentQId = qid || (Math.floor(Math.random() * Math.floor(qs.length - 1)) + 1);

        this.loadLeaders(this.qs[this.currentQId].id);
        this.afs.collection('/trivia/' + this.sport + '/team/', ref => ref
          .limit(100))
          .valueChanges({ idField: 'id' }).subscribe((teamData) => {
            this.teams = teamData.sort(() => .5 - Math.random()).slice(0, this.rowCount * this.columnCount / 2);
            this.teams2 = JSON.parse(JSON.stringify(this.teams)).sort(() => .5 - Math.random());
            // this.teams2 = JSON.parse(JSON.stringify(this.teams));
          });
      });
  }

  getRandomQs() {
    let random = this.getRandomInt(100);
    let selectedQs = {};
    if (random <= 30) {
      selectedQs = this.randomQ(1);
    } else if (random > 30 && random <= 50) {
      selectedQs = this.randomQ(2);
    } else if (random > 50 && random <= 60) {
      selectedQs = this.randomQ(3);
    } else if (random > 60 && random <= 70) {
      selectedQs = this.randomQ(4);
    } else if (random > 70 && random <= 75) {
      selectedQs = this.randomQ(5);
    } else if (random > 75 && random <= 80) {
      selectedQs = this.randomQ(6);
    } else if (random > 80 && random <= 85) {
      selectedQs = this.randomQ(7);
    } else if (random > 85 && random <= 90) {
      selectedQs = this.randomQ(8);
    } else if (random > 90 && random <= 95) {
      selectedQs = this.randomQ(9);
    } else if (random > 95 && random <= 100) {
      selectedQs = this.randomQ(10);
    }
    if (selectedQs) {
      this.question = selectedQs;
    }
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  randomQ(difficulty) {
    let qss = this.qs.filter(qs => qs.difficulty === difficulty ? qs : null);
    if (qss && qss.length) {
      return qss[Math.floor((Math.random() * qss.length))];
    } else {
      this.getRandomQs();
    }
  }

  saveSuccess(time) {
    const data = { [this.user.uid]: time };
    this.afs.collection('/trivia/' + this.sport + '/users').doc(String(this.qs[this.currentQId].id)).get().subscribe(item => {
      if (!item.exists) {
        item.ref.set(data).then(() => { });
      }
      else {
        if (!item.data()[this.user.uid] || time < item.data()[this.user.uid]) {
          item.ref.update(data).then(() => { });
          this.currentBest = true;
        }
      }
      this.usersService.load([this.user.uid]);
    });
  }

  secondsToTime(seconds) {
    return new Date(seconds);
  }

  orderLeaders(leaders) {
    const sorted = {}
    Object.keys(leaders).sort((a, b) => leaders[a] - leaders[b]).forEach(uid => sorted[uid] = leaders[uid]);
    return sorted;
  }

  leaderKeys(leaders) {
    return Object.keys(leaders).sort((a, b) => leaders[a] - leaders[b]);
  }

  loadUserData() {
    const user = this.userService.user;
    this.afs.collection("user").doc(user.uid).valueChanges().subscribe(i => {
      let userData = i;
      if (userData && userData['friends']) {
        (userData['friends'] || []).forEach(friend => {
          if (!friend.sent && !friend.blocked) {
            this.userFriends.push(friend.uid);
          }
        })
      }
    });
  }

  isFriend(uid, i) {
    return this.userFriends.includes(uid);
  }

  segmentChanged(event) {
    this.segmentSelected = event.detail.value;
  }

}
