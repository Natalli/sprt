import { Component, OnInit, AfterViewChecked, ViewChild, OnDestroy, ViewChildren, ElementRef, QueryList, HostListener } from '@angular/core';
import { LoadingController, ModalController, NavParams, Platform, IonIcon } from '@ionic/angular';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { ProfilePage } from 'src/app/pages/profile/profile.page';
import { debounceTime } from 'rxjs/operators';
import { Constants } from 'src/app/constants';
import { LayoutService } from 'src/app/services/layout.service';
import { DatePipe } from '@angular/common';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { UserService } from 'src/app/service/user.service';
import { ModalChatCreateComponent } from '../new/modal-chat-create/modal-chat-create.component';
import { ModalChatManageComponent } from '../new/modal-chat-manage/modal-chat-manage.component';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-popup-messages',
  templateUrl: './popup-message.component.html',
  styleUrls: ['./popup-message.component.scss'],
})
export class PopupMessageComponent implements OnInit, AfterViewChecked, OnDestroy {
  @ViewChild('focusInput') focusInput;
  @ViewChild('focusGroupName') focusGroupName;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  private subject: Subject<string> = new Subject();
  private subscription: Subscription;
  messages;
  chatId;
  chatRef;
  messageRefs;
  page;
  popoverCtrl;
  console = console;
  user;
  recepient;
  chatUsers;
  users = {};
  afs;
  message: string = '';
  userData: Object = {};
  hiddenBlock: boolean;
  myUid;
  timer: number = 0;

  url: string = '';
  UrlEmitter = new BehaviorSubject<string>(this.url);

  objectKeys = Object.keys;
  constants = Constants;
  isRecepient: boolean;
  friends: Array<Object>;
  recepientId: string = '';
  showUserName; boolean;
  selectGroupName: boolean = false;
  // groupName;
  // groupPhoto;
  showSentBubble: boolean;
  userTyping: Array<string> = [];
  chatSubscription: any;
  chatPrivate: boolean;

  messageUniqueName: boolean

  popup;
  changedValue: boolean;
  chat;
  joinMessage;
  dateJoined;
  deleteChat;
  alertController;
  mentions: any;
  mentionName: string;
  userMentions: any[] = [];

  constructor(private navParams: NavParams,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public platform: Platform,
    protected layoutService: LayoutService,
    protected datePipe: DatePipe,
    protected elementRef: ElementRef,
    private fns: AngularFireFunctions,
    private userService: UserService,
    protected modaService: ModalService) {
    this.backButtonEvent();

  }

  ionViewDidEnter() {
    this.scrollToBottom();
    if (!this.hiddenBlock) {
      this.autoFocus();
    }
  }

  autoFocus() {
    setTimeout(() => {
      if (this.focusInput) {
        this.focusInput.setFocus();
      }
    }, 100)

  }

  @HostListener('click', ['$event'])
  onClick(e) {
    // let mentionBlock = e['target'].classList[0];
    if (e['target'].classList[0] === 'decorate') {
      let id = e['target'].classList[1];
      this.userInfo(id);
    }
  }

  ngOnInit() {
    this.page = this.navParams.get('data');
    this.getUser();
    this.scrollToBottom();
    // this.getChat();
    // this.url = this.chat && `this.chat['groupPhoto'] || Constants.GROUP_AVATAR_DEMO;
    // this.UrlEmitter.next(this.url);
    // console.log(this.url, 'URL')
    // console.log(this.chat['groupPhoto'])
  }

  addMention(e) {
    let txt = e.target.value;
    if (!txt.length && this.userMentions.length) {
      this.userMentions = [];
    }
    if (txt.includes('@')) {
      let names = txt.split('@');
      let name = names[names.length - 1].toLowerCase();
      if (name.length > 1) {
        this.mentions = Object.values(this.users).filter(user => user['keywords'].includes(name) ? user : null);
        if (this.mentions.length) {
          this.mentions = this.mentions.filter(function (e) { return this.indexOf(e) < 0 }, this.userMentions);
        }
      } else {
        this.mentions = [];
      }
    }
  }

  selectedMention(data) {
    this.mentionName = data.name;
    let mention = this.message.split('@').pop();
    this.message = this.message.replace(mention, this.mentionName);
    this.userMentions.push(data);
    this.mentions = null;
    this.focusInput.setFocus();
    this.message = this.message + ' ';
  }

  async showUserProfile(uid, e) {
    e.preventDefault();
    e.stopPropagation();
    if (!uid) return;
    const profilePopup: boolean = true;
    const popover = await this.modalController.create({

      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await popover.present();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  ngAfterViewChecked() { }

  getChat() {
    // this.afs.collection('/chat/').doc(this.chatId).get().subscribe(item => {
    //   console.log(item.data(), 'chat')
    // })
  }

  ngOnDestroy() {
    this.chatSubscription.unsubscribe(); // Do not reset unseen status in background!
  }

  getUser() {
    this.user = this.userService.user;
    this.myUid = this.user.uid;
    this.initMessage();
    this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(user => {
      this.userData = user;
      this.friends = user.friends || [];
      if (this.recepient) {
        this.recepientId = this.recepient.uid
      } else {
        this.recepientId = this.getChatUsers()[0];
      }
      if (this.friends.length) {
        if (this.recepient && this.recepient.uid) {
          var messageSender = this.friends.find(friend => (friend['uid'] === this.recepient.uid) ? friend : null);
        } else {
          var messageSender = this.friends.find(friend => (friend['uid'] === this.getChatUsers()[0]) ? friend : null);
        }
        // let messageSender = this.friends.find(friend => ((this.recepient && friend['uid'] === this.recepient.uid) || friend['uid'] === this.getChatUsers()[0]) ? friend : null);
        if (!messageSender) {
          if (this.messages && this.messages.length === 1 && this.messages[0].sender === ((this.recepient && this.recepient.uid) || this.getChatUsers()[0])) {
            this.hiddenBlock = true;
          }
        }
      } else {
        if (this.messages.length === 1) {
          if ((this.recepient && this.messages[0].sender === this.recepient.uid) || this.messages[0].sender === this.getChatUsers()[0]) {
            this.hiddenBlock = true;
          } else {
            this.hiddenBlock = false;
          }
        }
      };
      this.detectTypeBubble();
    });
    if (this.chat && this.chat.join) {
      this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId)
        .update({ join: false, dateJoined: new Date(Date.now()) });
      this.chatRef.add({
        created: new Date(Date.now()),
        sender: this.user.uid,
        receiver: this.recepient ? this.recepient.uid : null,
        joined: true
      });
    }
  }

  detectTypeBubble() {
    this.chatSubscription = this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId)
      .valueChanges().subscribe(response => {
        if (response) {
          this.checkUnseenMessages(response);
          // this.chatPrivate = response['private'] ? true : false;
          this.userTyping = response.typeListener || [];
          if (this.userTyping.length > 0) {
            this.userTyping.forEach(item => {
              let start = new Date(item['time']['seconds']).getTime() * 1000;
              let now = new Date().getTime();
              (now - start) < 15000 ?
                this.showSentBubble = true :
                this.showSentBubble = false;
            })
          } else {
            this.showSentBubble = false;
          }
        }
      });
  }

  checkUnseenMessages(data) {
    let unseenMessages = data.status || [];
    if (unseenMessages.length > 0) {
      this.afs.collection('/user/' + this.myUid + '/chat').doc(this.chatId).update({ status: [] });
    }
  }

  scrollToBottom(): void {
    const element = document.getElementById('scrollChat');
    if (element) {
      setTimeout(() => {
        element.scrollIntoView({ behavior: 'auto', block: 'end' });
      }, 1000)
    }
  }

  initMessage() {
    delete this.chatRef;
    if (this.user) {
      if (this.chatId) {
        this.chatRef = this.afs.collection('/chat/' + this.chatId + '/message', ref => ref.orderBy('created'));
        this.chatRef.valueChanges({ idField: 'id' }).subscribe((messages: Array<any>) => {
          this.messages = messages;
          this.dateJoined = this.chat.dateJoined || '';
        });
        this.getChatUsers();
      } else if (this.recepient) {
        this.afs.collection('/user/' + this.user.uid + '/chat', ref => ref
          .where('private', '==', (this.recepient.uid || this.recepient.id))).valueChanges({ idField: 'id' }).subscribe((message_refs: Array<any>) => {
            if (!message_refs || !message_refs.length) {
              this.afs.collection('/chat').add(
                { users: [this.user.uid, this.recepient.uid] }
              )
                .then((docRef) => {
                  this.afs.collection('/user/' + this.user.uid + '/chat').doc(docRef.id).set({ private: this.recepient.uid })
                  this.afs.collection('/user/' + this.recepient.uid + '/chat').doc(docRef.id).set({ private: this.user.uid })
                  // TODO: re-init?
                });
            }
            if (message_refs.length) {
              const message_ref = message_refs.pop();
              if (message_ref && message_ref.id) {
                this.chatId = message_ref.id;
                this.chatRef = this.afs.collection('/chat/' + message_ref.id + '/message', ref => ref.orderBy('created'));
                this.chatRef.valueChanges({ idField: 'id' }).subscribe((messages: Array<any>) => {
                  this.messages = messages;
                });
                this.getChatUsers();
              }
            } else {
              console.log('/user/' + this.user.uid + '/chat' + ' for ' + this.recepient.uid + ' does not exist or empty');
            }
          });
      } else {
        console.log('empty chat');
      }
    }
  }

  getChatUsers() {
    if (!this.chatUsers && this.chatId) {
      this.afs.doc('/chat/' + this.chatId).valueChanges().subscribe(resp => {
        const chat = resp || { users: [] };
        if (chat.users.indexOf(this.user.uid) !== -1) {
          chat.users.splice(chat.users.indexOf(this.user.uid), 1);
        }
        this.chatUsers = chat.users;
        if (this.chatUsers.length > 0) {
          this.hiddenBlock = false;
          this.showUserName = true;
        }
        this.getUserProfiles(chat.users);
      });
    }
    return this.chatUsers;
  }

  getUserProfiles(uids: Array<string>) {
    uids.forEach(uid => {
      this.afs.doc('/user/' + uid).get().subscribe(user => {
        this.users[uid] = user.data();
        this.users[uid].uid = uid;
      });
    });
  }

  listenerTypeMessage(removeFlag?) {
    if (this.users) {
      Object.keys(this.users).forEach(item => {
        this.afs.collection('/user/' + item + '/chat').doc(this.chatId)
          .valueChanges().subscribe(response => {
            let userTyping = response && response['typeListener'] || [];

            if (userTyping.length > 0) {
              let user = userTyping.find(item => {
                if (item.uid === this.myUid) {
                  item.time = new Date()
                  return item.uid
                } else {
                  return null
                }
              });
              if (!user) {
                userTyping.push({ uid: this.myUid, time: new Date() })
              }
            } else {
              userTyping.push({ uid: this.myUid, time: new Date() })
            }
            if (removeFlag) {
              // this.afs.collection('/user/' + item + '/chat').doc(this.chatId).update({ typeListener: null });
            } else if (this.timer === 0 || this.timer === 15) {
              // this.afs.collection('/user/' + item + '/chat').doc(this.chatId).update({ typeListener: userTyping });
            }
          })
      });
    }
  }

  sendMessage(ev) {
    if (this.message && this.message.length > 0 && this.message.length < 5 || this.timer === 15) {
      this.subject.next(this.message);
      this.subscription = this.subject.pipe(
        debounceTime(1000))
        .subscribe(searchTextValue => {
          // console.log(searchTextValue, 'text value')
        })
      this.timer = 0;
      //this.listenerTypeMessage(); // TODO: investigate message send slowdown
      this.timerFn();
    }
    if (ev && ev.keyCode === 13 || ev && ev.type === 'click') {
      const text = this.message || ev.target.value;
      if (this.CheckSpaces(text)) {
        if (this.chatId) {
          this.chatRef.add({
            created: new Date(Date.now()),
            sender: this.user.uid,
            receiver: this.recepient ? this.recepient.uid : null,
            text
          });
          this.scrollToBottom();
          this.listenerTypeMessage('remove');
          this.userMentions.forEach(user => {
            const callable = this.fns.httpsCallable('sendNativeNotification');
            Object.values(user.tokens).forEach(token => {
              if (token) callable({ tokens: token, title: 'Mention in chat', body: this.user.displayName + " mentioned you in chat\n", gotoUrl: '/chat' })
                .toPromise().then((resp) => {
                  //console.log(resp);
                });
            });
          })
          this.mentions = null;
          this.userMentions = [];
          Object.keys(this.users).forEach((user_uid, index) => {
            if (user_uid !== this.user.uid) { // Do not notify sender            
              this.afs.collection('/user/' + user_uid + '/chat/').doc(this.chatId).get().subscribe(response => {
                if (this.chat.private) {
                  let status = response.data() && response.data().status || [];
                  status.push(user_uid);
                  if (!response.exists) {
                    response.ref.set({ status, private: this.myUid });
                  }
                  else {
                    response.ref.update({ status });
                  }
                } else {
                  let status = response.data() && response.data().status || [];
                  status.push(user_uid);
                  let copyUsers = [...Object.keys(this.users)];
                  copyUsers.splice(index, 1, this.myUid);
                  if (!response.exists) {
                    response.ref.set({
                      status,
                      groupName: this.chat.groupName,
                      groupPhoto: this.chat.groupPhoto,
                      private: false,
                      users: copyUsers,
                      dateJoined: new Date(Date.now())
                    });
                  }
                  else {
                    response.ref.update({ status });
                  }
                }

                // response.ref.update({
                //   status
                // status: firebase.firestore.FieldValue.arrayUnion(user_uid)
                // });
              })
            }
          })
        } else {
          console.log('chat_id is empty');
        }
        ev.target.value = '';
        this.message = '';
      }
    }
  }

  CheckSpaces = (str) => str.trim() !== '';

  timerFn() {
    const intervalId = setInterval(() => {
      this.timer++;
      if (this.timer === 15) {
        clearInterval(intervalId);
      }
    }, 1000);
  }

  transformText(ev) {
    this.message = ev.target.value.replace(/((?:(?:^|[.?!])\s*)+)(.)/g, function (m, tail, ch) {
      return tail + ch.toUpperCase();
    });
    this.addMention(ev);
  }

  async addFriend(uid, closePopup) {
    let friends = this.userData['friends'] || [];
    friends.push({ uid, sent: true });
    this.afs.collection("user").doc(uid).get().subscribe(user => {
      var vFriends = user.data()['friends'] || [];
      vFriends.push({ uid: this.myUid, sent: true, pending: true });
      this.saveData(this.myUid, friends, uid, vFriends, 'Your invitation has been sent', null, closePopup);
    });
    this.autoFocus()
  }

  async blockedFriend(uid, blocked, myFriend?, closePopup?) {
    if (myFriend) {
      this.userData['friends'].find((u, i) => {
        if (u['uid']) {
          if (u['uid'] === uid) {
            let vFriends;
            this.userData['friends'][i] = {
              uid,
              blocked
            };
            vFriends = this.userData['friends'];
            this.saveData(null, null, this.myUid, vFriends, 'The user is blocked', 'blocked', closePopup);
          }
        } else {
          if (u['id'] === uid) {
            let vFriends;
            this.userData['friends'][i] = {
              uid,
              blocked
            }

            vFriends = this.user.friends;
            this.saveData(null, null, this.myUid, vFriends, 'The user is blocked', 'blocked', closePopup);
          }
        }
      })
    } else {
      let friends = this.userData['friends'] || [];
      friends.push({
        uid,
        blocked,
        notFriend: true
      });
      this.saveData(null, null, this.myUid, friends, 'The user is blocked', 'blocked', closePopup);
    }
  }

  deleteFriend(uid, closePopup) {
    this.afs.collection("user").doc(uid).get().subscribe(user => {
      let friends = user.data()['friends'];
      let removeUser = friends.find(user => {
        if (user.uid) {
          return user.uid === this.myUid
        } else {
          return user.id === this.myUid
        }
      });
      let i = friends.indexOf(removeUser);
      if (i != -1) {
        friends.splice(i, 1);
      }
      this.saveData(uid, friends, null, null, null)
    })
    let vFriends = this.userData['friends']
    let removeVUser = vFriends.find(user => {
      if (user.uid) {
        return user.uid === uid
      } else {
        return user.id === uid
      }
    });
    let vI = vFriends.indexOf(removeVUser);
    if (vI != -1) {
      vFriends.splice(vI, 1);
    }

    this.saveData(null, null, this.myUid, vFriends, 'Successfully deleted', null, closePopup);
  }

  saveData(myUid, friends, userUid, vFriends, text1, blocked?, closePopup?) {
    if (myUid && friends) {
      this.afs.collection("user").doc(myUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends });
        }
        else {
          item.ref.update({ friends });
        }
      });
    }
    if (userUid && vFriends && text1) {
      this.afs.collection("user").doc(userUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends: vFriends });
        }
        else {
          item.ref.update({ friends: vFriends });
          this.modaService.presentLoading(text1);
          if (closePopup) {
            this.popoverCtrl.dismiss();
          }
          if (blocked) {
            setTimeout(() => {
              this.loadingController.dismiss();
              this.modalController.dismiss();
            }, 3000)
          } else {
            setTimeout(() => {
              this.hiddenBlock = false;
              this.loadingController.dismiss();
            }, 3000)
          }

        }
      });
    }
  }

  close() {
    this.hiddenBlock = false;
    this.autoFocus();
  }

  async loading(text) {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: text,
      translucent: true
    });
    return await loading.present();
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const modal = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid,
        modalController: this.modalController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await modal.present();
  }

  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      this.modalController.dismiss();
    })
  }

  closeJoinMessage() {
    // this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId).update({ join: null });
    this.joinMessage = true;
  }

  async manageChat(ev: any, isAdmin) {
    const popover = await this.popoverCtrl.create({
      component: ModalChatManageComponent,
      // event: ev,
      componentProps: {
        chatId: this.chatId,
        afs: this.afs,
        messageRefs: this.messageRefs,
        isAdmin
      },
      id: "popupMessages",
      // swipeToClose: true,
      // showBackdrop: true,
      // backdropDismiss: true,
      cssClass: 'modal-wrap-default',
    });

    return await popover.present();
  }

  async changeChatInfo(chat) {
    const popover = await this.popoverCtrl.create({
      component: ModalChatCreateComponent,
      // event: ev,
      componentProps: {
        chatId: this.chatId,
        chat,
        modalController: this.modalController,
        afs: this.afs,
        messageRefs: this.messageRefs,
        myUid: this.myUid,
        chatUsers: this.chatUsers
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  uniqueDate(value, prevValue,) {
    if (!prevValue) {
      let date = this.datePipe.transform(value.created.toDate(), 'MMMM d');
      return date;
    }
    let date = this.datePipe.transform(value.created.toDate(), 'MMMM d');
    let prevDate = this.datePipe.transform(prevValue.created.toDate(), 'MMMM d');
    if (date !== prevDate) {
      return date;
    } else {
      return null;
    }
  }
}
