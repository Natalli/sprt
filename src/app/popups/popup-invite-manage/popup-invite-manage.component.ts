import { Component, OnInit, ViewChild, Input, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { NavParams, ModalController, AlertController, IonSearchbar, Platform, LoadingController, IonIcon } from '@ionic/angular';
import { Router } from '@angular/router';
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import { Constants } from 'src/app/constants';
import { ProfilePage } from 'src/app/pages/profile/profile.page';
import { LayoutService } from 'src/app/services/layout.service';
import { UsersService } from 'src/app/service/users.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-popup-invite-manage',
  templateUrl: './popup-invite-manage.component.html',
  styleUrls: ['./popup-invite-manage.component.scss'],
})
export class PopupInviteManageComponent implements OnInit {
  @ViewChild(IonSearchbar) searchbar: IonSearchbar;
  // @Input() isFriendsManage: boolean;
  @Input() checkFriendQ: boolean;
  @Input() globalManage: boolean;
  @ViewChild('focusInput') focusInput;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  user: any;
  chatRef: any;
  recepient: any;
  afs: any;
  chatId: any;
  inviteId: any;
  messages: any[];
  invite: any;
  chat: any;
  users = {};
  search;
  searchUsers = {};
  messageRefs;
  popoverCtrl;
  addFriend;
  friends;
  saveData;
  checkFriend;
  constants = Constants;
  objectKeys = Object.keys;
  myUid: string;
  searchFriendsUsers: any;
  isAdmin;
  manageChat: any;
  searchService;
  game_id;

  constructor(
    private navParams: NavParams,
    private router: Router,
    public modalController: ModalController,
    public alertController: AlertController,
    public loadingController: LoadingController,
    private platform: Platform,
    protected layoutService: LayoutService,
    private usersService: UsersService,
    private fns: AngularFireFunctions,
    private userService: UserService) {
    this.backButtonEvent();
  }

  ngOnInit() {
    this.user = this.userService.user;
    if (this.inviteId) {
      this.initInvite();
    }
    this.afs.doc('/user/' + this.user.uid).get().subscribe(response => {
      this.friends = [];
      response.data().friends.forEach(friend => {
        this.friends[friend.uid] = friend;
      })
      this.afs.collection("/user/").valueChanges({ idField: 'id' }).subscribe(data => {
        const userFriends = [];
        response.data().friends.forEach(friend => {
          const userFriend = data.find(user => user.id === friend.uid ? user : null);
          userFriends.push(userFriend);
          this.searchFriendsUsers = userFriends;
          this.searchUsers = userFriends;
        })
      })
    });
  }

  ionAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  initInvite() {
    this.afs.collection('invite/').doc(this.inviteId).valueChanges({ idField: 'id' }).subscribe((invite: object) => {
      this.invite = invite;
      this.users = {};
      this.isAdmin = this.invite && this.invite.admins.indexOf(this.user.uid) !== -1;
      if (this.invite && this.invite.users) {
        this.invite.users.forEach(user_uid => {
          this.afs.doc('/user/' + user_uid).valueChanges().subscribe(user => {
            this.users[user_uid] = user;
            this.users[user_uid].uid = user_uid;
          });
        });
      }
    });
  }

  isUserAdmin(uid) {
    return this.invite && this.invite && this.invite.admins.indexOf(uid || this.user.uid) !== -1;
  }

  async confirmChatRemove(user_uid) {
    const alert = await this.alertController.create({
      header: 'Confirm delete chat',
      message: 'Are you sure you want to delete this chat?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Delete',
          cssClass: 'danger',
          handler: () => {
            this.afs.collection('/chat/').doc(this.chatId).update({ users: this.chat.users });
            this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).delete();
            this.afs.collection('/user/' + this.user.uid + '/chat').doc(this.chatId).delete();
            this.afs.collection('/chat/').doc(this.chatId).delete();
            this.modalController.dismiss();
          }
        }
      ]
    });
    await alert.present();
  }

  searchUser(ev) {
    this.search = (ev.target.value.length > 1) ? ev.target.value : '';
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.searchService.searchUser(this.search).subscribe(data => {
        this.searchUsers = this.filterUsers(data, (this.chat || { users: [this.user.uid] }).users);
        this.friends.forEach(friend => {
          let a = Object.values(this.searchUsers).map(user => {
            user['id'] === friend.uid ? user : null
          })
        })
      })
    } else {
      if (this.friends.length) {
        this.searchUsers = this.searchFriendsUsers;
      } else {
        this.searchUsers = {}
      }
    }
  }

  filterUsers(users, excluded) {
    if (users && excluded) {
      return users.filter(user => excluded.indexOf(user.id) === -1);
    }
    return users;
  }

  persistInvite(invite, id?) {
    if (id) {
      this.afs.collection('invite/').doc(id).update(Object.assign({}, invite));
    } else {
      console.log('shouldn\'t be possible');
    }
  }


  async addUser(userUid: string) {
    this.usersService.load([userUid]);
    this.persistInvite({ users: firebase.firestore.FieldValue.arrayUnion(userUid) }, this.inviteId);
    this.afs.collection('notification').add({
      title: "Game Invite Request",
      text: "invited you to join a Game",
      platforms: ['ios', 'android', 'web'],
      start: new Date(),
      scheduling: 'Now',
      receivers: [userUid],
      created: new Date(),
      friendRequest: false,
      sendRequest: true,
      inviteRequest: this.inviteId,
      sender: this.user.uid,
    })
      .then((docRef) => {
        console.log('added Noification', docRef.id);
        this.users = this.usersService.users;
        if (this.users[userUid].tokens) {
          const callable = this.fns.httpsCallable('sendNativeNotification');
          Object.values(this.users[userUid].tokens).forEach(token => {
            callable({ tokens: token, title: 'Game Invite', body: this.users[this.user.uid].name + " invited you to join a Game.\n" })
              .toPromise().then((resp) => {
                //console.log(resp);
              });
          });
        }
      });
  }

  async deleteUser(userUid: string) {
    console.log("🚀 ~ userUid", userUid)
    let bids = this.invite.bids[this.game_id];
    delete bids[userUid];
    console.log(this.invite)
    this.persistInvite({ users: firebase.firestore.FieldValue.arrayRemove(userUid), bids }, this.inviteId);
    this.deleteInviteNotifications([userUid]);
  }

  deleteInviteNotifications(users: Array<string>) {
    users.forEach(userUid => {
      this.afs.collection('notification', ref => ref
        .where('inviteRequest', '==', this.inviteId)
        .where('receivers', 'array-contains', userUid)
      ).get().subscribe(resp => {
        resp.docs.forEach(ref => {
          const inviteNotification = ref.data();
          if (inviteNotification.receivers.length > 1) {
            this.afs.doc('notification/' + ref.id).update({ receivers: firebase.firestore.FieldValue.arrayRemove(userUid) });
          }
          else {
            this.afs.doc('notification/' + ref.id).delete();
          }
        });
      });
    });
    const notification = {
      title: "Game Invite Delete",
      text: "deleted Invite Game " + (this.invite ? this.invite.name : ''),
      platforms: ['ios', 'android', 'web'],
      start: new Date(),
      scheduling: 'Now',
      receivers: users,
      sender: this.user.uid,
      friendRequest: false,
      sendRequest: true,
      created: new Date()
    };
    this.afs.collection('/notification/').add(notification).then((docRef) => { });
  }

  async deleteInviteGame() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm Delete',
      message: 'Are you sure you want to delete this Private Game?',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'black',
          handler: () => { }
        }, {
          text: 'Delete Private Game',
          cssClass: 'danger',
          handler: () => {
            this.deleteInviteNotifications(this.invite.users);
            this.afs.collection('invite').doc(this.inviteId).delete();
            this.modalController.dismiss().then(() => {
              this.router.navigate(['challenge']);
            });
          }
        }
      ]
    });
    await alert.present();
  }

  capitalize(text) {
    return text.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const popover = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: this.searchUsers[uid].id,
        modalController: this.modalController
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await popover.present();
  }

  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      this.modalController.dismiss();
    })
  }

  async addAdmin(user_uid, admin) {
    if (admin) {
      this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).update({ admin: false });
      const alert = await this.alertController.create({
        header: 'Remove admin',
        message: 'You removed ' + this.users[user_uid].name + ' is not an admin',
      });
      await alert.present();
    } else {
      this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).update({ admin: true });
      const alert = await this.alertController.create({
        header: 'Add admin',
        message: 'You added ' + this.users[user_uid].name + ' to admins',
      });
      await alert.present();
    }

  }

  adminDetect(user_uid) {
    this.afs.collection('/user/' + user_uid + '/chat').doc(this.chatId).valueChanges().subscribe(data => {
      // return data.admin
      if (data.admin) {
        return true
      } else {
        return false
      }
    });
  }

  async loading(text) {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: text,
      translucent: true
    });
    return await loading.present();
  }

}
