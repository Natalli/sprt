import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popup-select-sport',
  templateUrl: './popup-select-sport.component.html',
  styleUrls: ['./popup-select-sport.component.scss'],
})
export class PopupSelectSportComponent implements OnInit {
  sport;
  constructor(
    private router: Router,
    private popoverCtrl: PopoverController
  ) { }

  ngOnInit() { }

  change() {
    this.router.navigate(['/invite/' + this.sport]);
    this.popoverCtrl.dismiss();
  }

}
