import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { debounceTime, distinctUntilChanged, first, take } from 'rxjs';
import { SearchService } from 'src/app/services/search/search.service';

@Component({
  selector: 'app-admin-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class AdminUsersPage implements OnInit {
  users: any[] = [];
  data: any[];
  search: '';
  limit: number;
  hideMoreBtn: boolean;
  latestDoc: any = {};
  usersSubs: any;
  isBusy: boolean = false;
  page: number;
  constructor(
    private afs: AngularFirestore,
    protected searchService: SearchService
  ) {
    this.limit = 15;
    this.page = 1;
  }

  ngOnInit() {
    this.loadUsers();
  }

  ngOnDestory() {
    this.usersSubs.unsubscribe();
  }

  loadUsers() {
    this.isBusy = true;
    this.usersSubs = this.afs.collection("user", ref => ref
      .orderBy('creationTime', 'desc').startAfter(this.latestDoc).limit(this.limit))
      .snapshotChanges()
      .pipe(
        first()
      )
      .subscribe({
        next: (response) => {
          if(this.page === 1 && response && response.length === 1) {
            this.loadUsers();
            return
          }
          this.hideMoreBtn = false;
          this.latestDoc = response[response.length - 1].payload.doc;
          if (response.length < this.limit) {
            this.hideMoreBtn = true;
            this.isBusy = false;
            return;
          }
          response.forEach((doc, index) => {
            this.users.push(Object.assign(doc.payload.doc.data(), { uid: doc.payload.doc.id }));
          });
          this.data = this.users;
          this.isBusy = false;
        }
      })
  }

  filteredUsers(event) {

    if (event.target.value.length > 2) {
      this.isBusy = true;
      let search = event.target.value;
      this.searchService.searchUser(search)
        .pipe(
          debounceTime(500),
          distinctUntilChanged()
        )
        .subscribe(data => {
          if (data) {
            this.users = data;
            this.hideMoreBtn = true;
            this.isBusy = false;
          } else {
            this.isBusy = false;
          }
        })
    } else {
      this.users = this.data;
    }
  }

  clear() {
    this.users = this.data;
    this.hideMoreBtn = false;
  }

  more() {
    this.page += 1;
    this.loadUsers();
  }

}
