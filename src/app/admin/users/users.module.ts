import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminUsersPageRoutingModule } from './users-routing.module';

import { DirectivesModule } from 'src/app/directives/directives.module';
import { AdminUsersPage } from './users.page';
import { AdminUserComponent } from './user/user.component';
import { SearchServiceModule } from 'src/app/services/search/search.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DirectivesModule,
    AdminUsersPageRoutingModule,
    ReactiveFormsModule,
    SearchServiceModule
  ],
  declarations: [
    AdminUsersPage,
    AdminUserComponent
  ],
  exports: [
    AdminUserComponent
  ],
  providers: [DatePipe]
})
export class AdminUsersPageModule {}
