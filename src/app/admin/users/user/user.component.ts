import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Constants } from 'src/app/constants';
import { ModalService } from 'src/app/service/modal.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-admin-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class AdminUserComponent implements OnInit {
  @Input() set innerData(value) {
    if (value) {
      this.data = value;
    }
  }
  data: any;
  isEditForm: boolean;
  messageUniqueName: boolean;
  messageUniqueShortName: boolean;
  public form: FormGroup;
  constants = Constants;
  someUser: any;
  time: any;
  email: any;

  get user() {
    return this.data;
  }

  constructor(
    private afs: AngularFirestore,
    private formBuilder: FormBuilder,
    protected datePipe: DatePipe,
    protected alertController: AlertController,
    private userService: UserService,
    protected modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.formInit();
    this.patchForm();
  }

  formInit() {
    this.form = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, this.UniqueName.bind(this, 'name'), Validators.pattern(Constants.FULL_NAME_REGEXP)])],
      publicName: ['', Validators.compose([Validators.required, this.UniqueName.bind(this, 'publicName'), Validators.pattern(Constants.NAME_REGEXP)])],
      legalName: [''],
      color: ['', Validators.compose([Validators.required])],
      keywords: ['']
    });
  }

  patchForm() {
    this.email = this.user['email'];
    this.form.get('name').setValue(this.form.value.subject = this.user['name'] || this.user.displayName);
    this.form.get('color').setValue(this.form.value.subject = this.user['color'] || '');
    this.form.get('publicName').setValue(this.form.value.subject = this.user['publicName'] || '');
    this.form.get('legalName').setValue(this.form.value.subject = this.user['legalName'] || '');
    this.form.get('keywords').setValue(this.form.value.subject = this.user['keywords'] || '');
    if (this.user['creationTime']) {
      this.time = this.datePipe.transform(this.user['creationTime'].seconds * 1000, 'M/d/yy h:mma');
    }

  }

  UniqueName(valueName: string, name: FormControl) {
    let newName = name.value;
    this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(data => {
      let users = data;
      if (this.someUser) {
        let uniqueName = users.find(data => {
          return data[valueName] !== this.someUser[valueName] && data[valueName] === newName
        })
        if (valueName === 'name') {
          uniqueName ? this.messageUniqueName = true : this.messageUniqueName = false;
        } else if (valueName === 'publicName') {
          uniqueName ? this.messageUniqueShortName = true : this.messageUniqueShortName = false
        }
      }
    })
  }

  edit() {
    this.isEditForm = true;
  }

  save() {
    if (this.user.name !== this.form.value.name ||
      this.user.color !== this.form.value.color ||
      this.user.publicName !== this.form.value.publicName ||
      this.user.legalName !== this.form.value.legalName ||
      this.user.keywords !== this.form.value.keywords) {
      console.log(this.form.value, 'save')
      this.afs.collection('user').doc(this.user.uid).update(this.form.value);
      this.isEditForm = false;
      this.modalService.presentLoading('Information updated');
    } else {
      this.modalService.errorModal('Error', 'No changed data');
    }
  }

  cancel() {
    this.isEditForm = false;
  }

  async recover() {
    const alert = await this.alertController.create({
      header: 'Are you sure you want to reset the password?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.alertController.dismiss();
          }
        }, {
          text: 'Ok',
          handler: (input) => {
            this.userService.sendPasswordResetEmail(this.user.email);
          }
        }
      ]
    });
    await alert.present();
  }


  // isString(string) {
  //   if (typeof string === 'string') {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
