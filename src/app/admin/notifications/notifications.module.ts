import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DirectivesModule } from 'src/app/directives/directives.module';
import { AdminNotificationsPage } from './notifications.page';
import { AdminNotificationsPageRoutingModule } from './notifications-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DirectivesModule,
    ReactiveFormsModule,
    AdminNotificationsPageRoutingModule
  ],
  declarations: [
    AdminNotificationsPage
  ],
  providers: [DatePipe]
})
export class AdminNotificationsPageModule {}
