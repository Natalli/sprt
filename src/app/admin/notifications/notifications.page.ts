import { DatePipe } from '@angular/common';
import { Component, ElementRef, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IonIcon, LoadingController, PopoverController } from '@ionic/angular';
import { EngineService } from 'src/app/engine.service';
import { UserService } from 'src/app/service/user.service';
import { UsersService } from 'src/app/service/users.service';
import { LayoutService } from 'src/app/services/layout.service';

@Component({
  selector: 'app-admin-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class AdminNotificationsPage implements OnInit {
  @Input() isFriendsManage: boolean;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  notificationForm: FormGroup;
  currentDate = new Date();

  userBids: Object = null;
  user: any;

  challenge_id: string;
  challenges: Array<Object> = [];
  challenge: any = {};
  history: Array<Object> = [];
  games: Array<Object> = [];
  users: Object = {};
  notificationUsers: Object = {};
  searchUsers: Object = {};
  stats: Object = {};
  challengeWinners: Object = {};
  teams: Object;
  teams2: Object = {};
  bids: Object = {};

  objectKeys = Object.keys;
  window = window;
  alert = alert;
  date = Date;
  today = '';
  isLoading: any;
  intervalId: any = '';
  public autoShuffle = false;
  public adminTab = 'notifications';
  public notificationTab = 'past';
  public challengeTab = 'info';
  target = 'platform';
  notifications: any[];
  search: any;
  max: number;
  selectedChallenge: any;
  assignDisabled = false;

  constructor(
    public router: Router,
    private afs: AngularFirestore,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    public engineService: EngineService,
    public loadingController: LoadingController,
    private fns: AngularFireFunctions,
    protected layoutService: LayoutService,
    public usersService: UsersService,
    protected popoverCtrl: PopoverController,
    private userService: UserService) {
    // this.fns.httpsCallable('https://sprtmtrx.com');
    this.loadUser();
    this.users = this.usersService.users;

  }

  ngOnInit() {
    this.initForms();
    this.loadNotifications();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  ionViewDidEnter() {
  }

  ionViewWillLeave() {
    if (this.intervalId) {
      //clearTimeout(this.intervalId);
    }
  }

  selectDate(value) {
    this.notificationForm.get('start').patchValue(this.datePipe.transform(value, 'yyyy-MM-dd'));
  }

  initForms() {
    this.notificationForm = this.fb.group({
      title: ['', [Validators.required]],
      text: ['', [Validators.required]],
      notes: [''],
      platforms: [[]],
      receivers: [[]],
      start: [this.datePipe.transform(new Date(), 'yyyy-MM-dd'), [Validators.required]],
      scheduling: ['Now', [Validators.required]],
    });
    this.notificationUsers = {};
  }

  get nfc() { return this.notificationForm.controls; }
  get nf() { return this.notificationForm; }

  isTarget(target) {
    return this.nfc.platforms.value && this.nfc.platforms.value.includes(target);
  }

  adminTabChanged($ev) {
    if ($ev) {
      this.notificationTab = $ev.detail.value || 'new';
    }
  }

  notificationTabChanged($ev) {
    if ($ev) {
      this.notificationTab = $ev.detail.value || 'new';
    }
  }

  targetTabChanged($ev) {
    if ($ev) {
      this.target = $ev.detail.value || 'platform';
      this.notificationForm.controls.platforms.markAsTouched();
    }
  }

  platformChanged(platform) {
    if (this.notificationForm.value.platforms.includes(platform)) {
      this.notificationForm.value.platforms.splice(this.notificationForm.value.platforms.indexOf(platform), 1);
    }
    else {
      this.notificationForm.value.platforms.push(platform);
    }
    this.notificationForm.controls.platforms.markAsTouched();
  }

  createNotification() {
    this.notificationForm.markAllAsTouched();
    console.log(this.notificationForm);
    if (this.notificationForm.valid &&
      ((this.notificationForm.value.platforms && this.notificationForm.value.platforms.length) ||
        (this.notificationUsers && Object.keys(this.notificationUsers).length))) { // TODO: Make Receivers part of the Form?
      console.log('form valid');
      console.log(this.notificationForm.value);
      const notification = { ...this.notificationForm.value, receivers: Object.keys(this.notificationUsers || {}), created: this.currentDate, admin: true };
      notification.start = new Date(notification.start);
      if (notification.platforms.length > 0 && notification.receivers.length === 0) {
        notification.receivers = ['all'];
      }
      this.afs.collection('/notification/').add(notification)
        .then(() => {
          this.notificationTab = 'past';
          this.initForms();
        })
    }
  }

  deleteNotification(id) {
    this.afs.collection('/notification/').doc(id).delete();
  }

  async resendNotification(id) {
    await document.querySelector('ion-item-sliding').closeOpened();
    alert('not implemented');
  }

  searchUser(ev, search?) { // TODO: use Service instead
    if (typeof search === 'undefined') {
      this.search = (ev.target.value.length > 1) ? ev.target.value : '';
    }
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.afs.collection("/user/", ref => ref
        .orderBy('keywords')
        .startAt(this.search.toLowerCase())
        .endAt(this.search.toLowerCase() + "\uf8ff")
      ).valueChanges({ idField: 'id' }).subscribe(data => {
        if (data.length) {
          this.searchUsers = this.filterUsers(data, Object.keys(this.users));
        } else { // search without capitalization
          this.afs.collection("/user/", ref => ref
            .orderBy('keywords')
            .startAt(this.search)
            .endAt(this.search + "\uf8ff")
          ).valueChanges({ idField: 'id' }).subscribe(data => {
            this.searchUsers = this.filterUsers(data, Object.keys(this.users));
          });
        }
      });
    } else {
      this.searchUsers = {};
    }
  }

  addNotificationReceiver(user_index) {
    if (this.searchUsers[user_index]) {
      this.notificationUsers[this.searchUsers[user_index].id] = this.searchUsers[user_index];
      delete this.searchUsers[user_index];
    }
  }

  deleteNotificationReceiver(user_index) {
    if (this.notificationUsers[user_index]) {
      delete this.notificationUsers[user_index];
      this.searchUser(null, this.search);
    }
  }

  filterUsers(users, excluded) { // TODO: use Service instead?
    if (users && excluded) {
      return users.filter(user => excluded.indexOf(user.id) === -1);
    }
    return users;
  }

  customNotification() {
    let allowedUsers = [];
    let challengeUsers = {};
    let allUsers = [];
    if (this.games && this.challenges && this.teams2) {
      if (this.challenge.games) {
        this.challenge.games.forEach(gamePk => {
          if (this.teams2[gamePk] && Object.keys(this.teams2[gamePk]).length > 0) {
            let users = [];
            Object.keys(this.teams2[gamePk]).forEach(team_id => {
              users = users.concat(this.teams2[gamePk][team_id].users || []); // TODO: Check if winners calculated
            });
            challengeUsers[gamePk] = users;
            allUsers = [...new Set([...allUsers, ...users])];
          }
        });
        //allUsers = ['JTdh9Y9eO2MzzPJnEGboOIsdtxC3']
        let title = 'COVID Rampage.';
        let body = 'Sorry, due to COVID rampaging through the NHL, the Saturday NHL Challenge for Feb. 13 has been cancelled.';
        allUsers.forEach(uid => {
          let user = this.getUser(uid);
          setTimeout(() => {
            user = this.users[uid];
            if (user.tokens || false) {
              const callable = this.fns.httpsCallable('sendNativeNotification');
              Object.values(user.tokens).forEach(token => {
                if (token !== 'eV3UN7iG_KD9uKx1mYPjxr:APA91bGPDhdwH_QjJEK3HX8e_STqlh_XD7PFtz5cDp7JJlLtnYQ0t8krHqnnCAjpRWqH3-TBRCiEQRkPttjg5RYx20GqpvKRPT1ID2_O_rzVM-SPCtqLk2sg0by0AAvFRjlDBsIT--nA')
                  callable({ tokens: token, title: title, body: body, gotoUrl: ('/' + this.challenge.sport + '/' + this.challenge.id) })
                    .toPromise().then((resp) => {
                      console.log(resp);
                    })
                    .catch(error => {
                      console.log('Cloud function failed', error);
                    })
              })
            }
          }, (!user || !user.id) ? 3000 : 0);
        });
      }
    }
  }

  resetNotificationTokens() {
    console.log('resetNotificationTokens');
    this.afs.collection("user", ref => ref
    ).get().subscribe(users => {
      users.forEach((user: any) => {
        if (typeof user.data().tokens !== 'undefined') {
          //if ( user.id === 'JTdh9Y9eO2MzzPJnEGboOIsdtxC3') {
          console.log('tokens deleted', user.id);
          //user.ref.update({tokens: firebase.firestore.FieldValue.delete()});
          //}
        }
      });
    });
  }

  loadUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(i => {
          var item = i;
          if (item && Object.values(item).length > 0) {
            var userBids = this.userBids || {};
            this.userBids = {};
            Object.keys(item['games'] || {}).forEach(game_id => {
              this.userBids[game_id] = userBids[game_id] || { [user.uid]: item['games'][game_id] };
            });
          }
        });
      }
    });
  }

  loadUsers(users) {
    this.usersService.load(users);
  }

  getUser(uid) {
    if (!this.users || !this.users[uid]) {
      this.loadUsers([uid]);
    }
    return this.users[uid] || {};
  }

  loadNotifications() {
    console.log('loadNotifications');
    this.afs.collection('/notification/', ref => ref
      .orderBy('created', 'desc')
      .where('admin', '==', true))
      .valueChanges({ idField: 'id' }).subscribe((notifications: Array<any>) => {
        this.notifications = notifications;
      });
  }

  testNativeNotification() {

    const tokens = this.users[this.user.uid].tokens;

    const callable = this.fns.httpsCallable('sendNativeNotification');
    Object.values(tokens).forEach(token => {
      callable({ tokens: token, title: 'Test Notification', body: 'With App Redirect', gotoUrl: ('/' + this.challenge.sport + '/' + this.challenge.id) })
        //callable({ tokens: "dkPG7LBYTP-gyRrQBkGUPl:APA91bFjN1SL1_WVuSdHSVVUjuvZhfwzT3M9kWLufwmDLTwKEoUU__CwcfxrhCY9EMfgDKKsuifFY5WtHg6_dAAx_MmLt93Sc4kRr9BtVwGQ0PH5vAxx1OKEYgOHFKEp28_TWvO5sVmY", title: 'Game Invite', body: 'Game Body' })
        .toPromise().then((resp) => {
          console.log(resp);
        })
        .catch(error => {
          console.log('Cloud function failed', error);
        })

    })
  }

}

