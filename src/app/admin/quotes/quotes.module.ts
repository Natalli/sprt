import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminQuotesPageRoutingModule } from './quotes-routing.module';

import { DirectivesModule } from 'src/app/directives/directives.module';
import { AdminQuotesPage } from './quotes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DirectivesModule,
    ReactiveFormsModule,
    AdminQuotesPageRoutingModule
  ],
  declarations: [
    AdminQuotesPage
  ]
})
export class AdminQuotesPageModule {}
