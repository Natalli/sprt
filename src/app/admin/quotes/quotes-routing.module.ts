import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminQuotesPage } from './quotes.page';

const routes: Routes = [
  {
    path: '',
    component: AdminQuotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminQuotesPageRoutingModule {}
