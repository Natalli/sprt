import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { LayoutService } from '../../services/layout.service';
import { IonIcon } from '@ionic/angular';

@Component({
  selector: 'app-admin-quotes',
  templateUrl: './quotes.page.html',
  styleUrls: ['./quotes.page.scss'],
})
export class AdminQuotesPage implements OnInit {
  public QuotesForm: FormGroup;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  quotes: Array<Object> = [];
  editedQuote: Object;

  constructor(
    private formBuilder: FormBuilder,
    private afs: AngularFirestore,
    protected layoutService: LayoutService
  ) { }

  ngOnInit() {
    this.createQuotesForm();
    this.afs.collection("quotes").valueChanges({ idField: 'id' }).subscribe(quotes => {
      this.quotes = quotes || [];
    });
    
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  createQuotesForm() {
    this.QuotesForm = this.formBuilder.group({
      text: ['', Validators.compose([Validators.required])],
      title: [''],
      descr: ['']
    });
  }

  submitForm() {
    if (this.editedQuote && (this.editedQuote['text'] != this.QuotesForm.value.text ||
      this.editedQuote['title'] != this.QuotesForm.value.title ||
      this.editedQuote['descr'] != this.QuotesForm.value.title)) {
      this.afs.collection('/quotes/').doc(this.editedQuote['id']).update(this.QuotesForm.value);
      this.QuotesForm.reset();
    } else {
      this.afs.collection('/quotes/').add(this.QuotesForm.value);
      this.QuotesForm.reset();
    }
  }

  editQuote(id: string) {
    this.editedQuote = this.quotes.find(item => item['id'] === id ? item : null);
    this.QuotesForm.get('text').setValue(this.editedQuote['text']);
    this.QuotesForm.get('title').setValue(this.editedQuote['title' || '']);
    this.QuotesForm.get('descr').setValue(this.editedQuote['descr'] || '');
  }

  deleteQuote(id: string) {
    this.afs.collection('/quotes/').doc(id).delete();
  }

}
