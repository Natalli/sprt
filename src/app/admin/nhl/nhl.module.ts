import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NhlPageRoutingModule } from './nhl-routing.module';

import { NhlPage } from './nhl.page';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NhlPageRoutingModule,
    DirectivesModule
  ],
  declarations: [NhlPage],
  providers: [DatePipe]
})
export class NhlPageModule {}
