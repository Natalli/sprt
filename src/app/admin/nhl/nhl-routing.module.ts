import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NhlPage } from './nhl.page';

const routes: Routes = [
  {
    path: '',
    component: NhlPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NhlPageRoutingModule {}
