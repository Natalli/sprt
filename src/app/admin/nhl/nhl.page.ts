import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { LoadingController, Config, Platform, PopoverController, AlertController, ModalController, IonIcon } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { CommonService } from 'src/app/common.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { UserService } from 'src/app/service/user.service';
import { InviteService } from 'src/app/service/invite.service';
import *  as deepEqual from 'fast-deep-equal';
import { LayoutService } from 'src/app/services/layout.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-nhl',
  templateUrl: './nhl.page.html',
  styleUrls: ['./nhl.page.scss'],
})
export class NhlPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  today: any;
  events: any;
  games = {};
  date = Date;
  dayCount = 5;
  start;
  end;
  autoReload = false;
  intervalId: number;

  constructor(
    private datePipe: DatePipe,
    public commonService: CommonService,
    private http: HttpClient,
    public router: Router,
    public platform: Platform,
    public loadingController: LoadingController,
    private afs: AngularFirestore,
    public userService: UserService,
    public inviteService: InviteService,
    public alertController: AlertController,
    public modalController: ModalController,
    protected layoutService: LayoutService,
    protected modalService: ModalService
  ) {
    this.loadSchedule(); // 5 * 2 weeks +
    this.autoUpdate();
    this.start = this.datePipe.transform(new Date(Date.now() - 3600 * 24 * 1000), 'yyyy-MM-dd');
    this.end = this.datePipe.transform(new Date(Date.now() + 0 * 2096e5), 'yyyy-MM-dd');
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  autoUpdate() {
    this.intervalId = window.setInterval(
      () => {
        if ( this.autoReload ) {
          this.processSchedule();
        }
      }, 5 * 1000);
  }

  async loadSchedule(start?: Date, end?: Date) {
    console.log('loadSchedule', environment.nhl_legacy_api);
    start = start || this.start;
    end = end || this.end;
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.modalService.presentLoading().then(()=>{

      this.afs.collection('nhl', ref => ref
        .where('date', '>', '2020-07-20')
        .orderBy('date', 'desc')
        )
        .valueChanges().subscribe((events: Array<any>) => {
          console.log('schedule updated');
          this.events = events;

      });
    });
  }

  loadGame(gamePk: string) {
    this.games[gamePk] = null;
    this.afs.doc('live/'+gamePk)
      .valueChanges().subscribe((game: Array<any>) => {
      console.log('game loaded', 'live/'+gamePk);
      this.games[gamePk] = game;
    });
  }

  updateGame(gamePk: string) {
    console.log('updateGame:', gamePk);
    if ( gamePk ) {
      if ( !this.games || (typeof !this.games[gamePk] === 'undefined' && this.games[gamePk] !== null ) ) {
        console.log('game not loaded:', gamePk);
        this.loadGame(gamePk);
        setTimeout(() => {
          this.updateGame(gamePk);
        }, 200);
        return;
      };

      this.http.get(environment.nhl_legacy_api + '/game/' + gamePk + '/feed/live')
      .subscribe((data: Config) => {
          let game = data;
          delete game['gameData']['players'];
          game['liveData'] = {linescore: data['liveData']['linescore'], plays: {currentPlay: (data['liveData']['plays']['currentPlay']||{})}};
          //console.log(game);
          if ( !this.games || !this.games[gamePk] ) {
            console.log('game not loaded2:', 'live/'+gamePk);
            this.loadGame(gamePk);
            this.games[gamePk] = game;
            this.afs.doc('live/'+gamePk).set(game);
          }
          else {
            //console.log('game loaded');
            if (  !deepEqual(this.games[gamePk], game) ) {
              console.log('game has to be updated:', gamePk);
              console.log(this.games[gamePk]);
              console.log(game);
              this.afs.doc('live/'+gamePk).update(game);
            }
          }

      });
    }
  }

  updateStandings(season: string) {
    console.log('updateStandings', season);
    if ( season ) {
      this.http.get(environment.nhl_api + '/standings?season=' + season + '&expand=standings.record')
      .subscribe((standings: any) => {
        standings['records'].forEach((standing, index) => {
          this.afs.doc('standings/'+index).set(standing);
        })
        console.log('standings', standings['records']);
      });
    }
  }

  updateTeams(season?: string) {
    console.log('updateTeams', season);
    if ( true ) {
      let nhlTeams = {}
      for (let i=0; i<this.events.length; i++) {
        const event = this.events[i];
          //console.log('found games:', event.date);
          if ( event.games ) {
            event.games.forEach((game, id) => {
              nhlTeams[game.teams.away.team.name] = game.teams.away.team.id;
              nhlTeams[game.teams.home.team.name] = game.teams.home.team.id;
            });
          }
        
      }
      this.http.get('http://site.api.espn.com/apis/site/v2/sports/hockey/nhl/teams')
      .subscribe((data: any) => {
        if ( true ) {
              const teams = data.sports[0].leagues[0].teams;
              teams.forEach(el => {
                const nhl = el.team;
                const team:any = this.mapNhlTeams(nhl);
                if ( nhlTeams[team.displayName] ) {
                  team.oid = nhlTeams[team.displayName];
                }
                else {
                  console.log(`couldn't find team ${team.displayName}`);
                }
                console.log(nhl.abbreviation, nhl, team);
                const id = nhl.id == 124292 ? 40 : nhl.id;
                this.afs.doc('nhl-team/'+id).set(team);
              });
            }
      });
    }
  }

  mapNhlTeams(nhl) {
    let items: any = {};
    if ( nhl ) {
      for (const [key, value] of Object.entries(nhl)) {
        switch(key) {
          case 'id':
          case 'links':
          case 'uid':
          case 'logos':
          case 'slug':
              break;
          case 'displayName':
            items[key] = value === 'Montreal Canadiens' ? 'Montréal Canadiens' : value;
            items['logoName'] = String(value).replace(/[\.\s]+/g,'');
            break;
          default:
            items[key] = value;
        }
      }
    }
    items = Object.entries(items).sort().reduce( (o,[k,v]) => (o[k]=v,o), {} )
    return items;
  }

  getGames() {
    return (this.events||[]);
  }

  async processSchedule(start?: Date, end?: Date) {
    start = start || this.start;
    end = end || this.end;
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.modalService.presentLoading().then(()=>{
      this.http.get(environment.nhl_legacy_api + '/schedule?expand=schedule.linescore&startDate=' + new Date(start).toJSON().slice(0, 10) + '&endDate=' + new Date(end).toJSON().slice(0, 10))
      .subscribe((data: Config) => {
        const games = data['dates'];
        if ( games ) {
          games.forEach((event, index) => {
              let gameFound = false;
              for (let i=0; i<this.events.length; i++) {
                const g = this.events[i];
                if ( g.date === event.date) {
                  console.log('found games:', event.date);
                  gameFound = true;
                  //console.log(event);
                  if ( event.games ) {
                    event.games.forEach((game, id) => {
                      if ( id === 0 ) {
                        //this.updateStandings(game.season);
                      }
                      event.games[id].sport = 'nhl'
                      this.updateGame(game.gamePk);
                    });
                  }
                  if (  !deepEqual(g, event) ) {
                    this.afs.collection('nhl').doc(event.date).set( event )
                    .then(()=> {
                      console.log('event updated', event);
                    });
                  }
                }
              }
              if ( !gameFound ) {
                this.afs.collection('nhl').doc(event.date).set( event )
                .then(()=> {
                  console.log('added', event);
                });
              }
            });
        }
        if ( this.loadingController ) {
          this.loadingController.dismiss();
        }
      });
    });
  }

  setStart(val) {
    this.start = this.datePipe.transform(val, 'yyyy-MM-dd');
  }

  setEnd(val) {
    this.end = this.datePipe.transform(val, 'yyyy-MM-dd');
  }

  public get environment() {
    return environment;
  }

}
