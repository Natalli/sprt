import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NhlPage } from './nhl.page';

describe('NhlPage', () => {
  let component: NhlPage;
  let fixture: ComponentFixture<NhlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NhlPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NhlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
