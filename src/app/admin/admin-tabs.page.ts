import { Component, OnInit, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { ActivityService } from '../services/activity.service';
import { Router } from '@angular/router';
import { LayoutService } from '../services/layout.service';
import { IonIcon } from '@ionic/angular';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-admin-tabs',
  templateUrl: './admin-tabs.page.html',
  styleUrls: ['./admin-tabs.page.scss']
})
export class AdminTabsPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;

  constructor(
    public activityService: ActivityService,
    public router: Router,
    protected layoutService: LayoutService,
    public myapp: AppComponent) {
  }

  ngOnInit() {
   
  }

}
