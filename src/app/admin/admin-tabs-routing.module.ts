import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminTabsPage } from './admin-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: AdminTabsPage,
    children: [
      {
        path: 'invites',
        loadChildren: () => import('./invite/invite.module').then(m => m.AdminInvitePageModule)
      },
      {
        path: 'nhl',
        loadChildren: () => import('./nhl/nhl.module').then(m => m.NhlPageModule)
      },
      {
        path: 'nfl',
        loadChildren: () => import('./nfl/nfl.module').then(m => m.NflPageModule)
      },
      {
        path: 'nba',
        loadChildren: () => import('./nba/nba.module').then(m => m.NbaPageModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./users/users.module').then(m => m.AdminUsersPageModule)
      },
      {
        path: 'banners',
        loadChildren: () => import('./banners/banners.module').then(m => m.AdminBannersPageModule)
      },
      {
        path: 'quotes',
        loadChildren: () => import('./quotes/quotes.module').then(m => m.AdminQuotesPageModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./notifications/notifications.module').then(m => m.AdminNotificationsPageModule)
      },
      {
        path: 'challenges',
        loadChildren: () => import('./challenges/challenges.module').then(m => m.AdminChallengesPageModule)
      },
      {
        path: '',
        redirectTo: '/admin/challenges',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AdminTabsPageRoutingModule {}
