import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NbaPageRoutingModule } from './nba-routing.module';

import { NbaPage } from './nba.page';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NbaPageRoutingModule,
    DirectivesModule
  ],
  declarations: [NbaPage],
  providers: [DatePipe]
})
export class NbaPageModule {}
