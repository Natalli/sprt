import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { LoadingController, Config, Platform, PopoverController, AlertController, ModalController, IonIcon } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { CommonService } from 'src/app/common.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { UserService } from 'src/app/service/user.service';
import { NbaService } from 'src/app/service/nba.service';
import { InviteService } from 'src/app/service/invite.service';
import *  as deepEqual from 'fast-deep-equal';
import { LayoutService } from 'src/app/services/layout.service';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-nba',
  templateUrl: './nba.page.html',
  styleUrls: ['./nba.page.scss'],
})
export class NbaPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  today: any;
  events: any;
  games = {};
  date = Date;
  dayCount = 5;
  start;
  end;
  //start: Date = new Date('2020-01-11');
  //end: Date = new Date('2020-01-13');
  autoReload = false;
  intervalId: number;

  constructor(
    private datePipe: DatePipe,
    public commonService: CommonService,
    private http: HttpClient,
    public router: Router,
    public platform: Platform,
    public loadingController: LoadingController,
    private afs: AngularFirestore,
    public userService: UserService,
    public nbaService: NbaService,
    public inviteService: InviteService,
    public alertController: AlertController,
    public modalController: ModalController,
    protected layoutService: LayoutService,
    protected modalService: ModalService
  ) {
    this.loadSchedule(); // 5 * 2 weeks +
    this.autoUpdate();
    this.start = this.datePipe.transform(new Date(Date.now() - 3600 * 24 * 1000), 'yyyy-MM-dd');
    this.end = this.datePipe.transform(new Date(Date.now() + 1 * 3600 * 24 * 1000), 'yyyy-MM-dd');
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  autoUpdate() {
    this.intervalId = window.setInterval(
      () => {
        if ( this.autoReload ) {
          this.processSchedule();
        }
      }, 15 * 1000);
  }

  async loadSchedule(start?: Date, end?: Date) {
    // console.log('loadSchedule', environment.nba_legacy_api);
    start = start || this.start;
    end = end || this.end;
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.modalService.presentLoading().then(()=>{

      this.afs.collection('nba', ref => ref
        .where('date', '>', '2019-07-20')
        .orderBy('date', 'desc')
        )
        .valueChanges().subscribe((events: Array<any>) => {
          console.log('schedule updated');
          this.events = events;
      });
    });
  }

  loadGame(gamePk: string) {
    this.games[gamePk] = null;
    this.afs.doc('live/'+gamePk)
      .valueChanges().subscribe((game: Array<any>) => {
      console.log('game loaded', 'live/'+gamePk);
      this.games[gamePk] = game;
    });
  }

  persistNbaLive(game: any) {
    if ( game.gamePk ) {
      if ( !this.games || (typeof !this.games[game.gamePk] === 'undefined' && this.games[game.gamePk] !== null ) ) {
        console.log('game not loaded:', game.gamePk);
        this.loadGame(game.gamePk);
        setTimeout(() => {
          this.persistNbaLive(game);
        }, 200);
        return;
      };

      delete game['gameData']['players'];
      game['liveData'] = {linescore: game['liveData']['linescore'], plays: {currentPlay: (game['liveData']['plays']['currentPlay']||{})}};
      //console.log(game);
      if ( !this.games || !this.games[game.gamePk] ) {
        console.log('game not loaded2:', 'live/'+game.gamePk);
        this.loadGame(game.gamePk);
        this.games[game.gamePk] = game;
        this.afs.doc('live/'+game.gamePk).set(game);
      }
      else {
        if ( this.games[game.gamePk].scoringPlays ) {
          game.scoringPlays = this.games[game.gamePk].scoringPlays;
        }
        game.currentDrive = this.games[game.gamePk].currentDrive || '';
        if (  !deepEqual(this.games[game.gamePk], game) ) {
          if ( parseInt(game.liveData?.linescore.teams.home.goals) <  parseInt(this.games[game.gamePk].liveData?.linescore.teams.home.goals) ||
            parseInt(game.liveData?.linescore.teams.away.goals) <  parseInt(this.games[game.gamePk].liveData?.linescore.teams.away.goals) ||
            game.liveData?.plays.currentPlay?.about.periodTimeRemaining < this.games[game.gamePk].liveData?.plays.currentPlay?.about.periodTimeRemaining ) {
            console.log('game data is outdated', game.gamePk);
          }
          else {
            console.log('game has to be updated:', game.gamePk);
            this.afs.doc('live/'+game.gamePk).update(game);
          }
        }
      }
    }
  }

  getGames() {
    return (this.events||[]);
  }

  updateStandings(season?: string) {
    console.log('updateStandings', season);
    if ( true ) {
      this.http.get('https://site.api.espn.com/apis/v2/sports/basketball/nba/standings')
      .subscribe((standings: any) => {
        console.log(standings);
        if ( true ) {
              const afc = standings.children[0].standings.entries
              const nfc = standings.children[1].standings.entries

              console.log(afc);
              afc.forEach(data => {
                const nba = data.team;
                const stats = data.stats;
                console.log(nba.abbreviation, nba, stats);
                const team:any = this.mapNbaStandings(data);
                console.log(team);
                this.afs.doc('nba-stats/'+nba.id).set(team);

              });
              console.log(nfc);
              nfc.forEach(data => {
                const nba = data.team;
                const stats = data.stats;
                console.log(nba.abbreviation, nba, stats);
                const team:any = this.mapNbaStandings(data);
                console.log(team);
                this.afs.doc('nba-stats/'+nba.id).set(team);

              });
            }

        //standings['records'].forEach((standing, index) => {
          //this.afs.doc('standings/'+index).set(standing);
        //})
        //console.log('standings', standings['records']);
      });
    }
  }

  mapNbaStandings(nba) {
    let records: any = {TG: 0};
    if ( nba.stats ) {
      nba.stats.forEach(item => {
        if ( !['W', 'L', 'DT', 'DW', 'GB', 'Any', 'T', 'PPG', 'OPP PPG'].includes(item.abbreviation) ) {
          records[item.shortDisplayName||item.abbreviation] = item.displayValue || item.value;
        }
        if ( item.abbreviation === 'Total' ) {
          records['W/L'] = item.displayValue || item.summary;
        }
        if ( item.abbreviation === 'PPG' ) {
          records['avgPF'] = item.displayValue || item.summary;
        }
        if ( item.abbreviation === 'OPP PPG' ) {
          records['avgPA'] = item.displayValue || item.summary;
        }
        if ( ['W', 'L', 'T'].includes(item.abbreviation) ) {
          records['TG'] += item.value;
        }
      });
    }
    records = Object.entries(records).sort().reduce( (o,[k,v]) => (o[k]=v,o), {} )
    console.log(records);
    return records;
  }

  async processSchedule(start?: Date, end?: Date) {
    start = start || this.start;
    end = end || this.end;

    let nbaService = new NbaService();

    this.modalService.presentLoading().then(()=>{
      this.http.get(environment.nba_legacy_api + 'scoreboard?dates=' + this.datePipe.transform(start, 'yyyyMMdd') + '-' + this.datePipe.transform(end, 'yyyyMMdd'))
      .subscribe((data: any) => {
        nbaService.initEvent();
        if ( data.events ) {
          data.events.forEach((espnEvent: any) => {
            const convertedData = nbaService.processEspnEvent(espnEvent);
            const game:any = convertedData.game;
            const live:any = convertedData.live;
            const gameDate = new Date(game.gameDate)
            if ( !nbaService.nbaEvent.date || nbaService.nbaEvent.date !== new Date(gameDate.getTime() - (gameDate.getTimezoneOffset() * 60000)).toISOString().substr(0, 10) ) {
              if ( nbaService.nbaEvent.date ) {
                this.persistNbaEvent(nbaService.nbaEvent);
              }
              nbaService.initEvent();
              nbaService.nbaEvent.date = new Date(gameDate.getTime() - (gameDate.getTimezoneOffset() * 60000)).toISOString().substr(0, 10);
              nbaService.nbaEvent.games.push(game);
            }
            else {
              nbaService.nbaEvent.games.push(game);
            }
            this.persistNbaLive(live);
          });
          this.persistNbaEvent(nbaService.nbaEvent);
        }
      });
    });
  }

  persistNbaEvent(nbaEvent) {
    const event = this.events.find(event => {
      return event.date === nbaEvent.date;
    });
    if ( event ) { // update exisiting event
      nbaEvent.games.forEach(game => {
        const gameIndex = event.games.findIndex(g => {return g.gamePk === game.gamePk});
        if ( gameIndex !== -1 ) {
          event.games[gameIndex] = game;
        }
        else {
          event.games.push(game);
        }
        // TODO: deep equal to include updated objects only
      });
      return this.afs.collection('nba').doc(event.date).update({games: event.games});
    }
    else if (nbaEvent.date) { // add new event
      return this.afs.collection('nba').doc(nbaEvent.date).set( Object.assign({}, nbaEvent) );
    }
  }

  setStart(val) {
    this.start = this.datePipe.transform(val, 'yyyy-MM-dd');
  }

  setEnd(val) {
    this.end = this.datePipe.transform(val, 'yyyy-MM-dd');
  }

  public get environment() {
    return environment;
  }

}
