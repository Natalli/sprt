import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminBannersPage } from './banners.page';

const routes: Routes = [
  {
    path: '',
    component: AdminBannersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminBannersPageRoutingModule {}
