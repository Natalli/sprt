import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminBannersPageRoutingModule } from './banners-routing.module';

import { DirectivesModule } from 'src/app/directives/directives.module';
import { AdminBannersPage } from './banners.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DirectivesModule,
    AdminBannersPageRoutingModule
  ],
  declarations: [
    AdminBannersPage
  ]
})
export class AdminBannersPageModule {}
