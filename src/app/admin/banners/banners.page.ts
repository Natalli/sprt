import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { LayoutService } from '../../services/layout.service';
import { IonIcon } from '@ionic/angular';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/compat/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-admin-banners',
  templateUrl: './banners.page.html',
  styleUrls: ['./banners.page.scss'],
})
export class AdminBannersPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;

  public BannerForm: FormGroup;

  banners: Array<any> = [];
  bannersBySport: Array<any> = [];
  pageBanners: Array<any> = [];
  refBanners: Array<any> = [];
  pageSelected: any;
  editedBanner: Object;
  changedValue: boolean;
  pages = [{ id: 'home', value: 'Home page' }, { id: 'login', value: 'Login page' }, { id: 'trivia', value: 'Trivia page' }];
  sports = [{ name: 'all' }, { name: 'nhl' }, { name: 'nfl' }, { name: 'nba' }];

  customAlertOptions: any = {
    header: 'Select page',
    translucent: true
  };
  customAlertOptionsSport: any = {
    header: 'Select sport',
    translucent: true
  };
  customAlertOptionsC: any = {
    header: 'Select Challenge',
    translucent: true
  };
  percentage: Observable<number>;
  percent: number = 0;
  PercentageEmitter = new BehaviorSubject<number>(this.percent);

  task: AngularFireUploadTask;
  snapshot: Observable<any>;
  snap: any = {};
  SnapshotEmitter = new BehaviorSubject<any>(this.snap);
  isShowProgress: boolean = false;
  ShowProgressEmitter = new BehaviorSubject<boolean>(this.isShowProgress);

  url: string = '';
  UrlEmitter = new BehaviorSubject<string>(this.url);
  date: string;
  segmentSelected: string = 'banners';
  validationImage: boolean;
  validationLink: boolean;
  validationPage: boolean;
  path: string = '';
  link: string = '';
  challenges: any[];
  challengesBySport: any[];
  challenge: any;
  sport: any;

  constructor(
    private formBuilder: FormBuilder,
    private afs: AngularFirestore,
    protected layoutService: LayoutService,
    private storage: AngularFireStorage,
    protected modalService: ModalService
  ) {
    let year = new Date(Date.now()).getFullYear();
    let month = new Date(Date.now()).getMonth() + 1;
    let day = new Date(Date.now()).getDate();
    this.date = year.toString() + month.toString() + day.toString();
    this.banners = [];
  }

  ngOnInit() {
    this.loadBanners();
    this.createBannersForm();
    this.pageSelected = 'home';
    this.select();
    this.loadChallenges();
  }

  loadChallenges() {
    this.afs.collection("challenge", ref => ref
      .where('start', '>', new Date("2019-11-01"))
      .where('start', '<', new Date(Date.now() + 30 * 24 * 60 * 60000))
    )
      .valueChanges({ idField: 'id' }).subscribe(challenges => {
        this.challenges = [];

        challenges.forEach((challenge: any, index) => {

          this.challenges = this.challenges || [];
          if (challenge['start']['seconds'] * 1000 < (Date.now() - 10 * 24 * 60 * 60000)) {
            this.challenges[index] = Object.assign({ name: '', text: '', games: [] }, challenge);
          }
          else {
            this.challenges[index] = Object.assign({ name: '', text: '', games: [] }, challenge);
          }
        });
        this.challenges.reverse();
        this.sport = 'all';
        this.sortChellenges(this.sport);
      });

  }

  loadBanners() {
    this.storage.ref('/banner/').listAll().subscribe(res => {
      this.refBanners = res.items;
      this.refBanners.forEach((banner, index) => {
        banner.getDownloadURL().then((item) => {
          this.banners.push({ id: index, src: item, path: banner.fullPath });
          this.banners = this.banners.sort((a, b) => a.id - b.id);
        })
      })
    })
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  createBannersForm() {
    this.BannerForm = this.formBuilder.group({
      page: ['', Validators.required],
      link: ['', Validators.required],
      image: ['', Validators.required]
    });
  }

  submitForm() {
    // console.log(this.BannerForm.value)
    if (!this.BannerForm.value.image) {
      this.validationImage = true;
    }
    if (!this.link) {
      this.validationLink = true;
    }
    if (!this.BannerForm.value.page) {
      this.validationPage = true;
    }
    if (this.BannerForm.valid) {
      // console.log('valid');
      this.afs.collection('/banner/' + this.BannerForm.value.page + '/slides').add(this.BannerForm.value);
      this.modalService.confirmModal('Success', 'Banner loaded successfully');
      this.segmentSelected = 'banners';
      this.pageSelected = this.BannerForm.value.page;
      this.validationImage = false;
      this.validationPage = false;
      this.validationLink = false;
      this.select();
      this.BannerForm.reset();
      this.path = '';
      this.challenge = null;
    } else {
      // console.log('not v')
    }
    if (this.url) {
      this.BannerForm.value.image = this.url;
    }
    // console.log(this.BannerForm.value)

  }

  addImage(event) {
    const file = event.target.files[0];
    const path = `banner/banner-${this.date}-${file['name']}`;
    const fireRef = this.storage.ref(path);

    this.task = fireRef.put(file);
    this.task.then(resp => {
      if (resp.state === 'success') {
        this.ShowProgressEmitter.next(this.isShowProgress = true);
      }
    }).catch(err => {
      console.log(err.message)
    })
    this.percentage = this.task.percentageChanges();
    this.percentage.subscribe(item => {
      this.PercentageEmitter.next(item);
    });

    this.snapshot = this.task.snapshotChanges().pipe(
      tap(console.log),
      finalize(async () => {
        await this.storage.ref(path).getDownloadURL()
          .subscribe(resp => {
            this.url = resp;
            this.banners.push({ id: this.banners.length, src: resp, path: this.url });

            this.UrlEmitter.next(resp);
            setTimeout(() => {
              this.ShowProgressEmitter.next(this.isShowProgress = false);
            }, 1000)
            this.modalService.confirmModal('Success', 'Image uploaded successfully');
            this.BannerForm.get('image').setValue(resp);
          }, error => {
            console.log(error, 'ERROR')
          });
      }),
    );
    this.snapshot.subscribe(snap => {
      this.SnapshotEmitter.next(snap);
    })

  }

  removeImage(path) {
    this.banners.find((item, index) => {
      if (item && item.path === path) {
        this.banners.splice(index, 1)
      }
    })
    this.storage.ref(path).delete();
    this.modalService.confirmModal('Confirm', 'Image removed');
  }

  select() {
    this.afs.collection('/banner/' + this.pageSelected + '/slides').valueChanges({ idField: 'id' }).subscribe((banners: any) => {
      this.pageBanners = banners;
    })
  }

  deleteBanner(id) {
    this.afs.collection('/banner/' + this.pageSelected + '/slides').doc(id).delete();
    this.modalService.confirmModal('Confirm', 'Banner removed');
  }

  selectedBanner(banner) {
    this.path = banner.path;
  }

  sortChellenges(sport) {
    this.challenges.sort((a, b) => new Date(a['start']).getTime() - new Date(b['start']).getTime());
    if (sport === 'all') {
      this.bannersBySport = this.banners;
      this.challengesBySport = this.challenges.filter(challenge => (!challenge.disabled) ? challenge : null);
    } else {
      this.bannersBySport = this.banners.filter(banner => banner.path.includes(sport) ? banner : null);
      this.challengesBySport = this.challenges.filter(challenge => (!challenge.disabled && challenge.sport === sport) ? challenge : null);
    }
  }

  changed() {
    if (this.challenge) {
      const link = '/' + this.challenge.sport + '/' + this.challenge.id;
      this.BannerForm.get('link').setValue(link);
    }
  }

  segmentChanged(ev: any) {
    this.segmentSelected = ev.detail.value;
  }

  changedSport() {
    this.sortChellenges(this.sport);
  }
}