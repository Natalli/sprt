import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DirectivesModule } from '../directives/directives.module';
import { AdminTabsPage } from './admin-tabs.page';
import { AdminTabsPageRoutingModule } from './admin-tabs-routing.module';
import { AdminInvitePageModule } from '../admin/invite/invite.module';
import { NhlPageModule } from '../admin/nhl/nhl.module';
import { NflPageModule } from '../admin/nfl/nfl.module';
import { NbaPageModule } from '../admin/nba/nba.module';
import { AdminUsersPageModule } from '../admin/users/users.module';
import { AdminBannersPageModule } from '../admin/banners/banners.module';
import { AdminQuotesPageModule } from '../admin/quotes/quotes.module';
import { AdminNotificationsPageModule } from '../admin/notifications/notifications.module';
import { AdminChallengesPageModule } from '../admin/challenges/challenges.module';

@NgModule({
  declarations: [
    AdminTabsPage
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    DirectivesModule,
    AdminTabsPageRoutingModule,
    AdminInvitePageModule,
    NhlPageModule,
    NflPageModule,
    NbaPageModule,
    AdminUsersPageModule,
    AdminBannersPageModule,
    AdminQuotesPageModule,
    AdminNotificationsPageModule,
    AdminChallengesPageModule
  ],
  providers: [DatePipe]
})
export class AdminTabsPageModule { }
