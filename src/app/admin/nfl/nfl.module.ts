import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NflPageRoutingModule } from './nfl-routing.module';

import { NflPage } from './nfl.page';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NflPageRoutingModule,
    DirectivesModule
  ],
  declarations: [NflPage],
  providers: [DatePipe]
})
export class NflPageModule {}
