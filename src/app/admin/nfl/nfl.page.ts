import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { LoadingController, Config, Platform, PopoverController, AlertController, ModalController, IonIcon } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
import { CommonService } from 'src/app/common.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { UserService } from 'src/app/service/user.service';
import { NflService } from 'src/app/service/nfl.service';
import { InviteService } from 'src/app/service/invite.service';
import *  as deepEqual from 'fast-deep-equal';
import { LayoutService } from 'src/app/services/layout.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-nfl',
  templateUrl: './nfl.page.html',
  styleUrls: ['./nfl.page.scss'],
})
export class NflPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  today: any;
  events: any;
  games = {};
  date = Date;
  dayCount = 5;
  start;
  end;
  //start: Date = new Date('2020-01-11');
  //end: Date = new Date('2020-01-13');
  autoReload = false;
  intervalId: number;

  constructor(
    private datePipe: DatePipe,
    public commonService: CommonService,
    private http: HttpClient,
    public router: Router,
    public platform: Platform,
    public loadingController: LoadingController,
    private afs: AngularFirestore,
    public userService: UserService,
    public nflService: NflService,
    public inviteService: InviteService,
    public alertController: AlertController,
    public modalController: ModalController,
    protected layoutService: LayoutService,
    private modalService: ModalService
  ) {
    this.loadSchedule(); // 5 * 2 weeks +
    this.autoUpdate();
    this.start = this.datePipe.transform(new Date(Date.now() - 3600 * 24 * 1000), 'yyyy-MM-dd');
    this.end = this.datePipe.transform(new Date(Date.now() + 1 * 3600 * 24 * 1000), 'yyyy-MM-dd');
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  autoUpdate() {
    this.intervalId = window.setInterval(
      () => {
        if ( this.autoReload ) {
          this.processSchedule();
        }
      }, 15 * 1000);
  }

  async loadSchedule(start?: Date, end?: Date) {
    console.log('loadSchedule', environment.nfl_legacy_api);
    start = start || this.start;
    end = end || this.end;
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.modalService.presentLoading().then(()=>{

      this.afs.collection('nfl', ref => ref
        .where('date', '>', '2019-07-20')
        .orderBy('date', 'desc')
        )
        .valueChanges().subscribe((events: Array<any>) => {
          console.log('schedule updated');
          this.events = events;
      });
    });
  }

  loadGame(gamePk: string) {
    this.games[gamePk] = null;
    this.afs.doc('live/'+gamePk)
      .valueChanges().subscribe((game: Array<any>) => {
      console.log('game loaded', 'live/'+gamePk);
      this.games[gamePk] = game;
    });
  }

  persistNflLive(game: any) {
    if ( game.gamePk ) {
      if ( !this.games || (typeof !this.games[game.gamePk] === 'undefined' && this.games[game.gamePk] !== null ) ) {
        console.log('game not loaded:', game.gamePk);
        this.loadGame(game.gamePk);
        setTimeout(() => {
          this.persistNflLive(game);
        }, 200);
        return;
      };

      delete game['gameData']['players'];
      game['liveData'] = {linescore: game['liveData']['linescore'], plays: {currentPlay: (game['liveData']['plays']['currentPlay']||{})}};
      //console.log(game);
      if ( !this.games || !this.games[game.gamePk] ) {
        console.log('game not loaded2:', 'live/'+game.gamePk);
        this.loadGame(game.gamePk);
        this.games[game.gamePk] = game;
        this.afs.doc('live/'+game.gamePk).set(game);
      }
      else {
        if ( this.games[game.gamePk].scoringPlays ) {
          game.scoringPlays = this.games[game.gamePk].scoringPlays;
        }
        game.currentDrive = this.games[game.gamePk].currentDrive || '';
        if (  !deepEqual(this.games[game.gamePk], game) ) {
          if ( parseInt(game.liveData?.linescore.teams.home.goals) <  parseInt(this.games[game.gamePk].liveData?.linescore.teams.home.goals) ||
            parseInt(game.liveData?.linescore.teams.away.goals) <  parseInt(this.games[game.gamePk].liveData?.linescore.teams.away.goals) ||
            game.liveData?.plays.currentPlay?.about.periodTimeRemaining < this.games[game.gamePk].liveData?.plays.currentPlay?.about.periodTimeRemaining ) {
            console.log('game data is outdated', game.gamePk);
          }
          else {
            console.log('game has to be updated:', game.gamePk);
            this.afs.doc('live/'+game.gamePk).update(game);
          }
        }
      }
    }
  }

  getGames() {
    return (this.events||[]);
  }

  updateStandings(season?: string) {
    console.log('updateStandings', season);
    if ( true ) {
      this.http.get('https://site.api.espn.com/apis/v2/sports/football/nfl/standings')
      .subscribe((standings: any) => {
        console.log(standings);
        if ( true ) {
              const afc = standings.children[0].standings.entries
              const nfc = standings.children[1].standings.entries

              console.log(afc);
              afc.forEach(data => {
                const nfl = data.team;
                const stats = data.stats;
                console.log(nfl.abbreviation, nfl, stats);
                const team:any = this.mapNflStandings(data);
                console.log(team);
                this.afs.doc('nfl-stats/'+nfl.id).set(team);

              });
              console.log(nfc);
              nfc.forEach(data => {
                const nfl = data.team;
                const stats = data.stats;
                console.log(nfl.abbreviation, nfl, stats);
                const team:any = this.mapNflStandings(data);
                console.log(team);
                this.afs.doc('nfl-stats/'+nfl.id).set(team);

              });
            }

        //standings['records'].forEach((standing, index) => {
          //this.afs.doc('standings/'+index).set(standing);
        //})
        //console.log('standings', standings['records']);
      });
    }
  }

  updateTeams(season?: string) {
    console.log('updateTeams', season);
    if ( true ) {
      this.http.get('http://site.api.espn.com/apis/site/v2/sports/football/nfl/teams')
      .subscribe((data: any) => {
        if ( true ) {
              const teams = data.sports[0].leagues[0].teams;
              teams.forEach(el => {
                const nfl = el.team;
                const team:any = this.mapNflTeams(nfl);
                console.log(nfl.abbreviation, nfl, team);
                const id = nfl.id == 124292 ? 40 : nfl.id;
                this.afs.doc('nfl-team/'+nfl.id).set(team);
              });
            }
      });
    }
  }

  mapNflStandings(nfl) {
    let records: any = {TG: 0};
    if ( nfl.stats ) {
      nfl.stats.forEach(item => {
        if ( !['W', 'L', 'DT', 'DW', 'GB', 'Any', 'T'].includes(item.abbreviation) ) {
          records[item.shortDisplayName||item.abbreviation] = item.displayValue || item.value;
        }
        if ( item.abbreviation === 'Any' ) {
          records['W/L'] = item.displayValue || item.value;
        }
        if ( ['W', 'L', 'T'].includes(item.abbreviation) ) {
          records['TG'] += item.value;
        }
      });
    }
    records = Object.entries(records).sort().reduce( (o,[k,v]) => (o[k]=v,o), {} )
    console.log(records);
    return records;
  }

  mapNflTeams(nfl) {
    let items: any = {};
    if ( nfl ) {
      for (const [key, value] of Object.entries(nfl)) {
        switch(key) {
          case 'id':
          case 'links':
          case 'uid':
          case 'logos':
          case 'slug':
            break;
          case 'name':
            items[key] = value === 'Golden Knights' ? 'G. Knights' : value;
            break;
          case 'displayName':
            items[key] = value;
            items['logoName'] = String(value).replace(/[\.\s]+/g,'');
            break;
          default:
            items[key] = value;
        }
      }
    }
    items = Object.entries(items).sort().reduce( (o,[k,v]) => (o[k]=v,o), {} )
    return items;
  }

  async processSchedule(start?: Date, end?: Date) {
    start = start || this.start;
    end = end || this.end;

    let nflService = new NflService();

    this.modalService.presentLoading().then(()=>{
      this.http.get(environment.nfl_legacy_api + 'scoreboard?dates=' + this.datePipe.transform(start, 'yyyyMMdd') + '-' + this.datePipe.transform(end, 'yyyyMMdd'))
      .subscribe((data: any) => {
        nflService.initEvent();
        if ( data.events ) {
          data.events.forEach((espnEvent: any) => {
            const convertedData = nflService.processEspnEvent(espnEvent);
            const game:any = convertedData.game;
            const live:any = convertedData.live;
            const gameDate = new Date(game.gameDate)
            if ( !nflService.nflEvent.date || nflService.nflEvent.date !== new Date(gameDate.getTime() - (gameDate.getTimezoneOffset() * 60000)).toISOString().substr(0, 10) ) {
              if ( nflService.nflEvent.date ) {
                this.persistNflEvent(nflService.nflEvent);
              }
              nflService.initEvent();
              nflService.nflEvent.date = new Date(gameDate.getTime() - (gameDate.getTimezoneOffset() * 60000)).toISOString().substr(0, 10);
              nflService.nflEvent.games.push(game);
            }
            else {
              nflService.nflEvent.games.push(game);
            }
            this.persistNflLive(live);
          });
          this.persistNflEvent(nflService.nflEvent);
        }
      });
    });
  }

  persistNflEvent(nflEvent) {
    const event = this.events.find(event => {
      return event.date === nflEvent.date;
    });
    if ( event ) { // update exisiting event
      nflEvent.games.forEach(game => {
        const gameIndex = event.games.findIndex(g => {return g.gamePk === game.gamePk});
        if ( gameIndex !== -1 ) {
          event.games[gameIndex] = game;
        }
        else {
          event.games.push(game);
        }
        // TODO: deep equal to include updated objects only
      });
      return this.afs.collection('nfl' ).doc(event.date).update({games: event.games});
    }
    else if (nflEvent.date) { // add new event
      return this.afs.collection('nfl').doc(nflEvent.date).set( Object.assign({}, nflEvent) );
    }
  }

  setStart(val) {
    this.start = this.datePipe.transform(val, 'yyyy-MM-dd');
  }

  setEnd(val) {
    this.end = this.datePipe.transform(val, 'yyyy-MM-dd');
  }

  public get environment() {
    return environment;
  }

}
