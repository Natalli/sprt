import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NflPage } from './nfl.page';

describe('NflPage', () => {
  let component: NflPage;
  let fixture: ComponentFixture<NflPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NflPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NflPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
