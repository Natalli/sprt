import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NflPage } from './nfl.page';

const routes: Routes = [
  {
    path: '',
    component: NflPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NflPageRoutingModule {}
