import { DatePipe } from '@angular/common';
import { Component, ElementRef, Input, OnInit, Output, QueryList, ViewChildren, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { ActivatedRoute, Router } from '@angular/router';
import { IonIcon, LoadingController, PopoverController } from '@ionic/angular';
import { CommonService } from 'src/app/common.service';
import { EngineService } from 'src/app/engine.service';
import { UsersService } from 'src/app/service/users.service';
import { LayoutService } from 'src/app/services/layout.service';
import firebase from 'firebase/compat/app';
import { UserService } from 'src/app/service/user.service';
import { ModalBatchNbaComponent } from 'src/app/popups/new/modal-batch-nba/modal-batch-nba.component';
import { ModalTeamComponent } from 'src/app/popups/new/modal-team/modal-team.component';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-admin-list-challenges',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class AdminListChallengesPage implements OnInit {
  @Input() isFriendsManage: boolean;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  @Output() onSelectedChallenge: EventEmitter<any> = new EventEmitter();
  currentDate = new Date();

  userBids: Object = null;
  user: any;

  challenge_id: string;
  challenges: Array<Object> = [];
  challenge: any = {};
  history: Array<Object> = [];
  games: Array<Object> = [];
  users: Object = {};
  notificationUsers: Object = {};
  searchUsers: Object = {};
  stats: Object = {};
  challengeWinners: Object = {};
  teams: Object;
  teams2: Object = {};
  bids: Object = {};

  objectKeys = Object.keys;
  window = window;
  alert = alert;
  date = Date;
  today = '';
  isLoading: any;
  intervalId: any = '';
  public autoShuffle = false;
  public adminTab = 'notifications';
  public notificationTab = 'past';
  public challengeTab = 'info';
  target = 'platform';
  notifications: any[];
  search: any;
  max: number;
  selectedChallenge: any;
  assignDisabled = false;

  constructor(private route: ActivatedRoute, 
    public router: Router, 
    private afs: AngularFirestore,
    private datePipe: DatePipe,
    public commonService: CommonService,
    public engineService: EngineService,
    public loadingController: LoadingController, 
    private fns: AngularFireFunctions, 
    protected layoutService: LayoutService, 
    public usersService: UsersService,
    protected popoverCtrl: PopoverController,
    private userService: UserService,
    protected modalService: ModalService) {
    this.loadUser();
    this.users = this.usersService.users;
    this.init();
  }

  ngOnInit() {
    this.loadChallenges(); 
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  ionViewDidEnter() {
    //this.init();
  }

  ionViewWillLeave() {
    if (this.intervalId) {
      //clearTimeout(this.intervalId);
    }
  }

  init() {
    this.route.params.subscribe(params => {
      if(params.id) {
       
          this.challenge_id = params.id;
          //this.loadTeams(); // Do not preload all Teams
      }
      this.loadChallenges();
      if (params.id) {
        this.loadUsers(null); // load all users
      }
    });
  }

  challengeTabChanged($ev) {
    if ($ev) {
      this.challengeTab = $ev.detail.value || 'info';
    }
  }

  capitalize(text) { // TODO: use Service instead
    return text.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
  }

  loadBids(gamePk) {
    if (!this.bids || !this.bids[gamePk]) {
      const legacyBid = (parseInt(gamePk) > 2019020402 && parseInt(gamePk) < 2019020565);
      this.afs.collection("bid/").doc(String(gamePk)).valueChanges().subscribe((item: object) => {
        if (legacyBid) {
          Object.values(item).forEach(team => {
            this.bids[gamePk] = Object.assign({}, this.bids[gamePk], team);
          });
        } else {
          this.bids[gamePk] = item;
        }
      });
    }
    return this.bids[gamePk];
  }

  processGames() {
    if (this.games && this.challenges && this.teams2) {
      if (this.challenge.games) {
        this.challenge.games.forEach(gamePk => {
          let start = new Date(this.challenge['start']['seconds']*1000 - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
          this.games.forEach(stats => {
            if (stats['date'] == start) {
              if (stats['games']) {
                let bids = this.loadBids(gamePk);
                if (!bids) {
                  return;
                }
                stats['games'].forEach(game => {
                  if (game['gamePk'] == gamePk && game.status && ['1'].includes(game.status.statusCode) && this.teams2[game['gamePk']]) {
                    //console.log('game has not started');
                  }
                  else if (game['gamePk'] == gamePk && game.status && ['6', '7'].includes(game.status.statusCode) && this.teams2[game['gamePk']]) {
                    // GAME OVER PROCESSING ENABLED
                    if (this.teams2[game['gamePk']][0] && this.teams2[game['gamePk']][0]['winners']
                      && this.teams2[game['gamePk']][0]['winners'].length === 0) {
                      this.engineService.process2GameOver(game['gamePk'], game, bids);
                    } else {
                      //
                    }
                  }
                  else if (game['gamePk'] == gamePk && game.status && !['1'].includes(game.status.statusCode) && this.teams2[game['gamePk']]
                    && new Date(Date.parse(game.gameDate)) < new Date(Date.now() + 10 * 60000)
                  ) {
                    if (this.autoShuffle) // GAME LOCKING AND SHUFFLING DISABLED BY DEFAULT
                      if (this.teams2[game['gamePk']] && (typeof this.teams2[game['gamePk']][0].locked === 'undefined' || !this.teams2[game['gamePk']][0].locked)) {
                        console.log('locking game bidding and shuffling Teams for game ' + game['gamePk']);
                        this.modalService.presentLoading();
                        this.challenge.games.forEach((shuffleGamePk, index) => {
                          setTimeout(() => {
                            this.shuffleTeam(JSON.parse(JSON.stringify(shuffleGamePk)), false, index / 10, true, this.challenge.sport === 'nba');
                          }, 100);
                        });
                        this.loadingController.dismiss();
                      } else {
                        console.log('game ' + game['gamePk'] + ' team is already locked');
                      }
                  }
                });
              }
            }
          });
        });
      } else {
        setTimeout(() => {
          if (this.challenge.games) {
            this.processGames();
          }
        }, 500);
      }
    }
  }

  fixMissingBids(missingUsers) {
    let status = '';
    new Promise(resolve => {
      this.challenge.games.forEach(gamePk => {
        let teamUsers = []
        Object.keys(this.teams2[gamePk]).forEach(team_id => {
          teamUsers = teamUsers.concat(this.teams2[gamePk][team_id].users);
        });
        const recoveries = missingUsers.filter(x => !teamUsers.includes(x));
        if ( recoveries && recoveries.length ) {
          recoveries.forEach(recovery => {
            console.log('recovering user ' + recovery + ' for game ' + gamePk);
            if ( this.bids[gamePk][recovery] ) {
              console.log('found user ' + recovery + ' bids for game ' + gamePk);
              this.afs.collection("team/").doc(String(gamePk)).update({ ['0.users']: firebase.firestore.FieldValue.arrayUnion(...recoveries) }).finally(() => {
                console.log('recovered user ' + recoveries + ' to game ' + gamePk);
                status += 'user ' + recovery + ' game ' + gamePk + " reference recovered\n";
                if ( typeof this.users[recovery].games[gamePk] === 'undefined' ) {
                  console.log('found user ' + recovery + ' without game '+gamePk+' reference');
                  this.afs.collection("user").doc(String(recovery)).update({ ['games.' + gamePk]: '0' }).finally(() => {
                    console.log('user ' + recovery + ' game ' + gamePk + ' reference recovered');
                    status += 'user ' + recovery + ' game ' + gamePk + " reference recovered\n";
                    alert(status);
                  })
                }
                else {
                  alert(status);
                }
              });
            }
          });
        }
      });
    }).finally(() => {
    });
  }

  customNotification() {
    let allUsers = [];
    if (this.games && this.challenges && this.teams2) {
      if (this.challenge.games) {
        this.challenge.games.forEach(gamePk => {
          if (true) {
            if (this.teams2[gamePk] && Object.keys(this.teams2[gamePk]).length > 0) {
              let users = [];
              Object.keys(this.teams2[gamePk]).forEach(team_id => {
                users = users.concat(this.teams2[gamePk][team_id].users || []); // TODO: Check if winners calculated
              });
              allUsers = [...new Set([...allUsers, ...users])];
            }
          }
        });
        console.log('allUsers', allUsers);
        const title = prompt('Enter Notification Title', 'Game has been canceled');
        if (title) {
          console.log('title', title);
          const body = prompt('Enter Notification Text', 'A Game has been cancelled due to COVID.  Please come on back to the SPRT MTRX and bid on a new Game.');
          console.log('text', body);
          if (body) {
            if (confirm('Would you like to send it to all Challenge bidders?')) {
              console.log('SEND', { tokens: '', title, body, gotoUrl: ('/' + this.challenge.sport + '/' + this.challenge.id) });
              const callable = this.fns.httpsCallable('sendNativeNotification');
              allUsers.forEach(uid => {
                let user = this.getUser(uid);
                setTimeout(() => {
                  user = this.users[uid];
                  if (user.tokens) {
                    Object.values(user.tokens).forEach(token => {
                      console.log('sending it to ', token);
                      callable({ tokens: token, title, body, gotoUrl: ('/' + this.challenge.sport + '/' + this.challenge.id) })
                        .toPromise().then((resp) => {
                          console.log(resp);
                        })
                        .catch(error => {
                          console.log('Cloud function failed', error);
                        })
                    })
                  }
                }, (!user || !user.id) ? 3000 : 0);
              });
              return;
            }
          }
          return;
        }
      }
    }
  }

  calcMissingBids(excludedOnly?) {
    let allowedUsers = [];
    let challengeUsers = {};
    let allUsers = [];
    if (this.games && this.challenges && this.teams2) {
      if (this.challenge.games) {
        this.challenge.games.forEach(gamePk => {
          if (true) {
            if (this.teams2[gamePk] && Object.keys(this.teams2[gamePk]).length > 0) {
              let users = [];
              Object.keys(this.teams2[gamePk]).forEach(team_id => {
                users = users.concat(this.teams2[gamePk][team_id].users || []); // TODO: Check if winners calculated
              });
              challengeUsers[gamePk] = users;
              allUsers = [...new Set([...allUsers, ...users])];
            }
          }
        });
        Object.values(challengeUsers).forEach((gameUsers: Array<string>) => {
          if (allowedUsers.length === 0) {
            allowedUsers = gameUsers;
          } else {
            allowedUsers = allowedUsers.filter(value => gameUsers.includes(value));
          }
        });
      }
    }

    let max_users = this.engineService.calcMaxUsers(allowedUsers);
    let teams_count = Math.floor(allowedUsers.length / max_users);
    let removedUsers = [];
    if (allowedUsers.length > max_users * teams_count) {
      removedUsers = allowedUsers.slice(max_users * teams_count);
    }

    const callable = this.fns.httpsCallable('sendNativeNotification');
    if (!excludedOnly) {

      let confirmation = "Missing Bids reminder is being sent to:\n";
      const missingBids = allUsers.filter(x => !allowedUsers.includes(x));
      const fixedReport = this.fixMissingBids(missingBids);
      if( missingBids && missingBids.length ) {
        missingBids.forEach(uid => {
          let user = this.getUser(uid);
          confirmation += user.name + ' (' + user.id + ')' + (!user.tokens || !Object.values(user.tokens).length ? ' - Missing Tokens' : '') + "\n";
        });
        confirmation += "Would you like to proceed?\n";
        if (!confirm(confirmation)) {
          return;
        }
      }
      else {
        if (confirm('No missing bids') || true) {
          return;
        }
      }

      missingBids.forEach(uid => {
        let user = this.getUser(uid);
        setTimeout(() => {
          user = this.users[uid];
          if (user.tokens) {
            Object.values(user.tokens).forEach(token => {
              callable({ tokens: token, title: 'Missing Bids', body: 'You have to place your bids for all games to participate in the Challenge', gotoUrl: ('/' + this.challenge.sport + '/' + this.challenge.id) })
                .toPromise().then((resp) => {
                  console.log(resp);
                })
                .catch(error => {
                  console.log('Cloud function failed', error);
                })
            })
          }
        }, (!user || !user.id) ? 3000 : 0);
      });
    }
    else {

      let confirmation = "The following users will receive Bids Excluded notification:\n";
      removedUsers.forEach(uid => {
        let user = this.getUser(uid);
        confirmation += user.name + "\n";
      });
      confirmation += "\n";
      confirmation += "Would you like to proceed?\n";
      if (!confirm(confirmation)) {
        return;
      }

      removedUsers.forEach(uid => {
        let user = this.getUser(uid);
        setTimeout(() => {
          user = this.users[uid];
          if (user.tokens) {
            Object.values(user.tokens).forEach(token => {
              callable({ tokens: token, title: 'Excluded From Challenge', body: 'You have been excluded from the Challenge due to random team assignement' })
                .toPromise().then((resp) => {
                  console.log(resp);
                })
                .catch(error => {
                  console.log('Cloud function failed', error);
                })
            })
          }
        }, (!user || !user.id) ? 3000 : 0);
      });
    }

  }

  calcExcludedBids() {
    let allowedUsers = [];
    let challengeUsers = {};
    let allUsers = [];
    if (this.games && this.challenges && this.teams2) {
      if (this.challenge.games) {
        this.challenge.games.forEach(gamePk => {
          if (true) {
            if (this.teams2[gamePk] && Object.keys(this.teams2[gamePk]).length > 0) {
              let users = [];
              Object.keys(this.teams2[gamePk]).forEach(team_id => {
                users = users.concat(this.teams2[gamePk][team_id].users || []); // TODO: Check if winners calculated
              });
              challengeUsers[gamePk] = users;
              allUsers = [...new Set([...allUsers, ...users])];
            }
          }
        });
        Object.values(challengeUsers).forEach((gameUsers: Array<string>) => {
          if (allowedUsers.length === 0) {
            allowedUsers = gameUsers;
          } else {
            allowedUsers = allowedUsers.filter(value => gameUsers.includes(value));
          }
        });
      }
    }

    let max_users = this.engineService.calcMaxUsers(allowedUsers);
    let teams_count = Math.floor(allowedUsers.length / max_users);
    let removedUsers = [];
    if (allowedUsers.length > max_users * teams_count) {
      removedUsers = allowedUsers.slice(max_users * teams_count);
    }

    let confirmation = "Missing Bids reminder is being sent to:\n";
    const missingBids = allUsers.filter(x => !allowedUsers.includes(x));
    missingBids.forEach(uid => {
      let user = this.getUser(uid);
      confirmation += user.name + "\n";
    });
    confirmation += "\n";
    confirmation += "The following users are going to be excluded:\n";
    removedUsers.forEach(uid => {
      let user = this.getUser(uid);
      confirmation += user.name + "\n";
    });
    confirmation += "\n";
    confirmation += "Would you like to proceed?\n";

    if (!confirm(confirmation)) {
      return;
    }

    const callable = this.fns.httpsCallable('sendNativeNotification');
    missingBids.forEach(uid => {
      let user = this.getUser(uid);
      setTimeout(() => {
        user = this.users[uid];
        if (user.tokens) {
          Object.values(user.tokens).forEach(token => {
            callable({ tokens: token, title: 'Missing Bids', body: 'You have to place your bids for all games to participate in the Challenge', gotoUrl: ('/' + this.challenge.sport + '/' + this.challenge.id) })
              .toPromise().then((resp) => {
                console.log(resp);
              })
              .catch(error => {
                console.log('Cloud function failed', error);
              })
          })
        }
      }, (!user || !user.id) ? 3000 : 0);
    });

    removedUsers.forEach(uid => {
      let user = this.getUser(uid);
      setTimeout(() => {
        user = this.users[uid];
        if (user.tokens) {
          Object.values(user.tokens).forEach(token => {
            callable({ tokens: token, title: 'Excluded From Challenge', body: 'You have been excluded from the Challenge due to random team assignement' })
              .toPromise().then((resp) => {
                console.log(resp);
              })
              .catch(error => {
                console.log('Cloud function failed', error);
              })
          })
        }
      }, (!user || !user.id) ? 3000 : 0);
    });
  }

  shuffleAllTeams() {

    let start = new Date(this.challenge['start']['seconds']*1000 - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    this.games.forEach(data => {
      if (data['games'] && data['date'] == start) {
        data['games'].forEach((game, index) => {
          if (this.teams2[game['gamePk']] && (typeof this.teams2[game['gamePk']][0].locked === 'undefined' || !this.teams2[game['gamePk']][0].locked)) {
            this.modalService.presentLoading();
            setTimeout(() => {
              console.log('locking game bidding and shuffling Teams for game ' + game['gamePk']);
              this.shuffleTeam(JSON.parse(JSON.stringify(game['gamePk'])), false, index / 10, true, this.challenge.sport === 'nba');
            }, index * 1000);
            this.loadingController.dismiss();
          } else {
            console.log('game ' + game['gamePk'] + ' team is already locked');
          }
        });
      }
    });


  }

  shuffleAllTeamsV2() {
    let start = new Date(this.challenge['start']['seconds']*1000 - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    this.games.forEach(data => {
      if (data['games'] && data['date'] == start) {
        data['games'].forEach((game, index) => {
          if (!this.teams2[game['gamePk']] ) {
            console.log(game['gamePk'], 'is undefined');
          }
          else if (this.teams2[game['gamePk']] && (typeof this.teams2[game['gamePk']][0].locked === 'undefined' || !this.teams2[game['gamePk']][0].locked)) {
            this.modalService.presentLoading();
            setTimeout(() => {
              console.log('locking game bidding and shuffling Teams for game ' + game['gamePk']);
              this.shuffleTeam(JSON.parse(JSON.stringify(game['gamePk'])), false, index / 10, true, this.challenge.sport === 'nba');
            }, index * 200);
            this.loadingController.dismiss();
          } else {
            console.log('game ' + game['gamePk'] + ' team is already locked');
          }
        });
      }
      else {
        console.log('Event date '+data['date']+' does not match Challenge date '+start);
      }
    });
  }

  cloudShuffleAllTeams() {

    let start = new Date(this.challenge['start']['seconds']*1000 - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    this.games.forEach(data => {
      if (data['games'] && data['date'] == start) {
        data['games'].forEach(game => {
          if (this.teams2[game['gamePk']] && (typeof this.teams2[game['gamePk']][0].locked === 'undefined' || !this.teams2[game['gamePk']][0].locked)) {
            console.log('locking game bidding and shuffling Teams for game ' + game['gamePk']);
            this.modalService.presentLoading().then(() => {
              this.cloudShuffleTeam(JSON.parse(JSON.stringify(game['gamePk'])));
              this.loadingController.dismiss();
            });
          } else {
            console.log('game ' + game['gamePk'] + ' team is already locked');
          }
        });
      }
    });


  }

  reassignAllUserTeams() {
    this.challenge.games.forEach(gamePk => {
      console.log('reassign', gamePk);
      this.reassignUserTeams(gamePk);
    });
  }

  reassignAllUsers() {
    let users = {};
    this.challenge.games.forEach((gamePk, index) => {
      this.assignDisabled = true;
      this.getTeams(gamePk).subscribe(teamsDoc => {
        if (teamsDoc.exists) {
          const game = teamsDoc.data();
          Object.keys(game).forEach(team_id => {
            const team = game[team_id];
            team.users.forEach(uid => {
              users[uid] = users[uid] || {};
              users[uid]['games.' + gamePk] = '' + team_id;
            });
          });
          if (index === this.challenge.games.length - 1) {
            Object.keys(users).forEach((uid, idx) => {
              console.log(uid, users[uid]);
              if (true || uid === 'JTdh9Y9eO2MzzPJnEGboOIsdtxC3')
                this.afs.collection("user").doc(String(uid))
                  .update(users[uid]).finally(() => {
                    console.log('reassigned user ' + uid + ' games: ' + JSON.stringify(users[uid]));
                    if ( Object.keys(users).length - 1 === idx ) {
                      //this.loadingController.dismiss().finally(() => {
                        this.assignDisabled = false;
                        alert('All '+(idx+1)+' Users Reassigned');
                      //});
                    }
                  })
            });
          }
        }
        //if ( loading ) this.loadingController.dismiss();
      });
    });

  }

  deshuffleAllTeams() {
    let start = new Date(this.challenge['start']['seconds']*1000 - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    this.games.forEach(data => {
      if (data['games'] && data['date'] == start) {

        data['games'].forEach(game => {
          console.log(game['gamePk']);
          console.log(this.teams2[game['gamePk']]);

          if (this.teams2[game['gamePk']] && this.teams2[game['gamePk']] && Object.keys(this.teams2[game['gamePk']]).length > 1) {
            console.log('deshuffle game bidding and shuffling Teams for game ' + game['gamePk']);
            this.modalService.presentLoading().then(() => {
              this.deshuffleTeam(JSON.parse(JSON.stringify(game['gamePk'])));
              this.loadingController.dismiss();
            });
          }
        });
      }
    });
  }

  recalcAllWinners() {
    let start = new Date(this.challenge['start']['seconds']*1000 - new Date().getTimezoneOffset() * 60000).toISOString().substr(0, 10);
    this.games.forEach(data => {
      if (data['games'] && data['date'] == start) {
        data['games'].forEach(game => {
          if (this.teams2[game['gamePk']] && this.teams2[game['gamePk']] && Object.keys(this.teams2[game['gamePk']]).length > 1) {
            console.log('re-calc Winners for game ' + game['gamePk']);
            this.modalService.presentLoading();
            setTimeout(() => {
              if (true || game['gamePk'] === '401220229') {
                this.calcWinners(game['gamePk'], game);
              }
            }, 100);
            this.loadingController.dismiss();
          }
        });
      }
    });
  }

  deshuffleTeam(gamePk: string) {
    console.log('deshuffleTeam', gamePk);
    let teams = JSON.parse(JSON.stringify(this.teams2[gamePk]));
    console.log(teams);
    let deshuffledTeam = [];
    Object.values(teams || {}).forEach((team: any) => {
      deshuffledTeam = deshuffledTeam.concat(team.users);
      team.users.forEach(user_uid => {
        this.afs.collection("user").doc(String(user_uid)).update({ ['games.' + gamePk]: '0' }).finally(() => {
          console.log('reassigned user ' + user_uid + ' to team ' + 0);
        })
      });
    })
    teams[0]['users'] = [...new Set(deshuffledTeam)];
    teams[0]['max_users'] = 8;
    delete teams[0].locked;
    console.log({ 0: teams[0] });
    this.afs.collection("team/").doc(String(gamePk)).set({ 0: teams[0] });

  }

  checkChallenge() {
    let allowedUsers = [];
    if (this.games && this.challenges && this.teams2) {
      if (this.challenge.games) {
        let challengeUsers = {};
        this.challenge.games.forEach(gamePk => {
          if (true) {
            if (this.teams2[gamePk] && Object.keys(this.teams2[gamePk]).length > 0) {
              let users = [];
              Object.keys(this.teams2[gamePk]).forEach(team_id => {
                users = users.concat(this.teams2[gamePk][team_id].users || []); // TODO: Check if winners calculated
              });
              challengeUsers[gamePk] = users;
            }
          }
        });
        Object.values(challengeUsers).forEach((gameUsers: Array<string>) => {
          if (allowedUsers.length === 0) {
            allowedUsers = gameUsers;
          } else {
            allowedUsers = allowedUsers.filter(value => gameUsers.includes(value));
          }
        });
      }
    }
    let max_users = this.engineService.calcMaxUsers(allowedUsers);
    let teams_count = Math.floor(allowedUsers.length / max_users);
    if (allowedUsers.length > max_users * teams_count) {
      allowedUsers = allowedUsers.slice(0, max_users * teams_count);
    }
    return allowedUsers;
  }

  async loadGames(date?) {
    if (this.games && this.games.length) {
      console.log('already loaded');
      return;
    }
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    let startDate;
    let endDate;
    if (!date) {
      if (this.challenge.id) {
        date = this.datePipe.transform(new Date(this.challenge['start']['seconds'] * 1000), 'yyyy-MM-dd');
      }
      else {
        date = '2019-08-01';
      }
    }
    if (date) {
      startDate = new Date((new Date(date)).getTime() - 1000 * 60 * 60 * 24 * 2);
      endDate = new Date((new Date(date)).getTime() + 1000 * 60 * 60 * 24 * 2);
    }
    this.afs.collection(this.router.url.indexOf('nfl') !== -1 ? 'nfl' : (this.router.url.indexOf('nba') !== -1 ? 'nba' : 'nhl'), ref => ref
      .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
      .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
      .valueChanges().subscribe((games: Array<any>) => {
        this.games = games;
        this.processGames();
      });
  }

  loadUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(i => {
          var item = i;
          if (item && Object.values(item).length > 0) {
            var userBids = this.userBids || {};
            this.userBids = {};
            Object.keys(item['games'] || {}).forEach(game_id => {
              this.userBids[game_id] = userBids[game_id] || { [user.uid]: item['games'][game_id] };
            });
          }
        });
      }
    });
  }

  loadUsers(users) {
    this.usersService.load(users);
  }

  getUser(uid) {
    if (!this.users || !this.users[uid]) {
      this.loadUsers([uid]);
    }
    return this.users[uid] || {};
  }

  loadTeams() {
    this.afs.collection("team").valueChanges({ idField: 'id' }).subscribe(games => {
      games.forEach(i => {
        var game = i;
        var game_id = i.id;
        delete game['id'];
        Object.keys(game).forEach(team_id => {
          var team = game[team_id];
          this.teams2[game_id] = this.teams2[game_id] || {};
          this.teams2[game_id][team_id] = team;
        });
      })
      setTimeout(() => {
        this.calcChallengeWinners();
      }, (!this.challenge_id || !this.games) ? 0 : 500);
    });
  }

  calcChallengeWinners() {
    this.challengeWinners = [];
    if (this.games instanceof Array && Object.keys(this.teams2).length) {
      for (var i = 0; i < this.games.length; i++) {
        if (!this.challenge_id || !this.challenge_id.length) {
          return;
        }
        else if ((this.challenge_id.length && (this.selectedChallenge || { games: [] })['games'].length)) {
          this.selectedChallenge['games'].forEach(gamePk => {
            if (this.teams2[gamePk]) {
              Object.keys(this.teams2[gamePk]).forEach(team_id => {
                this.loadUsers(this.teams2[gamePk][team_id].winners);
                this.challengeWinners[gamePk] = Array.from(new Set((this.challengeWinners[gamePk] || []).concat(this.teams2[gamePk][team_id]['winners'] || [])));
              });
            }
          });
        }
      }
    }
    this.calcWinnersStats();
    return this.challengeWinners;
  }

  calcWinnersStats() {
    if (!this.user) return [];
    this.stats = { [this.user.uid]: 0 };
    Object.keys(this.challengeWinners || {}).forEach(gamePk => {
      if (this.challengeWinners[gamePk]) {
        this.challengeWinners[gamePk].forEach(winner_uid => {
          this.stats[winner_uid] = this.stats[winner_uid] || 0;
          this.stats[winner_uid]++;
        });
      }
    });
    if (Object.keys(this.stats).length === 1 && this.stats[this.user.uid] === 0) {
      this.stats = {};
    }
    return Object.entries(this.stats).sort((a, b) => a[1] > b[1] ? -1 : a[1] === b[1] ? 0 : 1);
  }

  getChallengeGames(challenge_id?) {
    challenge_id = challenge_id || this.challenge_id;
    let challenge = this.challenges.find(ch => ch['id'] === challenge_id ? ch : null) || this.challenge || { games: [] };
    if (!challenge['start']) return;
    let games = [];
    let date = new Date(challenge['start']['seconds'] * 1000);
    for (var i = 0; i < this.games.length; i++) {
      if (this.games[i]['date'] == date.toJSON().slice(0, 10) || true) { // Temp allow multi day Challenges
        let game = Object.assign({}, this.games[i], { games: [] });
        for (var j = 0; j < this.games[i]['games'].length; j++) {
          if (challenge['games'].includes(String(this.games[i]['games'][j]['gamePk']))) {
            game['games'].push(this.games[i]['games'][j]);
          }
        }
        if (game['games'].length > 0) {
          games.push(game);
        }

      }
    }
    return games;
  }

  getChallengeGamesSubmitted(challenge_id?) {
    challenge_id = challenge_id || this.challenge_id;
    let userBids = Object.keys(this.userBids || {});
    if (!userBids || !this.challenge['games']) return [];
    return this.challenge['games'].filter(function (n) {
      return userBids.indexOf(n) !== -1;
    });
  }

  loadChallenges() {
    this.afs.collection("challenge", ref => ref
      .where('start', '>', new Date("2019-11-01"))
      .where('start', '<', new Date(Date.now() + 30 * 24 * 60 * 60000))
    )
      .valueChanges({ idField: 'id' }).subscribe(challenges => {
        this.challenges = [];
        this.max = 0;
        challenges.forEach((challenge: any, index) => {
          if (+challenge['id'] > this.max) {
            this.max = +challenge['id'];
            localStorage.setItem('maxId', JSON.stringify(this.max + 1));
          }
          this.challenges = this.challenges || [];
          if (challenge['start']['seconds'] * 1000 < (Date.now() - 10 * 24 * 60 * 60000)) {
            this.history = this.history || [];
            this.history.push(Object.assign({ name: '', text: '', games: [] }, challenge));
            this.challenges[index] = Object.assign({ name: '', text: '', games: [] }, challenge);
          }
          else {
            this.challenges[index] = Object.assign({ name: '', text: '', games: [] }, challenge);
          }

          if (challenge['id'] === this.challenge_id) {
            this.challenge = challenge;
            this.onSelectedChallenge.emit(this.challenge);
            this.getChallengeTeams();
            this.loadGames();
          }
        });
        this.challenges.reverse();
        if (this.challenge_id && this.challenges && this.challenges.length) {
          this.selectedChallenge = this.challenges.find(challenge => challenge && challenge['id'] === this.challenge_id ? challenge : null)
        }
      });

  }

  getChallengeTeams(game_id?) {
    game_id = game_id||'';
    if (this.challenge) {
      if (!this.teams) {
        this.teams = {};
        let allusers = [];
        Object.values(this.challenge.games).map(gamePk => {
          this.afs.collection("team/").doc(String(gamePk)).valueChanges().subscribe((teams: any) => {
            this.teams[String(gamePk)] = teams;
            this.teams2[String(gamePk)] = teams;
            if (false) { // temp fix for 0 team bids issue
              if ( gamePk === '401326466') {
                allusers = [];
                Object.keys(teams).forEach(team_id => {
                  allusers = allusers.concat(teams[team_id].users)
                })
                console.log('allusers1', allusers);
              }
              if ( gamePk === '401326471') {
                let gameUsers = [];
                Object.keys(teams).forEach(team_id => {
                  if( team_id !== '0') {
                    gameUsers = gameUsers.concat(teams[team_id].users)
                  }
                });
                console.log('0 team users', allusers.filter(uid => !gameUsers.includes(uid)));
              }
            }
            Object.values(teams || {}).forEach((team: any) => {
              this.loadUsers(team.winners || []);
            });
          });
        });
      }
    } else {
      console.log('empty challenge');
    }
    return this.teams[game_id] ? this.teams[game_id] : { users: [], winners: [] };
  }

  getTeams(gamePk) {
    return this.afs.collection('team/').doc(String(gamePk)).get();
  }

  async resetWinners(gamePk) {
    await this.modalService.presentLoading();
    this.getTeams(gamePk).subscribe(teamsDoc => {
      if (teamsDoc.exists) {
        let teams = teamsDoc.data();
        Object.keys(teams || {}).map(id => {
          if (teams[id].winners && teams[id].winners.length) {
            teams[id].winners = [];
            teamsDoc.ref.update({ [id]: teams[id] });
          }
        });
      }
      this.loadingController.dismiss();
    });
  }

  async checkDeshuffledUsers(gamePk, game) {
    await this.modalService.presentLoading();
    this.getTeams(gamePk).subscribe(teamsDoc => {
      if (teamsDoc.exists) {
        let teams = teamsDoc.data();
        console.log('teams', teams);
        let bids = this.loadBids(gamePk);
        setTimeout(() => {
          console.log('waiting for bids to load');
          bids = this.loadBids(gamePk);
          if (!bids) {
            console.log('couldn\'t load the bids for ' + gamePk);
            return;
          }
          console.log('bids', bids);
          console.log('missing uids', Object.keys(bids).filter(x => !teams[0].users.includes(x)));
          
          this.loadingController.dismiss();
        }, bids ? 0 : 3000);

      } else {
        this.loadingController.dismiss();
      }
    });
  }

  async checkUnshuffledUsers(gamePk, game) {
    await this.modalService.presentLoading();
    this.getTeams(gamePk).subscribe(teamsDoc => {
      if (teamsDoc.exists) {
        let teams = teamsDoc.data();
        console.log('teams', teams);
        let teamUsers = [];
        Object.keys(teams).forEach(team_id => {
          console.log('team_id', team_id);
          //teams
          if ( parseInt(team_id) ) {
            teamUsers.push(...teams[team_id].users);
            console.log('adding team' , team_id, ' users ', teams[team_id].users);
            
          }
        });
        console.log('teamUsers', teamUsers);
        
        let bids = this.loadBids(gamePk);
        setTimeout(() => {
          console.log('waiting for bids to load');
          bids = this.loadBids(gamePk);
          if (!bids) {
            console.log('couldn\'t load the bids for ' + gamePk);
            return;
          }
          console.log('bids', bids);
          console.log('missing uids for team 0', Object.keys(bids).filter(x => !teamUsers.includes(x)));
          
          this.loadingController.dismiss();
        }, bids ? 0 : 3000);

      } else {
        this.loadingController.dismiss();
      }
    });
  }

  async calcWinners(gamePk, game) {
    await this.modalService.presentLoading();
    this.getTeams(gamePk).subscribe(teamsDoc => {
      if (teamsDoc.exists) {
        let teams = teamsDoc.data();
        Object.keys(teams || {}).map(id => {
          if (teams[id].winners && teams[id].winners.length) {
            teams[id].winners = [];
            teamsDoc.ref.update({ [id]: teams[id] });
          }
          let bids = this.loadBids(gamePk);
          if (!bids) {
            setTimeout(() => {
              console.log('waiting for bids to load');
              bids = this.loadBids(gamePk);
              if (!bids) {
                console.log('couldn\'t load the bids for ' + gamePk);
                return;
              }
              this.engineService.process2GameOver(gamePk, game, bids);
              this.loadingController.dismiss();
            }, 1000);
          } else {
            this.engineService.process2GameOver(gamePk, game, bids);
            this.loadingController.dismiss();
          }
        });
      } else {
        this.loadingController.dismiss();
      }
    });
  }

  async shuffleTeam(gamePk, loading, index?, skipReassign?, isReversed?) {
    if (loading) await this.modalService.presentLoading();
    const allowedUsers = this.checkChallenge();
    this.getTeams(gamePk).subscribe(teamsDoc => {
      if (teamsDoc.exists) {
        let new_teams = Object.assign({}, this.engineService.shuffleTeams(teamsDoc.data(), allowedUsers, isReversed));
        if (new_teams) {
          teamsDoc.ref.set(new_teams).then(() => {
            if (!skipReassign) {
              setTimeout(() => {
                this.reassignUserTeams(gamePk);
              }, (index || 1) * 1000);
            }
          });
        }
      }
      if (loading) this.loadingController.dismiss();
    });
  }

  async reassignUserTeams(gamePk) {
    await this.modalService.presentLoading();
    this.getTeams(gamePk).subscribe(teamsDoc => {
      if (teamsDoc.exists) {
        let teams = teamsDoc.data();
        if (teams) {
          Object.keys(teams).forEach(team_id => {
            Object.values(teams[team_id].users).forEach((user_uid: string) => {
              if (this.users[user_uid] && this.users[user_uid].games) {
                const user = this.users[user_uid];
                const userGames = Object.assign({}, user.games);
                if (!userGames[gamePk] || parseInt(userGames[gamePk], 10) !== parseInt(team_id, 10)) {
                  userGames[gamePk] = team_id;
                  this.afs.collection("user").doc(String(user_uid))
                    .update({ ['games.' + gamePk]: team_id }).finally(() => {
                      console.log('reassigned user ' + user_uid + ' to team ' + team_id);
                    })
                  console.log('reassign user ' + user_uid + ' to team ' + team_id);
                } else {
                  console.log('user ' + user_uid + ' team ' + team_id + ' does match user.games[gamePk] ' + user.games[gamePk]);
                }
              }
              else {
                this.afs.collection("user").doc(String(user_uid)).get().subscribe(data => {
                  const user = data.data();
                  if (!user || !user['games']) {
                    console.log('empty user games for', user_uid);
                  }
                  const userGames = Object.assign({}, (user || { games: {} })['games']);
                  if (userGames && userGames[gamePk] != team_id) {
                    userGames[gamePk] = team_id;
                    data.ref.update({ games: userGames }).finally(() => {
                      console.log('reassigned user ' + user_uid + ' to team ' + team_id);
                    })
                    console.log('reassign user ' + user_uid + ' to team ' + team_id);
                  } else {
                    console.log('user ' + user_uid + ' team ' + team_id + ' does match user.games[gamePk] ' + user['games'][gamePk]);
                  }
                });
              }
            });
          });
        }
      }
      this.loadingController.dismiss();
    });
    return;
  }

  async cloudShuffleTeam(gamePk) {
    console.log('cloudShuffleTeam');
    await this.modalService.presentLoading();
    if (true) {
      const callable = this.fns.httpsCallable('shuffle');
      callable({ challengePk: this.challenge_id, gamePk: gamePk })
        .toPromise().then((resp) => {
          console.log(resp);
        })
        .catch(error => {
          console.log('Cloud function failed', error);
        })
    }

    this.loadingController.dismiss();
  }

  async editTeam(gamePk) {
    await this.modalService.presentLoading();
    //console.log('editTeam: '+gamePk);
    if (false) {
      const callable1 = this.fns.httpsCallable('testMail');
      callable1({})
        .toPromise().then((data) => {
          //console.log(data);  
        });
    }
    if (true) {
      const callable = this.fns.httpsCallable('shuffle/' + this.challenge_id + '/' + gamePk);
      callable({})
        .toPromise().then((data) => {
          console.log(data);
        });
    }

    this.loadingController.dismiss();
  }


  async notifyWinners(gamePk) {
    await this.modalService.presentLoading();
    console.log('notifyWinners: ' + gamePk);
    this.loadingController.dismiss();
  }

  async createNbaBatch() {
    const popover = await this.popoverCtrl.create({
      component: ModalBatchNbaComponent,
      componentProps: {
        max: this.max,
        popoverCtrl: this.popoverCtrl,
        afs: this.afs,
      },
       cssClass: 'modal-wrap-default'
    });
    popover.onDidDismiss().then((data) => {
    });
    return await popover.present();
  }

  async openTeam(ev, team, gamePk, team_id) {
    ev.stopPropagation();
    const popover = await this.popoverCtrl.create({
      component: ModalTeamComponent,
      componentProps: {
        team,
        gamePk,
        team_id,
        users: this.users,
        popoverCtrl: this.popoverCtrl,
        afs: this.afs,
      },
      cssClass: 'modal-wrap-default'
    });
    popover.onDidDismiss().then((data) => {
    });
    return await popover.present();
  }

  addChallenge() {
    this.router.navigate(['/admin/challenges/add']);
  }
}

