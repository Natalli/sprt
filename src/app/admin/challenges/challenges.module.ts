import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminChallengesPageRoutingModule } from './challenges-routing.module';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { AdminChallengesPage } from './challenges.page';
import { AdminChallengeEditComponent } from './edit/edit.component';
import { AdminChallengeAddPage } from './add/add.page';
import { AdminListChallengesPage } from './list/list.page';
import { ModalBatchNbaComponent } from 'src/app/popups/new/modal-batch-nba/modal-batch-nba.component';
import { IcoSvgModule } from 'src/app/components/ico-svg/ico-svg.module';
import { ModalTeamComponent } from 'src/app/popups/new/modal-team/modal-team.component';
import { UserModule } from 'src/app/components/user/user.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        DirectivesModule,
        AdminChallengesPageRoutingModule,
        IcoSvgModule,
        UserModule
    ],
    declarations: [
        AdminChallengesPage,
        AdminChallengeEditComponent,
        AdminChallengeAddPage,
        AdminListChallengesPage,
        ModalBatchNbaComponent,
        ModalTeamComponent
    ],
    exports: [
        AdminChallengeEditComponent
    ],
    providers: [DatePipe]
})
export class AdminChallengesPageModule {}
