import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import *  as deepEqual from 'fast-deep-equal';
import { CommonService } from 'src/app/common.service';
import { ModalService } from 'src/app/service/modal.service';
@Component({
  selector: 'app-admin-challenge-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class AdminChallengeEditComponent implements OnInit {
  @Input() set data(value) {
    if (value) {
      this.challenge = value;
      // this.init();
    }
  }

  @Input() set max(value) {
    if (value) {
      this.maxId = value;
    }
  }

  @Input() set isNewData(value) {
    if (value) {
      this.isNew = true;
    }
  }
  dateValue;
  challenge: any;
  form: FormGroup;
  sports = ['nhl', 'nfl', 'nba'];
  customAlertOptions: any = {
    header: 'Select sport',
    translucent: true
  };
  games;
  maxId: number;
  updatedFormValues: any;
  newGames = new FormArray([]);
  challengeGames: any[] = [];
  isNew: boolean;
  submitted: boolean;

  constructor(
    protected formBuilder: FormBuilder,
    private afs: AngularFirestore,
    public commonService: CommonService,
    protected router: Router,
    protected datePipe: DatePipe,
    protected modalService: ModalService
  ) {

  }

  ngOnInit() {
    // this.afs.collection("challenge").doc('215').delete();
    // this.afs.collection("challenge").doc('215').valueChanges().subscribe(item => {
    //   console.log(item, 'item')
    // })
    this.init();
    if (!this.challenge) {
      this.form.get('games').patchValue([]);
    }
  }

  init() {
    this.initForm();
    this.dateValue = this.datePipe.transform(this.form.get('start').value, 'dd MMM, yyyy hh:mm');
    if (this.challenge) {
      this.patchValue();
      this.loadGames();
      this.afs.collection(this.challenge.sport)
        .valueChanges().subscribe((games: Array<any>) => {
          this.games = games;
        });
    }
  }

  initForm() {
    this.form = this.formBuilder.group({
      id: [],
      sport: ['', [Validators.required]],
      name: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      text: [],
      start: [this.datePipe.transform(new Date(), 'yyyy-MM-dd'), [Validators.required]],
      games: new FormArray([]),
      info: [],
      disabled: [true],
      newGames: new FormArray([]),
      finalGame: []
    });
  }

  patchValue() {
    this.form.get('id').patchValue(this.challenge['id']);
    this.form.get('sport').patchValue(this.challenge['sport']);
    this.form.get('name').patchValue(this.challenge['name']);
    this.form.get('amount').patchValue(this.challenge['amount']);
    this.form.get('text').patchValue(this.challenge['text']);
    // this.form.get('start').patchValue(this.datePipe.transform(new Date(this.challenge['start']['seconds'] * 1000).toISOString(), 'dd MMM, yyyy hh:mm'));
    this.form.get('start').patchValue(new Date(this.challenge['start']['seconds'] * 1000).toISOString());
    this.dateValue = this.datePipe.transform(this.form.get('start').value, 'dd MMM, yyyy hh:mm');
    this.form.get('games').patchValue(this.challenge['games'] || []);
    this.form.get('info').patchValue(this.challenge.info || '');
    this.form.get('disabled').patchValue(this.challenge.disabled || false);
    this.form.get('finalGame').patchValue(this.challenge.finalGame || '');
  }

  loadGames(value?) {
    if(value) {
      this.form.get('start').patchValue(value);
      this.dateValue = this.datePipe.transform(this.form.get('start').value, 'dd MMM, yyyy hh:mm');
    }
    console.log(this.form.value, 'form')
    if (this.form.value.start && this.form.value.sport) {

      let startDate = new Date((new Date(this.form.value.start)).getTime() - 1000 * 60 * 60 * 12);
      let endDate = new Date((new Date(this.form.value.start)).getTime() + 1000 * 60 * 60 * 36);
      this.afs.collection(this.form.value.sport, ref => ref
        .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
        .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
        .valueChanges().subscribe((games: Array<any>) => {
          this.challengeGames = [];
          if (games && games.length) {
            for (var i = 0; i < games.length; i++) {
              for (var j = 0; j < games[i]['games'].length; j++) {
                this.challengeGames.push(games[i]['games'][j]);
              }
            }
            this.challengeGames.sort((a, b) => new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime());
            if (this.challenge && this.challenge.games && this.challenge.games.length) {
              this.challenge.games.forEach(game => {
                this.challengeGames.find(chGame => {
                  if (+chGame.gamePk === +game) {
                    let formArray: FormArray = this.form.get('games') as FormArray;
                    if (formArray.controls.length < this.challenge.games.length) {
                      formArray.push(new FormControl(game));
                    }
                  }
                })
              })
            }
          }
        })
    }
  }

  selectedGames(event) {
    const formArray: FormArray = this.form.get('games') as FormArray;

    /* Selected */
    if (event.target.checked) {
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      let i: number = 0;

      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value == event.target.value) {
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  getUpdatedValues() {
    this.updatedFormValues = {};
    this.form['_forEachChild']((control, name) => {
      if (control.dirty) {
        this.updatedFormValues[name] = control.value;
      }
      if (this.challenge && this.challenge.games) {
        if (!deepEqual(this.challenge.games, this.form.value.games)) {
          this.form.value.games = this.form.value.games.filter(this.onlyUnique);
          this.updatedFormValues['games'] = this.form.value.games;
        }
      }
      if (this.challenge && (this.challenge.finalGame !== this.form.value.finalGame)) {
        this.updatedFormValues['finalGame'] = this.form.value.finalGame;
      }
    });
  }

  submit() {
    if (this.form.valid) {
      this.getUpdatedValues();
      if (Object.keys(this.updatedFormValues).length) {
        if (this.updatedFormValues.start) {
          this.form.get('start').patchValue(new Date(new Date(new Date(this.form.value.start).getTime()).toISOString()));
          this.updatedFormValues.start = this.form.value.start;
        }

        if (this.form.value.newGames && this.form.value.newGames.length) {
          this.form.value.games.push(...this.form.value.newGames);
          this.updatedFormValues.games = this.form.value.games;
          if (this.updatedFormValues['newGames']) {
            delete this.updatedFormValues['newGames'];
          };
          this.newGames.removeAt(0);
          this.form.removeControl('newGames');
        }
        if (!this.challenge) {
          if (localStorage.getItem('maxId')) {
            this.form.get('id').setValue(String(JSON.parse(localStorage.getItem('maxId'))));
            if (this.form.value.games && this.form.value.games.length) {
              const chGames = this.challengeGames.filter(selectGame => this.form.value.games.find(game => game === selectGame.gamePk ? game : null))
              this.form.get('start').patchValue(new Date(new Date(new Date(chGames[0].gameDate).getTime()).toISOString()));
              this.form.get('finalGame').patchValue(chGames[chGames.length - 1].gamePk);
            } else {
              this.form.get('start').patchValue(new Date(new Date(new Date(this.form.value.start).getTime()).toISOString()));
            }
          }
        } else {
          if (this.form.value.games && this.form.value.games.length && this.form.value.games.indexOf(this.form.value['finalGame']) === -1) {
            const chGames = this.challengeGames.filter(selectGame => this.form.value.games.find(game => game === selectGame.gamePk ? game : null))
            this.form.get('finalGame').patchValue(chGames[chGames.length - 1].gamePk);
            this.updatedFormValues['finalGame'] = this.form.value['finalGame'];
          }
        }
        // console.log(this.form.value, 'form')
        // console.log(this.updatedFormValues, 'upd f v')
        this.afs.collection("challenge").doc(this.form.value.id).get().subscribe(item => {
          if (!item.exists) {
            this.form.value.games = this.form.value.games.filter(this.onlyUnique);
            this.form.removeControl('newGames');
            const challenge = Object.assign({}, this.form.value);
            delete challenge.id;
            item.ref.set(challenge).then(() => {
              this.modalService.confirmModal('Success', 'New challenge created');
              this.router.navigateByUrl('/admin/challenges');
              window.location.href = '/admin/challenges';
            });
          }
          else {
            item.ref.update(this.updatedFormValues).then(() => {
              this.modalService.confirmModal('Success', 'Challenge updated');
              this.router.navigate(['/admin/challenges'], { replaceUrl: true });
            });
          }
        });
      } else {
        this.modalService.errorModal('Error', 'No changed data');
      }
    } else {
      this.submitted = true;
    }

  }

  remove(gamePk) {
    this.challenge.games.splice(this.challenge.games.indexOf(gamePk.toString()), 1);
    this.modalService.confirmModal('Success', 'Game deleted');
  }

  duplicate() {
    let id = this.maxId + 1;
    this.form.get('id').patchValue(id.toString());
    this.modalService.errorModal('Error', 'You have duplicated the challenge');
  }

  addGames() {
    this.newGames = this.form.get('newGames') as FormArray;
    this.newGames.push(this.formBuilder.control(''));
  }

  removeGames(i) {
    this.newGames.removeAt(i);
    this.modalService.confirmModal('Confirm', 'Game deleted');
  }

  changedFinalyGame(id) {
    this.form.get('finalGame').patchValue(id.toString());
  }

  uniqueDate(value, prevValue) {
    if (!prevValue) {
      let date = this.datePipe.transform(value.gameDate, 'MMMM d');
      return date;
    }
    let date = this.datePipe.transform(value.gameDate, 'MMMM d');
    let prevDate = this.datePipe.transform(prevValue.gameDate, 'MMMM d');
    if (date !== prevDate) {
      return date;
    } else {
      return null;
    }
  }

  checkedGame(gamePk) {
    if (this.challenge) {
      if (this.challenge.games.indexOf(gamePk.toString()) !== -1) {
        return true
      } else {
        return false
      }
    }
  }

  fillText() {
    this.form.get('text').setValue('$' + this.form.value.amount + ' Prize!');
    this.form.get('text').markAsDirty();
  }

  fillName() {
    console.log('fill name');
    
    if (this.form.value.sport && this.form.value.start) {
      const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      let day = new Date(this.form.value.start).getDay();
      !this.form.value.name ?
        this.form.get('name').setValue(days[day] + ' ' + this.form.value.sport.toUpperCase() + ' Challenge') :
        this.form.value.name;
      this.form.get('name').markAsDirty();
    }
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
}