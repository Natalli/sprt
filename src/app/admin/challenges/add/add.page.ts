import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common.service';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-admin-challenge-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AdminChallengeAddPage implements OnInit {

  challenge: any;
  form: FormGroup;
  sports = ['nhl', 'nfl', 'nba'];
  customAlertOptions: any = {
    header: 'Select sport',
    translucent: true
  };
  maxId: number;
  total: number;
  updatedFormValues: any;
  games = new FormArray([]);
  submitted: boolean;
  challengeGames: any[] = [];
  constructor(
    protected formBuilder: FormBuilder,
    private afs: AngularFirestore,
    public commonService: CommonService,
    protected router: Router,
    private datePipe: DatePipe,
    protected modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.initForm();
    this.form.get('disabled').setValue(false);
    let date = new Date();
    date.setUTCHours(0, 0, 0);
    this.form.get('start').patchValue(new Date(date).toISOString().slice(0, 10));
    // this.loadGames();
  }

  loadGames() {
    if (!this.form.value.id && this.form.value.start && this.form.value.sport) {
      let startDate = new Date((new Date(this.form.value.start)).getTime() - 1000 * 60 * 60 * 12);
      let endDate = new Date((new Date(this.form.value.start)).getTime() + 1000 * 60 * 60 * 36);
      this.afs.collection(this.form.value.sport, ref => ref
        .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
        .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
        .valueChanges().subscribe((games: Array<any>) => {
          if (games && games.length) {
            for (var i = 0; i < games.length; i++) {
              for (var j = 0; j < games[i]['games'].length; j++) {
                games[i]['games'][j]['checked'] = false;
                this.challengeGames.push(games[i]['games'][j]);
              }
            }
          }
        })
    }
  }


  initForm() {
    this.form = this.formBuilder.group({
      id: [],
      sport: ['', [Validators.required]],
      name: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      text: [],
      start: [new Date(), [Validators.required]],
      games: new FormArray([]),
      info: [],
      disabled: [],
      finalGame: []
    });
  }

  addGames() {
    this.games = this.form.get('games') as FormArray;
    this.games.push(this.formBuilder.control(''));
  }

  selectedGames(i) {
    this.challengeGames[i]['checked'] ? this.challengeGames[i]['checked'] = false : this.challengeGames[i]['checked'] = true;
  }

  submit() {
    if (this.form.valid) {
      if (localStorage.getItem('maxId')) {
        this.form.get('id').setValue(String(JSON.parse(localStorage.getItem('maxId'))));
      }
      if (this.challengeGames && this.challengeGames.length) {
        this.challengeGames = this.challengeGames.filter(game => game.checked ? game.gamePk : null).map(game => game ? String(game.gamePk) : null);
        this.form.value.games.push(...this.challengeGames);
        this.form.get('finalGame').patchValue(this.form.value.games[this.form.value.games.length - 1]);
      }
      this.form.value.start = new Date(new Date(new Date(this.form.value.start).getTime() + (480 * 60 * 1000)).toISOString());
      const challenge = Object.assign({}, this.form.value);
      delete challenge.id;
      this.afs.collection('challenge').doc(this.form.value.id).set(challenge);
      this.modalService.confirmModal('Success', 'Challenge loaded successfully');
      this.router.navigate(['/admin'], { replaceUrl: true });
      window.location.href = '/admin';
    } else {
      this.submitted = true;
    }

  }

  removeGames(i) {
    this.games.removeAt(i);
    this.modalService.confirmModal('Success', 'Game deleted');
  }

  uniqueDate(value, prevValue,) {
    if (!prevValue) {
      let date = this.datePipe.transform(value.gameDate, 'MMMM d');
      return date;
    }
    let date = this.datePipe.transform(value.gameDate, 'MMMM d');
    let prevDate = this.datePipe.transform(prevValue.gameDate, 'MMMM d');
    if (date !== prevDate) {
      return date;
    } else {
      return null;
    }
  }

}