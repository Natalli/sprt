import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminChallengeAddPage } from './add/add.page';
import { AdminChallengesPage } from './challenges.page';
import { AdminListChallengesPage } from './list/list.page';

const routes: Routes = [
  {
    path: '',
    component: AdminChallengesPage,
    children: [
      { path: '', component: AdminListChallengesPage },
      { path: ':sport_id/:id', component: AdminListChallengesPage },
      { path: 'add', component: AdminChallengeAddPage }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminChallengesPageRoutingModule {}
