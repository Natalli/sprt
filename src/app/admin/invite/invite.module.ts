import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { InvitePageRoutingModule } from './invite-routing.module';
import { InvitePage } from './invite.page';
import { UserModule } from '../../components/user/user.module';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { PopupInviteManageComponent } from 'src/app/popups/popup-invite-manage/popup-invite-manage.component';
import { SearchService } from 'src/app/services/search/search.service';
import { SearchServiceModule } from 'src/app/services/search/search.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvitePageRoutingModule,
    UserModule,
    DirectivesModule,
    SearchServiceModule
  ],
  declarations: [
    InvitePage, 
    PopupInviteManageComponent
  ],
  exports: [InvitePage],
  providers: [SearchService, DatePipe],
  
})
export class AdminInvitePageModule {}
