import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { LoadingController, Platform, PopoverController, AlertController, ModalController, IonItemSliding } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { CommonService } from '../../common.service';
import { UserService } from '../../service/user.service';
import { Invite } from '../../model/invite';
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import { InviteService } from '../../service/invite.service';
import { PopupInviteManageComponent } from 'src/app/popups/popup-invite-manage/popup-invite-manage.component';
import { SearchService } from 'src/app/services/search/search.service';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.page.html',
  styleUrls: ['./invite.page.scss'],
})
export class InvitePage implements OnInit {
  @ViewChild(IonItemSliding) slidingItem: IonItemSliding;
  date = Date;
  objectKeys = Object.keys;
  today: any;
  games: any;
  challenge_id: any;
  challenge: any;
  selectInvite: any;
  invite: Invite = new Invite();
  invites: object = {};
  dayCount = 5;
  user: any;
  eventExpanded = [];
  eventEdited = [];

  constructor(
    private datePipe: DatePipe,
    public commonService: CommonService,
    public router: Router,
    public platform: Platform,
    public loadingController: LoadingController,
    private afs: AngularFirestore,
    public userService: UserService,
    public inviteService: InviteService,
    private popoverCtrl: PopoverController,
    public alertController: AlertController,
    public modalController: ModalController,
    protected searchService: SearchService,
    protected modalService: ModalService
    ) {

      this.userService.user$.subscribe((user) => {
        if (user) {
          this.user = user;
          console.log(user);
          this.loadInvites();
        }
      });  
      this.loadGames();
  }

  ngOnInit() {
  }

  async loadInvites() {
      this.afs.collection('invite', ref => ref
      //const query = col.where('items', 'array-contains', 'fruit loops')
        .where('users', 'array-contains', this.user.uid)
        //.orderBy('start', 'desc')
        //.where('start', '>', new Date('2020-04-01'))
        //.where('start', '<', new Date(Date.now() + 120 * 24 * 60 * 60000))
        //.where('disabled', '==', false)
        )
        .valueChanges({ idField: 'id' }).subscribe((invites: Array<any>) => {
          invites.forEach((invite: Invite) => {
            this.invites = this.invites || {};
            this.invites[Invite.startDate(invite.start)] = this.invites[Invite.startDate(invite.start)] || {};
            this.invites[Invite.startDate(invite.start)][invite.id] = invite;
          });
        });
  }

  async loadGames() {
    this.modalService.presentLoading('Loading Games...');
    const startDate = new Date((new Date()).getTime() - 30 * 3600 * 24 * 1000);
    const endDate = new Date((new Date()).getTime() + 90 * 3600 * 24 * 1000);
    this.afs.collection('nhl', ref => ref
      .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
      .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
      .valueChanges().subscribe((games: Array<any>) => {
        console.log('games updated');
        this.games = games;
        this.loadingController.dismiss();
    });
  }

  getGames() {
    return (this.games||[]).slice(0, this.dayCount);
  }

  getChallengeGames(challenge_id?) {
    challenge_id = challenge_id || this.challenge_id;
    let challenge = this.invites[challenge_id] || this.challenge || { games: [] };
    if (!challenge['start']) return;
    let games = [];
    let date = new Date(challenge['start']['seconds'] * 1000);
    for (var i = 0; i < this.games.length; i++) {
      if (this.games[i]['date'] == date.toJSON().slice(0, 10)) {
        let game = Object.assign({}, this.games[i], { games: [] });
        for (var j = 0; j < this.games[i]['games'].length; j++) {
          if (challenge['games'].includes(String(this.games[i]['games'][j]['gamePk']))) {
            game['games'].push(this.games[i]['games'][j]);
          }
        }
        if (game['games'].length > 0) {
          games.push(game);
        }

      }
    }
    if (typeof games[0] === 'undefined') return [];
    let statusLastGame = games[0]['games'][games[0]['games'].length - 1]['status']['statusCode'];
    return games;
  }

  getChallengeGamesSubmitted(challenge_id?) {
    return [];
  }

  click(ev, index?, inviteId?) {
    switch((ev.target.name||ev.target.title)) {
      case 'checkmark-circle':
        this.removeGame(ev.target.title, index, inviteId);
        break;
      case 'add-circle':
        this.addGame(ev.target.title, index, inviteId);
        break;
      case 'add-event':
        this.addEvent(ev.target.id, index);
        break;
      }
  }

  addEvent(id, index) {
    if ( typeof this.games[index] === 'undefined') return;
    const event = this.games[index];
    this.invite = new Invite(this.user.uid, new Date(event.date + 'T16:00:00Z'), 'New Game' ); // TODO: uniform timezone date
    this.persistInvite(this.invite);
  }

  addGame(id, index, inviteId) {
    if ( typeof this.games[index] === 'undefined' ||
          !this.isGameAdmin(this.inviteService.startDate(this.games[index].date), inviteId) ) return;
    this.persistInvite({games: firebase.firestore.FieldValue.arrayUnion(id)}, inviteId);
  }

  removeGame(id, index, inviteId) {
    if ( typeof this.games[index] === 'undefined' ||
          !this.isGameAdmin(this.inviteService.startDate(this.games[index].date), inviteId) ) return;
    this.persistInvite({games: firebase.firestore.FieldValue.arrayRemove(id)}, inviteId);
  }

  setInviteStatus(id, index, status) {
    const event = this.games[index];
    const inviteId = this.getInviteId(event.date, this.user.uid);
    this.afs.collection('invite/').doc(inviteId).update({disabled: status});
  }

  getInvite(id) {
    return this.afs.collection('invite/').doc(String(id)).get();
  }

  generateInviteId(date, uid?) {
    return new Date(date).toJSON().substr(0,10).replace(/-/g,'') + '-' + this.afs.createId();
  }

  getInviteId(date, uid) {
    return date + '-' + uid;
  }

  isGameAdded(inviteId: string, gamePk: string) {
    if ( !this.user ) return false;
    const date = inviteId.substr(0, 8);
    if ( this.invites[date] && this.invites[date][inviteId] &&
          this.invites[date][inviteId].games.indexOf(String(gamePk)) !== -1 ) {
      return true;
    } else {
      return false;
    }
  }

  persistInvite(invite, id?) {
    if ( id ) {
      this.afs.collection('invite/').doc(id).update(Object.assign({},invite));
    } else {
      this.afs.collection('invite/').doc(this.generateInviteId(invite.start)).set(Object.assign({},invite));
    }
  }

  handleName(ev: any, inviteId: string) {
    //if ( this.isAdmin) return;
    console.log('handleName', ev, inviteId);
    
    ev.stopPropagation();
    console.log(ev, inviteId, ev.type, ev.keyCode);
    if ( ev.keyCode ) {
      if ( ev.keyCode === 13 ) {
        console.log('enter');
        this.eventEdited[inviteId] = false;
        this.persistInvite({name: ev.target.value}, inviteId);
      }
      if ( ev.keyCode === 27 ) {
        console.log('esc');
        this.eventEdited[inviteId] = false;
      }
    }
    if ( ev.type === 'focusout' ) {
      this.eventEdited[inviteId] = false;
      this.persistInvite({name: ev.target.value}, inviteId);
    }
  }

  isGameAdmin(date, inviteId) {
    return this.invites[date] && this.invites[date][inviteId] && this.invites[date][inviteId].admins.indexOf(this.user.uid) !== -1;
  }
 
  async manageInvite(id: string, ev: Event) {
    ev.stopPropagation();
    const modal = await this.modalController.create({
      component: PopupInviteManageComponent,
      componentProps: {
        inviteId: id,
        popoverCtrl: this.popoverCtrl,
        afs: this.afs,
        searchService: this.searchService
      },
      cssClass: 'popover-default',
      swipeToClose: true
    });
    modal.onDidDismiss().then((data) => {
      this.slidingItem.closeOpened();
    });
    await modal.present();
  }


  toggleGames(id) {
    if ( !this.eventEdited[id] ) {
      this.eventExpanded[id] = !(this.eventExpanded[id] || false);
    }
  }

  getInvites(date: string) {
    return Object.values(this.invites[date]||{});
  }

}
