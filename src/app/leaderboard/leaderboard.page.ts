import { Component, ViewChild, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IonContent } from '@ionic/angular';
import { Constants } from '../constants';
import { UserService } from '../service/user.service';
import { AppComponent } from '../app.component';
import { SPORT_TABS } from '../constants/sport-tabs';
import { ModalService } from '../service/modal.service';

export type SortDirection = 'asc' | 'desc';

export interface Leader {
  id: string,
  sport: {
    all: number,
    nhl: number,
    nfl: number,
    nba: number,
  },
  win: {
    all: number,
    nhl: number,
    nfl: number,
    nba: number,
  }
}
@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.page.html',
  styleUrls: ['./leaderboard.page.scss'],
})

export class LeaderboardPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;

  user: any;
  challenges: Array<Object> = [];
  bidsLoading: boolean = true;
  users: any = {};
  stats: Object = {};
  leaders: any;
  userData: any;
  segmentSelected: string = 'players';
  sportSelected: string = 'all';
  userFriends: string[] = [];
  optimizedWinners: Array<Leader> = [];
  userLimit = 25;
  tabsBySport = SPORT_TABS;
  rotate = { sport: { 'asc': 'desc', 'desc': 'asc' }, win: { 'asc': 'desc', 'desc': 'asc' } };
  direction = 'desc';
  columnOrdered = 'win';
  defaultList: Array<Leader> = [];
  sortClass = { sport: '', win: '' };

  constructor(
    public loadingController: LoadingController,
    private afs: AngularFirestore,
    public modalController: ModalController,
    public userService: UserService,
    public myapp: AppComponent,
    protected modalService: ModalService) {
    this.modalService.presentLoading();
    this.afs.collection("user", ref => ref
      .where('stats.gamesWon', '>', 0)
      .orderBy('stats.gamesWon', 'desc')
      .limit(3000)
    ).valueChanges({ idField: 'id' }).subscribe(users => {
      this.loadingController.dismiss().catch(() => {
        setTimeout(() => {
          this.loadingController.dismiss().catch(() => { });
        }, 1000)
      });
      users.forEach(user => {
        this.users[user.id] = user;
        if (!this.users[user.id]['publicName']) {
          if (this.users[user.id]['name']) {
            this.users[user.id]['publicName'] = this.users[user.id]['name'].slice(0, 3).toUpperCase();
          } else {
            this.users[user.id]['publicName'] = 'ANON';
          }
        }
        if (this.users[user.id].stats && this.users[user.id].stats.challengesPrizes) {
          this.users[user.id].stats.rewards = this.users[user.id].stats.challengesPrizes.reduce((acc: number, val: number) => {
            return acc + Object.values(val)[0] || 0;
          }, 0);
        }
        if (this.users[user.id].stats && this.users[user.id].stats.challengesPrizesBySport) {
          this.users[user.id].stats.rewards = { 'all': 0 };
          Object.keys(this.users[user.id].stats.challengesPrizesBySport).forEach(sport => {
            Object.keys(this.users[user.id].stats.challengesPrizesBySport[sport]).forEach((challenge_id: any) => {
              this.users[user.id].stats.rewards.all += this.users[user.id].stats.challengesPrizesBySport[sport][challenge_id];
              this.users[user.id].stats.rewards[sport] = this.users[user.id].stats.rewards[sport] || 0;
              this.users[user.id].stats.rewards[sport] += this.users[user.id].stats.challengesPrizesBySport[sport][challenge_id];
            });
          });
        }
      });
      this.calcOptimizedWinners();
    });
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.loadUserData();
  }

  updateUserStats() {
    alert('Users stats will be updated in background, keep the app open on any page');
    Object.keys(this.users).forEach((user_uid, index) => {
      setTimeout(() => {
        this.userService.calcStats(user_uid, true, true);
      }, index * 5 * 1000);
    });
  }

  calcOptimizedWinners() {
    this.optimizedWinners = [];
    if (this.users) {
      let stats: any = {};
      Object.keys(this.users).forEach(user_uid => {
        if ((this.users[user_uid]?.stats && this.users[user_uid]?.stats.rewards && this.users[user_uid]?.stats.rewards[this.sportSelected])) {
          stats = Object.assign({ gamesWon: 0, gamesWonBySport: { nhl: 0, nfl: 0, nba: 0 } }, this.users[user_uid].stats);
        }
        if (this.sportSelected === 'all' || (stats.gamesWonBySport[this.sportSelected])) {
          this.optimizedWinners.push({ sport: { all: stats.gamesWon || 0, nhl: stats.gamesWonBySport && stats.gamesWonBySport.nhl || 0, nfl: stats.gamesWonBySport && stats.gamesWonBySport.nfl || 0, nba: stats.gamesWonBySport && stats.gamesWonBySport.nba || 0 }, win: { all: this.users[user_uid]?.stats.rewards.all || 0, nhl: this.users[user_uid]?.stats.rewards.nhl || 0, nba: this.users[user_uid]?.stats.rewards.nba || 0, nfl: this.users[user_uid]?.stats.rewards.nfl || 0 }, id: user_uid })
        }
      });
      this.resetSortList();
      this.defaultList = this.optimizedWinners;
    }
  }

  segmentChanged(event) {
    this.segmentSelected = event.detail.value;
  }

  selectSport(sport) {
    this.sportSelected = sport;
    this.direction = 'desc';
    this.columnOrdered = 'win';
    if (this.sportSelected !== 'all') {
      this.optimizedWinners = this.optimizedWinners.sort((a, b) => {
        return a[this.sportSelected] > b[this.sportSelected] ? -1 : 1
      });
      this.resetSortList();
    } else {
      this.optimizedWinners = this.defaultList;
      this.resetSortList();
    }
  }

  resetSortList() {
    this.optimizedWinners = this.optimizedWinners.sort((a, b) => b['win'][this.sportSelected] - a['win'][this.sportSelected]);
    this.sortClass['sport'] = '';
    this.sortClass['win'] = 'desc';
  }

  isFriend(uid: string) {
    return this.userFriends.includes(uid);
  }

  loadUserData() {
    this.bidsLoading = true;
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(i => {
          this.userData = i;
          if (this.userData && this.userData['friends']) {
            (this.userData['friends'] || []).forEach(friend => {
              if (!friend.sent && !friend.blocked) {
                this.userFriends.push(friend.uid);
              }
            })
          }
        });
      }
      else {
        this.bidsLoading = false;
      }
    })
  }

  getUrl(uid) {
    return this.users[uid]?.photoURL || Constants.AVATAR_DEMO;
  }

  onSort(sortField) {
    this.direction = this.columnOrdered === sortField ? this.rotate[sortField][this.direction] : 'desc';
    if (this.direction === 'asc') {
      this.sortClass[sortField] = 'asc';
      if (sortField === 'sport') {
        this.sortClass['win'] = '';
      } else {
        this.sortClass['sport'] = '';
      }
      this.optimizedWinners = this.optimizedWinners.sort((a, b) => {
        return a[sortField][this.sportSelected] - b[sortField][this.sportSelected]
      });
    } else if (this.direction === 'desc') {
      this.sortClass[sortField] = 'desc';
      if (sortField === 'sport') {
        this.sortClass['win'] = '';
      } else {
        this.sortClass['sport'] = '';
      }
      this.optimizedWinners = this.optimizedWinners.sort((a, b) => {
        return b[sortField][this.sportSelected] - a[sortField][this.sportSelected]
      });
    }
    this.columnOrdered = sortField;
  }

}
