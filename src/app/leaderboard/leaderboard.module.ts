import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LeaderboardPage } from './leaderboard.page';
import { UserModule } from '../components/user/user.module';
import { DirectivesModule } from '../directives/directives.module';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';
const routes: Routes = [
  {
    path: '',
    component: LeaderboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    UserModule,
    DirectivesModule,
    HeaderModule,
    SubHeaderModule
  ],
  declarations: [LeaderboardPage]
})
export class LeaderboardPageModule {}
