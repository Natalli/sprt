import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  
  { path: 'start', loadChildren: () => import('./pages/auth/start/start.module').then(m => m.StartPageModule) },
  { path: 'login', loadChildren: () => import('./pages/auth/login/login.module').then(m => m.LoginPageModule) },
  { path: 'signup', loadChildren: () => import('./pages/auth/signup/signup.module').then(m => m.SignupPageModule) },
  { path: 'forgot', loadChildren: () => import('./pages/auth/forgot/forgot.module').then(m => m.ForgotPageModule) },
  { path: 'preferences', loadChildren: () => import('./pages/auth/preferences/preferences.module').then(m => m.PreferencesPageModule) },
  
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule) },
  { path: 'upcoming', loadChildren: () => import('./pages/upcoming/upcoming.module').then(m => m.UpcomingPageModule) },
  { path: 'challenges', loadChildren: () => import('./pages/challenges/challenges.module').then(m => m.ChallengesPageModule) },
  { path: 'private-games', loadChildren: () => import('./pages/challenges/challenges.module').then(m => m.ChallengesPageModule)},
  { path: 'trivia', loadChildren: () => import('./trivia/trivia.module').then(m => m.TriviaPageModule) },
  { path: 'chat', loadChildren: () => import('./chat/chat.module').then(m => m.ChatPageModule)},


  { path: '', canActivate: [AuthGuard], loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule) },
  { path: 'board', canActivate: [AuthGuard], loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule) },
  { path: 'detail', loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'detail/:team_id/:id/invite/:invite_id',  loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'detail/:team_id/:id/:challenge_id', loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'detail/:team_id/:id', loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'bid/:team_id/:id/invite/:invite_id', canActivate: [AuthGuard], loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'bid/:team_id/:id/invite/:invite_id/:user_id', canActivate: [AuthGuard], loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'bid/:team_id/:id/:challenge_id', canActivate: [AuthGuard], loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'bid/:team_id/:id', canActivate: [AuthGuard], loadChildren: () => import('./pages/detail/detail.module').then(m => m.DetailPageModule) },
  { path: 'edit-profile', canActivate: [AuthGuard], loadChildren: () => import('./edit-profile/edit-profile.module').then(m => m.EditProfilePageModule) },
  { path: 'profile', canActivate: [AuthGuard], loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule) },
  { path: 'profile/:id', canActivate: [AuthGuard], loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule) },
  { path: 'challenge', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'challenge/:id', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'nhl', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'nhl/:id', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'nfl', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'nfl/:id', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'nba', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'nba/:id', canActivate: [AuthGuard], loadChildren: () => import('./challenge/challenge.module').then(m => m.ChallengePageModule) },
  { path: 'rules', canActivate: [AuthGuard], loadChildren: () => import('./rules/rules.module').then(m => m.RulesPageModule) },
  { path: 'leaderboard', canActivate: [AuthGuard], loadChildren: () => import('./leaderboard/leaderboard.module').then(m => m.LeaderboardPageModule) },
  { path: 'history', canActivate: [AuthGuard], loadChildren: () => import('./history/history.module').then(m => m.HistoryPageModule) },
  
  { path: 'admin', canActivate: [AuthGuard], loadChildren: () => import('./admin/admin-tabs.module').then(m => m.AdminTabsPageModule) },
  
  { path: 'contact', canActivate: [AuthGuard], loadChildren: () => import('./contact/contact.module').then(m => m.ContactPageModule) },
  { path: 'privacy', canActivate: [AuthGuard], loadChildren: () => import('./privacy/privacy.module').then(m => m.PrivacyPageModule) },

  { path: 'friends', canActivate: [AuthGuard], loadChildren: () => import('./friends/friends.module').then(m => m.FriendsPageModule) },
  { path: 'tutorial', loadChildren: () => import('./tutorial/tutorial.module').then(m => m.TutorialPageModule) },
  { path: 'howto', loadChildren: () => import('./tutorial/tutorial.module').then(m => m.TutorialPageModule) },
  { path: 'invite/:sport_id', canActivate: [AuthGuard], loadChildren: () => import('./invite/invite.module').then(m => m.InvitePageModule) },
  { path: 'settings', canActivate: [AuthGuard], loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule) },
  { path: 'how-to-play', loadChildren: () => import('./how-to-play/how-to-play.module').then(m => m.HowToPlayPageModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
