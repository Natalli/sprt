import { Component, OnInit, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { LoadingController, ModalController, PopoverController, IonIcon } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';

import { Constants } from '../constants';
import { PopupMessageComponent } from '../popups/popup-message/popup-message.component';
import { ProfilePage } from '../pages/profile/profile.page';
import { LayoutService } from '../services/layout.service';
import { AppComponent } from '../app.component';
import { SearchService } from '../services/search/search.service';
import { UserService } from '../service/user.service';
import { ModalFriendInfoComponent } from '../popups/new/modal-friend-info/modal-friend-info.component';
import { ModalFriendManageComponent } from '../popups/new/modal-friend-manage/modal-friend-manage.component';
import { ModalConfirmComponent } from '../popups/new/modal-confirm/modal-confirm.component';
import { first } from 'rxjs';
import { ModalService } from '../service/modal.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.page.html',
  styleUrls: ['./friends.page.scss'],
})
export class FriendsPage implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  user;
  selfUser;
  users;
  friends: Array<Object> = [];
  myUid;
  invitations: Array<object> = [];
  recepient;
  search = '';
  pendingFriends: Array<Object> = [];
  blockedFriends: Array<Object> = [];
  checkFriendQ: boolean;
  closeBlock: boolean;
  hiddenFriend: boolean;
  props;
  constants = Constants;
  count: number;
  tabs = [{ name: 'friends' }, { name: 'requests' }, { name: 'sent' }, { name: 'blocked'}];
  tabSelected: string = 'friends';
  constructor(
    private afs: AngularFirestore,
    private loadingController: LoadingController,
    private modalController: ModalController,
    private popoverCtrl: PopoverController,
    protected layoutService: LayoutService,
    public myapp: AppComponent,
    protected searchService: SearchService,
    private userService: UserService,
    protected modalService: ModalService) { }

  ngOnInit() {
    this.getUser();
    this.getUsers();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  getUser() {
    this.userService.user$.subscribe((user) => {
      if ( !user ) {
        return;
      }
      else if ( user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2' ) {
        this.myapp.loginAlert();
      }
      this.myUid = user.uid;
      this.afs.collection("user").doc(this.myUid).valueChanges().subscribe(response => {
        this.user = response;
        this.selfUser = response;
        if (this.user.friends) {
          this.friends = this.user.friends.filter(friend => (friend.sent === false) ? friend : null);
          this.invitations = this.user.friends.filter(friend => (friend.isRecipient || friend.pending) ? friend : null);
          this.pendingFriends = this.user.friends.filter(friend => ((friend.sent) && (!friend.isRecipient && !friend.pending)) ? friend : null)
          this.blockedFriends = this.user.friends.filter(friend => friend.blocked ? friend : null)
        }
      })
    }) 
  }

  getUsers() {
    this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(users => {
      this.users = users;
      this.users.forEach(user => {
        this.users[user.id] = user;
      })
    })
  }

  selectedTab(event) {
    this.tabSelected = event.detail.value;
  }

  getFriendName(id: string) {
    let friend = this.users.filter((item) => (item['id'] || item['uid']) === id ? item : '')
    return friend[0]['name']
  }

  getFriendPhoto(id) {
    let friend = this.users.filter((item) => (item['id'] || item['uid']) === id ? item : '')
    return friend[0]['photoURL'] || Constants.AVATAR_DEMO
  }

  addFriend(user_uid) {
    var friends = this.selfUser.friends || [];
    friends.push({ uid: user_uid, sent: true });

    this.afs.collection("user").doc(user_uid).get().subscribe(user => {
      var vFriends = user.data()['friends'] || [];
      vFriends.push({ uid: this.user.uid, sent: true, pending: true });
      this.saveData(this.user.uid, friends, user_uid, vFriends, 'Your invitation has been sent');

      const notification = {
        title: "Friend request",
        text: "sent you an invitation to become a friend",
        platforms: ['ios', 'android', 'web'],
        start: new Date(),
        scheduling: 'Now',
        receivers: [user_uid],
        created: new Date(),
        friendRequest: true,
        sendRequest: true,
        sender: this.user.uid,
        // name: this.user.displayName,
        // photoURL: this.user.photoURL || Constants.AVATAR_DEMO
      };
      this.afs.collection('/notification/').add(notification);
      this.popoverCtrl.dismiss();
    })
  }

  blockedFriend(user, sent) {
    let inviteFriend = this.user.friends.find((u, i) => {
      if (u['uid'] === user) {
        let vFriends;
        this.user.friends[i] = {
          uid: user,
          blocked: sent
        };
        vFriends = this.user.friends
        this.saveData(null, null, this.myUid, vFriends, 'The user is blocked', sent)
        return u
      }
    })
    let index = this.invitations.indexOf(inviteFriend);
    if (index != -1) {
      this.invitations.splice(index, 1);
    }
  }

  acceptInvite(user, sent, unblocked?) {
    if (unblocked) {
      this.user.friends.find((u, i) => {
        if (u['uid'] === user) {
          let vFriends;
          this.user.friends[i] = {
            uid: user,
            sent
          }
          vFriends = this.user.friends;
          this.saveData(null, null, this.myUid, vFriends, 'You have unblocked ' + this.users[user]?.name, sent)
        }
      })
    } else {
      this.afs.collection("user").doc(user).get().subscribe(resp => {
        let userData = resp.data();
        userData['friends'].find((u, i) => {
          if (u['uid'] === this.myUid) {
            let friends = userData['friends'];
            userData['friends'][i] = {
              uid: this.myUid,
              sent
            }
            this.saveData(user, friends, null, null, null)
          }
        })
        let inviteFriend = this.user.friends.find((u, i) => {
          if (u['uid'] === user) {
            let vFriends;
            this.user.friends[i] = {
              uid: user,
              sent
            }
            vFriends = this.user.friends;
            this.saveData(null, null, this.myUid, vFriends, 'You accepted the invitation', sent)
            const notification = {
              title: "Friend request confirmed",
              text: "accepted your friend request",
              platforms: ['ios', 'android', 'web'],
              start: new Date(),
              scheduling: 'Now',
              receivers: [user],
              sender: this.myUid,
              sendRequest: true,
              created: new Date(),
            };
            this.afs.collection('/notification/').add(notification);
            return u
          }
        })
        let index = this.invitations.indexOf(inviteFriend);
        if (index != -1) {
          this.invitations.splice(index, 1);
        }
      })
    }

  }

  deleteFriend(index, uid, nameArray?, id?) {

    this.afs.collection("user").doc(uid).get().subscribe(user => {
      let friends = user.data()['friends'];
      let removeUser = friends.find(user => {
        if (user.uid && !user.blocked) {
          return user.uid === this.myUid
        }
      });
      let i = friends.indexOf(removeUser);
      if (i != -1) {
        friends.splice(i, 1);
      }
      this.saveData(uid, friends, null, null, null)
    })
    let vFriends = this.user.friends
    let removeVUser = vFriends.find(user => {
      if (user.uid) {
        return user.uid === uid
      }
    });
    let vI = vFriends.indexOf(removeVUser);
    if (vI != -1) {
      vFriends.splice(vI, 1);
    }
    this.saveData(null, null, this.myUid, vFriends, 'Successfully deleted');
    this.friends.splice(index, 1);
    if (nameArray === 'pending') {
      this.pendingFriends.splice(index, 1);
    } else if (nameArray === 'blocked') {
      this.blockedFriends.splice(index, 1);
    }
  }

  async confirmDelete(index, uid, text?, nameArray?, id?) {
    let confirmDeletePopup: boolean = true;
    const popover = await this.popoverCtrl.create({
      component: ModalConfirmComponent,
      componentProps: {
        confirmDeletePopup,
        confirmDeleteBtnName: 'Ok',
        deleteFriend: this.deleteFriend,
        saveData: this.saveData,
        index, uid, nameArray, id,
        afs: this.afs,
        user: this.user,
        friends: this.friends,
        myUid: this.myUid,
        loading: this.modalService.presentLoading,
        loadingController: this.loadingController,
        text,
        invitations: this.invitations,
        pendingFriends: this.pendingFriends,
        blockedFriends: this.blockedFriends
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  saveData(myUid, friends, userUid, vFriends, text1, sent?) {
    if (myUid && friends) {
      this.afs.collection("user").doc(myUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends });
        }
        else {
          item.ref.update({ friends });
        }
      });
    }
    if (userUid && vFriends && text1) {
      this.afs.collection("user").doc(userUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends: vFriends });
        }
        else {
          item.ref.update({ friends: vFriends });
          this.modalService.presentLoading(text1);
          // setTimeout(() => {
          //   this.loadingController.dismiss();
          // }, 3000)
        }
      });
    }
  }

  async manageFriends(ev: any, id: any) {    
    const popover = await this.popoverCtrl.create({
      component: ModalFriendManageComponent,
      componentProps: {
        chatId: id,
        afs: this.afs, 
        addFriend: this.addFriend,
        friends: this.user.friends,
        saveData: this.saveData,
        loading: this.modalService.presentLoading,
        loadingController: this.loadingController,
        checkFriend: this.checkFriend,
        checkFriendQ: this.checkFriendQ,
        selfUser: this.selfUser,
        popoverCtrl: this.popoverCtrl,
        searchService: this.searchService
      },
      cssClass: 'modal-wrap-default',
    });
    await popover.present();
  }

  async openChat(user_uid, event) {
    let friend = this.users.find((item) => item['id'] === user_uid ? item : '');
    friend.uid = friend.id;
    this.afs.collection('/user/' + this.myUid + '/chat', ref => ref
      .where('private', '==', user_uid))
      .valueChanges({ idField: 'id' })
      .pipe(
        first()
      ).subscribe((chat) => {
        if (chat.length) {
          this.props = {
            recepient: friend,
            chatId: chat[0]['id'],
            chat: chat[0],
            popoverCtrl: this.popoverCtrl,
            afs: this.afs,
          }
        } else {
          this.props = {
            recepient: friend,
            popoverCtrl: this.popoverCtrl,
            afs: this.afs,
          }
        }
        this.chatModal(this.props)
      })
  }

  async chatModal(props) {
    const modal = await this.modalController.create({
      component: PopupMessageComponent,
      componentProps: props,
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-default',
      id: 'popupMessages'
    });
    return await modal.present();
  }

  filterFriendList(array) {
    if (array && this.search) {
      const filteredRefs = [];
      array.forEach((friend: any) => {
        const user = this.users.filter(u => (friend.id || friend.uid) === u.id ? u : null);
        const name = user[0].name;
        if (name && name.toLowerCase().indexOf(this.search.toLowerCase()) !== -1) {
          filteredRefs.push(friend);
        }
      });
      return filteredRefs;
    }
    return array
  }

  checkFriend(uid) {
    if (this.selfUser.friends && this.selfUser.friends.length) {
      this.selfUser.friends.find(friend => {
        if (friend['uid'] === uid) {
          this.checkFriendQ = true;
          return this.checkFriendQ
        } else {
          this.checkFriendQ = false;
          return this.checkFriendQ
        }
      });
    }
  }

  hiddenUnblockedBtn(uid) {
    this.hiddenFriend = false;
    this.user.friends.find(friend => {
      if (friend.uid === uid) {
        if (friend.notFriend) {
          this.hiddenFriend = true;
        } else {
          this.hiddenFriend = false;
        }
      }
    });
  }

  async userInfo(uid) {
    const profilePopup: boolean = true;
    const modal = await this.modalController.create({
      component: ProfilePage,
      componentProps: {
        profilePopup,
        userId: uid,
        myUid: this.myUid,
        modalController: this.modalController,
        // recepient: this.users[uid],
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-profile',
    });
    return await modal.present();
  }

  async openInfo() {
    const popover = await this.popoverCtrl.create({
      component: ModalFriendInfoComponent,
      componentProps: {
        popoverCtrl: this.popoverCtrl,
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

}
