import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FriendsPage } from './friends.page';
import { UserModule } from '../components/user/user.module';
import { DirectivesModule } from '../directives/directives.module';
import { SearchServiceModule } from '../services/search/search.module';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';
import { ModalFriendInfoComponent } from '../popups/new/modal-friend-info/modal-friend-info.component';
import { IcoSvgModule } from '../components/ico-svg/ico-svg.module';
import { ModalFriendManageComponent } from '../popups/new/modal-friend-manage/modal-friend-manage.component';

const routes: Routes = [
  {
    path: '',
    component: FriendsPage
  }
];

@NgModule({
  declarations: [
    FriendsPage,
    ModalFriendInfoComponent,
    ModalFriendManageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    UserModule,
    DirectivesModule,
    SearchServiceModule,
    HeaderModule,
    SubHeaderModule,
    IcoSvgModule
  ]
})
export class FriendsPageModule { }
