import { Component, Input } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
})
export class TeamComponent {

  @Input() title: string;
  @Input() id: string;
  @Input() src: string;
  @Input() show: boolean = true;
  @Input() selected: boolean = false;

  Constants = Constants;
  constructor() {
    
  }

 

}