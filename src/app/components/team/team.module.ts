
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeamComponent } from './team.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { AvatarModule } from '../avatar/avatar.module';

@NgModule({
    declarations: [
        TeamComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        AvatarModule
    ],
    exports: [
        TeamComponent
    ],
})
export class TeamModule {
}
