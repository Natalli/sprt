
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NumberKeyboardComponent } from './number-keyboard.component';
import { IcoSvgModule } from 'src/app/components/ico-svg/ico-svg.module';
import { CloseModule } from '../close/close.module';

@NgModule({
    declarations: [
        NumberKeyboardComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule,
        CloseModule
    ],
    exports: [
        NumberKeyboardComponent
    ],
})
export class NumberKeyboardModule {
}
