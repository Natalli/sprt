import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-number-keyboard',
  templateUrl: './number-keyboard.component.html',
  styleUrls: ['./number-keyboard.component.scss'],
})

export class NumberKeyboardComponent implements OnInit {
  numerics = Constants.NUMERIC;
  activeClass: string = '';
  @Input() top: number;
  @Output() onSubmit = new EventEmitter();
  @Output() onClose = new EventEmitter();
  ratioTablet: number = 1;
  constructor(
    public popoverController: PopoverController) {
  }

  ngOnInit() {
    if (window.innerWidth >= 575) {
      this.ratioTablet = 1.25;
    } else {
      this.ratioTablet = 1;
    }
  }

  cancel() {
    this.onClose.emit();
  }

  setNumeric(event, value) {
    event.stopPropagation();
    const data = {
      ev: event,
      value
    }
    this.activeClass = value;
    this.onSubmit.emit(data);
  }

}
