
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SubHeaderComponent } from './sub-header.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { TeamModule } from '../team/team.module';

@NgModule({
    declarations: [
        SubHeaderComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule,
        TeamModule
    ],
    exports: [
        SubHeaderComponent
    ],
})
export class SubHeaderModule {
}
