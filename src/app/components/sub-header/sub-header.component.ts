import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Constants } from '../../constants';

import { UserService } from 'src/app/service/user.service';
import { IonSlides } from '@ionic/angular';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss'],
})
export class SubHeaderComponent implements OnInit {
  @ViewChild('slider') slides: IonSlides;
  @Input() active: boolean = false;
  @Input() selectedLeague: string = '';
  @Output() selectedLeagueChange = new EventEmitter<string>();

  Constants = Constants;
  Object = Object;

  slideOpts = {
    slidesPerView: 3,
    initialSlide: 1,
    centeredSlides: true,
    loop: true,
    slideToClickedSlide: false
  }

  leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
  userPrefsSub: any;
  user: any;
  routeSubs: any;

  constructor(private afs: AngularFirestore,
    private router: Router,
    private userService: UserService) {
    if (window.location && window.location.href.match(/challenges|bid|detail/g) !== null) {
      this.loadUserData();
    }
  }

  loadLeagues() {
    this.leagues = Object.keys(this.userService.userPrefs?.leagues || {}).length ?
      this.userService.userPrefs?.leagues : JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
  }

  ngOnInit() { }

  selectLeague(league) {
    if (this.selectedLeague && this.selectedLeague !== league && 
        window.location.href.match(/bid|detail|profile/g) !== null) {
        this.router.navigate(['/challenges'], {state: {league: league}});
    }
    else {
      this.selectedLeague = league;
      this.selectedLeagueChange.emit(this.selectedLeague);
    }
  }

  isLeagueActive(league) {
    return league && this.userService.userPrefs &&
      typeof this.userService.userPrefs.leagues &&
      typeof this.userService.userPrefs.leagues[league] !== 'undefined';
  }

  loadUserData() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.userService.loadUserPrefs();
        this.loadUserPrefs();
      }
    });
  }

  loadUserPrefs() {
    this.userPrefsSub = this.userService.userPrefs$.subscribe((userPrefs) => {
      if (userPrefs) {
        this.loadLeagues();
        this.setLeague();
      }
    });
  }

  setLeague() {
    if (!Object.keys(this.userService.userPrefs?.leagues||{}).length && 
        !this.selectedLeague && !history.state.league) {
      this.selectLeague('nfl');
    } 
    else if(Object.keys(this.userService.userPrefs?.leagues || {}).length && !history.state.league &&
            !Object.keys(this.userService.userPrefs?.leagues).includes(this.selectedLeague)) {
      this.selectLeague(Object.keys(this.userService.userPrefs?.leagues)[0]);
    }
  }

  slideChanged(data) {
    this.slides?.getActiveIndex().then(index => {
      const league = Object.keys(Constants.DEFAULT_LEAGUES)[index%3];
      if (league !== this.selectedLeague) {
        this.selectLeague(league);
      }
    });
  }

}
