
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CloseComponent } from './close.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';

@NgModule({
    declarations: [
        CloseComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        CloseComponent
    ],
})
export class CloseModule {
}
