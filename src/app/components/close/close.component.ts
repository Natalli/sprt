import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-close',
  templateUrl: './close.component.html',
  styleUrls: ['./close.component.scss'],
})
export class CloseComponent {
  @Output() onEvent = new EventEmitter();

  constructor() { }
  
  cancel() {
    this.onEvent.emit();
  }
}