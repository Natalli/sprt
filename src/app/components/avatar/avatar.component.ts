import { Component, Input } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent {

  @Input() id: string;
  @Input() src: string;
  @Input() show: boolean = true;

  Constants = Constants;
  constructor() {
    
  }
}