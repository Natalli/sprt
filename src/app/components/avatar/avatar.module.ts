
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AvatarComponent } from './avatar.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';

@NgModule({
    declarations: [
        AvatarComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        AvatarComponent
    ],
})
export class AvatarModule {
}
