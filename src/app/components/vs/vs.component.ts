import { Component, Input } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-vs',
  templateUrl: './vs.component.html',
  styleUrls: ['./vs.component.scss'],
})
export class VsComponent {
  @Input() class: string = '';

  Constants = Constants;
  constructor() { }

}