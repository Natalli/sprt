
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { VsComponent } from './vs.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';

@NgModule({
    declarations: [
        VsComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        VsComponent
    ],
})
export class VsModule {
}
