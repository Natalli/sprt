import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MentionComponent } from './mention.component';
import { UserModule } from '../user/user.module';

@NgModule({
    declarations: [
        MentionComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        UserModule
    ],
    exports: [
        MentionComponent
    ],
})
export class MentionModule {
}
