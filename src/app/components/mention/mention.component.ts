import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-mention',
  templateUrl: './mention.component.html',
  styleUrls: ['./mention.component.scss'],
})
export class MentionComponent implements OnInit {
  // @Output() onSelected: EventEmitter<any> = new EventEmitter();
  @Output() onSelected: EventEmitter<any> = new EventEmitter();
  @Input() set data(value) {
    if (value) {
      this.mentions = value;
    }
  }
  mentions: any;
  constants = Constants;

  constructor() { }

  ngOnInit() { }

  selected(item) {
    this.onSelected.emit(item);
  }

  getTrophyColor(winsCount: number) {
    let color = 'gray';
    switch (true) {
      case (winsCount === 1):
        color = '#cd7f32';
        break;
      case (winsCount < 5):
        color = 'silver';
        break;
      case (winsCount < 10):
        color = 'gold';
        break;
      case (winsCount >= 10):
        color = 'gold';
        break;
    }
    return color;
  }
}
