
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeamCardComponent } from './team-card.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { AvatarModule } from '../avatar/avatar.module';

@NgModule({
    declarations: [
        TeamCardComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        AvatarModule
    ],
    exports: [
        TeamCardComponent
    ],
})
export class TeamCardModule {
}
