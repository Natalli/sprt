import { Component, Input } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss'],
})
export class TeamCardComponent {

  @Input() game;
  @Input() league: string;

  Constants = Constants;

  constructor() {
    
  }
}