import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BalanceComponent } from './balance.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@NgModule({
    declarations: [
        BalanceComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        BalanceComponent
    ],
})
export class BalanceModule {
}
