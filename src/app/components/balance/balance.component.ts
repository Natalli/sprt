import { Component, Input } from '@angular/core';
import { Constants } from 'src/app/constants';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss'],
})
export class BalanceComponent {

  @Input() balance;
  Constants = Constants;

  constructor() {

  }

}