
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CardComponent } from './card.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';

@NgModule({
    declarations: [
        CardComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        CardComponent
    ],
})
export class CardModule {
}
