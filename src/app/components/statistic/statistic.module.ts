import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StatisticComponent } from './statistic.component';

@NgModule({
    declarations: [
        StatisticComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule
    ],
    exports: [
        StatisticComponent
    ],
})
export class StatisticModule {
}
