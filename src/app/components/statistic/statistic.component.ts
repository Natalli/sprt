import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss'],
})
export class StatisticComponent {
  @Input() stats;
  @Input() keys;
  @Input() show: boolean = false;
  @Input() homeId: number;
  @Input() awayId: number;
  Object = Object;
  
  constructor() {

  }

}