
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GameComponent } from './game.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { NumberKeyboardModule } from 'src/app/components/number-keyboard/number-keyboard.module';
import { VsModule } from '../vs/vs.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { ArticleModule } from '../article/article.module';
import { StatisticModule } from '../statistic/statistic.module';

@NgModule({
    declarations: [
        GameComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule,
        NumberKeyboardModule,
        VsModule,
        NgxSkeletonLoaderModule,
        ArticleModule,
        StatisticModule
    ],
    exports: [
        GameComponent
    ],
    providers: [
        InAppBrowser
    ]
})
export class GameModule {
}
