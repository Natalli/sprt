import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, HostListener, Input, Output, ViewChild, ViewChildren } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { LoadingController, PopoverController, RangeCustomEvent } from '@ionic/angular';
import { RangeValue } from '@ionic/core';
import { Constants } from 'src/app/constants';
import { environment } from 'src/environments/environment';
import { EngineService } from 'src/app/engine.service';
import { ModalDefaultComponent } from 'src/app/popups/new/modal-default/modal-default.component';
import { UserService } from 'src/app/service/user.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { Capacitor } from '@capacitor/core';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent {
  @ViewChildren('rangeAway') range_away: Range = new Range();
  @ViewChildren('rangeHome') range_home: Range = new Range();
  user: any;
  bids: any;
  newsSubs = [];
  @Input() set dataGame(value) {
    if (Object.keys(value).length) {
      this.game = value;
      if (this.game.gameData) {
        this.loadLeague();
        this.game.status = this.game.gameData.status;
      } else {
        this.inner.homeId = this.game.teams.home.team.id;
        this.inner.awayId = this.game.teams.away.team.id;
      }
      this.buildstandingKeys();
      this.loadGameBids()
      this.setCountdown();
    }
  }
  @Input() set dataSetTeams(value) {
    if (value) {
      this.setTeams = value;
      if (this.game && this.game.gameData) {
        this.loadLeague();
      }
    }
  }
  @Input() teams: any;
  @Input() title: string;
  @Input() src: string;
  @Input() id: string;
  @Input() challengeId: string;
  @Input() isPastGame: boolean;
  @Input() isChallengeGame: boolean;
  @Input() className: string;
  @Input() stats: any = {};
  @Input() countBids: number = 0;
  @Input() set reset(v) {
    if (v && this.range_away && this.range_away['first']) {
      this.resetSliders();
    }
  }
  @Input() set matrix(v) {
    if (typeof v !== 'undefined') {
      this.isFlipped = v;
      if (window.location && window.location.href.match(/detail/g) !== null) {
        this.isMatrixPage = true;
      }
    }
  }

  @Output() onCancel = new EventEmitter();
  @Output() onShowStats = new EventEmitter();

  pressedCount: number = 0;

  protected inner: { homeId?: any, awayId?: any } = {};
  Constants = Constants;
  Object = Object;

  lastEmittedValue: RangeValue;

  showMoreInfoHome: boolean = false;
  showMoreInfoAway: boolean = false;
  showGameInfo: boolean = false;
  showNumpad: boolean = false;
  setTeams: boolean = false;

  bid: any = { away: 0, home: 0, amount: 0, balance: 0 };
  numpadArr = [];
  game: any;
  standingKeys = [];
  leagues: any = {};
  countdown: string;
  countdownInterval: any;
  slideAway: RangeValue;
  slideHome: RangeValue;
  isFlipped: boolean = false;
  timer;
  scoreSide = { home: false, away: false };
  isScoreSide: boolean = false;
  numpadArrScore = { home: [], away: [] };
  isMatrixPage: boolean = false;
  isShowNews = { home: false, away: false };
  awayNews = [];
  homeNews = [];
  clicked: number = 0;

  constructor(private afs: AngularFirestore,
    private userService: UserService,
    private engineService: EngineService,
    private http: HttpClient,
    private router: Router,
    private loadingController: LoadingController,
    private datePipe: DatePipe,
    private popoverCtrl: PopoverController,
    private iab: InAppBrowser,
    protected modalService: ModalService) {
    this.loadUser();
  }

  get homeId() {
    return this.inner.homeId;
  }

  get awayId() {
    return this.inner.awayId;
  }

  get home() {
    if (this.game.gameData) {
      return this.game && this.leagues ?
        this.leagues[this.inner.homeId] : {};
    } else {
      return this.game && this.teams ?
        this.teams[this.inner.homeId] : {};
    }
  }

  get linescore() {
    return this.game.liveData ? this.game.liveData.linescore : this.game.linescore;
  }

  get away() {
    if (this.game.gameData) {
      return this.game && this.leagues ?
        this.leagues[this.inner.awayId] : {};
    } else {
      return this.game && this.teams ?
        this.teams[this.inner.awayId] : {};
    }
  }

  get homeScore() {
    if (this.game.gameData) {
      return this.game.liveData.linescore.teams.home.goals || 0;
    } else {
      return this.game.teams.home.score || 0;
    }
  }

  get awayScore() {
    if (this.game.gameData) {
      return this.game.liveData.linescore.teams.away.goals || 0;
    } else {
      return this.game.teams.away.score || 0;
    }
  }

  get gameTime() {
    return this.game?.gameData ? this.game.gameData.datetime.dateTime : (this.game?.gameDate ? this.game.gameDate : '');
  }

  get isGameScheduled() {
    return ['1'].includes(String(this.game.status?.statusCode || '1'));
  }

  get isGameOn() {
    return ['2', '3', '4', '5'].includes(String(this.game.status?.statusCode));
  }

  get isGameOver() {
    return ['6', '7'].includes(String(this.game.status?.statusCode));
  }

  get timePeriod() {
    return this.linescore?.currentPeriodTimeRemaining || this.game.liveData?.plays.currentPlay?.about.periodTimeRemaining || this.game.gameData?.status.detailedState || '-'
  }

  loadUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
      }
    })
  }

  loadLeague() {
    if (!this.setTeams) return;
    let teamNames = [this.game.gameData.teams.away.name, this.game.gameData.teams.home.name];
    teamNames.forEach(name => {
      this.afs.collection(`${this.game.sport}-team`, ref => ref
        .where('displayName', '==', name)
      ).get().subscribe(snapshot => {
        snapshot.docs.forEach(doc => {
          this.leagues[doc.data()['oid'] || doc.id] = doc.data();
        })
        this.inner.homeId = this.game.gameData.teams.home.id;
        this.leagues[this.inner.homeId]
        this.inner.awayId = this.game.gameData.teams.away.id;
      });
    })
  }

  loadGameBids() {
    if (this.game.gamePk) {
      if (!this.bids || !this.bids[this.game.gamePk]) {
        this.bids = this.bids || {};
        this.afs.collection("bid/").doc(String(this.game.gamePk)).valueChanges().subscribe(data => {
          if (this.game.gamePk) {
            this.bids[this.game.gamePk] = data;
            if (this.className === 'activated') {
              this.calculateBalance();
            }
          }
        });
      }
      else {
        if (this.className === 'activated') {
          this.calculateBalance();
        }
      }
    }
  }

  calculateBalance() {
    let count = 0;
    let balance = Constants.GRID_PREFS[this.game.sport] ? Constants.GRID_PREFS[this.game.sport].balance : 0;
    if (this.bids && this.bids[this.game.gamePk] &&
      this.bids[this.game.gamePk][this.user.uid]) {
      const userBids = Object.values(this.bids[this.game.gamePk][this.user.uid]) || [];
      userBids.forEach((row: Array<number>) => {
        row.forEach((bid: number) => {
          if (bid > 0.01) {
            balance -= bid - 0.01;
            count++;
          }
        });
      });
    }
    this.bid.count = count;
    this.bid.balance = balance <= 0.0109 ? 0 : Math.round(balance * 100) / 100;
  }

  cancel() {
    this.onCancel.emit();
  }

  show(ev, data, state, teamId, challengeId?, game?) {
    if (state === 'activated' && ev.target.classList?.value.indexOf('article') === -1) {
      if (data === 'away') {
        if (challengeId && game && (ev.target.classList?.value === 'trophy-box' || ev.target.classList?.value === 'ico-trophy')) {
          this.navigateChallenge(challengeId, game)
        }
        else {
          this.showMoreInfoAway = !this.showMoreInfoAway;
        }
      } else {
        this.showMoreInfoHome = !this.showMoreInfoHome;
      }
      Object.entries(this.isShowNews).forEach(v => this.isShowNews[v[0]] = false);
      this.onShowStats.emit(teamId);
    }
  }

  openDetail(game) {

  }

  showGame() {
    if (this.bid !== 'undefined' && this.bid.balance > 0) {
      this.showGameInfo = !this.showGameInfo;
    }
    else if (this.className === 'activated') {
      this.router.navigate([`/${(this.isPastGame || !this.isChallengeGame) ? 'bid' : 'detail'}/0/${this.game.gamePk}/${this.challengeId || 0}`]);
    }
  }

  onIonChange(ev: Event, side) {
    this.bid[side] = (ev as RangeCustomEvent).detail.value;
  }

  openNumpad(side) {
    if (side) {
      this.showNumpad = true;
      this.isScoreSide = true;
      this.scoreSide = { home: side === 'home', away: side === 'away' };
    } else {
      this.showNumpad = this.showGameInfo;
      this.resetBidAmount();
      this.isScoreSide = false;
      Object.values(this.isScoreSide).forEach(i => i = false);
    }
  }

  closeNumpad() {
    this.showNumpad = false;
  }

  resetBid() {
    this.bid = {
      away: 0,
      home: 0,
      amount: 0,
      balance: 0,
      count: 0
    };
    this.numpadArr = [];
    this.numpadArrScore = { home: [], away: [] };
    this.resetSliders();
  }

  resetBidAmount() {
    this.bid.amount = 0;
    this.numpadArr = [];
  }

  resetSliders(side?) {
    if (side && this[`range_${side}`]) {
      this[`range_${side}`]['first'].value = 0;
    }
    else {
      if (this.range_away && this.range_away['first']) {
        this.range_away['first'].value = 0;
      }
      if (this.range_home && this.range_home['first']) {
        this.range_home['first'].value = 0;
      }
    }
  }

  getGameBids() {

  }

  randomBid() {
    this.resetBidAmount();
    this.tapSingleBid(Math.floor(Math.random() * this.bid.balance * 100) / 100);
    return;
  }

  randomScore() {
    const side = Object.keys(this.scoreSide).find(key => this.scoreSide[key] ? key : null);
    const max = Constants.GRID_PREFS[this.game.sport]?.max;
    const min = 0;
    this.bid[side] = 0;
    this.bid[side] = Math.round(Math.random() * (max - min) + min);
    this.numpadArrScore[side] = String(this.bid[side]).split('');
    this[`range_${side}`]['first'].value = this.bid[side];
  }

  async placeBid() {
    if (!this.bid.away || !this.bid.home) {
      this.closeNumpad();
      return;
    }
    if (!this.bid.amount) {
      this.confirmModal(`Would you like to set your bid?`)
        .then(async (ev) => {
          if (!ev.data) {
            this.closeNumpad();
          }
        });
      return;
    }
    if (this.bid.balance <= 0) {
      this.confirmModal(`You don't have any balance to place a bid. Would you like to reset your bid?`)
        .then(async (ev) => {
          if (ev.data) {
            this.bid.amount = this.bid.balance;
          }
        });
      return;
    }
    if (this.bid.amount > this.bid.balance) {
      this.bid.amount = this.bid.balance;
    }
    await this.persistBid();
    this.resetBid();
    this.calculateBalance();
    if (this.bid.balance) {
      this.confirmModal(`Would you like to place another bid?`)
        .then(async (ev) => {
          if (!ev.data) {
            this.closeNumpad()
            this.resetBidAmount();
            this.resetSliders();
            this.showGameInfo = false;
            this.showMoreInfoAway = false;
            this.showMoreInfoHome = false;
          }
        });
    }
    else {
      this.closeNumpad()
      this.resetBidAmount();
      this.resetSliders();
      this.showGameInfo = false;
      this.showMoreInfoAway = false;
      this.showMoreInfoHome = false;
    }
  }

  async persistBid() {
    const bid = JSON.parse(JSON.stringify(this.bid));
    const bidPath = 'bid/' + this.game.gamePk;
    let bids = [];
    bids = this.generateBidsMatrix(bid);
    let updateData = { [this.user.uid]: Object.assign({}, bids) };
    this.modalService.presentLoading();
    return new Promise(resolve => {
      this.afs.doc(bidPath).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set(updateData, { merge: true });
        }
        else {
          item.ref.update(updateData);
        }
        this.bid.balance -= this.bid.amount + 0.01;
      }, error => {
        alert('Error: ' + error);
      }, () => {
        this.afs.doc("team/" + this.game.gamePk).get().subscribe(teams => {
          let team = JSON.parse(JSON.stringify(Constants.DEFAULT_GAME));
          new Promise(resolve => {
            if (!teams.exists) {
              team['users'] = [this.user.uid];
              teams.ref.set({ [team.id]: Object.assign({}, team) }, { merge: true }).finally(() => {
                resolve('set');
              });
            } else {
              team = teams.data()[team.id] || team;
              team['users'].push(this.user.uid);
              team['users'] = Array.from(new Set(team['users']));
              teams.ref.update({ [team.id]: Object.assign({}, team) }).finally(() => {
                resolve('update');
              });
            }
          }).finally(() => {
            const games = this.user.games || {};
            games[this.game.gamePk] = String(team.id || '0');
            this.afs.doc("user/" + this.user.uid).update({ games })
              .then(() => {
                this.loadingController.dismiss();
              })
              .catch((reason: any) => {
                alert('Error: ' + reason);
              })
              .finally(() => {
                resolve('saved');
              });
            this.afs.doc('debug/' + this.game.gamePk).get().subscribe(debugGame => {
              if (debugGame.exists) {
                debugGame.ref.update({ [this.user.uid]: new Date() });
              } else {
                debugGame.ref.set({ [this.user.uid]: new Date() });
              }
            });
            this.sendBidEmail();
          });
        });
      });
    }).finally(() => {
      this.loadingController.dismiss();
    });
  }

  generateBidsMatrix(bid) {
    let bids = [];
    if (this.bids && this.bids[this.game.gamePk] &&
      this.bids[this.game.gamePk][this.user.uid]) {
      bids = Object.values(this.bids[this.game.gamePk][this.user.uid]) || [];
    }
    const count = Constants.GRID_PREFS[this.game.sport].count;
    const defaultAmount = 0.01;
    const scoreMultiplier = Constants.GRID_PREFS[this.game.sport].scoreMultiplier;
    const scoreShift = Constants.GRID_PREFS[this.game.sport].scoreShift;
    let awayRow = this.engineService.calcWinScoreMiltiplied(bid.away, scoreMultiplier, scoreShift);
    awayRow = awayRow < count ? awayRow : count - 1;
    let homeCol = this.engineService.calcWinScoreMiltiplied(bid.home, scoreMultiplier, scoreShift);
    homeCol = homeCol < count ? homeCol : count - 1;
    let totalAmount = 0
    for (let r = 0; r < count; r++) {
      bids[r] = bids[r] || [];
      for (let c = 0; c < count; c++) {
        if (c != r || c == 6 || r == 6 || this.game.sport !== 'nhl' || !this.game.sport) {
          if (awayRow == r && homeCol == c) {
            bids[r][c] = (!bids[r][c] || bids[r][c] <= 0.0109 ? 0 : parseFloat(bids[r][c])) + Math.round(bid.amount * 100) / 100;
          }
          else { // User Bid
            bids[r][c] = bids[r][c] || defaultAmount;
          }
        } else { // Tied Score
          bids[r][c] = 0;
        }
        totalAmount += bids[r][c];
      }
    }
    return bids;
  }

  clearBid() {
    if (this.numpadArr[this.numpadArr.length - 1] === '.') {
      this.numpadArr.pop();
    }
    if (this.numpadArr.indexOf('.') === 1 && this.numpadArr.indexOf('.', 2) !== -1) {
      this.numpadArr = ['0', '.']
    }
    if (this.numpadArr.indexOf('0') === 1 && this.numpadArr.indexOf('0', 1) === 1) {
      this.numpadArr.shift();
    }
    this.numpadArr.pop();
    this.bid.amount = parseFloat(this.numpadArr.join('')) || 0;
  }

  clearScore() {
    const side = Object.keys(this.scoreSide).find(key => this.scoreSide[key] ? key : null);
    this.bid[side] = 0;
    this.numpadArrScore[side] = [];
    this.resetSliders(side);
  }

  tapBid(val) {
    if ((this.numpadArr.indexOf('.') !== -1 && val === '.') ||
      (this.numpadArr.indexOf('0') === 0 && val === '0' && this.numpadArr[this.numpadArr.length - 1] !== '.')) {
      return;
    }
    this.numpadArr.push(val);
    if (parseFloat(this.numpadArr.join('')) > this.bid.balance) {
      this.tapSingleBid(this.bid.balance);
    }
    else {
      this.bid.amount = parseFloat(this.numpadArr.join('')) || 0;
    }
  }

  tapScore(data, sides) {
    const side = Object.keys(sides).find(key => sides[key] ? key : null);
    const max = Constants.GRID_PREFS[this.game.sport]?.max;
    const value = parseFloat(this.numpadArrScore[side].join(''));
    this.numpadArrScore[side].push(data);

    if (value > max) {
      this.bid[side] = max;
    }
    else {
      this.bid[side] = parseFloat(this.numpadArrScore[side].join(''));
    }
    this[`range_${side}`]['first'].value = this.bid[side];
  }

  tapSingleBid(amount?) {
    this.bid.amount = typeof amount !== 'undefined' ? amount : this.bid.amount || this.bid.balance;
    this.numpadArr = String(this.bid.amount).split('');
  }

  submit(data) {
    if (this.isScoreSide) {
      switch (data.value) {
        case 'check':
          this.placeBid();
          this.isScoreSide = false;
          Object.values(this.numpadArrScore).forEach(i => i = []);
          Object.values(this.isScoreSide).forEach(i => i = false);
          break;
        case 'close':
          this.closeNumpad();
          break;
        case 'random':
          this.randomScore();
          break;
        case 'clear':
          this.clearScore();
          break;
        default:
          this.tapScore(data.value, this.scoreSide);
      }
    } else {
      switch (data.value) {
        case 'check':
          this.placeBid();
          break;
        case 'close':
          this.closeNumpad();
          this.resetBidAmount();
          break;
        case 'random':
          this.randomBid();
          break;
        case 'clear':
          this.resetBidAmount();
          break;
        default:
          this.tapBid(data.value);
      }
    }
  }

  navigateChallenge(challengeId, game, event?) {
    if (challengeId && game) {
      event?.stopPropagation();
      this.router.navigate([`/${game.sport}/${challengeId}`]);
    }
  }

  buildstandingKeys() {
    if (this.standingKeys.length) return;
    if (this.game && this.game.sport) {
      switch (this.game.sport) {
        case 'nfl':
          this.standingKeys = ['POS', 'W/L', 'HOME', 'AWAY', 'STRK', 'PF', 'PA', 'DIFF'];
          break;
        case 'nba':
          this.standingKeys = ['POS', 'W/L', 'HOME', 'AWAY', 'STRK', 'avgPF', 'avgPA', 'DIFF'];
          break;
        default:
      } 
    }
  }

  setCountdown() {
    if (!this.countdownInterval && this.className === 'activated') {
      this.countdown = undefined;
      let diff = 0;
      if (this.gameTime) {
        diff = (new Date(this.gameTime).getTime()) - (new Date().getTime());
        if (diff > 0) {
          this.countdown = this.calcCountdown(diff);
          this.countdownInterval = setInterval(() => { // TODO: Cleanup abandoned countdowns
            if (diff) {
              this.countdown = this.calcCountdown(diff);
              diff -= 1000;
            }
            else {
              this.resetCountdown();
            }
          }, 1000);
        }
      }
    }
  }

  calcCountdown(diff) {
    const date = new Date(Date.parse("2022-09-11T00:00:00") + diff);
    const days = Math.floor(diff / (24 * 60 * 60 * 1000));
    let countdown;
    if (days) {
      let hours = days * 24 + date.getHours();
      //countdown = String(hours) + this.datePipe.transform(date, ':mm:ss');
      countdown = `${days} days`;
    }
    else {
      countdown = this.datePipe.transform(date, 'HH:mm:ss');
    }
    return countdown;
  }

  resetCountdown() {
    if (typeof this.countdownInterval !== 'undefined') {
      clearInterval(this.countdownInterval)
      this.countdownInterval = undefined;
      this.countdown = undefined;
    }
  }

  flipped(force?) {
    this.isFlipped = typeof force !== 'undefined' ? force : !this.isFlipped;
  }

  get webFlipperBtn() {
    return Capacitor.getPlatform() === 'web' && window.innerWidth >= 1024;
  }

  pressed(event?) {
    if (Capacitor.getPlatform() === 'web' && window.innerWidth >= 1024) {
      this.clicked++;
      let time = setTimeout(() => {
        if (this.clicked === 1) {
          this.clicked = 0;
          this.switchPress(event);
        } else if (this.clicked === 2) {
          this.clicked = 0;
          this.flipped();
        }
      }, 200)
    } else {
      if (!this.timer) {
        this.timer = setInterval(() => {
          this.pressedCount++;
          if (this.pressedCount === 2) {
             this.switchPress(event);
            this.clearPress();
          }
        }, 100);
      }
    }
  }

  switchPress(event) {
    switch (event) {
      case Constants.BIDS.SINGLE:
        this.showGame();
        break;
      case Constants.BIDS.MATRIX:
        switch (this.game.status.statusCode) {
          case '1':
            this.router.navigate([`/detail/0/${this.game.gamePk}/${this.challengeId || 0}`]);
            break;
          default:
            this.router.navigate([`/bid/0/${this.game.gamePk}/${this.challengeId || 0}`]);
        }
        break;
      default:
    }
  }

  clearPress() {
    this.pressedCount = 0;
    clearInterval(this.timer);
    this.timer = null;
  }

  pressedFinish() {
    if (this.timer) {
      this.clearPress();
    }
  }

  showNews(ev) {
    if (this.showMoreInfoHome) {
      this.isShowNews['home'] = !this.isShowNews['home'];
      this.loadNews(this.homeId, 'home');
    }
    if (this.showMoreInfoAway) {
      this.isShowNews['away'] = !this.isShowNews['away'];
      this.loadNews(this.awayId, 'away');
    }
  }

  loadNews(teamId, side) {
    if (['nfl', 'nba'].includes(this.game.sport) && !this.newsSubs[teamId]) {
      this.newsSubs[teamId] = this.afs.collection(`/${this.game.sport}-news/`, ref => ref
        .where('teams', 'array-contains', parseInt(teamId))
        .orderBy('lastModified', 'desc')
        .limit(20)
      )
        .valueChanges().subscribe((articles: Array<any>) => {
          if (!this[`${side}News`]?.length) {
            this.updateNews(teamId);
          }
          this[`${side}News`] = articles;
        });
    }
  }

  updateNews(teamId) {
    if (['nfl', 'nba'].includes(this.game.sport)) {
      this.http.get(environment[this.game.sport + '_legacy_api'] + 'news?team=' + teamId + '&limit=20')
        .subscribe((data: any) => {
          this.persistNews(data.articles);
        });
    }
  }

  persistNews(articles) {
    articles?.forEach(article => {
      if (!article?.links?.api?.self?.href ||
        !article?.type || article.type === 'media') return;
      const match = (article?.links?.api?.self?.href).match(/(\d+)$/gm);
      article.src = 'espn';
      const articleId = article.src + (match?.length ? match[0] : 'unknown');
      this.afs.doc(`/${this.game.sport}-news/${articleId}`).get().subscribe(teamsDoc => {
        if (!teamsDoc.exists) {
          article.teams = [];
          article?.categories.forEach(category => {
            if (category.type === 'team') {
              article.teams.push(category.teamId);
            }
          });
          if (!article.teams.length || article.teams.length > 4) return;
          delete article.categories;
          article.images = article.images.slice(0, 2);
          article.type = article.type.toLowerCase();
          article.src = (article.src || 'news').toLowerCase();
          article.published = new Date(article.published);
          article.lastModified = new Date(article.lastModified);
          teamsDoc.ref.set(article);
        }
      });
    });
  }

  openArticle(data) {
    this.iab.create(data.article?.links?.web?.href, '_blank', 'location=no');
    setTimeout(() => {
      this.isShowNews[data.side] = false;
    }, 1000);
    return false;
  }

  sendBidEmail() {
    let subject = 'Your SPRT MTRX Bid ' + this.away.name + '@' + this.home.name + ' on ' + this.gameTime + '';
    let html = "Hello " + this.user.displayName + ",<br><br>\n\n"
      + 'Your SPRT MTRX Bid for ' + this.away.name + '@' + this.home.name + ' on ' + this.gameTime + ' was placed.'
      + "<br><br>\n\n" + 'See your Bid details and status here https://sprtmtrx.com/bid/0/' + this.game.gamePk + ('/' + this.challengeId)
      + "<br><br>\n\n" + 'SPRT MTRX Team.';
    //this.commonService.sendEmail(this.user.email, subject, html); // disable Bid notification
    //console.log((this.user.email, subject, html));
  }

  async confirmModal(text) {
    const popover = await this.popoverCtrl.create({
      component: ModalDefaultComponent,
      componentProps: {
        title: text,
        popoverCtrl: this.popoverCtrl
      },
      cssClass: 'modal-wrap-default'
    });
    await popover.present();
    return popover.onDidDismiss();
  }
}