import { environment } from 'src/environments/environment';
import { Component, OnInit, ViewChild, HostListener, ViewChildren, ElementRef, QueryList, Input, Output, EventEmitter, ChangeDetectorRef, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { IonContent, LoadingController, NavController, ModalController, IonIcon, ActionSheetController, ToastController, Platform } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import 'firebase/database';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Location, DatePipe } from '@angular/common';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import * as firebase from 'firebase/app';
import *  as deepEqual from 'fast-deep-equal';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Keyboard } from '@capacitor/keyboard';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { AppComponent } from 'src/app/app.component';
import { UserService } from 'src/app/service/user.service';
import { SettingsNotification } from 'src/app/interfaces/enum/settings.enum';
import { UsersService } from 'src/app/service/users.service';
import { NflService } from 'src/app/service/nfl.service';
import { NbaService } from 'src/app/service/nba.service';
import { Constants } from 'src/app/constants';
import { CommonService } from 'src/app/common.service';
import { EngineService } from 'src/app/engine.service';
import { LayoutService } from 'src/app/services/layout.service';
import { PopupTeamSearchComponent } from 'src/app/popups/popup-team-search/popup-team-search.component';
import { BalanceService } from 'src/app/service/balance/balance.service';
import { ModalGameProgressComponent } from 'src/app/popups/new/modal-game-progress/modal-game-progress.component';
import { ModalConfirmComponent } from 'src/app/popups/new/modal-confirm/modal-confirm.component';
import { ProfilePage } from 'src/app/pages/profile/profile.page';
import { ModalWinnerComponent } from 'src/app/popups/new/modal-winner/modal-winner.component';
import { ModalBidComponent } from 'src/app/popups/new/modal-bid/modal-bid.component';
import { ModalService } from 'src/app/service/modal.service';

export interface Config { }

@Component({
    selector: 'app-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit, AfterViewInit {
    @ViewChild(IonContent) content: IonContent;
    @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
    // @ViewChild('numpad', { read: ElementRef }) numpad: QueryList<ElementRef>;
    @Output() onOpenNumpad = new EventEmitter;
    @Output() onMultiBid = new EventEmitter();
    @Output() OnSelectCell = new EventEmitter();

    @Input() baseScore: any;
    @Input() winTabs: boolean = false;
    @Input() set data(value) {
        if (value) {
            this.finalGame = value;
            this.game = value;
            if (this.game.challenge_id && !this.challenge_id) {
                this.challenge_id = this.game.challenge_id;
            }
            this.scoreMultiplier = Constants.GRID_PREFS[this.game.sport].scoreMultiplier;
            this.scoreShift = Constants.GRID_PREFS[this.game.sport].scoreShift
        }
    }
    @Input() set bidData(value) {
        if (value) {
            this.finalistBids = value;
        }
    }
    @Input() fakeFinalist;
    @Input() bidSelected = '';
    @Input() sport;
    invite: any;
    sportTeams: any;
    @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
        this.resetBid(this.isBid ? 0 : '', false);
    }

    private gameSubs: Subscription;
    private inviteSubs: Subscription;
    private routeSubs: Subscription;
    public item: any;
    title = 'app';
    elementType = 'url';
    // url = window.location.href.replace('detail', 'product');
    readonly = true;
    game: any;
    grid = { count: 7, amount: 0, total: 0, max_users: 8, team_id: 0 };
    bids: Object = {};
    allBids: Object = {};
    winners: Object = {};
    bid = [];
    touchedBid = [];
    winningBid = [];
    winningUser = [];
    user: any;
    users: Object = {};
    standings: Object = {};
    standingKeys = [];
    isBid = this.router.url.indexOf('bid') !== -1;
    objectKeys = Object.keys;
    capacitor = Capacitor;
    intervalId: any = '';
    legacyBid: boolean;
    teams: any;
    isAdmin: boolean = false;

    ref;
    moreStats: boolean;
    morePlays: boolean;
    game_id: string;
    challenge_id: string;
    invite_id: string;
    team_id: string;
    isWinning: boolean;
    awayGoals: number;
    homeGoals: number;
    isFillGroup: boolean;
    fillCells: Array<Object> = [];
    selectedRow: number = -1;
    selectedCol: number = -1;
    scoreMultiplier = 1;
    scoreShift = 0;
    isSelected: boolean;
    currentWinner: any;
    isShowPopup: boolean;
    showPopup: boolean;
    // settings: ISettings;
    notificationLabel: typeof SettingsNotification = SettingsNotification;
    templates;
    isDisabledSubmit: boolean;
    userBids: Object = null;
    saveTemplate: boolean;
    isFavorite: boolean = false;
    favoriteEmitter = new BehaviorSubject<boolean>(this.isFavorite);
    nextBtn: boolean;
    prevBtn: boolean;
    challengeGames: any;
    currentGame: any;
    challenge: any;
    games: any;
    goals: any;
    invite_user: any;
    notLoggedUser: boolean = false;
    auth;
    Constants = Constants;
    leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
    showNumpad: boolean = false;
    numpadArr = [];
    topPosition: number = 0;
    isSetValue: boolean = false;
    isTablet: boolean = false;
    isDesktop: boolean = false;
    finalGame;
    finalistBids;
    finalistBidsValue = [];
    gridClasses = Array.apply(null, Array(7)).map(() => Array.apply(null, ['', '', '', '', '', '', '']));
    zoomRatio = 1;

    constructor(private route: ActivatedRoute,
        public router: Router,
        private popoverCtrl: PopoverController,
        private http: HttpClient,
        private commonService: CommonService,
        public engineService: EngineService,
        private afs: AngularFirestore,
        public alertController: AlertController,
        private location: Location,
        private fns: AngularFireFunctions,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        public modalController: ModalController,
        protected layoutService: LayoutService,
        private datePipe: DatePipe,
        public usersService: UsersService,
        protected userService: UserService,
        private actionSheetController: ActionSheetController,
        public myapp: AppComponent,
        // protected searchService: SearchService,
        protected toastController: ToastController,
        private nativeAudio: NativeAudio,
        protected platform: Platform,
        private balanceService: BalanceService,
        protected modalService: ModalService
    ) {
        this.routeSubs = this.router.events.subscribe((e) => {
            if (e instanceof NavigationEnd && e.url.match(/bid|detail/g) === null) {
                this.ionViewWillLeave();
            }
        });
        this.userService.auth = getAuth();
        this.backButton();
        this.game = {};
        this.bids = {};
        this.user = { balance: 10 };
        this.users = this.usersService.users;
        this.resetBid(this.isBid ? 0 : 0.01, false);
        onAuthStateChanged(this.userService.auth, (user) => {
            if (user) {
                this.user = user;
                this.isAdmin = environment.admins.includes(user.uid);

                this.route.params.subscribe(params => {
                    this.isBid = this.router.url.indexOf('bid') !== -1;
                    this.game_id = params.id;
                    this.invite_id = params.invite_id || '';
                    this.challenge_id = this.challenge_id || params.challenge_id || '';
                    this.team_id = params.team_id;
                    this.invite_user = params.user_id;
                    this.legacyBid = (parseInt(params.id) > 2019020402 && parseInt(params.id) < 2019020565);

                    if (!this.intervalId) { // TODO: optimize live updates
                        this.intervalId = setInterval(
                            () => {
                                if (this.game_id) {

                                    this.updateLiveGame(this.game_id);
                                }
                            },
                            1000 * ((this.isAdmin || this.isInviteAdmin()) ? 10 : 120));
                    }

                    this.afs.collection("user").doc(user.uid).valueChanges().subscribe(item => {
                        if (item) {
                            this.user.balance = item['balance'] | 10; // $10 default balance
                            this.user.color = item['color'] || '';
                            this.user.publicName = item['publicName'] || this.user.displayName.slice(0, 3).toUpperCase();
                            this.user.games = item['games'];
                            this.user.friends = item['friends'];
                            var userBids = this.userBids || {};
                            this.userBids = {};
                            Object.keys(item['games'] || {}).forEach(game_id => {
                                this.userBids[game_id] = userBids[game_id] || { [user.uid]: item['games'][game_id] };
                            });
                            this.user.name = item['name'];
                            this.user.tokens = item['tokens'];
                            if (item['template']) {
                                let templates = item['template'];
                                // this.afs.doc('/user/' + this.user.uid + '/template/' + 'nhl').delete();
                                this.afs.doc('/user/' + this.user.uid + '/template/nhl').get().subscribe(data => {
                                    if (!data.exists) {
                                        data.ref.set(templates);
                                    } else {
                                        data.ref.update(templates)
                                    }
                                })
                                this.afs.collection('user').doc(this.user.uid).update({ template: null });
                            }
                            if (this.isBid || true) { // TODO: optimize live updates
                                if (item['games'] && !this.invite_id) {
                                    this.grid.team_id = params.team_id || 0;
                                    this.afs.collection("team/").doc(params.id).valueChanges().subscribe(teams => {
                                        var team = (teams || { [this.grid.team_id]: { bid_amount: this.user.balance } })[this.grid.team_id];
                                        this.grid.max_users = (team || { max_users: 8 }).max_users;
                                        this.grid.total = (team.bid_amount || this.user.balance) * (team.users || []).length;
                                        this.teams = teams;
                                    });
                                }
                            }

                        } else {
                            this.user.balance = 10; // $10 default balance
                        }
                    })

                    if (this.challenge_id) {
                        this.afs.collection("team/").doc(params.id).valueChanges().subscribe(teams => {
                            let team;
                            if (this.grid.team_id && teams) {
                                team = teams[this.grid.team_id];
                            } else {
                                team = this.getOpenGame(teams);
                                this.grid.max_users = team.max_users || 8;
                                this.grid.team_id = parseInt(params.team_id || 0, 10);
                            }
                            if (true) { // Allow multiple bids
                                if (teams && teams[this.grid.team_id] && typeof team.locked === 'undefined' &&
                                    this.game && this.game.gameData && team.users && team.users.length && // fix possible overwriting of existing shuffled team
                                    ['2', '3', '6', '7'].includes(this.game.gameData.status.statusCode)) { // Auto lock Game, not including Pre-Game
                                    team.locked = true;
                                    this.afs.collection("team/").doc(params.id).update({ [this.grid.team_id]: team });
                                }
                                this.afs.collection("bid/").doc(params.id).valueChanges().subscribe(item => {
                                    if (this.legacyBid) {
                                        this.bids[params.id] = item[this.grid.team_id];
                                    } else {
                                        this.bids[params.id] = this.commonService.getObjectOrderedKeys(item || {}, team['users']);
                                        this.allBids[params.id] = item || {};
                                    }

                                    if (this.bids[params.id] && typeof this.bids[params.id][user.uid] !== 'undefined') {
                                        this.bid = Object.values(this.bids[params.id][user.uid]);
                                        this.bidSelected = user.uid;
                                        this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0));
                                        this.calculateBalance();
                                    }
                                    this.loadUsers(team.users).then(() => {
                                        this.currentWinner = this.engineService.getTeamDetails(this.engineService.calcCurrentWinner(this.game, teams, this.bids[params.id], this.scoreMultiplier, this.scoreShift), this.users, true)[0];
                                        // this.sendNotification('GameCurrentWinner', null, this.game, this.teams[this.team_id].winners)
                                        // this.gameCurrentWinner(this.currentWinner)
                                    });
                                });
                            }
                        });
                        this.loadGames();
                    }
                    else if (this.invite_id) {
                        this.getInvite(params, user);
                    }
                });

            } else {
                this.notLoggedUser = true;
                this.route.params.subscribe(params => {
                    this.game_id = params.id;
                    this.invite_id = params.invite_id || '';
                    this.team_id = params.team_id;
                    this.invite_user = params.user_id;
                    this.getInvite(params);
                })
            }
        });
        this.loadSchedule();
        // this.settings = this.userService.settings;
    }

    get homeName() {
        if (this.game && this.game.gameData) {
            return this.game.gameData.teams.home.name;
        } else {
            return this.finalGame ? this.finalGame?.teams.home.team.name : '';
        }
    }

    get awayName() {
        if (this.game && this.game.gameData) {
            return this.game.gameData.teams.away.name;
        } else {
            return this.finalGame ? this.finalGame.teams.away.team.name : '';
        }
    }

    get gameStatus() {
        if (this.game && this.game.gameData) {
            return this.game.gameData.status.statusCode;
        } else {
            return this.finalGame ? this.finalGame.status.statusCode : '';
        }
    }

    get homeScore() {
        if (this.game && this.game.liveData) {
            return this.game.liveData.linescore.teams.home.goals;
        } else {
            return this.finalGame ? this.finalGame?.teams.home.score : '';
        }
    }

    get awayScore() {
        if (this.game && this.game.liveData) {
            return this.game.liveData.linescore.teams.away.goals;
        } else {
            return this.finalGame ? this.finalGame?.teams.away.score : '';
        }
    }

    getInvite(params, user?) {
        this.inviteSubs = this.afs.collection('invite').doc(this.invite_id).valueChanges().subscribe((invite: any) => {
            this.grid.max_users = 8;
            this.grid.team_id = 0;
            if (invite && typeof invite.locked === 'undefined' && this.game && this.game.gameData &&
                ['2', '3', '6', '7'].includes(this.game.gameData.status.statusCode)) { // Auto lock Invite Game
                console.log('locking');
                this.afs.collection('invite').doc(this.invite_id).update({ locked: true });
                invite.locked = true;
                this.sendNotification(this.notificationLabel.gameStart, invite);
            }
            this.invite = invite;
            this.teams = [invite];
            if (this.isBid) {
                if (invite && invite.bids && invite.bids[params.id]) {
                    this.bids[params.id] = this.commonService.getObjectOrderedKeys(invite.bids[params.id], invite.users);
                    this.allBids[params.id] = invite.bids[params.id];
                }
                if (this.bids[params.id] && typeof this.bids[params.id][this.invite_user ? this.invite_user : user.uid] !== 'undefined') {
                    this.bid = Object.values(this.bids[params.id][this.invite_user ? this.invite_user : user.uid]);
                    this.bidSelected = this.invite_user ? this.invite_user : user.uid;
                    this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0));
                    this.calculateBalance();
                }
                this.loadUsers(invite.users).then(() => {
                    this.currentWinner = this.engineService.getTeamDetails(this.engineService.calcCurrentWinner(this.game, [{ users: invite.users }], this.bids[params.id], this.scoreMultiplier, this.scoreShift), this.users, true)[0];
                    if (typeof this.invite !== 'undefined' && this.invite.locked) {
                        setTimeout(() => {
                            this.openTab('winning');
                        }, 1000);
                    }
                    if (this.currentWinner && this.currentWinner.uid && (!this.invite.winners || !this.invite.winners.length) &&
                        this.game.gameData && this.game.gameData.status && ['6', '7'].includes(this.game.gameData.status.statusCode)) {
                        // INVITE GAME OVER PROCESSING ENABLED
                        this.openWinnerPopup(this.invite_id, this.currentWinner.uid, 'inviteShowPopups');
                        this.invite.winners = [this.currentWinner.uid];
                        this.afs.collection('invite').doc(this.invite_id).update({ winners: [this.currentWinner.uid] });
                        this.sendNotification(this.notificationLabel.gameEnd, this.invite);
                    }
                });
            }
        });
    }

    buildstandingKeys() {
        if (this.standingKeys.length) return;
        if (this.game && this.game.sport) {
            switch (this.game.sport) {
                case 'nfl':
                    this.standingKeys = ['POS', 'W/L', 'HOME', 'AWAY', 'STRK', 'PF', 'PA', 'DIFF'];
                    break;
                case 'nba':
                    this.standingKeys = ['POS', 'W/L', 'HOME', 'AWAY', 'STRK', 'avgPF', 'avgPA', 'DIFF'];
                    break;
                default:
            }
        }
    }

    async loadUsers(uids) {
        this.usersService.load(uids);
    }

    async loadUsers2(uids?) {
        return new Promise((resolve, reject) => {
            if (typeof uids !== 'undefined' && uids.length) {
                uids.forEach((uid, index) => {
                    if (!this.users[uid]) {
                        this.afs.doc("user/" + uid).valueChanges().subscribe(user => {
                            this.users[uid] = user;
                            this.users[uid].id = uid;
                            if (uids.length === index + 1) {
                                resolve(this.users);
                            }
                        });
                    }
                    else if (uids.length === index + 1) {
                        resolve(this.users);
                    }
                });
            }
            else {
                this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(users => {
                    users.forEach(user => {
                        this.users[user.id] = user;
                        if (!this.users[user.id]['publicName']) {
                            if (this.users[user.id]['name']) {
                                this.users[user.id]['publicName'] = this.users[user.id]['name'].slice(0, 3).toUpperCase();
                            } else {
                                this.users[user.id]['publicName'] = 'ANON';
                            }
                        }
                    });
                    resolve(this.users);
                });
            }
        });
    }

    ngOnInit() {
        setTimeout(() => {
            if (!this.game) { // Navigate back to Challenge in case of loading error
                this.router.navigate(['/' + (this.game.sport || 'nhl') + '/' + (this.challenge_id || '')]);
            }
            else {
                if (Capacitor && ['ios', 'android'].includes(Capacitor.getPlatform())) {
                    //this.nativeAudio.preloadComplex('ping', 'assets/audio/ping.m4a', 0.3, 1, 0);
                    //this.nativeAudio.preloadComplex('pong', 'assets/audio/pong.m4a', 0.3, 1, 0);
                }
            }
        }, 3000);
        if (window.innerWidth >= 575 && window.innerWidth < 1024) {
            this.isTablet = true;
            this.zoomRatio = 1.25;
        } else {
            this.isTablet = false;
            this.zoomRatio = 1;
        }
        if (window.innerWidth >= 1024) {
            this.isDesktop = true;
        } else {
            this.isDesktop = false;
        }
        if (this.isBid || this.finalGame) {
            if (!Object.values(this.game || {}).length || !Object.values(this.bids || {}).length) {
                setTimeout(() => {
                    this.ionViewDidEnter();
                }, 1500);
                return;
            }
        }
    }

    loadGames() {
        this.afs.collection('challenge').doc(this.challenge_id).valueChanges().subscribe((challenge: any) => {
            this.challenge = challenge;
            if (this.challenge) {
                this.afs.collection(this.challenge.sport)
                    .valueChanges().subscribe((games: Array<any>) => {
                        this.games = games;
                        if (this.games) {
                            this.getChallengeGames();
                            this.currentGame = this.challengeGames.findIndex(game => game === this.game_id ? game : null);
                            if (this.currentGame > 0) {
                                this.prevBtn = true;
                            }
                            if (this.currentGame < this.challenge.games.length - 1) {
                                this.nextBtn = true;
                            }
                        }
                    });
            }
        })
    }

    async loadSportStandings(sport) {
        this.afs.collection(sport + '-stats').valueChanges({ idField: 'id' }).subscribe(standings => {
            standings.forEach(stat => {
                this.standings[stat.id] = stat;
                delete this.standings[stat.id].id;
            });
        });
    }

    async loadNHLStandings() {
        this.afs.collection('standings').valueChanges().subscribe(standings => {
            standings.forEach(standing => {
                if (standing['teamRecords']) {
                    standing['teamRecords'].forEach(teamRecord => {
                        if (teamRecord['team']['id'] == this.game['gameData']['teams']['away']['id']
                            || teamRecord['team']['id'] == this.game['gameData']['teams']['home']['id']) {
                            let overallRecords = [];
                            teamRecord['records']['overallRecords'].forEach((record, i) => {
                                if (['home', 'away', 'lastTen'].includes(record['type'])) {
                                    record['type'] = record['type'].replace(/lastTen/gi, 'last10');
                                    overallRecords.push(record);
                                }
                            })
                            teamRecord['records']['overallRecords'] = overallRecords;
                            teamRecord['streak'] = teamRecord['streak'] || { streakCode: "-", streakNumber: "-", streakType: "-" };
                            this.standings[teamRecord['team']['id']] = teamRecord;
                        }
                    })
                }
            });
        });
    }

    async loadSchedule(event?) {
        if (event && event.type === 'ionRefresh') {
            setTimeout(() => {
                event.target.complete();
            }, 2000);
        }

        this.route.params.subscribe(params => {
            this.gameSubs = this.afs.doc('live/' + params.id).valueChanges().subscribe((game: any) => {
                setTimeout(() => {

                    this.updateLiveGame(params.id);
                }, !game ? 0 : 1000);
                if (!game) {
                    return;
                }
                if (!this.game.sport && !game.sport && !this.isBid) {
                    setTimeout(() => {
                        this.resetBid(this.isBid ? 0 : 0.01, false);
                    }, 10);
                }
                this.game = game;
                //this.game.gameData.status.statusCode = '6';
                this.game.sport = this.game.sport || 'nhl';
                this.sportTeams = { [this.game.gameData.teams.away.id]: this.game.gameData.teams.away.triCode, [this.game.gameData.teams.home.id]: this.game.gameData.teams.home.triCode };
                this.buildstandingKeys();
                this.scoreMultiplier = Constants.GRID_PREFS[this.game.sport].scoreMultiplier;
                this.scoreShift = Constants.GRID_PREFS[this.game.sport].scoreShift
                if (this.game.sport === 'nba' && this.game.scoringPlays) {

                    this.getGameGoals(this.game.scoringPlays.reverse());
                    this.game.scoringPlays.reverse()
                }
                this.afs.doc('/user/' + this.user.uid + '/template/' + this.game.sport).valueChanges().subscribe(data => {
                    this.templates = data || {};
                });
                if (!this.isBid && this.game.gameData && this.game.gameData.status &&
                    ((!['1', '2', '8', '9'].includes(String(this.game.gameData.status.statusCode))) ||
                        (typeof this.teams !== 'undefined' && typeof this.teams[0] !== 'undefined' && typeof this.teams[0].locked !== 'undefined' && this.teams[0].locked))) {
                    this.router.navigate(['/bid/' + params.team_id + '/' + params.id + (params.invite_id ? ('/invite/' + params.invite_id) : ('/' + this.challenge_id))], { replaceUrl: true });
                }
                else {
                    if (!this.isBid) {
                        const bidPath = params.invite_id ? ('invite/' + params.invite_id) : ('bid/' + this.game.gamePk);
                        this.afs.doc(bidPath).get().subscribe(docSnapshot => {
                            const data = docSnapshot.data();
                            let bids = {};
                            if (params.invite_id) {
                                if (data && data['bids'] && data['bids'][params.id]) {
                                    bids = data['bids'][params.id]
                                }
                            }
                            else {
                                bids = data;
                            }
                            if (docSnapshot.exists && !this.invite_user && (!['1'].includes(String(this.game.gameData.status.statusCode)) ||
                                (params.invite_id && data['locked']))) {
                                console.log('game started or invite game is locked');
                                this.router.navigate(['/bid/' + params.team_id + '/' + params.id + (params.invite_id ? ('/invite/' + params.invite_id) : ('/' + this.challenge_id))], { replaceUrl: true });
                                return;
                            }
                        });
                    }
                    if (this.game['gameData']['game']['season']) {
                        if (params.invite_id && this.homeGoals !== this.game['liveData']['linescore']['teams']['home']['goals'] || this.awayGoals !== this.game['liveData']['linescore']['teams']['away']['goals']) {
                            const curWinner = this.engineService.getTeamDetails(this.engineService.calcCurrentWinner(this.game, this.teams, this.bids[params.id], this.scoreMultiplier, this.scoreShift), this.users, true)[0];
                            //this.sendNotification(this.notificationLabel.goalScored, this.invite);
                            if (curWinner && curWinner !== this.currentWinner && !['6', '7'].includes('' + this.game.gameData.status.statusCode)) { // disabled
                                //console.log('new current winner', curWinner.uid,this.currentWinner?.uid);
                                //this.sendNotification(this.notificationLabel.gameCurrentWinner, null, this.game, curWinner)
                            }
                            else {
                                //console.log('no new winner');
                            }
                        }
                        this.awayGoals = this.game['liveData']['linescore']['teams']['away']['goals'];
                        this.homeGoals = this.game['liveData']['linescore']['teams']['home']['goals'];
                        setTimeout(() => {
                            const currentWinners = this.engineService.calcCurrentWinner(this.game, this.teams, this.bids[params.id], this.scoreMultiplier, this.scoreShift);
                            this.setCurrentWinner(currentWinners);
                        }, (!this.game || !this.teams || !this.bids[params.id]) ? 2000 : 10);
                    }
                    if (!this.game.sport || this.game.sport === 'nhl') {
                        this.loadNHLStandings();
                    }
                    else {
                        this.loadSportStandings(this.game.sport);
                    }

                    if (this.game.gameData && this.game.gameData.status && ['6', '7'].includes('' + this.game.gameData.status.statusCode)) {
                        // INVITE GAME OVER PROCESSING ENABLED
                        if (this.invite_id && this.invite && (!this.invite.winners || !this.invite.winners.length)) {
                            if (this.currentWinner) {
                                console.log('updating winners');
                                this.invite.winners = [this.currentWinner.uid];
                                this.afs.collection('invite').doc(this.invite_id).update({ winners: [this.currentWinner.uid] });
                                this.sendNotification(this.notificationLabel.gameEnd, this.invite, this.game, [this.currentWinner.uid]);
                            }
                        }
                        else { // GAME OVER PROCESSING TEMP DISABLED
                            setTimeout(() => {
                                if (false && this.teams && this.teams[this.team_id]
                                    && (!this.teams[this.team_id].winners || !this.teams[this.team_id].winners.length)) {
                                    console.log('winner empty', params.id, this.game, this.allBids);
                                    this.updatePlayStats(this.game.gamePk);
                                    this.engineService.process2GameOver(params.id, this.game, this.allBids);
                                    // this.sendNotification(this.notificationLabel.gameCurrentWinner, null, this.game, this.teams[this.team_id].winners)
                                }
                            }, typeof this.teams === 'undefined' ? 10000 : 3000);
                        }
                    } else {
                        if (event && event.type === 'ionRefresh') {
                            event.target.complete();
                        }
                    }
                }
                if (this.game && this.game.gameData && this.game.gameData.status && ['7'].includes(this.game.gameData.status.statusCode)) {
                    if (this.intervalId) {
                        clearTimeout(this.intervalId);
                    }
                }
            });
        });
    }

    setCurrentWinner(uids) {
        const winnerInterval = setInterval(() => {
            if (Object.keys(this.users || {}).length) {
                this.currentWinner = this.engineService.getTeamDetails(uids, this.users, true)[0];
                clearInterval(winnerInterval);
            }
        }, 100);
    }

    gameCurrentWinner(currentWinner) {
        this.sendNotification(this.notificationLabel.gameCurrentWinner, null, this.game, currentWinner)
    }

    openWinnerPopup(id, winner, localStorageKey) {
        const showPopups = JSON.parse(localStorage.getItem(localStorageKey)) || [];
        if (showPopups.indexOf(id) === -1 && winner) {
            if (!this.showPopup) {
                this.showPopup = true;
                this.winnerPopup(id, winner, localStorageKey)
            }
        }
    }

    updatePlayStats(gamePk) {
        this.http.get(environment[this.game.sport + '_legacy_api'] + 'summary?event=' + gamePk)
            .subscribe((data: any) => {
                if (data.pickcenter && data.pickcenter.length) {
                    let gameOdds = {}
                    data.pickcenter.forEach((odds, index) => {
                        if (index === 0 || parseInt(odds.provider.id) === 41) { // default 'SugarHouse' pickcenter provider
                            gameOdds = odds;
                        }
                    });
                    if (Object.keys(gameOdds).length && !deepEqual(this.game.odds, gameOdds)) {
                        this.afs.doc('live/' + gamePk).update({ odds: gameOdds });
                        console.log('updated odds');
                    }
                }

                if ((data.scoringPlays && data.scoringPlays.length) || (data.drives && Object.keys(data.drives).length)) { // NFL
                    const updateData: any = { currentDrive: '' };
                    if (data.drives && data.drives.current && data.drives.current.plays
                        && data.drives.current.plays && data.drives.current.plays.length) {
                        if (data.drives.current.plays[data.drives.current.plays.length - 1].end.downDistanceText) {
                            updateData.currentDrive = data.drives.current.plays[data.drives.current.plays.length - 1].end.possessionText + ' ' + data.drives.current.plays[data.drives.current.plays.length - 1].end.shortDownDistanceText;
                            if (data.drives.current.plays[data.drives.current.plays.length - 1].end.team && this.game.gameData) {
                                if (this.game.gameData.teams.away.id === data.drives.current.plays[data.drives.current.plays.length - 1].end.team.id) {
                                    updateData.currentDrive = this.game.gameData.teams.away.abbreviation + ' Ball at ' + updateData.currentDrive;
                                }
                                else if (this.game.gameData.teams.home.id === data.drives.current.plays[data.drives.current.plays.length - 1].end.team.id) {
                                    updateData.currentDrive = this.game.gameData.teams.home.abbreviation + ' Ball at ' + updateData.currentDrive;
                                }
                            }
                        }
                        else if (data.drives.current.plays[data.drives.current.plays.length - 1].text) {
                            updateData.currentDrive = data.drives.current.plays[data.drives.current.plays.length - 1].text;
                        }
                        else {
                            updateData.currentDrive = '';
                        }
                    }
                    if (data.scoringPlays && data.scoringPlays.length) {

                        this.getGameGoals(data.scoringPlays);
                        data.scoringPlays.reverse();
                        updateData.scoringPlays = data.scoringPlays;
                    }
                    if (!deepEqual(this.game.scoringPlays, data.scoringPlays)
                        || this.game.currentDrive !== updateData.currentDrive) {
                        //console.log('updating stats', !deepEqual(this.game.scoringPlays, data.scoringPlays), this.game.currentDrive !== updateData.currentDrive);
                        console.log('stats updated');
                        this.afs.doc('live/' + gamePk).update(updateData);
                    }
                    else {
                        console.log('no stats to update', gamePk);
                    }
                }
                else if (data.plays && data.plays.length) { // NBA
                    const updateData: any = { currentDrive: '' };

                    // const plays = data.plays.filter(play => play.scoreValue > 0);
                    const plays = data.plays.filter(play => play.scoreValue > 0 || play.type.id === '16');

                    this.getGameGoals(plays);

                    // const plays = data.plays.filter(play => play.scoreValue > 0 || play.type.id === '16');
                    // let goals = plays.map(item => {
                    //     return {
                    //         row: this.engineService.calcWinScoreMiltiplied(item.awayScore, this.scoreMultiplier, this.scoreShift),
                    //         col: this.engineService.calcWinScoreMiltiplied(item.homeScore, this.scoreMultiplier, this.scoreShift)
                    //     }
                    // });
                    // this.goals = goals.filter((goal, index) => {
                    //     return (!index || goals[index - 1].row !== goal.row || goals[index - 1].col !== goal.col)
                    // });
                    plays.reverse();
                    updateData.scoringPlays = plays;
                    if (!deepEqual(this.game.scoringPlays, plays)
                        || this.game.currentDrive !== updateData.currentDrive) {
                        console.log('stats updated');
                        this.afs.doc('live/' + gamePk).update(updateData);
                    }
                    else {
                        console.log('no stats to update2');
                    }
                }
                else {
                    // console.log('not updating stats');
                }
            })
    }

    getGameGoals(scoringPlays) {
        let goals = scoringPlays.map(item => {
            return {
                row: this.engineService.calcWinScoreMiltiplied(item.awayScore, this.scoreMultiplier, this.scoreShift),
                col: this.engineService.calcWinScoreMiltiplied(item.homeScore, this.scoreMultiplier, this.scoreShift)
            }
        })
        this.goals = goals.filter((goal, index) => {
            return (!index || goals[index - 1].row !== goal.row || goals[index - 1].col !== goal.col)
        });
        this.classCalculate();
    }

    updateLiveGame(gamePk) {
        if ((!this.game || !this.game.gamePk) && parseInt(this.challenge_id) > 53) return;
        if (['nfl', 'nba'].includes(this.game.sport)) {
            let sportService;
            if (this.game.sport === 'nfl') {
                sportService = new NflService();
            }
            else if (this.game.sport === 'nba') {
                sportService = new NbaService();
            } else return;
            sportService.initEvent();
            this.http.get(environment[this.game.sport + '_legacy_api'] + 'scoreboard?dates=' + this.datePipe.transform(new Date(this.finalGame ? this.game.gameDate : this.game.gameData.datetime.dateTime), 'yyyyMMdd') + '-' + this.datePipe.transform(new Date(this.finalGame ? this.game.gameDate : this.game.gameData.datetime.dateTime), 'yyyyMMdd'))
                .subscribe((data: any) => {
                    if (data.events) {
                        data.events.forEach((espnEvent: any) => {
                            // console.log("🚀 ~ file: grid.component.ts ~ line 716 ~ GridComponent ~ data.events.forEach ~ espnEvent", espnEvent)
                            // console.log(this.game);

                            if (espnEvent.id === this.game.gamePk) {
                                const convertedData = sportService.processEspnEvent(espnEvent);
                                const live: any = convertedData.live;
                                delete live['gameData']['players'];
                                live['liveData'] = { linescore: live['liveData']['linescore'], plays: { currentPlay: (live['liveData']['plays']['currentPlay'] || {}) } };
                                if (Object.keys(this.game || {}).length === 0) {
                                    this.afs.doc('live/' + live.gamePk).set(live);
                                }
                                else {
                                    const compareGame = Object.assign({}, this.game);
                                    delete compareGame.scoringPlays;
                                    delete compareGame.currentDrive;
                                    delete compareGame.odds;
                                    if (!deepEqual(compareGame, live)) {
                                        live.scoringPlays = this.game.scoringPlays || [];
                                        live.currentDrive = this.game.currentDrive || '';
                                        live.odds = this.game.odds || {};
                                        this.afs.doc('live/' + live.gamePk).update(live)
                                            .then(() => {
                                                // console.log('updated live/' + live.gamePk);
                                                this.updatePlayStats(espnEvent.id);
                                            });
                                    }
                                    else {
                                        live.scoringPlays = this.game.scoringPlays || [];
                                        live.currentDrive = this.game.currentDrive || '';
                                        live.odds = this.game.odds || {};
                                        if (!deepEqual(this.game, live)) {
                                            this.afs.doc('live/' + live.gamePk).update(live)
                                                .then(() => {
                                                });
                                        }
                                        this.updatePlayStats(espnEvent.id);
                                    }
                                    const awayGoals = this.awayScore;
                                    const homeGoals = this.homeScore;
                                    if (this.homeGoals != homeGoals || this.awayGoals != awayGoals) {
                                        //this.sendNotification(this.notificationLabel.goalScored, this.invite);
                                    }
                                }
                            } else {
                                // this.updatePlayStats(this.game.gamePk);
                            }
                        })
                    }
                })
        }
        else {
            //console.log('updateLiveGame NHL', this.game.gamePk);
            this.http.get(environment.nhl_legacy_api + '/game/' + gamePk + '/feed/live')
                .subscribe((data: Config) => {
                    let game: any = data;
                    delete game['gameData']['players'];
                    game['liveData'] = { linescore: data['liveData']['linescore'], plays: { currentPlay: (data['liveData']['plays']['currentPlay'] || {}) } };
                    if (Object.keys(this.game || {}).length === 0) {
                        // console.log('game not loaded:', 'live/' + gamePk);
                        this.afs.doc('live/' + gamePk).set(game);
                    }
                    else {
                        if (this.game.metaData && game.metaData && this.game.metaData.timeStamp !== game.metaData.timeStamp) { //!deepEqual(this.game, game)
                            const awayGoals = game['liveData']['linescore']['teams']['away']['goals'];
                            const homeGoals = game['liveData']['linescore']['teams']['home']['goals'];
                            if (this.homeGoals != homeGoals || this.awayGoals != awayGoals) {
                                //this.sendNotification(this.notificationLabel.goalScored, this.invite);
                            }
                            // console.log('game has to be updated:', gamePk);
                            this.afs.doc('live/' + gamePk).update(game);
                        }
                    }
                });
        }
    }

    // async manageInvite(id: string) {
    //     const modal = await this.modalController.create({
    //         component: PopupInviteManageComponent,
    //         componentProps: {
    //             inviteId: id,
    //             popoverCtrl: this.popoverCtrl,
    //             afs: this.afs,
    //             searchService: this.searchService,
    //             game_id: this.game_id
    //         },
    //         swipeToClose: true,
    //         cssClass: 'popover-default',
    //     });
    //     modal.onDidDismiss().then((data) => {
    //         //this.slidingItem.closeOpened();
    //     });
    //     await modal.present();
    // }

    documentToDomainObject = _ => {
        const object = _.payload.doc.data();
        object.id = _.payload.doc.id;
        return object;
    }

    getOpenGame(teams?) {
        let team = { id: 0, bid_amount: 10, max_users: 8, users: [], winners: [] };
        if (false) {  // temporarely disable team overflow
            Object.keys(teams || {}).map(function (i) {
                if (teams[i].max_users && teams[i].max_users > (teams[i].users || []).length) {
                    team = teams[i];
                    team.id = parseInt(i);
                    return;
                } else {
                    team.id = parseInt(i) + 1;
                    team.max_users = teams[i].max_users || team.max_users;
                }
            });
        }
        return team;
    }

    getWinColor(row, col) {
        let color = 'inherit';
        if (this.winningUser && this.winningUser[row] && this.winningUser[row][col]
            && this.users[this.winningUser[row][col]]) {
            const user = this.users[this.winningUser[row][col]];
            if (this.bidSelected === 'winning') {
                color = (this.users[this.winningUser[row][col]] || { color: '' }).color;
                if ((user.uid === this.user.uid) || user.id === this.user.uid) {
                    return { 'background': '#fff', 'color': '#333', 'box-shadow': '2px 3px 4px' + color };
                }
                return { 'color': color };
            } else if (this.bidSelected === user.uid || this.bidSelected === user.id) {
                if (typeof this.teams[0].locked !== 'undefined' && this.teams[0].locked) {
                    return { 'background': '#fff', 'color': '#333', 'font-weight': 'bold', 'box-shadow': '2px 3px 4px' + user.color };
                }
            }
        }
    }

    // getGoals(row, col) {
    //     if (this.goals) {
    //         if (JSON.stringify(this.goals).indexOf(JSON.stringify({ row, col })) !== -1) {
    //             const index = this.goals.findIndex(item => item.row == row && item.col == col);
    //             if (index > 0) {
    //                 const prevElem = this.goals[index - 1];
    //                 if (row !== prevElem.row && row > prevElem.row && col == prevElem.col) {
    //                     if ((row - prevElem.row) > 1) {
    //                         return ' goal top double'
    //                     } else {
    //                         return ' goal top'
    //                     }
    //                 } else if (col !== prevElem.col && col > prevElem.col && row == prevElem.row) {
    //                     if ((col - prevElem.col) > 1) {
    //                         return ' goal left double'
    //                     } else {
    //                         return ' goal left'
    //                     }
    //                 } else if (col !== prevElem.col && col > prevElem.col && row !== prevElem.row && row > prevElem.row) {
    //                     return ' goal'
    //                 }
    //             } else {
    //                 return ' goal'
    //             }
    //         }
    //     }
    // }

    classCalculate() {
        this.goals.forEach((goal, i) => {
            const row = goal.row;
            const col = goal.col;

            this.gridClasses[goal.row][goal.col] = 'goal';
            if (i > 0) {
                const prevElem = this.goals[i - 1];
                if (row !== prevElem.row && row > prevElem.row && col == prevElem.col) {
                    if ((row - prevElem.row) > 1) {
                        this.gridClasses[row][col] = 'goal top';
                        row - 1 > 0 ? this.gridClasses[row - 1][col] = 'goal top' : this.gridClasses[row - 1][col] = 'goal';
                    } else {
                        this.gridClasses[row][col] = 'goal top';
                    }
                } else if (col !== prevElem.col && col > prevElem.col && row == prevElem.row) {
                    if ((col - prevElem.col) > 1) {
                        this.gridClasses[row][col] = 'goal left';
                        col - 1 > 0 ? this.gridClasses[row][col - 1] = 'goal left' : this.gridClasses[row][col - 1] = 'goal';
                    } else {
                        this.gridClasses[row][col] = 'goal left';
                    }
                }
            }
        })
    }

    isInviteAdmin() {
        return this.invite && this.invite.admins && this.invite.admins.indexOf(this.user.uid) !== -1;
    }

    ngAfterViewInit() {
        this.layoutService.fixIconEdge(this.icons)
    }

    async ionViewDidEnter() {
        if (this.isBid) {
            if (!Object.values(this.game || {}).length || !Object.values(this.bids || {}).length) {
                setTimeout(() => {
                    this.ionViewDidEnter();
                }, 1500);
                return;
            }
            if (this.content) {
                this.content.scrollToBottom(1000);
            }
            this.winners = this.engineService.calcBidWinner(this.game, this.bids, this.scoreMultiplier, this.scoreShift);
            this.engineService.calcWinningBids(this.game, this.bids, this.winners, this.winningUser, this.winningBid);
            this.winningUser = this.engineService.winningUser;
            if (this.winners && this.game.gameData && this.game.gameData.status && ['6', '7'].includes('' + this.game.gameData.status.statusCode)) {
                this.openWinnerPopup(this.game_id, Object.keys(this.winners)[0], 'gameShowPopups');
            }
            this.winningBid = this.engineService.winningBid;
            setTimeout(() => {
                if (typeof this.teams !== 'undefined' && typeof this.teams[0] !== 'undefined'
                    && typeof this.teams[0].locked !== 'undefined' && this.teams[0].locked) {
                    this.openTab(this.user.uid || 'winning');
                }
            }, 1000);
        }
        Keyboard.setAccessoryBarVisible({
            isVisible: true
        }).catch((e) => {
            //console.log('Keyboard: ' + e);
        });
    }

    async ionViewWillLeave() {
        if (this.intervalId) {
            clearTimeout(this.intervalId);
        }
        if (typeof this.gameSubs !== 'undefined') {
            this.gameSubs.unsubscribe();
        }
        if (typeof this.inviteSubs !== 'undefined') {
            this.inviteSubs.unsubscribe();
        }
        if (typeof this.routeSubs !== 'undefined') {
            this.routeSubs?.unsubscribe();
        }
    }

    async openTab(user_uid, showProfile?) {
        if (this.finalGame) {
            this.bidSelected = user_uid;
        } else {
            if (showProfile && this.bidSelected !== 'winning' && this.bidSelected === user_uid) {
                // this.userInfo(user_uid);
            }
            else if (this.bids[this.game.gamePk] && typeof this.bids[this.game.gamePk][user_uid] !== 'undefined') {
                this.bid = Object.values(this.bids[this.game.gamePk][user_uid]);
                this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0));
                this.calculateBalance();
                this.bidSelected = user_uid;
            } else if (user_uid === 'winning') {
                if (this.user.games && ['2', '6', '7'].includes(this.gameStatus)) {
                    if ((this.invite && this.invite.users.indexOf(this.user.uid) !== -1) || this.user.games.hasOwnProperty(this.game_id) || this.user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
                        this.bid = Object.values(this.winningBid);
                        this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0));
                        this.calculateBalance();
                        this.bidSelected = 'winning';
                        //     this.isWinning = true;
                    } else {
                        // this.isWinning = false;
                        if (false) {
                            this.resetBid(this.isBid ? 0 : 0.01, true);
                            const popup = await this.popoverCtrl.create({
                                component: ModalConfirmComponent,
                                componentProps: { 
                                    text: "You are not a part of the Challenge" 
                                }
                            });
                            return await popup.present();
                        }
                    }
                } else {
                    this.bid = Object.values(this.winningBid);
                    this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0));
                    this.calculateBalance();
                    this.bidSelected = 'winning';
                }
            }
            if (!this.bid || !this.bid.length) {
                this.resetBid(this.isBid ? 0 : 0.01, false);
            }
        }


    }

    async userInfo(uid) {
        const profilePopup: boolean = true;
        const modal = await this.modalController.create({
            component: ProfilePage,
            componentProps: {
                profilePopup,
                userId: uid,
                modalController: this.modalController
            },
            swipeToClose: true,
            showBackdrop: true,
            backdropDismiss: true,
            cssClass: 'popover-profile',
        });
        return await modal.present();
    }

    async winnerPopup(id, winner: any, localStorageKey, event?) {
        let text = '';
        let isWinner = false;
        winner === this.user.uid ? (text = 'You Won!', isWinner = true) : text = 'Won!';

        const popover = await this.popoverCtrl.create({
            component: ModalWinnerComponent,
            event: event,
            componentProps: {
                winner: this.users[winner],
                url: this.users[winner].photoURL || Constants.AVATAR_DEMO,
                text, isWinner
            },
            cssClass: 'modal-wrap-default'
        });

        popover.onDidDismiss()
            .then((result) => {
                this.isShowPopup = true;
                if (this.isShowPopup) {
                    let showPopups = JSON.parse(localStorage.getItem(localStorageKey)) || [];
                    if (showPopups.indexOf(id) === -1) {
                        showPopups.push(id);
                        localStorage.setItem(localStorageKey, JSON.stringify(showPopups));
                    }
                }
            });
        return await popover.present();
        // }
    }

    async switchTeam() {
        if (this.teams && Object.keys(this.teams).length > 1) {
            const buttons = [];
            Object.keys(this.teams).forEach((team_id: string) => {
                buttons.push({
                    text: 'Team #' + (1 + parseInt(team_id, 10)),
                    icon: this.isOwnTeam(team_id) ? 'person-outline' : (this.team_id === team_id ? 'checkmark-outline' : 'remove-outline'),
                    handler: () => {
                        this.router.navigate([this.router.url.replace('bid/' + this.team_id, 'bid/' + team_id)], { replaceUrl: true });
                    },
                    data: { team_id }
                });
            });
            buttons.push({ text: 'Cancel', role: 'cancel' });
            const actionSheet = await this.actionSheetController.create({
                header: 'Teams',
                buttons
            });
            await actionSheet.present();
            actionSheet.onDidDismiss().then(resolve => { });
        }
    }

    isOwnTeam(team_id) {
        return this.game_id && this.user.games && this.user.games[this.game_id] && this.user.games[this.game_id] === team_id;
    }

    async presentAlertConfirm() {
        if (this.isFillGroup) {
            this.setValue();
        } else {
            this.saveBid();
        }
    }

    async confirmBid() {
        const popover = await this.popoverCtrl.create({
            component: ModalConfirmComponent,
            componentProps: {
                text: Math.round((this.user.balance - this.grid.amount) * 100) / 100 == 0 ? 'Your ' + Math.round((this.grid.amount) * 100) / 100 + '$M amount will be placed.' : 'Confirm you would like to place your bids. You still have ' + Math.round((this.user.balance - this.grid.amount) * 100) / 100 + '$M available.',
                btnName: Math.round((this.user.balance - this.grid.amount) * 100) / 100 != 0 ? 'Bid More' : null,
                btnNameConfirm: 'Confirm',
                submit: Math.round((this.user.balance - this.grid.amount) * 100) / 100 != 0 ? this.cancelPopoverCtrl : null,
                submitConfirm: this.saveBid,
                user: this.user,
                myapp: this.myapp,
                isDisabledSubmit: this.isDisabledSubmit,
                invite_id: this.invite_id,
                game: this.game,
                afs: this.afs,
                bid: this.bid,
                sendNotification: this.sendNotification,
                notificationLabel: this.notificationLabel,
                invite: this.invite,
                getOpenGame: this.getOpenGame,
                grid: this.grid,
                loadingController: this.loadingController,
                nextGameBids: this.nextGameBids,
                sendBidEmail: this.sendBidEmail,
                router: this.router
            },
            cssClass: 'modal-wrap-default'
        });
        popover.onDidDismiss().then((data) => {
        });
        return await popover.present();
    }

    cancelPopoverCtrl() {
        this.popoverCtrl.dismiss();
    }

    sendBidEmail() {
        let subject = 'Your SPRT MTRX Bid ' + this.game.gameData.teams.away.name + '@' + this.game.gameData.teams.home.name + ' on ' + this.game.gameData.datetime.dateTime + '';
        let html = "Hello " + this.user.displayName + ",<br><br>\n\n"
            + 'Your SPRT MTRX Bid for ' + this.game.gameData.teams.away.name + '@' + this.game.gameData.teams.home.name + ' on ' + this.game.gameData.datetime.dateTime + ' was placed.'
            + "<br><br>\n\n" + 'See your Bid details and status here https://sprtmtrx.com/bid/' + this.grid.team_id + '/' + this.game.gamePk + ('/' + this.challenge_id)
            + "<br><br>\n\n" + 'SPRT MTRX Team.';
        //this.commonService.sendEmail(this.user.email, subject, html); // disable Bid notification
    }

    handleBid(ev, row, col) {
        if (this.finalGame) {
            const data = {
                ev,
                row,
                col
            }
            this.OnSelectCell.emit(data);
        }

        this.favoriteEmitter.next(false);
        if (['touchmove'].includes(ev.type)) { // fix negative bid on swipe action
            // ev.stopPropagation();
            return;
        } else if (['input'].includes(ev.type)) {
            this.touchedBid[row][col] = this.bid[row][col];
            return;
        }
        if (ev.type === 'click' && this.user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
            this.myapp.loginAlert('detail');
        }
        if ((this.isBid && this.game.gameData.status.statusCode !== '1') || this.finalGame) {
            var msg = '';
            if (this.winningUser[row] && typeof this.users[this.winningUser[row][col]] !== 'undefined') {
                msg = this.users[this.winningUser[row][col]].name + ' placed highest bid for ' + this.winningBid[row][col] + "$M\n";
                Object.keys(this.bids[this.game.gamePk]).forEach(user_uid => {
                    let bid = Object.values(this.bids[this.game.gamePk][user_uid]);
                    if (user_uid != this.winningUser[row][col]) {
                        msg += "\n" + this.users[user_uid].name + ': $' + bid[row][col];
                    }
                });
                alert(msg);
            }
            return;
        }
        if ((parseFloat(this.bid[row][col]) < 0.01 || isNaN(parseFloat(this.bid[row][col]))) && (row !== col || (row === 6 && col === 6) || ['nfl', 'nba'].includes(this.game.sport))) {
            this.bid[row][col] = 0.01;
        }

        if (this.bid[row][col] < 0) {
            this.bid[row][col] = Math.abs(this.bid[row][col]);
        }

        this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100;

        if (this.user.balance - this.grid.amount < 0) {
            this.bid[row][col] = 0.00;
            this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100;
            this.bid[row][col] = Math.round((this.user.balance - this.grid.amount) * 100) / 100;
            this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100;
            this.calculateBalance();
        }

        this.bid[row][col] = +((+this.bid[row][col]).toFixed(2));

        // if (ev.type !== 'focusout' && ev.type !== 'change') {
        //     ev.target.focus();
        //     if (Capacitor.getPlatform() !== 'android') {
        //         ev.target.select();
        //     }
        // }

        if (ev.type === 'change') {
            this.touchedBid[row][col] = this.bid[row][col];
        }
    }

    onKeydown(event: KeyboardEvent) {
        if (event.key == "z" && event.ctrlKey) {
            document.body.style.backgroundColor = 'red'
        }
    }

    resetBid(val, keepTouched, keepFilled?) {
        if (val > 0.01) {
            //this.nativeAudio.play('pong');
        }
        this.isFillGroup = keepFilled ? true : false;
        this.fillCells = keepFilled ? this.fillCells : [];
        if (val === null || typeof val === 'undefined' || val === '') return false;
        if (parseFloat(val) < 0.01) val = 0.01;
        val = Math.abs(Math.round(parseFloat(val) * 100) / 100);
        let max = Math.floor(((10 - Math.round(this.touchedBid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100) / (43 + (this.game && this.game.sport !== 'nhl' ? 6 : 0) - [].concat(...this.touchedBid).filter((el) => {
            return el
        }).length)) * 100) / 100;
        if (val > max && max > 0) val = max;
        for (let r = 0; r < this.grid.count; r++) {
            this.bid[r] = keepTouched ? (this.bid[r] || []) : [];
            this.touchedBid[r] = keepTouched ? (this.touchedBid[r] || []) : [];
            for (let c = 0; c < this.grid.count; c++) {
                if (c != r || c == 6 || r == 6 || this.game.sport !== 'nhl' || !this.game.sport) {
                    if ((!keepTouched || !this.touchedBid[r][c]) &&
                        (!keepFilled || this.bid[r][c] < 0.02)) { // keep user defined amounts
                        this.bid[r][c] = val;
                    }
                } else { // tied score
                    this.bid[r][c] = 0;
                }
            }
        }
        this.grid.amount = this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0);
        this.calculateBalance();
        return true;
    }

    resetMultiBid(val) {
        if (this.fillCells) {
            this.fillCells.forEach((fillCell: any) => {
                this.bid[fillCell.row][fillCell.col] = val;
            })
            this.grid.amount = this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0);
            this.calculateBalance();
        }
        return true;
    }

    async saveBid() {
        this.isDisabledSubmit = true;
        const loading = await this.loadingController.create({
            spinner: null,
            duration: 5000,
            message: 'Please wait...',
            translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        await loading.present();
        const bidPath = this.invite_id ? ('invite/' + this.invite_id) : ('bid/' + this.game.gamePk);
        this.afs.collection('invite').doc(this.invite_id || '0').get().subscribe((resp: any) => {
            const invite = resp.data();
            let gameBids = {};
            if (invite && invite.bids && invite.bids[this.game.gamePk]) {
                gameBids = invite.bids[this.game.gamePk];
            }
            gameBids[this.user.uid] = Object.assign({}, this.bid);
            let updateData = this.invite_id ? { bids: { [this.game.gamePk]: gameBids } } : { [this.user.uid]: Object.assign({}, this.bid) };
            this.afs.doc(bidPath).get().subscribe(item => {
                if (!item.exists) {
                    item.ref.set(updateData);
                } else {
                    item.ref.update(updateData);
                }
                if (this.invite_id && item.data()['admins'].indexOf(this.user.uid) === -1) {
                    const notification = {
                        title: 'Game Bids Placed',
                        text: 'placed bids for your ' + (item.data() ? ('"' + item.data()['name'] + '"') : '') + ' Private Game',
                        platforms: ['ios', 'android', 'web'],
                        start: new Date(),
                        scheduling: 'Now',
                        receivers: item.data()['admins'],
                        sender: this.user.uid,
                        friendRequest: false,
                        sendRequest: true,
                        created: new Date()
                    }
                    // console.log(notification);
                    this.afs.collection('/notification/').add(notification)
                        .then((docRef) => {
                            this.sendNotification(this.notificationLabel.gameBidsPlaced, this.invite);
                        });
                }
            },
                error => {
                    alert('Error: ' + error);
                },
                () => {
                    if (!this.invite_id) { // Private Game is saved along with bids
                        this.afs.doc("team/" + this.game.gamePk).get().subscribe(teams => {
                            let team = this.getOpenGame(teams.data());
                            this.grid.team_id = team.id;
                            new Promise(resolve => {
                                if (!teams.exists) {
                                    team['users'] = [this.user.uid];
                                    teams.ref.set({ [team.id]: Object.assign({}, team) }).finally(() => {
                                        resolve('set');
                                    });
                                } else {
                                    team = teams.data()[team.id] || team;
                                    team['users'].push(this.user.uid);
                                    team['users'] = Array.from(new Set(team['users']));
                                    teams.ref.update({ [team.id]: Object.assign({}, team) }).finally(() => {
                                        resolve('update');
                                    });
                                }
                            }).finally(() => {
                                const games = this.user.games || {};
                                games[this.game.gamePk] = String(this.grid.team_id || '0');
                                this.afs.doc("user/" + this.user.uid).update({ games })
                                    .then(() => {
                                        this.loadingController.dismiss();
                                        this.nextGameBids()
                                    })
                                    .catch((reason: any) => {
                                        alert('Error: ' + reason);
                                    });
                                this.afs.doc('debug/' + this.game.gamePk).get().subscribe(debugGame => {
                                    if (debugGame.exists) {
                                        debugGame.ref.update({ [this.user.uid]: new Date() });
                                    } else {
                                        debugGame.ref.set({ [this.user.uid]: new Date() });
                                    }
                                });
                                this.sendBidEmail();
                            });
                        });
                    }
                    else {
                        this.loadingController.dismiss();
                        this.router.navigate([this.router.url.replace('detail', 'bid')], { replaceUrl: true });
                    }
                })
        });
    }

    getChallengeGames(challenge_id?) {
        challenge_id = this.challenge.id;
        // let challenge = this.challenges[challenge_id] || this.challenge || {games: []};
        if (!this.challenge['start']) return;
        let games = [];
        let date = new Date(this.challenge['start']['seconds'] * 1000);
        if (this.games && this.games.length) {
            for (var i = 0; i < this.games.length; i++) {
                if (this.games[i]['date'] == date.toJSON().slice(0, 10) || true) { // Temp allow multi day Challenges
                    let game = Object.assign({}, this.games[i], { games: [] });
                    for (var j = 0; j < this.games[i]['games'].length; j++) {
                        if (this.challenge['games'].includes(String(this.games[i]['games'][j]['gamePk']))) {
                            game['games'].push(this.games[i]['games'][j]);
                        }
                    }
                    if (game['games'].length > 0) {
                        games.push(game);
                    }
                }
            }
        }
        games[0].games.sort((a, b) => new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime());
        let chalengeGames = games;
        this.challengeGames = chalengeGames[0].games.map(game => game.gamePk ? game.gamePk.toString() : null);
    }

    nextGameBids() {
        if (this.challenge_id) {
            let userBids = Object.keys(this.userBids || {});
            if (!userBids || !this.challenge || !this.challenge['games']) return [];
            let bidsGame = this.challenge['games'].filter(function (n) {
                return userBids.indexOf(n) !== -1;
            });
            let next_game_id;
            next_game_id = this.challengeGames.find(game => (bidsGame.indexOf(game) !== -1 || this.game_id === game) ? null : game);
            if (!this.saveTemplate) {
                // this.progressGamePopup(this.challengeGames, bidsGame, this.game_id, next_game_id);
            }
        }
    }

    next(next) {
        let index, next_game_id;
        index = this.challengeGames.findIndex(game => game === this.game_id ? game : null);
        if (next) {
            next_game_id = this.challengeGames[index + 1];
        } else {
            next_game_id = this.challengeGames[index - 1]
        }
        let team_id;
        if (this.userBids[next_game_id] && this.userBids[next_game_id][this.user.uid]) {
            team_id = this.userBids[next_game_id][this.user.uid];
        }

        this.nextGame(next_game_id, team_id);
    }

    nextGame(next_game_id, team_id) {
        if (next_game_id) {
            let url = this.router.url.replace(this.team_id, team_id || 0).replace(this.game_id, next_game_id);
            this.router.navigate([url], { replaceUrl: true });
        } else {
            this.location.back();
        }
    }

    cancelNavigate() {
        this.location.back();
    }

    async progressGamePopup(challengeGames, bidsGame, game_id, next_game_id) {

        const popover = await this.popoverCtrl.create({
            component: ModalGameProgressComponent,
            componentProps: {
                challengeGames: challengeGames,
                bidsGame: bidsGame,
                game_id: game_id,
                next_game_id: next_game_id,
                router: this.router,
                location: this.location,
                popoverController: this.popoverCtrl,
                nextGame: this.nextGame,
                cancelNavigate: this.cancelNavigate
            },
            cssClass: 'modal-wrap-default'
        });
        popover.onDidDismiss().then((data) => {
            //this.cancelNavigate(); // temp disabled
        });
        return await popover.present();
    }

    async searchTeams(ev: any, id: any) {
        const modal = await this.modalController.create({
            component: PopupTeamSearchComponent,
            componentProps: {
                chatId: id,
                afs: this.afs,
                loadingController: this.loadingController,
                popoverCtrl: this.popoverCtrl,
                users: this.users,
                teams: this.teams,
                team_id: this.team_id,
                modalController: this.modalController,
                user: this.user
            },
            swipeToClose: true,
            showBackdrop: true,
            backdropDismiss: true,
            cssClass: 'popover-default',
        });
        modal.onDidDismiss().then((res) => {
            if (res && res.data) {
                this.openTab(res.data);
            }
        });
        await modal.present();
    }

    async save_template(bid) {
        const alert = await this.alertController.create({
            header: 'Save Current Bid',
            subHeader: 'The name of the template can only contain letters and numbers',
            inputs: [
                {
                    name: 'name',
                    type: 'text' as 'text',
                    placeholder: 'Bid Template Name',
                    min: 3,
                    id: 'textField'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        this.saveTemplate = false;
                    }
                }, {
                    text: 'Ok',
                    handler: (input) => {
                        var letters = /^[0-9a-zA-Z]+$/;
                        if (input.name.match(letters)) {
                            this.saveTemplateHandler(input);
                        } else {
                            this.modalService.errorModal('Error', 'The name of the template can only contain letters and numbers')
                        }
                    }
                }
            ],

        });
        const elem = document.getElementById('textField');
        if (elem) {
            setTimeout(() => { elem.focus(); }, 100);
            elem.onkeyup = (ev: any) => {
                if (ev.key === 'Enter') {
                    let value = ev.target.value;
                    this.saveTemplateHandler(null, value, bid);
                    return this.alertController.dismiss();
                }
            };
        }
        await alert.present();
    }

    saveTemplateHandler(input?, value?, bid?) {
        const name = (value ? value : input.name).replace(/[~*\/\[\]\\]+/g, ' ').trim();
        if (!name) {
            window.alert('Name is required')
            return false;
        } else {
            this.afs.doc('/user/' + this.user.uid + '/template/' + this.game.sport)
                .get().subscribe(data => {
                    if (!data.exists) {
                        data.ref.set({ [name]: Object.assign({}, this.bid) });
                    }
                    else {
                        data.ref.update({ [name]: Object.assign({}, this.bid) });
                    }
                    this.saveTemplate = false;
                    this.modalService.confirmModal('Success', 'Template saved successfully');
                    if (bid) {
                        this.nextGameBids();
                    } else {
                        this.favoriteEmitter.next(true);
                    }
                });
        }
        return true;
    }

    getTimezone() {
        var parts = (new Date().toString()).match(/\(([^)]+)\)/i);
        var timezone = parts[1];
        if (timezone.search(/\W/) >= 0) {
            const timeReg = timezone.match(/\b\w/g); // Match first letter at each word boundary.
            if (timeReg) {
                // console.log(timeReg);

                timezone = timeReg.join('').toUpperCase();
                return timezone;
            } else {
                return '';
            }
        }
    }

    async applyBid(bid) {
        //this.nativeAudio.play('pong');
        this.favoriteEmitter.next(true);
        Object.values(Object.assign({}, bid)).forEach((row, index) => {
            this.bid[index] = JSON.parse(JSON.stringify(row));
        });
        this.grid.amount = this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0);
        this.calculateBalance();
    }

    async bidPopup(ev: any) {
        //this.nativeAudio.play('ping');
        let items = ['0.05', '0.10', '0.15'];
        const popover = await this.popoverCtrl.create({
            component: ModalBidComponent,
            event: ev,
            componentProps: {
                items: items, resetBid: this.resetBid, bid: this.bid, grid: this.grid, game: this.game,
                touchedBid: this.touchedBid, user: this.user, applyBid: this.applyBid, afs: this.afs, templates: this.templates,
                favoriteEmitter: this.favoriteEmitter, nativeAudio: this.nativeAudio
            },
            cssClass: 'modal-wrap-default'
        });
        return await popover.present();
    }

    async scrollToElement(el) {
        const rect = document.getElementById(el).getBoundingClientRect();
        this.content.scrollToPoint(0, rect.top - 60, 1500);
    }

    randomBid() {
        if (this.balanceService.balance > 0.0109) {
            this.resetBidAmount()
            if (this.isFillGroup) {
                let randomBalance = parseFloat((this.balanceService.balance).toFixed(2));
                for (let i = 0; i < this.fillCells.length; i++) {
                    let topColl = this.fillCells[i]['row'],
                        leftCol = this.fillCells[i]['col'];
                    if (i < this.fillCells.length - 1) {
                        const min = 2,
                            max = randomBalance * 100;
                        const randomAmount = parseFloat(((Math.floor(min + Math.random() * (max - min))) / 100).toFixed(2));
                        this.bid[topColl][leftCol] = randomAmount;
                        randomBalance = parseFloat((randomBalance - randomAmount).toFixed(2));
                    }
                    else {
                        this.bid[topColl][leftCol] = randomBalance + this.fillCells.length / 100;
                    }
                }
            }
            else {
                const randomAmount = (Math.floor(Math.random() * this.balanceService.balance * 100) / 100).toFixed(2);
                this.tapBid({ value: String(randomAmount), ev: { type: 'click' } })
            }
            this.resetBidAmount()
        }
    }

    get bidS() {
        return this.bid;
    }

    get status() {
        return this.game.gameData ? this.game.gameData.status : this.game.status;
    }

    randomCols(index) {
        this.favoriteEmitter.next(false);
        var arr = [];
        for (let i = 0; i < index; i++) {
            let t = (Math.floor(Math.random() * 7) + 1) - 1,
                l = (Math.floor(Math.random() * 7) + 1) - 1;
            if (t != l || ['nfl', 'nba'].includes(this.game.sport)) {
                arr.push({ row: t, col: l })
            } else {
                if (l === 6) {
                    arr.push({ row: t, col: l - 1 })
                } else if (l >= 0 && l < 6) {
                    arr.push({ row: t, col: l + 1 })
                }
            }
        }
        return arr;
    }

    groupFill() {
        //this.nativeAudio.play('ping');
        if (!this.isFillGroup) {
            this.fillCells = [];
            this.isFillGroup = true;
        }
    }

    selectFillCell(ev, row, col) {
        this.selectedRow = row;
        this.selectedCol = col;
        this.favoriteEmitter.next(false);
        let obj = { row: this.selectedRow, col: this.selectedCol };

        if (!this.isFillGroup) { //once
            const pageHeight = window.innerHeight;
            let parentTop;
            if (ev.target.innerText) {
                parentTop = ev.target.offsetParent.offsetParent.offsetParent.offsetTop;
            } else {
                parentTop = ev.target.offsetParent.offsetParent.offsetTop;
            }
            let numpadMinHeight;
            if (this.isTablet || this.isDesktop) {
                // numpadMinHeight = (pageHeight - parentTop - 280) < 460;
                numpadMinHeight = true;
                this.topPosition = 0;
                this.setZoomRatio();
                this.onMultiBid.emit(true);
            } else {
                this.setZoomRatio();
                if (window.innerWidth <= 380) {
                    numpadMinHeight = true;
                } else {
                    numpadMinHeight = (pageHeight - parentTop - 200 - 45) < 384;
                }
                if (numpadMinHeight) {//remake numHeight
                    this.topPosition = 0;
                    this.onMultiBid.emit(true);
                } else {
                    this.topPosition = parentTop;
                    this.onMultiBid.emit(false);
                }
            }

            this.openNumpad();
            this.fillCells[0] = obj;
        } else { //multi
            this.topPosition = 0;
            let item = this.fillCells.find(item => {
                return item['row'] === obj['row'] && item['col'] === obj['col']
            })
            if (item) {
                let index = this.fillCells.indexOf(item);
                if (index != -1) {
                    this.fillCells.splice(index, 1);
                }
            } else {
                this.fillCells.push(obj);
            }
        }
    }

    setZoomRatio() {
        const pageHeight = window.innerHeight;
        let contHeight;
        if (window.innerWidth < 380) {
            contHeight = pageHeight - 60;
        } else if(window.innerWidth >= 380 && window.innerWidth < 1024) {
            contHeight = pageHeight - 75 - 58;
        } else {
            contHeight = pageHeight - 60 - 46;
        }
        
        const gridHeight = document.getElementById('gridContainer').offsetHeight * this.zoomRatio;
        const btnContainer = 366 * this.zoomRatio;//document.getElementById('btnContainer').offsetHeight;
        const boxGridHeight = contHeight - btnContainer;
        if (gridHeight > boxGridHeight) {
            this.zoomRatio = +(boxGridHeight / (gridHeight + 35)).toFixed(2);
        }
    }

    selectFillClass(row, col, arr?) {
        if (arr.length) {
            for (let i = 0; i < arr.length; i++) {
                if (row == arr[i]['row'] && col == arr[i]['col']) {
                    if (this.isSetValue) {
                        return ' active';
                    } else {
                        return 'selected';
                    }
                }
            }
            this.isSelected = false;
            return '';
        }
        return '';
    }

    async setValue() {
        if (this.fillCells.length) {
            this.isSetValue = true;
            for (let i = 0; i < this.fillCells.length; i++) {
                let row = this.fillCells[i]['row'],
                    col = this.fillCells[i]['col'];
                this.bid[row][col] = 0;
            }
            this.onMultiBid.emit(this.isSetValue);
            this.openNumpad();
        }
        else {
            this.isFillGroup = false;
        }
    }

    async SubmitFill(fillValue) {
        //this.nativeAudio.play('pong');
        fillValue = !fillValue || +fillValue < 0.01 ? 0.01 : +((+fillValue).toFixed(2));
        for (let i = 0; i < this.fillCells.length; i++) {
            let row = this.fillCells[i]['row'],
                col = this.fillCells[i]['col'];
            this.bid[row][col] = fillValue;
            this.touchedBid[row][col] = this.bid[row][col]
        }
        this.grid.amount = this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0);
        let balance = this.user.balance - this.grid.amount;
        if (balance < 0) {
            let fraction = Math.floor((fillValue - Math.abs(balance) / this.fillCells.length) * 100) / 100;
            for (let i = 0; i < this.fillCells.length; i++) {
                let row = this.fillCells[i]['row'],
                    col = this.fillCells[i]['col'];
                this.bid[row][col] = +fraction;
                this.touchedBid[row][col] = this.bid[row][col]
            }
            this.grid.amount = this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0);
            this.calculateBalance();
        }
    }

    isCellSelected(row, col) {
        if (this.game.sport && this.game.sport === 'nhl' &&
            ((row === 0) && (col === 0) ||
                (row === 1) && (col === 1) ||
                (row === 2) && (col === 2) ||
                (row === 3) && (col === 3) ||
                (row === 4) && (col === 4) ||
                (row === 5) && (col === 5))) {
            this.isSelected = false;
        } else {
            this.isSelected = true;
        }
    }

    async sendNotification(type: any, invite: any, game?, gameWinner?: any) {
        // console.log('sendNotification', type, gameWinner);

        let recipients = [];
        let title = '';
        let body = '';
        switch (type) {
            case this.notificationLabel.gameStart:
                recipients = invite.users;
                title = 'Game Started';
                body = invite.name + ' has started';
                break;
            case this.notificationLabel.goalScored:
                recipients = invite.users;
                title = 'Current Winner';
                if (invite.winners) {
                    invite.winners.forEach((winner, index) => {
                        body += 'You are currently winning the ' + invite.name;
                    });
                } else {
                    body = 'You are no longer winning the ' + invite.name;
                }
                break;
            case this.notificationLabel.gameEnd:
                recipients = invite.users;
                title = invite.name + ' Ended';
                if (invite.winners) {
                    invite.winners.forEach((winner, index) => {
                        body += this.users[winner].name + (invite.winners.length < index ? ', ' : ': ');
                    });
                } else {
                    body = 'Someone ';
                }
                body += 'Won Game!';
                break;
            case 'GameWinnerInChallenge':
                recipients = [gameWinner.uid];
                title = 'Winner';
                body = 'You Just Won the ' + game.gameData.teams.away.name + ' @ ' + game.gameData.teams.home.name + ' Game' + (gameWinner);
                break;
            case this.notificationLabel.gameCurrentWinner:
                recipients = [gameWinner.uid];
                title = 'Private Game Current Winner';
                body = 'You are currently winning the ' + game.gameData.teams.away.name + ' @ ' + game.gameData.teams.home.name;
                break;
            case this.notificationLabel.gameBidsPlaced:
                recipients = invite.admins;
                this.users[this.user.uid] = this.user;
                await this.loadUsers(invite.admins).then(() => { });
                title = 'Game Bids Placed';
                body = (this.user.name || 'User') + ' placed bids for your ' + (invite.name ? ('"' + invite.name + '"') : '') + ' Private Game';
                break;
            default:
        }
        recipients.forEach(uid => {
            if (this.users[uid]) {
                if (this.users[uid].tokens) {
                    // console.log('sending to ', this.users[uid].tokens);
                    const callable = this.fns.httpsCallable('sendNativeNotification');
                    Object.values(this.users[uid].tokens).forEach(token => {
                        if (false || type === this.notificationLabel.gameEnd) {
                            callable({ tokens: token, title: title, body: body })
                                .toPromise().then((resp) => { });
                        }
                        else {
                            console.log('notifications disabled');
                        }
                    });
                }
            }
        });
        return;
    }

    backButton() {
        this.platform.backButton.subscribe(() => {
            this.navCtrl.back({ animated: true });
        })
    }

    calcWinRangeMiltiplied(cellIndex) {
        switch (this.game.sport || this.sport) {
            case 'nfl':
            case 'nba':
                switch (cellIndex) {
                    case 6:
                        cellIndex = this.scoreShift + ((cellIndex) * this.scoreMultiplier + 1);
                        break;
                    default:
                        cellIndex = '<span>+</span>' + (this.scoreShift + (cellIndex + 1) * this.scoreMultiplier);
                }
                break;
            case 'nhl':
                break;
            default:
        }
        return cellIndex + (cellIndex >= 6 ? '+' : '');
    }

    calculateBalance() {
        if (this.game?.sport && Object.keys(this.bid).length) {
            let count = 0;
            let balance = Constants.GRID_PREFS[this.game.sport] ? Constants.GRID_PREFS[this.game.sport].balance : 0;
            this.bid.forEach((row: Array<number>) => {
                row.forEach((bid: number) => {
                    if (bid > 0.01) {
                        balance -= Math.round((bid - 0.01) * 100) / 100;
                        count++;
                    }
                });
            });
            this.balanceService.balance = balance <= 0.0109 ? 0 : Math.round(balance * 100) / 100;
        }
    }

    openNumpad() {
        this.showNumpad = true;
        this.onOpenNumpad.emit(true);
    }

    closeNumpad() {
        this.resetBidAmount();
        this.showNumpad = false;
        this.isSetValue = false;
        if (this.isTablet) {
            this.zoomRatio = 1.25;
        } else {
            this.zoomRatio = 1;
        }
        this.onOpenNumpad.emit(false);
    }

    resetBidAmount() {
        this.numpadArr = [];
    }

    submitKeyboard(data) {
        switch (data.value) {
            case 'check':
                this.checkBid();
                break;
            case 'close':
                this.closeNumpad();
                break;
            case 'random':
                this.randomBid();
                break;
            case 'clear':
                this.resetMultiBid(0);
                this.resetBidAmount();
                break;
            default:
                this.tapBid(data);
        }

    }

    checkBid() {
        this.fillCells = [];
        this.isSetValue = false;
        this.isFillGroup = false;
        this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100;
        this.calculateBalance();
        this.closeNumpad();
        this.saveBid();
    }

    clearBid() {
        if (!this.numpadArr.length) {
            this.tapBid({ value: '0', ev: { type: 'click' } })
        }
        else {
            if (this.numpadArr[this.numpadArr.length - 1] === '.') {
                this.numpadArr.pop();
            }
            if (this.numpadArr.indexOf('.') === 1 && this.numpadArr.indexOf('.', 2) !== -1) {
                this.numpadArr = ['0', '.']
            }
            if (this.numpadArr.indexOf('0') === 1 && this.numpadArr.indexOf('0', 1) === 1) {
                this.numpadArr.shift();
            }
            this.numpadArr.pop();
            this.tapBid({ value: '', ev: { type: 'click' } }, true);
        }
        this.isSetValue = false;
    }

    tapBid(data, skipNumpad?) {
        if ((this.numpadArr.indexOf('.') !== -1 && data.value === '.') ||
            (this.numpadArr.indexOf('0') === 0 && data.value === '0' && this.numpadArr[this.numpadArr.length - 1] !== '.')) {
            return;
        }
        if (!skipNumpad) {
            this.numpadArr.push(data.value);
        }
        const bidValue = Math.round(parseFloat(this.numpadArr.join('')) * 100) / 100;
        if (this.fillCells.length > 1) {
            this.SubmitFill(bidValue);
        } else {
            let currentCell = this.bid[this.fillCells[0]['row']][this.fillCells[0]['col']];
            if (currentCell > 0.01) {
                this.bid[this.fillCells[0]['row']][this.fillCells[0]['col']] = 0.01;
                this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100;
            } else {
                this.grid.amount = Math.round(this.bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0) * 100) / 100;
            }
            if (bidValue > this.balanceService.balance) {
                this.bid[this.fillCells[0]['row']][this.fillCells[0]['col']] = this.balanceService.balance;
            } else {
                this.bid[this.fillCells[0]['row']][this.fillCells[0]['col']] = bidValue || 0;
                this.handleBid(data.ev, this.fillCells[0]['row'], this.fillCells[0]['col']);
            }
        }
    }

    checkFinalist(uid, row, col) {
        return this.fakeFinalist && !this.fakeFinalist.includes(uid); // TODO: Check if required
    }

    finalistBidCell(row, col) {
        let arr = [];
        if (this.bidSelected === 'winning') {
            Object.keys(this.finalistBids).forEach(uid => {
                let v = this.finalistBids[uid][row][col] || 0;
                if (v) {
                    arr.push(v);
                }
            })
        } else {
            let v = this.finalistBids[this.bidSelected][row][col] || 0;
            if (v) {
                arr.push(v);
            }
        }
        let max = Math.max(...arr);
        return max;
    }

    back() {
        this.location.back();
    }
}
