
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GridComponent } from './grid.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { NumberKeyboardModule } from '../number-keyboard/number-keyboard.module';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
    declarations: [
        GridComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule,
        NumberKeyboardModule,
        PipesModule
    ],
    exports: [
        GridComponent
    ],
})
export class GridModule {
}
