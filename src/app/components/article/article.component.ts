import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent {
  @Input() news;
  @Input() side: string;
  @Output() onOpen = new EventEmitter();
  constructor() {

  }

  openArticle(article, side) {
    this.onOpen.emit({article, side});
  }
}