import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ArticleComponent } from './article.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@NgModule({
    declarations: [
        ArticleComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule,
        NgxSkeletonLoaderModule
    ],
    exports: [
        ArticleComponent
    ],
})
export class ArticleModule {
}
