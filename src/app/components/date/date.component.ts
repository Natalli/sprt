import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Constants } from 'src/app/constants';
import { SORT } from 'src/app/interfaces/enum/sort.enum';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
})
export class DateComponent {

  @Input() date: Date;
  @Input() title: string;
  @Input() sortBy: string;
  @Input() hideSort: boolean = false;
  @Output() onSortByChallenge = new EventEmitter();
  @Output() onSortByDate = new EventEmitter();

  Constants = Constants;
  SORT = SORT;
  constructor(private datePipe: DatePipe) { }
  
  sortByChallenge() {
    this.onSortByChallenge.emit();
  }

  isToday(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd') === this.datePipe.transform(new Date(), 'yyyy-MM-dd')
  }

  sortByDate() {
    this.onSortByDate.emit();
  }
}