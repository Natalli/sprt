
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DateComponent } from './date.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';

@NgModule({
    declarations: [
        DateComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        DateComponent
    ],
})
export class DateModule {
}
