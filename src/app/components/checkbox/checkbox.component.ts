
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent {
  @Output() onCheck = new EventEmitter();
  @Input() data: any;
  @Input() name: string;
  @Input() label: string;
  @Input() type: string = 'checkbox';

  constructor() { }

  check(data) {
    this.onCheck.emit(data);
  }

}