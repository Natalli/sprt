
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CheckboxComponent } from './checkbox.component';

@NgModule({
    declarations: [
        CheckboxComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule
    ],
    exports: [
        CheckboxComponent
    ],
})
export class CheckboxModule {
}
