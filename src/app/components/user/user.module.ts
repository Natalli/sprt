import {NgModule} from '@angular/core';
import { IonicModule } from '@ionic/angular';

import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {UserComponent} from './user.component'

@NgModule({
    declarations: [
        UserComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule
    ],
    exports: [
        UserComponent
    ],
})
export class UserModule {
}
