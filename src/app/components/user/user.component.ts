import { Component, OnInit, Input } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { ProfilePage } from '../../pages/profile/profile.page';
import { ModalUserGamesComponent } from 'src/app/popups/new/modal-user-games/modal-user-games.component';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @Input() public name: string;
  @Input() public src: string;
  @Input() public id: string;
  @Input() public challengesWon: number;
  @Input() public color: string;
  @Input() public trophyColor: string;
  @Input() public listComponent: boolean;
  @Input() public challenge_id: any;
  @Input() public users: any;

  challengeGames: any;
  isUserGamesPopup: boolean;

  @Input() set games(value) {
    if (value) {
      this.challengeGames = value[0].games;
      this.isUserGamesPopup = true;
    }
  }

  constructor(
    public modalController: ModalController,
    protected popoverCtrl: PopoverController
  ) { 
  }

  ngOnInit() { }

  async openModal(uid, e, openGame?) {
    e.preventDefault();
    e.stopPropagation();
    if (!uid) return;
    if (this.isUserGamesPopup && openGame) {
      this.openUserGamesPopup(uid);
    } else {
      const profilePopup: boolean = true;
      const popover = await this.modalController.create({
        component: ProfilePage,
        componentProps: {
          profilePopup,
          userId: uid
        },
        swipeToClose: true,
        showBackdrop: true,
        backdropDismiss: true,
        cssClass: 'popover-profile',
      });
      return await popover.present();
    }
  }

  async openUserGamesPopup(uid) {
    const popover = await this.popoverCtrl.create({
      component: ModalUserGamesComponent,
      componentProps: {
        userId: uid,
        name: this.name,
        challengeGames: this.challengeGames,
        challenge_id: this.challenge_id,
        users: this.users
      },
      cssClass: 'modal-wrap-default'
    });
    return await popover.present();
  }

  getTrophyColor(winsCount: number) {
    let color = 'gray';
    switch (true) {
      case (winsCount === 1):
        color = '#cd7f32';
        break;
      case (winsCount < 5):
        color = 'silver';
        break;
      case (winsCount < 10):
        color = 'gold';
        break;
      case (winsCount >= 10):
        color = 'gold';
        break;
    }
    return color;
  }

}
