import {NgModule} from '@angular/core';
import { IonicModule } from '@ionic/angular';

import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { BannerComponent } from './banner.component';

@NgModule({
    declarations: [
        BannerComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule
    ],
    exports: [
        BannerComponent
    ],
})
export class BannerModule {
}
