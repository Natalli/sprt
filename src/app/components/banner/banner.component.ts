import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { IonSlides } from '@ionic/angular';

interface IBanner {
  image: string,
  title: string,
  text: string,
  link: string
}

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  @ViewChild('slider', { static: true }) slides: IonSlides;
  banners: IBanner[];
  loading: boolean;
  showPager: boolean;
  currentSlide: number = 0;
  src: string = '';
  intervalTimer: any;
  data: any;
  timer: number = 0;
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true
    },
    autoplay: {
      delay: 8000,
    },
    loop: true
  };

  constructor(
    protected router: Router,
    protected afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.getBanner();
  }

  getBanner() {
    const page = this.router.url.split('/')[1];
    this.afs.collection('/banner/' + page + '/slides').valueChanges().subscribe((banners: any) => {
      if (banners && banners.length) {
        this.banners = banners;
        this.banners.forEach(banner => {
          banner = { ...banner, ...banner[Capacitor.getPlatform()] };
        })
        if (banners.length > 1) {
          this.showPager = true;
        }
        this.currentSlide = 0;
        this.slides.slideTo(0);
        this.slideTimer(this.currentSlide);
      }
    });
  }

  slideChanged() {
    this.slides.getActiveIndex().then(index => {
      this.currentSlide = index;
      this.src = '';
      if (this.banners && this.banners.length) {
        setTimeout(() => {
          this.slideTimer(this.currentSlide);
        })
        this.slides.lockSwipeToPrev(false);
      }
    });
  }

  slideTimer(currentSlide) {
    clearInterval(this.intervalTimer);
    let next = +currentSlide + 1;
    let slide = this.banners[currentSlide];
    let nextSlide = this.banners[next];
    this.src = slide.image;
    let nextSrc;
    if (nextSlide) {
      nextSrc = nextSlide.image;
    } else if(this.banners[0]) {
      nextSrc = this.banners[0].image;
    }

    this.timer = 5;
    this.intervalTimer = setInterval(() => {
      this.timer--;
      if (this.timer === 0) {
        clearInterval(this.intervalTimer);
        if (currentSlide !== this.banners.length - 1) {
          this.slides.update().then(() => {
            this.src = nextSrc;
            this.slides.slideTo(next);
          })
        } else {
          this.slides.update().then(() => {
            this.src = nextSrc;
            this.slides.slideTo(0);
          })
        }
      }
    }, 1000);
  }

}
