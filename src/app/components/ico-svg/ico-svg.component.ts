import { Component, Input } from '@angular/core';
import { Constants } from '../../constants';

@Component({
  selector: 'app-ico-svg',
  templateUrl: './ico-svg.component.html',
  styleUrls: ['./ico-svg.component.scss'],
})

export class IcoSvgComponent {
  @Input() id: string;
  @Input() src: string = Constants.ICONS;
  @Input() class: string = 'icon';
  @Input() disabled: boolean;

  constructor() { }

}
