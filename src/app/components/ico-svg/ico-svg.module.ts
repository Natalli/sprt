
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IcoSvgComponent } from './ico-svg.component';

@NgModule({
    declarations: [
        IcoSvgComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule
    ],
    exports: [
        IcoSvgComponent
    ],
})
export class IcoSvgModule {
}
