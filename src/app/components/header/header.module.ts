
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header.component';
import { IcoSvgModule } from '../ico-svg/ico-svg.module';
import { BalanceModule } from '../balance/balance.module';

@NgModule({
    declarations: [
        HeaderComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule,
        BalanceModule
    ],
    exports: [
        HeaderComponent
    ],
})
export class HeaderModule {
}
