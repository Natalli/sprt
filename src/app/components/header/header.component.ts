import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Constants } from 'src/app/constants';
import { BalanceService } from 'src/app/service/balance/balance.service';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Output() onCloseMenu = new EventEmitter();
  @Input() showMenu: boolean     = true;
  @Input() showAmount: boolean   = true;
  @Input() showSettings: boolean = true;
  @Input() isPopup: boolean = false;

  @Input() set balance(value) {
    if(typeof value !== 'undefined') {
      this.service.balance = value > 0.001 ? value : 0;
    }
  }
  Constants = Constants;
  
  constructor(
    private balanceService: BalanceService,
    public location: Location,
    private router: Router,
    public modalController: ModalController
  ) { }

  get service() {
    return this.balanceService;
  }

  open() {
    if (this.isPopup) {
      this.modalController?.dismiss();
    } else {
      this.onCloseMenu.emit();
      this.location.back();
    }
  }

}
