import { Component } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { Constants } from 'src/app/constants';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-forgot',
  templateUrl: './new-forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage {
  Constants = Constants;
  email: string = '';
  password: string = '';
  error: string = '';
  username: string = '';
  image: number;
  toast: any;
  public ForgotForm = this.formBuilder.group({ email: ['', Validators.compose([Validators.required, 
                                                            Validators.pattern(Constants.EMAIL_REGEXP)])] });

  constructor(
    private router: Router, 
    public loadingController: LoadingController,
    public alertController: AlertController,
    private userService: UserService,
    private toastController: ToastController,
    private formBuilder: FormBuilder) {
  }

  async recover() {
    if ( this.ForgotForm.value.email === 'info@sprtmtrx.com' ) {
      this.router.navigate(['/login']);
      return;
    }
    if (this.ForgotForm.invalid) {
      this.ForgotForm.markAllAsTouched();
      this.toast = await this.toastController.create({
        message: 'Email is invalid',
        position: 'bottom',
        duration: 5000,
        cssClass: 'ion-text-center'
      });
      this.toast.present();
    }
    else {
      this.userService.sendPasswordResetEmail(this.ForgotForm.value.email, true);
    }
  }

  cancel() {
    this.router.navigate(['/login']);
  }
}
