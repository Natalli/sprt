import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForgotPage } from './forgot.page';
import { IcoSvgModule } from '../../../components/ico-svg/ico-svg.module';
import { CardModule } from '../../../components/card/card.module';
import { NavigateModule } from '../components/navigate/navigate.module';

const routes: Routes = [
  {
    path: '',
    component: ForgotPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IcoSvgModule,
    CardModule,
    NavigateModule
  ],
  declarations: [ForgotPage]
})
export class ForgotPageModule { }
