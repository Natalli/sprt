import { Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

// const PREFERENCES = [
//   { id: 1, name: 'SINGLE BID ENABLE' },
//   { id: 2, name: 'MATRIX BID ENABLE' },
//   { id: 3, name: 'NEWS FEED' },
//   { id: 4, name: 'SIMPLIFIED STATS' },
//   { id: 5, name: 'AUTO DISTRIBUTE BID' }
// ]

const PREFERENCES = [
  {id: 'singleBid', name: 'Single Bid by default'},
  {id: 'matrixBid', name: 'Matrix Bid by default'},
  {id: 'hidePastGames', name: 'Don\'t show past games'},
  {id: 'notifications', name: 'Turn off notifications'},
  //{id: 'darkMode', name: 'Dark Mode'},
]

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.page.html',
  styleUrls: ['./preferences.page.scss'],
})
export class PreferencesPage {
  user: any;
  userPrefs: any;
  preferences = PREFERENCES;
  classId;
  constructor(
    private router: Router,
    public loadingController: LoadingController,
    private userService: UserService) {
      this.loadUser();
  }

  toggle(pref) {
    switch (pref) {
      case 'singleBid':
        this.userPrefs.user.matrixBid = !this.userPrefs.user.singleBid;
        break;
      case 'matrixBid':
        this.userPrefs.user.singleBid = !this.userPrefs.user.matrixBid;
        break;
      default:
    }
  }

  save() {
    this.userService.setUserPrefs({user: this.userPrefs.user}, true);
    this.next();
  }

  cancel() {
    this.userService.setUserPrefs({user: {}}, true);
    this.next();
  }

  next() {
    if ( Object.keys(this.userPrefs?.leagues||{}).length ) {
      this.router.navigate(['/challenges']);
    }
    else {
      this.router.navigate(['/home']);
    }
  }

  initUserPrefs() {
    if (!Object.keys(this.userPrefs||{}).length || 
        !Object.keys(this.userPrefs.user||{}).length) {
      this.userPrefs = {user: {}};
      this.preferences.forEach(pref => {
        switch (pref.id) {
          case 'singleBid':
            this.userPrefs.user.singleBid = true;
            break;
          default:
            this.userPrefs.user[pref.id] = false;
        }
      });
    }
  }

  loadUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
          this.user = user;
          this.userService.loadUserPrefs().then((userPrefs:any) => {
            this.userPrefs = {...this.userPrefs, ...userPrefs};
            if ( typeof userPrefs.leagues !== 'undefined' &&
                Object.keys(userPrefs.leagues).length ) {
            }
            this.initUserPrefs();
          });
        }
      });
  }

  setClass(id) {
    return this.userPrefs?.user && this.userPrefs.user[id] ? 'active' : '';
  }
}

