import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PreferencesPage } from './preferences.page';
import { IcoSvgModule } from '../../../components/ico-svg/ico-svg.module';
import { CardModule } from '../../../components/card/card.module';
import { NavigateModule } from '../components/navigate/navigate.module';

const routes: Routes = [
  {
    path: '',
    component: PreferencesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IcoSvgModule,
    CardModule,
    NavigateModule
  ],
  declarations: [PreferencesPage]
})
export class PreferencesPageModule { }
