import { Component } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Constants } from '../../../constants';
import { AppComponent } from '../../../app.component';
import { getAuth, onAuthStateChanged, signInWithEmailAndPassword, signInWithPopup, GoogleAuthProvider, signInWithCredential, OAuthProvider, signOut } from 'firebase/auth';
import { UserService } from 'src/app/service/user.service';
import { GoogleAuth } from "@codetrix-studio/capacitor-google-auth";
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import { SignInWithApple, SignInWithAppleOptions } from '@capacitor-community/apple-sign-in';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-login',
  templateUrl: './new-login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  public LoginForm: FormGroup;
  error: string = '';
  isVisible: boolean;
  capacitor = Capacitor;
  auth;

  constructor(
    public router: Router,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private formBuilder: FormBuilder,
    public platform: Platform,
    public myapp: AppComponent,
    protected userService: UserService,
    protected modalService: ModalService) {
    this.auth = getAuth();
    signOut(this.auth).then(() => { })
  }

  ngOnInit() {
    this.createLoginForm();
    GoogleAuth.initialize({
      clientId: '954133284025-oqcicb47q2f3r3hjpe500noou81fidi1.apps.googleusercontent.com',
      scopes: ['profile', 'email'],
      grantOfflineAccess: true,
    });

  }

  createLoginForm() {
    this.LoginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(Constants.EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  demo() {
    this.login('info@sprtmtrx.com', 'Demo123');
    return false;
  }

  async googleSignIn(): Promise<void> {
    if (Capacitor.isNativePlatform()) {
      this.googleNativeSignIn();
    }
    else {
      this.googleWebSignIn();
    }
  }

  async googleNativeSignIn(): Promise<void> {
    try {
      await this.modalService.presentLoading();
      const result = await GoogleAuth.signIn();
      const credential = firebase.auth.GoogleAuthProvider.credential(result.authentication.idToken);
      signInWithCredential(this.auth, credential)
        .then((userCredential) => {
          this.onSuccessLogin(userCredential);
        })
        .catch((error) => {
          const errorMessage = error.message;
          this.modalService.errorModal('Error', errorMessage);
          console.log(`login failed ${error}`);
        })
        .finally(() => {
          this.loadingController.dismiss();
        });
    } catch (err) {
      alert(err.error||err);
      console.log(err);
    }
  }

  async googleWebSignIn() {
    await this.modalService.presentLoading();
    try {
      const provider = new GoogleAuthProvider();
      this.userService.auth = getAuth();
      signInWithPopup(this.userService.auth, provider)
        .then((result) => {
          this.onSuccessLogin(result);
        })
        .catch((error) => {
          const errorMessage = error.message;
          this.modalService.errorModal('Error', errorMessage);
          console.log(`login failed ${error}`);
          this.loadingController.dismiss();
        })
        .finally(() => {
          this.loadingController.dismiss();
        });
    } catch (err) {
      alert(err.error||err);
      console.log(err);
    }
  }

  async appleSignIn(): Promise<void> {
    if (Capacitor.isNativePlatform()) {
      this.appleNativeSignIn();
    }
    else {
      this.appleWebSignIn();
    }
  }

  async appleNativeSignIn() {
    const nonce = 'nonce';
    let options: SignInWithAppleOptions = {
      clientId: 'com.googleusercontent.apps.954133284025-glui3p66jv5fb6lnftb0s0f7q0o12pbt',
      redirectURI: '/login',
      scopes: 'email name',
      state: '12345',
      nonce: await this.sha256(nonce),
    };

    SignInWithApple.authorize(options).then(async res => {
      if (res.response && res.response.identityToken) {
        const provider = new firebase.auth.OAuthProvider('apple.com');
        const credential = provider.credential({
          idToken: res.response.identityToken, rawNonce: nonce
        });
        signInWithCredential(this.auth, credential)
          .then((userCredential) => {
            console.log('userCredential', userCredential);
            this.onSuccessLogin(userCredential);
          })
          .catch((error) => {
            const errorMessage = error.message;
            this.modalService.errorModal('Error', errorMessage);
            console.log(`login failed ${error}`);
          })
          .finally(() => {
            this.loadingController.dismiss();
          });
      } else {
        this.loadingController.dismiss();
      }
    }).catch(response => {
      this.loadingController.dismiss();
    });
  }

  async appleWebSignIn() {
    await this.modalService.presentLoading();
    try {
      const provider = new OAuthProvider('apple.com');
      this.userService.auth = getAuth();
      signInWithPopup(this.userService.auth, provider)
        .then((result) => {
          this.onSuccessLogin(result);
        })
        .catch((error) => {
          const errorMessage = error.message;
          this.modalService.errorModal('Error', errorMessage);
          console.log(`login failed ${error}`);
        })
        .finally(() => {
          this.loadingController.dismiss();
        });
    } catch (err) {
      alert(err.error||err);
      console.log(err);
    }
  }

  async login(email?, password?) {
    await this.modalService.presentLoading();
    signInWithEmailAndPassword(this.auth, email || this.LoginForm.value.email, password || this.LoginForm.value.password)
      .then((userCredential) => {
        this.onSuccessLogin(userCredential);
      })
      .catch((error) => {
        const errorMessage = error.message;
        this.modalService.errorModal('Error', errorMessage);
        console.log(`login failed ${error}`);
      })
      .finally(() => {
        this.loadingController.dismiss();
      });
  }

  async sha256(message) {
    const msgBuffer = new TextEncoder().encode(message);
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
    return hashHex;
  }  

  onSuccessLogin(userCredential) {
    if (userCredential.user) {
      this.userService.auth = getAuth();
      onAuthStateChanged(this.userService.auth, (user) => {
        if (user) {
          this.userService.loadUserPrefs().then((userPrefs:any) => {
            if ( typeof userPrefs !== 'undefined' &&
              Object.keys(userPrefs).length ) {
              this.router.navigate(['/challenges'], { replaceUrl: true });
            }
            else {
              this.router.navigate(['/preferences'], { replaceUrl: true });
            }
          });
        }
      });  
    }
  }

  cancel() {
    this.router.navigate(['/start']);
  }

}
