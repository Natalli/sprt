import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './login.page';
import { BannerModule } from '../../../components/banner/banner.module';
import { IcoSvgModule } from '../../../components/ico-svg/ico-svg.module';
import { CardModule } from '../../../components/card/card.module';
import { NavigateModule } from '../components/navigate/navigate.module';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    BannerModule,
    IcoSvgModule,
    CardModule,
    NavigateModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule { }
