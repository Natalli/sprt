import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { StartPage } from './start.page';
import { IcoSvgModule } from '../../../components/ico-svg/ico-svg.module';
import { NavigateModule } from '../components/navigate/navigate.module';

const routes: Routes = [
  {
    path: '',
    component: StartPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IcoSvgModule,
    NavigateModule
  ],
  declarations: [StartPage]
})
export class StartPageModule { }
