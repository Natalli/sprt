import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { getAuth, onAuthStateChanged } from 'firebase/auth';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage {

  constructor(
    private router: Router,
    private userService: UserService
  ) {
    this.autoLogin();
  }

  autoLogin() {
    onAuthStateChanged(getAuth(), (user) => {
      if (user && user.uid && this.userService.isLoggedIn()) {
        this.userService.loadUserPrefs().then((userPrefs:any) => {
          if (typeof userPrefs !== 'undefined' && Object.keys(userPrefs).length) {
            if (window.location.href.indexOf('profile') === -1) {              
              this.router.navigate(['/challenges'], { replaceUrl: true });
            }
          }
          else {
            this.router.navigate(['/preferences'], { replaceUrl: true });
          }
        });
      }
    })
  }

  submit() {
    this.router.navigate(['/login']);
  }

}
