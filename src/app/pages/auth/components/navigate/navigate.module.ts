
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavigateComponent } from './navigate.component';
import { IcoSvgModule } from 'src/app/components/ico-svg/ico-svg.module';

@NgModule({
    declarations: [
        NavigateComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        IcoSvgModule
    ],
    exports: [
        NavigateComponent
    ],
})
export class NavigateModule {
}
