import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-navigate',
  templateUrl: './navigate.component.html',
  styleUrls: ['./navigate.component.scss'],
})
export class NavigateComponent {

  @Output() onSubmit = new EventEmitter();
  @Input() disabled: boolean;
  constructor() { }

  submit() {
    this.onSubmit.emit();
  }

}