import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SignupPage } from './signup.page';
import { IcoSvgModule } from '../../../components/ico-svg/ico-svg.module';
import { CardModule } from '../../../components/card/card.module';
import { NavigateModule } from '../components/navigate/navigate.module';

const routes: Routes = [
  {
    path: '',
    component: SignupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    IcoSvgModule,
    CardModule,
    NavigateModule
  ],
  declarations: [SignupPage]
})
export class SignupPageModule { }
