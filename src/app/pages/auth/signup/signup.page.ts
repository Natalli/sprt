import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Constants } from '../../../constants'
import { createUserWithEmailAndPassword, getAuth, updateProfile } from 'firebase/auth';
import ERRORS from './../../../errors.json'; 
import { ErrorService } from 'src/app/services/error.service';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-signup',
  templateUrl: './new-signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage {
  public SignUpForm: FormGroup; 
  error: string = '';
  isVisible: boolean;
  toast;
  isNameUnique: boolean;
  auth;
  usersSubs: any;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private afs: AngularFirestore,
    private fns: AngularFireFunctions,
    private formBuilder: FormBuilder,
    protected errorService: ErrorService,
    protected modalService: ModalService) {
    this.auth = getAuth();
  }

  ngOnInit() {
    this.createSignUpForm();
  }

  createSignUpForm() {
    this.SignUpForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, , this.checkUniqueName.bind(this, 'name'), Validators.pattern(Constants.FULL_NAME_REGEXP)])],
      email: ['', Validators.compose([Validators.required, Validators.pattern(Constants.EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.required, Validators.pattern(Constants.PASSWORD_REGEXP)])],
      password_confirmation: ['', Validators.required]
    });
  }

  checkUniqueName(valueName: string, name: FormControl) {
    if (!this.SignUpForm || !valueName || !name.value) {
      return;
    }
    let username = name.value.trim().toLowerCase();
    if (this.SignUpForm && username.length < 3) {
      this.isNameUnique = false;
      if (!this.SignUpForm.controls['username'].touched) {
        this.SignUpForm.controls['username'].markAsTouched();
      }
    }
    else {
      let usernameFound = false;
      this.afs.collection("/user/", ref => ref
      .orderBy('keywords')
      .startAt(username)
      .endAt(username + "\uf8ff")).get().subscribe(snap => {
        snap.forEach(user => {
          const name = user.data()['name'].toLowerCase();
          if (name === username) {
            usernameFound = true;
          }
        });
        this.isNameUnique = !usernameFound;
      })
    }
  }

  async signup() {
    this.SignUpForm.markAllAsTouched();
    if (this.SignUpForm.valid && 
        this.isNameUnique && 
        this.SignUpForm.value['password'] === this.SignUpForm.value['password_confirmation']) {
      this.modalService.presentLoading();
      createUserWithEmailAndPassword(this.auth, this.SignUpForm.value.email, this.SignUpForm.value.password)
        .then(userCredential => {
        if (userCredential.user && userCredential.user.uid) {
          this.updateProfile();
        }
        else {
          this.error = 'Empty response';
          this.toast.dismiss();
          this.modalService.errorModal('Error', this.error);
        }
      })
      .catch(err => {
        const CODE = this.errorService.codeTransform(err.code);
        console.log(ERRORS[CODE]);
        console.log(`login failed ${err}`);
        this.toast.dismiss();
        this.error = err.message;
        this.modalService.errorModal('Error', this.error);
      });
    }
  }

  updateProfile() {
    const user = this.auth.currentUser;

    updateProfile(user, {
      displayName: this.SignUpForm.value.username,
      photoURL: Constants.AVATAR_DEMO
    })
      .then(() => {
        const data = {
          name: this.SignUpForm.value.username,
          email: this.SignUpForm.value.email,
          publicName: this.SignUpForm.value.username.slice(0, 3).toUpperCase(),
          balance: 10,
          keywords: this.SignUpForm.value.username.toLowerCase() + ' ' + this.SignUpForm.value.username.slice(0, 3).toLowerCase(),
          color: ('#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6)),
          creationTime: new Date()
        };
        this.afs.doc(`/user/${user.uid}`).set(data, {merge: true})
          .then(() => { console.log('user set'); })
          .catch((reason) => console.log(reason))
          .finally(() => this.sendAccountEmail());
      })
      .catch((error) => {
        console.log(`update failed ${error.message}`);
        this.error = error.message;
        this.sendAccountEmail();
      });
   
  }

  sendAccountEmail() {
    let subject = 'Your SPRT MTRX Account';
    let html = "<h3 style='margin-bottom: 25px'>Hello " + this.SignUpForm.value.username + ",</h3>"
      + '<p>Your SPRT MTRX Account has been created.</p>'
      + '<p>You can access your SPRT MTRX Account here: </p>'
      + '<p><a href="https://apps.apple.com/app/sprt-mtrx/id1488373981?ls=1" target="_blank">App Store</a></p>'
      + '<p><a href="https://play.google.com/store/apps/details?id=com.livecurrent.sprtmtrx" target="_blank">Google Play</a></p>'
      + '<p>https://sprtmtrx.com/</p>'
      + '<p>SPRT MTRX Team.</p>';

    const callable = this.fns.httpsCallable('sendMail');
    callable({
      email: this.SignUpForm.value.email, subject: subject, html: html
    })
      .toPromise()
      .then((data) => {
        // console.log(data);
      })
      .catch((reason) => {
        console.log(reason);
      })
      .finally(() => {
        this.toast.dismiss();
        this.router.navigate(['/preferences']);
      });
  }

  cancel() {
    this.router.navigate(['/login']);
  }

}
