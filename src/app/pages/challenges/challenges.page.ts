import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Invite } from '../../model/invite';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { PopupSelectSportComponent } from '../../popups/popup-select-sport/popup-select-sport.component';
import { SPORT_TABS } from '../../constants/sport-tabs';
import { PopupShareComponent } from '../../popups/popup-share/popup-share.component';
import { SearchService } from '../../services/search/search.service';
import { UserService } from '../../service/user.service';
import { Constants } from '../../constants';
import { DatePipe } from '@angular/common';
import { GameComponent } from '../../components/game/game.component';
import { ModalDefaultComponent } from '../../popups/new/modal-default/modal-default.component';
import { SORT } from 'src/app/interfaces/enum/sort.enum';
import { filter } from 'rxjs';

@Component({
  selector: 'app-challenges',
  templateUrl: './new-challenges.page.html',
  styleUrls: ['./challenges.page.scss'],
})
export class ChallengesPage implements OnInit {
  @ViewChild('calendar', { read: ElementRef }) private calendar: ElementRef;
  @ViewChild(GameComponent) activeGame: GameComponent;

  user: any;
  tabsBySport = SPORT_TABS;
  league: string;

  challenges: {};
  events: any = {};
  eventsSubs: any = {};
  invites: object;
  linksInvites = {};
  linksChallenges = {};

  challengeSubs: any;
  challengesSubs: any;

  challenge: any = {};
  date = Date;
  quote: Object = {};
  capacitor = Capacitor;
  sports: Array<any> = [
    { name: 'nhl', icon: 'hockey' },
    { name: 'nfl', icon: 'american-football' },
    { name: 'nba', icon: 'basketball' }];
  isChallengePage: boolean = false;

  Constants = Constants;
  gamePkSelected: any = {};
  gameSelected: any = {};
  leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));

  today = new Date();
  gameChallengeMap = {};

  standings: Object = {};
  userPrefsSub: any;
  prefTeams: any = {};
  resetSlides: boolean = false;
  isLoading: boolean = false;
  sortBy: string;
  routeSubs: any;
  constructor(
    private afs: AngularFirestore,
    public router: Router,
    private popoverCtrl: PopoverController,
    private modalController: ModalController,
    private searchService: SearchService,
    public userService: UserService,
    private datePipe: DatePipe
  ) {
      this.league = history.state.league || 'nfl';
      this.routeSubs = this.router.events
      .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
      .subscribe(() => {
        if (history.state.league && history.state.league !== this.league) {
          this.league = history.state.league;
        }
       });
  }

  ngOnInit() {
    this.loadUser();
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (['/challenges'].includes(e.url)) {
          this.loadChallenges();
          this.loadUser();
          this.invites = undefined;
          this.isChallengePage = true;
        } else if (['/private-games'].includes(e.url)) {
          this.loadUser();
          this.challenges = {};
          this.isChallengePage = false;
        }
      }
    });
    this.randomQuote();
    this.setLeague(this.league);
    Object.keys(Constants.DEFAULT_LEAGUES).forEach(league => {
      this.loadLeague(league);
      this.loadLeagueStandings(league);
    });
  }

  ngOnDestroy() {
    this.routeSubs.unsubscribe();
  }

  loadUser() {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.userService.getUser$(user.uid).subscribe(data => {
          this.user.games = data.games;
        })
        this.loadUserPrefs();
      }
    })

  }

  loadUserPrefs() {
    if (!this.userPrefsSub) {
      this.userPrefsSub = {};
      this.userService.loadUserPrefs().then((userPrefs: any) => {
        this.userPrefsSub = this.userService.userPrefs$.subscribe((userPrefs) => {
          if (userPrefs) {
            delete this.prefTeams[this.league];
            this.buildPrefGames(this.league);
            this.activeGame?.flipped(this.userService?.userPrefs?.user?.matrixBid);
            Object.keys(this.userService.userPrefs.leagues || {}).forEach(league => {
              if (league !== this.league) {
                delete this.prefTeams[league];
                this.buildPrefGames(league);
              }
            });
          }
        });
      });
    }
  }

  async loadChallenges() {
    this.challengesSubs = this.afs.collection('challenge', ref => ref
      .orderBy('start', 'desc')
      .where('start', '>', new Date(Date.now() - 13 * 24 * 60 * 60000))
      .where('start', '<', new Date(Date.now() + 120 * 24 * 60 * 60000))
      // .where('sport', '==', this.sport || '')
      .where('disabled', '==', false)
      .limit(6))
      .valueChanges({ idField: 'id' }).subscribe((challenges) => {
        this.linksChallenges = {};
        this.challenges = {};
        let startDate = 0, endDate = 0;
        this.gameChallengeMap = {};
        challenges.forEach((challenge: any, index) => {
          this.challenges[challenge.id] = Object.assign({ name: '', text: '', games: [] }, challenge);
          this.linksChallenges[challenge.start.seconds] = { challengeId: challenge.id };
          startDate = !startDate || startDate > challenge.start.seconds ? challenge.start.seconds : startDate;
          endDate = !endDate || endDate < challenge.start.seconds ? challenge.start.seconds : endDate;
          challenge.games.forEach(gamePk => {
            this.gameChallengeMap[gamePk] = challenge.id;
          })
        });
        startDate = startDate || new Date('2022-08-01').getTime();
        endDate = new Date('2022-12-31').getTime();
        Object.keys(Constants.DEFAULT_LEAGUES).forEach(league => {
          this.loadGames(league, new Date(startDate * 1000), new Date(endDate * 1000));
        });
      });
  }

  loadLeague(league) {
    const lleague = league.toLowerCase();
    if (!Object.keys(this.leagues[lleague]).length) {
      this.afs.collection(`${lleague}-team`, ref => ref
        .where('isActive', '==', true)).get().subscribe(snapshot => {
          snapshot.docs.forEach(doc => {
            this.leagues[lleague][doc.data()['oid'] || doc.id] = doc.data();
          })
        });
    }
  }

  async loadGames(league, startDate, endDate) {
    if (typeof this.eventsSubs[league] !== 'undefined') {
      this.eventsSubs[league].unsubscribe();
    }
    this.eventsSubs[league] = this.afs.collection(league, ref => ref
      .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
      .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
      .valueChanges().subscribe((games: Array<any>) => {
        this.events[league] = games;
        this.buildPrefGames(league);
      });
  }

  buildPrefGames(league) {
    if (this.events[league] && typeof this.prefTeams[league] === 'undefined') {
      this.prefTeams[league] = [];
      this.gamePkSelected[league] = '';
      this.gameSelected[league] = {};
      this.events[league].forEach(event => {
        event.games.forEach(game => {
          if (this.isPrefTeam(league, game)) {
            this.prefTeams[league].push(game.gamePk);
            if (!this.gamePkSelected[league] &&
              !this.isPastGame(game) &&
              this.isChallengeGame(game) &&
              !this.isBidPlaced(game.gamePk)) {
              this.gameSelected[league] = game;
              this.gamePkSelected[league] = game.gamePk;
            }
          }
        })
      });
      if (!this.gamePkSelected[league]) {
        this.setDefaultGame(league);
      }
      this.resetActiveGame();
    }
  }

  setDefaultGame(league) {
    let defaultGame;
    if (this.events[league] && this.events[league][0] && this.events[league][0]['games']) {
      let loadCounter = 0
      const loadInterval = setInterval(() => {
        if (!this.gamePkSelected[league]) {
          this.events[league].forEach(event => {
            if (!defaultGame && event['games']) {
              event['games'].forEach(game => {
                if (!this.isPastGame(game) && this.isChallengeGame(game)) {
                  this.gameSelected[league] = game;
                  this.gamePkSelected[league] = game.gamePk;
                  clearInterval(loadInterval);
                }
              });
            }
          });
          loadCounter++;
          setTimeout(() => {
            this.isLoading = true;
          }, 200)
        }
        if (loadCounter > 10 || Object.keys(this.gameChallengeMap).length > 0) {
          clearInterval(loadInterval);
        }
        loadCounter++;
      }, 500)
    }
  }

  async loadInvites() {
    this.afs.collection('invite', ref => ref
      .where('users', 'array-contains', this.user.uid)
      .orderBy('start', 'desc')
      .where('start', '>', new Date(Date.now() - 3 * 24 * 3600 * 1000)))
      .valueChanges({ idField: 'id' }).subscribe((invites: Array<any>) => {
        this.invites = {};
        invites.forEach((invite: Invite) => {
          this.invites[invite.id] = invite;
          while (typeof this.linksInvites[invite.start.seconds] !== 'undefined' &&
            this.linksInvites[invite.start.seconds].inviteId && this.linksInvites[invite.start.seconds].inviteId !== invite.id) {
            invite.start.seconds++;
          }
          this.linksInvites[invite.start.seconds] = { inviteId: invite.id };
        });
      });
  }

  getSortedSchedule(links) {
    const linksSorted = Object.keys(links).sort((a: any, b: any) => { return a - b; });
    return linksSorted;
  }

  isUserBids(challenge) {
    if (this.user && this.user.games) {
      if (Object.keys(this.user.games).indexOf(challenge['games'][0]) !== -1) {
        return true;
      } else {
        return false;
      }
    }
  }

  isBidPlaced(gamePk) {
    return this.user && this.user.games &&
      this.user.games[gamePk];
  }

  countBids(gamePk) {
    return this.isBidPlaced(gamePk) ? 1 : 0;
  }

  isPastGame(game) {
    return game && (game.status?.statusCode == '7' ||
      (new Date(game.gameTime).getTime()) - (new Date().getTime()) > 0);
  }

  isChallengeGame(game) {
    return game && this.gameChallengeMap[game.gamePk];
  }

  isNonChallengeGame(game) {
    return !game || !this.gameChallengeMap[game.gamePk];
  }

  isInviteAdmin(invite: any) {
    return invite && invite.admins.indexOf(this.user.uid) !== -1;
  }

  isWinning(invite: any) {
    return invite && invite.winners.indexOf(this.user.uid) !== -1;
  }

  isPrefTeam(league, game) {
    if (league && game && this.leagues[league] &&
      this.userService?.userPrefs?.leagues &&
      this.userService?.userPrefs?.leagues[league]) {
      const away = game.teams.away.team;
      const home = game.teams.home.team;
      if (this.leagues[league] && this.leagues[league][away.id] &&
        this.userService?.userPrefs.leagues[league].includes(this.leagues[league][away.id].name)) {
        return true;
      }
      else if (this.leagues[league] && this.leagues[league][home.id] &&
        this.userService?.userPrefs.leagues[league].includes(this.leagues[league][home.id].name)) {
        return true;
      }
    }
    return false;
  }

  randomQuote() {
    this.afs.collection("quotes").valueChanges({ idField: 'id' }).subscribe(quotes => {
      this.quote = quotes[Math.floor(Math.random() * quotes.length)];
    });
  }

  async selectSport() {
    const popover = await this.popoverCtrl.create({
      component: PopupSelectSportComponent,
      cssClass: 'w340'
    });
    popover.onDidDismiss().then((data) => {
    });
    return await popover.present();
  }

  setLeague(league) {
    this.league = league;
    this.loadLeague(this.league);
  }

  clickGame(event, game) { 
    if (game?.status && !['trophy-box', 'ico-trophy'].includes(event.target.classList?.value) &&
        !['trophy-box', 'ico-trophy'].includes(event.target.parentNode?.classList?.value)) {
      switch (isNaN(parseInt(game?.status.statusCode)) || parseInt(game?.status.statusCode)) {
        case true: // not a number
          break;
        case 1: // Scheduled
          if (this.isPastGame(game) || this.isNonChallengeGame(game)) { // No Bids allowed
            this.router.navigate([`/bid/0/${game.gamePk}/${this.gameChallengeMap[game.gamePk] || 0}`]);
          }
          else { //
            this.gameSelected[this.league] = { ...{ sport: 'nhl' }, ...game };
            this.gamePkSelected[this.league] = game.gamePk;
            this.calendar?.nativeElement.scrollIntoView();
            this.resetActiveGame(true);
          }
          break;
        case 2: // Pre-Game
        case 3: // 
        case 4: // 
        case 5: // 
        case 6: // Pre-Final
        case 7: // Final
        default:
          this.router.navigate([`/bid/0/${game.gamePk}/${this.gameChallengeMap[game.gamePk] || 0}`]);
      }
    }
  }

  resetActiveGame(resetSlides?) {
    if (this.activeGame) {
      this.activeGame.showGameInfo = false;
      this.activeGame.showMoreInfoAway = false;
      this.activeGame.resetCountdown();
      this.activeGame.resetBid();
      this.resetSlides = resetSlides;
    }
  }

  getIcon(sport) {
    return this.sports.find(item => item.name === sport).icon;
  }

  async shareInviteGame(link) {
    const modal = await this.modalController.create({
      component: PopupShareComponent,
      componentProps: {
        searchService: this.searchService,
        afs: this.afs,
        link: link
      },
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-default',
    });
    await modal.present();
  }

  loadLeagueStandings(league) {
    this.afs.collection(league + '-stats').valueChanges({ idField: 'id' }).subscribe(standings => {
      this.standings[league] = {};
      standings.forEach(stat => {
        this.standings[league][stat.id] = stat;
        delete this.standings[league][stat.id].id;
      });
    });
  }

  showTeamStats(val) {
  }

  async openModalDefault() {
    const popover = await this.popoverCtrl.create({
      component: ModalDefaultComponent,
      componentProps: {
        title: 'Would you like to place another bid?',
        popoverCtrl: this.popoverCtrl
      },
      cssClass: 'modal-wrap-default'
    });
    popover.onDidDismiss().then((data) => {
    });
    return await popover.present();
  }

  sortByChallenge(data) {
    this.sortBy = this.sortBy === SORT.CHALLENGE ? undefined : SORT.CHALLENGE;
  }

  sortByDate(data) {
    this.sortBy = this.sortBy === SORT.DATE ? undefined : SORT.DATE;
  }

}
