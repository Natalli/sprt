import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChallengesPage } from './challenges.page';
import { DirectivesModule } from '../../directives/directives.module';
import { PopupSelectSportComponent } from '../../popups/popup-select-sport/popup-select-sport.component';
import { SearchServiceModule } from '../../services/search/search.module';
import { UserModule } from '../../components/user/user.module';
import { HeaderModule } from '../../components/header/header.module';
import { SubHeaderModule } from '../../components/sub-header/sub-header.module';
import { IcoSvgModule } from '../../components/ico-svg/ico-svg.module';
import { GameModule } from '../../components/game/game.module';
import { DateModule } from '../../components/date/date.module';
import { ModalDefaultComponent } from '../../popups/new/modal-default/modal-default.component';
import { CheckboxModule } from '../../components/checkbox/checkbox.module';
import { ModalErrorComponent } from 'src/app/popups/new/modal-error/modal-error.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

const routes: Routes = [
  {
    path: '',
    component: ChallengesPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        DirectivesModule,
        SearchServiceModule,
        UserModule,
        HeaderModule,
        SubHeaderModule,
        IcoSvgModule,
        GameModule,
        DateModule,
        CheckboxModule,
        NgxSkeletonLoaderModule
    ],
    declarations: [
        ChallengesPage,
        PopupSelectSportComponent,
        ModalDefaultComponent,
        ModalErrorComponent
    ]
})
export class ChallengesPageModule { }
