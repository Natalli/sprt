import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailPage } from './detail.page';
import { SnapchatModule } from '../../snapchat/snapchat.module';
import { DirectivesModule } from '../../directives/directives.module';
import { PopupTeamSearchComponent } from '../../popups/popup-team-search/popup-team-search.component';
import { SearchServiceModule } from '../../services/search/search.module';
import { UserModule } from '../../components/user/user.module';
import { HeaderModule } from '../../components/header/header.module';
import { SubHeaderModule } from '../../components/sub-header/sub-header.module';
import { IcoSvgModule } from '../../components/ico-svg/ico-svg.module';
import { GameModule } from '../../components/game/game.module';
import { DateModule } from '../../components/date/date.module';
import { GridModule } from '../../components/grid/grid.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

const routes: Routes = [
  {
    path: '',
    component: DetailPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SnapchatModule,
        DirectivesModule,
        SearchServiceModule,
        UserModule,
        HeaderModule,
        SubHeaderModule,
        IcoSvgModule,
        GameModule,
        DateModule,
        GridModule,
        NgxSkeletonLoaderModule
    ],
    declarations: [DetailPage, PopupTeamSearchComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DetailPageModule {}
