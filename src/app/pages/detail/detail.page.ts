import { environment } from 'src/environments/environment';
import { Component, OnInit, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { IonContent, LoadingController, NavController, ModalController, IonIcon, ToastController, Platform } from '@ionic/angular';
import 'firebase/database';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { EngineService } from '../../engine.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BehaviorSubject, Subscription } from 'rxjs';
import { LayoutService } from '../../services/layout.service';
import { Constants } from '../../constants';
import { UsersService } from '../../service/users.service';
import { ISettings } from '../../interfaces/settings';
import { SettingsNotification } from '../../interfaces/enum/settings.enum';
import { UserService } from '../../service/user.service';
import { AppComponent } from '../../app.component';
import { SearchService } from '../../services/search/search.service';
import { getAuth, onAuthStateChanged } from 'firebase/auth';

export interface Config { }

@Component({
    selector: 'app-detail',
    templateUrl: './new-detail.page.html',
    styleUrls: ['./new-detail.page.scss'],
})
export class DetailPage implements OnInit {
    @ViewChild('calendar', { read: ElementRef }) private calendar: ElementRef;
    game: any;

    Constants = Constants;
    today = new Date();

    isOpenKeyboard: boolean = false;
    bidCounter: number = 0;
    bidBalance: number = 0;
    challenge_id;
    baseScore: number = 0;

    @ViewChild(IonContent) content: IonContent;
    private subscription: Subscription;
    grid = { count: 7, amount: 0, total: 0, max_users: 8, team_id: 0 };
    bids: Object = {};
    bid = [];
    user: any;
    users: Object = {};
    standings: Object = {};
    standingKeys = [];
    isBid = this.router.url.indexOf('bid') != -1;
    objectKeys = Object.keys;
    intervalId: any = '';
    legacyBid: boolean;
    isAdmin: boolean = false;
    game_id: string;
    invite_id: string;
    team_id: string;
    settings: ISettings;
    challengeGames: any;
    challenge: any;
    games: any;
    invite_user: any;
    notLoggedUser: boolean = false;
    leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
    isMultiBid: boolean = false;
    isLoading: boolean = false;
    isHideSubHeader: boolean = false;

    constructor(private route: ActivatedRoute,
        public router: Router,
        public engineService: EngineService,
        private afs: AngularFirestore,
        public alertController: AlertController,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        public modalController: ModalController,
        protected layoutService: LayoutService,
        public usersService: UsersService,
        protected userService: UserService,
        public myapp: AppComponent,
        protected searchService: SearchService,
        protected toastController: ToastController,
        protected platform: Platform
    ) {
        this.userService.auth = getAuth();
        this.game = {};
        this.bids = {};
        this.user = { balance: 0 };
        this.users = this.usersService.users;
        onAuthStateChanged(this.userService.auth, (user) => {
            if (user) {
                this.user = user;
                this.isAdmin = environment.admins.includes(user.uid);

                this.route.params.subscribe(params => {
                    this.game_id = params.id;
                    this.invite_id = params.invite_id || '';
                    this.challenge_id = params.challenge_id || '';
                    this.team_id = params.team_id;
                    this.invite_user = params.user_id;
                    this.legacyBid = (parseInt(params.id) > 2019020402 && parseInt(params.id) < 2019020565);

                    if (this.challenge_id) {
                        this.afs.collection("team/").doc(params.id).valueChanges().subscribe(teams => {
                            if (this.isBid || true) {
                                this.afs.collection("bid/").doc(params.id).valueChanges().subscribe(item => {
                                    this.bids[params.id] = (this.legacyBid ? item[this.grid.team_id] : item) || {};
                                    if (this.bids[params.id] && typeof this.bids[params.id][user.uid] !== 'undefined') {
                                        this.bid = Object.values(this.bids[params.id][user.uid]);
                                        this.initBids();
                                     }
                                     else {
                                        this.calcBalance(0);
                                    }
                                });
                            }
                        });
                        this.loadGames();
                    }
                });
                
            } else {
                this.notLoggedUser = true;
                this.route.params.subscribe(params => {
                    this.game_id = params.id;
                    this.invite_id = params.invite_id || '';
                    this.team_id = params.team_id;
                    this.invite_user = params.user_id;
                })
            }
        });
        this.loadSchedule();
        this.settings = this.userService.settings;
    }

    ngOnInit() {
 
    }

    loadGames() {
        this.afs.collection('challenge').doc(this.challenge_id).valueChanges().subscribe((challenge: any) => {
            this.challenge = challenge;
        });
    }

    async loadSportStandings(sport) {
        this.afs.collection(sport + '-stats').valueChanges({ idField: 'id' }).subscribe(standings => {
            standings.forEach(stat => {
                this.standings[stat.id] = stat;
                delete this.standings[stat.id].id;
            });
        });
    }

    calcBalance(bidAmount?) {
        setTimeout(() => {
            this.bidBalance = Constants.GRID_PREFS[this.game.sport] ? (Constants.GRID_PREFS[this.game.sport].balance - bidAmount||0) : 0;
        }, this.game?.sport ? 0 : 500);
    }

    async loadSchedule(event?) {
        this.route.params.subscribe(params => {
            this.subscription = this.afs.doc('live/' + params.id).valueChanges().subscribe((game: any) => {
                if (!this.intervalId) {
                    if (!game) {
                        return;
                    }
                }
                this.game = game;
                setTimeout(() => {
                    this.isLoading = true;
                }, 500)
                this.game.sport = this.game.sport || 'nhl';
                this.loadSportStandings(this.game.sport);
               this.baseScore = Constants.GRID_PREFS[this.game.sport].scoreShift||0;
            });
        });
    }

    getChallengeGames(challenge_id?) {
        challenge_id = this.challenge.id;
        // let challenge = this.challenges[challenge_id] || this.challenge || {games: []};
        if (!this.challenge['start']) return;
        let games = [];
        let date = new Date(this.challenge['start']['seconds'] * 1000);
        if (this.games && this.games.length) {
            for (var i = 0; i < this.games.length; i++) {
                if (this.games[i]['date'] == date.toJSON().slice(0, 10) || true) { // Temp allow multi day Challenges
                    let game = Object.assign({}, this.games[i], { games: [] });
                    setTimeout(() => {
                        this.isLoading = true;
                    }, 200)
                    for (var j = 0; j < this.games[i]['games'].length; j++) {
                        if (this.challenge['games'].includes(String(this.games[i]['games'][j]['gamePk']))) {
                            game['games'].push(this.games[i]['games'][j]);
                        }
                    }
                    if (game['games'].length > 0) {
                        games.push(game);
                    }
                }
            }
        }
        games[0].games.sort((a, b) => new Date(a['gameDate']).getTime() - new Date(b['gameDate']).getTime());
        let chalengeGames = games;
        this.challengeGames = chalengeGames[0].games.map(game => game.gamePk ? game.gamePk.toString() : null);
    }

    initBids() {
        let bidCounter = 0;
        let bidAmount = 0;
        if (this.bid) {
            this.bid.forEach(row => {
                row.forEach(bid =>{
                    if (bid > 0.01) {
                        bidCounter++;
                    }
                    bidAmount += bid;
                })
            });
            this.bidCounter = bidCounter;
            this.calcBalance(bidAmount);
        }
    }

    multiCheck(data) {
        this.isMultiBid = data;
    }

    openNumpad(data) {
        if (data) {
            const content = document.querySelector('#scrollContainer');
            this.isOpenKeyboard = true;
            if(this.isMultiBid) {
                this.content.scrollToBottom(1000);
            } else {
                content.scrollIntoView({ behavior: 'smooth', block: 'end' });
            }
            if (window.innerWidth < 380) {
                this.isHideSubHeader = true;
            } else {
                this.isHideSubHeader = false;
            }
        } else {
            this.isOpenKeyboard = false;
            this.isHideSubHeader = false;
        }
    }

}
