import { DatePipe, Location } from '@angular/common';
import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Constants } from 'src/app/constants';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-upcoming',
  templateUrl: 'upcoming.page.html',
  styleUrls: ['upcoming.page.scss'],
})
export class UpcomingPage {
  league: string;
  leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
  upcomingGames;
  eventsSubs: any = {};
  events: any = {};
  prefTeams: any = {};
  Constants = Constants;
  selectedTeams: any = {};
  prefs;
  challengesSubs: any;
  gameChallengeMap: {};
  constructor(
    protected afs: AngularFirestore,
    public router: Router,
    public location: Location,
    protected userService: UserService,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.load();

  }

  load() {
    this.userService.user$.subscribe(resp => {
      if (resp) {
        this.userService.loadUserPrefs();
        this.userService.userPrefs$.subscribe(pref => {
          if (pref) {
            this.prefs = pref;
            Object.keys(Constants.DEFAULT_LEAGUES).forEach(league => {
              this.loadLeague(league);
            })
          }
        })
      }
    })
  }

  async loadChallenges(league) {
    this.upcomingGames = [];
    const startDate = new Date();
    const endDate = new Date(Date.now() + 120 * 24 * 60 * 60000);
    this.challengesSubs = this.afs.collection('challenge', ref => ref
      .orderBy('start', 'desc')
      .where('start', '>', startDate)
      .where('start', '<', endDate)
      .where('sport', '==', league)
      .where('disabled', '==', false)
      .limit(6))
      .valueChanges({ idField: 'id' }).subscribe((challenges) => {
        this.gameChallengeMap = this.gameChallengeMap || {};
        challenges.forEach((challenge: any, index) => {
          challenge.games.forEach(gamePk => {
            this.gameChallengeMap[gamePk] = challenge.id;
          })
        });
      });
    this.loadGames(league, startDate.getTime(), endDate.getTime());
  }

  loadLeague(league) {
    const lleague = league.toLowerCase();
    this.selectedTeams[league] = [];
    if (!Object.keys(this.leagues[lleague]).length) {
      this.afs.collection(`${lleague}-team`, ref => ref
        .where('isActive', '==', true)).get().subscribe(snapshot => {
          snapshot.docs.forEach(doc => {
            this.leagues[lleague][doc.data()['oid'] || doc.id] = doc.data();
          })
          if (this.prefs.leagues && this.prefs.leagues[league]) {
            this.prefs.leagues[league].forEach(item => {
              let selected = this.leagues[lleague].find(team => team && team.name === item);
              selected.games = [];
              this.selectedTeams[league].push(selected);
            })
          }
          this.loadChallenges(league);
        });
    }
  }

  loadGames(league, startDate, endDate) {
    if (typeof this.eventsSubs[league] !== 'undefined') {
      this.eventsSubs[league].unsubscribe();
    }
    this.eventsSubs[league] = this.afs.collection(league, ref => ref
      .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
      .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
      .valueChanges().subscribe((games: Array<any>) => {
        this.events[league] = games;
        this.buildPrefGames(league);
      });
  }

  buildPrefGames(league) {
    if (this.events[league] && typeof this.prefTeams[league] === 'undefined') {
      this.prefTeams[league] = [];
      this.events[league].forEach(event => {
        event.games.forEach(game => {
          if (this.isPrefTeam(league, game)) {
            this.prefTeams[league].push(game);
          }
        })
      });
    }
  }

  isPrefTeam(league, game) {
    if (league && game && this.leagues[league] &&
      this.prefs.leagues &&
      this.prefs.leagues[league]) {
      const away = game.teams.away.team;
      const home = game.teams.home.team;
      if (this.leagues[league] && this.leagues[league][away.id] &&
        this.prefs.leagues[league].includes(this.leagues[league][away.id].name)) {
        this.transformData(league, game, 'away', 'home');
        return true;
      }
      else if (this.leagues[league] && this.leagues[league][home.id] &&
        this.prefs.leagues[league].includes(this.leagues[league][home.id].name)) {
        this.transformData(league, game, 'home', 'away');
        return true;
      }
    }
    return false;
  }

  isChallengeGame(game) {
    return game && this.gameChallengeMap[game.gamePk];
  }

  clickGame(data) {
    const game = this.prefTeams[this.league].find(game => game.gamePk === data.gamePk);
    if (game.status.statusCode === '1') {
      this.router.navigate([`/detail/0/${game.gamePk}/${this.gameChallengeMap[game.gamePk] || 0}`]);
    } else {
      this.router.navigate([`/bid/0/${game.gamePk}/${this.gameChallengeMap[game.gamePk] || 0}`]);
    }
  }

  transformData(league, game, home, away) {
    const team = this.selectedTeams[league].find(item => game.teams[home].team.name === item.displayName)
    const data = {
      date: game.gameDate,
      gamePk: game.gamePk,
      team: this.leagues[league].find(item => item && item.displayName === game.teams[away].team.name)
    }
    team.games.push(data)
  }
}
