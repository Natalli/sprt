import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { UpcomingPage } from './upcoming.page';
import { BannerModule } from '../../components/banner/banner.module';
import { DirectivesModule } from '../../directives/directives.module';
import { HeaderModule } from '../../components/header/header.module';
import { SubHeaderModule } from '../../components/sub-header/sub-header.module';
import { IcoSvgModule } from '../../components/ico-svg/ico-svg.module';
import { TeamModule } from '../../components/team/team.module';
import { AvatarModule } from 'src/app/components/avatar/avatar.module';
import { TeamCardModule } from 'src/app/components/team-card/team-card.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: UpcomingPage
      }
    ]),
    BannerModule,
    DirectivesModule,
    HeaderModule,
    SubHeaderModule,
    IcoSvgModule,
    AvatarModule,
    TeamCardModule
  ],
  declarations: [UpcomingPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class UpcomingPageModule { }
