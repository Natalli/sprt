import { AngularFirestore } from '@angular/fire/compat/firestore';
import { LoadingController, PopoverController, ModalController, IonIcon, Platform, MenuController, AlertController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { PopupMessageComponent } from '../../popups/popup-message/popup-message.component';
import { Constants } from '../../constants';
import { UserService } from '../../service/user.service';
import { LayoutService } from '../../services/layout.service';
import { AppComponent } from '../../app.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ActivityService } from '../../services/activity.service';
import { MessagingService } from '../../services/messaging.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { Location } from '@angular/common';
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { doc, deleteDoc, getFirestore } from "firebase/firestore";
import { deleteUser, getAuth } from 'firebase/auth';
import { ModalService } from 'src/app/service/modal.service';

export class Stats {
  gamesCount: number;
  gamesWon: number;
  challengesCount: number;
  challengesWon: number;
  publicProfile: boolean;
  challengesPrizes: any;
  gamesCountBySport?: Object;
  gamesWonBySport?: Object;
  challengesCountBySport?: Object;
  challengesWonBySport?: Object;
  challengesPrizesBySport?: any
}

@Component({
  selector: 'app-profile',
  templateUrl: './new-profile.page.html',
  styleUrls: ['./new-profile.page.scss'],
})

export class ProfilePage extends AppComponent implements OnInit {
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  users: Array<Object> = [];
  user: any;
  email: string = '';
  password: string = '';
  username: string = '';
  publicName: string = '';
  legalName: string = '';
  color: string = '';
  url: string;
  publicProfile: boolean;
  userId: string;
  challenges: Array<Object> = [];
  friendsCount: number = 0;
  creationDate: string;
  challengeWinners: Object = {};
  challengesWon: number = 0;
  stats: Object = {};
  games: any;
  teams: Object = {};
  userChallenges: Array<Object> = [];
  userWinner: Array<Object> = [];

  myUid: string;
  friends: Array<Object>;
  isAddFriend: boolean;
  isHiddenBtn: boolean;
  visitor: any;
  invitations: Array<Object> = [];
  arrayStats: Stats = { gamesCount: 0, gamesWon: 0, challengesCount: 0, challengesWon: 0, publicProfile: false, challengesPrizes: 0 };
  userStats: Stats = { gamesCount: 0, gamesWon: 0, challengesCount: 0, challengesWon: 0, publicProfile: false, challengesPrizes: 0 };
  openUnfriend: boolean;
  totalGames: Array<Object> = [];
  chatManagerInfo;
  userInfo: any;
  profilePopup;
  props;
  challengesPrizes: any;
  tabsBySport = [{ name: 'all', icon: '' }, { name: 'nhl', icon: 'hockey' }, { name: 'nfl', icon: 'american-football' }, { name: 'nba', icon: 'basketball' }];
  sportSelected: string = 'all';
  optimizedStats: any = {all: {}, nhl: {}, nfl: {}, nba: {}};
  object = Object;
  publicFriends: any[] = [];
  Constants = Constants;
  showFriends: boolean;

  constructor(
    public loadingController: LoadingController,
    public popoverCtrl: PopoverController,
    protected routerActive: ActivatedRoute,
    public router: Router,
    protected afs: AngularFirestore,
    public modalController: ModalController,
    public userService: UserService,
    protected layoutService: LayoutService,
    protected platform: Platform,
    protected splashScreen: SplashScreen,
    protected statusBar: StatusBar,
    public activityService: ActivityService,
    protected messagingService: MessagingService,
    protected menu: MenuController,
    protected fns: AngularFireFunctions,
    protected keyboard: Keyboard,
    public alertController: AlertController,
    public location: Location,
    public screenshot: Screenshot,
    public openNativeSettings: OpenNativeSettings,
    private toastController: ToastController,
    protected menuCtrl: MenuController,
    protected modalService: ModalService) {
    super(platform, splashScreen, statusBar, afs, router, loadingController, activityService, messagingService, menu, layoutService, fns, keyboard, alertController, location, screenshot, openNativeSettings, userService, menuCtrl);
  }

  ngOnInit() {
    // this.getUser();
    // this.loadUsers();
  }

  ionViewDidEnter() {
    this.getUser();
    this.loadUsers();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  editProfile() {
    if (this.user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
      this.loginAlert();
    } else {
      this.router.navigate(['edit-profile'], { replaceUrl: true });
    }
  }

  closeProfile() {
    if (this.router.url.indexOf('profile') !== -1) {
      this.router.navigate(['challenges']);
    }
    else {
      this.modalController?.dismiss();
    }
  }

  async reportProfile() {
    const alert = await this.alertController.create({
      header: 'Report User',
      message: 'Are you sure you want to report this User?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }, {
          text: 'Report',
          cssClass: 'danger',
          handler: () => {
            this.afs.doc('/user/' + this.myUid).update({ reported: firebase.firestore.FieldValue.arrayUnion(this.userId) }).finally(() => {
              this.closeProfile();
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async forceUpdateStats() {
    this.modalService.presentLoading('Please wait for stats to update...');
    this.afs.collection('user').doc(this.user.uid).update({ stats: {} })
      .then(() => {
        this.userService.calcStats(this.user.uid, true, true);
      });
  }

  getUser() {
    this.userService.user$.subscribe(user => {
      console.log(user, 'UU')
      if (user) {
        this.user = user;
        this.userService.getUser$(user.uid).subscribe(resp => {
          this.userInfo = resp;
        })
        this.myUid = user.uid;
        this.routerActive.params.subscribe(params => {
          if (params.id) {  // Public profile
            this.userId = params.id;
          } else { }         // User profile
          this.afs.collection('user')
            .doc(this.userId || user.uid)
            .valueChanges().subscribe((resp: object) => {
              this.userData({ ...resp, ...{ uid: this.userId || user.uid } });
              this.publicProfile = (this.userId !== this.myUid) && typeof this.userId !== 'undefined';
              if (params.id || this.userId) {
                this.visitor = this.user;
                if (this.userInfo.friends) {
                  this.userInfo.friends.filter(friend => {
                    if (this.userId === friend['uid']) {
                      friend['sent'] === true ? this.isAddFriend = true : this.isHiddenBtn = true;
                    }
                  });
                }
              }
              if (this.userInfo.friends && this.userInfo.friends.length) {
                this.invitations = this.userInfo.friends.filter(friend => (friend.pending) ? friend : null);
              }
            })
        });
      }
    });
  } 

  loadUsers() {
    this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(users => {
      this.users = users;
      this.users.forEach(user => {
        this.users[user['id']] = user;
      })
    })
  }

  async getTotalGames() {
    await this.afs.collection("team").valueChanges({ idField: 'id' }).subscribe(games => {
      this.totalGames = games;
    });
  }

  getFriendName(id: string) {
    let friend = this.users.filter((item) => (item['id'] || item['uid']) === id ? item : '')
    return friend[0]['name']
  }

  getFriendPhoto(id) {
    let friend = this.users.filter((item) => (item['id'] || item['uid']) === id ? item : '')
    return friend[0]['photoURL'] || Constants.AVATAR_DEMO
  }

  userData(user) {
    this.user = user;
    if (this.user.stats) {
      this.userStats = this.user.stats;
      this.challengesPrizes = 0;
      if (this.userStats.challengesPrizesBySport && Object.values(this.userStats.challengesPrizesBySport).length) {
        Object.keys(this.userStats.challengesPrizesBySport).forEach(sport => {
          Object.keys(this.userStats.challengesPrizesBySport[sport]).forEach((challenge_id: any) => {
            this.challengesPrizes += this.userStats.challengesPrizesBySport[sport][challenge_id];
          });
        });
      }
    }

    this.userStats['challengesPrizes'] = this.challengesPrizes;
    this.getOptimizedStats();
    let friends = this.user['friends'] || [];
    this.friends = friends.filter(friend => {
      return (friend['sent'] === false) ? friend : null
    }) || [];
    if (!this.friendsCount && (!user.updated || (new Date(user.updated.seconds * 1000).getTime() + 12 * 60 * 60 * 1000) < new Date().getTime())) {
      setTimeout(() => {
        this.userService.calcStats(user.uid, true);
      }, 1000);
    }
    this.friendsCount = this.friends.length;
    if (user && Object.values(user).length > 0) {
      this.username = user['name'] || this.user.displayName;
      this.color = user['color'] || '';
      this.publicName = user['publicName'] || '';
      this.legalName = user['legalName'] || '';
      this.email = user['email'] || '';
      this.url = user['photoURL'] || Constants.AVATAR_DEMO;
      this.creationDate = user['creationTime'] || '';
    }
  }


  async openChat(user_uid, event) {
    this.afs.collection('/user/' + this.myUid + '/chat', ref => ref
      .where('private', '==', this.visitor.uid))
      .valueChanges({ idField: 'id' }).subscribe((chat) => {
        if (chat.length) {
          this.props = {
            recepient: this.visitor,
            chatId: chat[0]['id'],
            chat: chat[0],
            popoverCtrl: this.popoverCtrl,
            afs: this.afs,
            userService: this.userService,
            getChallengeWon: this.getChallengeWon
          }
        } else {
          this.props = {
            recepient: this.visitor,
            popoverCtrl: this.popoverCtrl,
            afs: this.afs,
            userService: this.userService,
            getChallengeWon: this.getChallengeWon
          }
        }
        this.chatModal(this.props)
      })
  }

  async chatModal(props) {
    const modal = await this.modalController.create({
      component: PopupMessageComponent,
      componentProps: props,
      swipeToClose: true,
      showBackdrop: true,
      backdropDismiss: true,
      cssClass: 'popover-default',
      id: 'popupMessages'
    });
    return await modal.present();
  }

  addFriend() {
    // let friends = [];
    // let vFriends = [];
    let friends = this.userInfo.friends || [];
    let vFriends = this.visitor['friends'] || [];
    friends.push({ uid: this.userId, sent: true });
    vFriends.push({ uid: this.myUid, sent: true, pending: true });
    this.saveData(this.myUid, friends, this.userId, vFriends, 'Your invitation has been sent');
    const notification = {
      title: "Friend request",
      text: "sent you an invitation to become a friend",
      platforms: ['ios', 'android', 'web'],
      start: new Date(),
      scheduling: 'Now',
      receivers: [this.userId],
      created: new Date(),
      friendRequest: true,
      sendRequest: true,
      sender: this.myUid,
      // name: this.userInfo.name,
      // photoURL: this.userInfo.photoURL || Constants.AVATAR_DEMO
    };
    this.afs.collection('/notification/').add(notification);
  }

  deleteFriend() {
    this.afs.collection("user").doc(this.userId).get().subscribe(user => {
      let friends = user.data()['friends'];
      let removeUser = friends.find(user => {
        return user.uid === this.myUid
      });
      let i = friends.indexOf(removeUser);
      if (i != -1) {
        friends.splice(i, 1);
      }
      this.saveData(this.userId, friends, null, null, null)
    })
    let vFriends = this.userInfo.friends
    let removeVUser = vFriends.find(user => {
      if (user.uid) {
        return user.uid === this.userId
      } else {
        return user.id === this.userId
      }
    });
    let vI = vFriends.indexOf(removeVUser);
    if (vI != -1) {
      vFriends.splice(vI, 1);
    }

    this.saveData(null, null, this.myUid, vFriends, 'Successfully deleted', 'delete');
  }

  saveData(myUid, friends, userUid, vFriends, text1, isDelete?) {
    if (myUid && friends) {
      this.afs.collection("user").doc(myUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends });
        }
        else {
          item.ref.update({ friends });
        }
      });
    }
    if (userUid && vFriends && text1) {
      this.afs.collection("user").doc(userUid).get().subscribe(item => {
        if (!item.exists) {
          item.ref.set({ friends: vFriends });
        }
        else {
          item.ref.update({ friends: vFriends });
          this.modalService.presentLoading(text1);
          if (isDelete) {
            setTimeout(() => {
              this.loadingController.dismiss().catch(() => { });
              this.openUnfriend = false;
              this.isAddFriend = false;
              this.isHiddenBtn = false;
            }, 3000)
          } else {
            setTimeout(() => {
              this.loadingController.dismiss().catch(() => { });
              this.isAddFriend = true;
            }, 3000)
          }
        }
      });
    }
  }

  async loading(text) {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: text,
      translucent: true
    });
    return await loading.present();
  }

  getTrophyColor(winsCount: number) {
    let className = '';
    switch (true) {
      case (winsCount === 1):
        className = 'bronze';
        break;
      case (winsCount < 5):
        className = 'silver';
        break;
      case (winsCount < 10):
        className = 'gold';
        break;
      case (winsCount >= 10):
        className = 'gold';
        break;
    }
    return className;
  }

  getChallengeWon(user) {
    if (user && user.stats && user.stats.challengesWon) {
      if (typeof user.stats.challengesWon === 'object') {
        return Object.values(user.stats.challengesWon).reduce((acc: number, val: number) => {
          return acc + val || 0;
        }, 0);
      } else {
        return user.stats.challengesWon;
      }
    }
  }

  getOptimizedStats() {
    this.optimizedStats = {};
    // console.log(this.userStats, 'stats')
    if (this.userStats) {
      let all = {};
      let nhl = {};
      let nfl = {};
      let nba = {};
      Object.entries(this.userStats).forEach(([key, value]) => {
        if (typeof value === 'number') {
          all[key] = value;
        } else if (value) {
          Object.entries(value).forEach(([sport, sportValue]) => {
            let stat = key.replace('BySport', '');
            if (sport === 'nhl') {
              if (key === 'challengesPrizesBySport') {
                if (sportValue && Object.values(sportValue).length) {
                  this.challengesPrizes = 0;
                  Object.keys(sportValue).forEach((challenge_id: any) => {
                    this.challengesPrizes += sportValue[challenge_id];
                  });
                  nhl[stat] = this.challengesPrizes;
                }
              } else {
                nhl[stat] = sportValue;
              }
            } else if (sport === 'nfl') {
              if (key === 'challengesPrizesBySport') {
                if (sportValue && Object.values(sportValue).length) {
                  this.challengesPrizes = 0;
                  Object.keys(sportValue).forEach((challenge_id: any) => {
                    this.challengesPrizes += sportValue[challenge_id];
                  });
                  nfl[stat] = this.challengesPrizes;
                }
              } else {
                nfl[stat] = sportValue;
              }
            } else if (sport === 'nba') {
              if (key === 'challengesPrizesBySport') {
                if (sportValue && Object.values(sportValue).length) {
                  this.challengesPrizes = 0;
                  Object.keys(sportValue).forEach((challenge_id: any) => {
                    this.challengesPrizes += sportValue[challenge_id];
                  });
                  nba[stat] = this.challengesPrizes;
                }
              } else {
                nba[stat] = sportValue;
              }
            }
          });
        }
      });
      this.optimizedStats = { all: all, nhl: nhl, nfl: nfl, nba: nba };
    }
  }

  selectSport(event) {
    this.sportSelected = event.detail.value;
  }

  openFriends() {
    if (this.user && this.user.friends) {
      this.publicFriends = this.user.friends.filter(friend => (friend.sent === false) ? friend : null);;
      this.showFriends = true;
    }
    // (!publicProfile) && router.navigate(['/friends/']); 
  }

  async confirmRemoveUser() {
    const toast = await this.toastController.create({
      header: 'Delete Account and Data',
      message: 'Are you sure you want to delete?',
      position: 'bottom',
      buttons: [
        {
          side: 'start',
          role: 'Cancel',
          text: 'Cancel',
          cssClass: 'btn-default'
        }, {
          text: 'Done',
          cssClass: 'btn-default green-btn',
          handler: () => {
            console.log('Done clicked');
            this.removeUser();
          }
        }
      ]
    });
    await toast.present();
  }

  removeUser() {
    const db = getFirestore();
    const auth = getAuth();
    const user = auth.currentUser;
    const id = this.user.uid;

    deleteUser(user).then(() => {
      const docRef = doc(db, "user", id);
      deleteDoc(docRef)
        .then(() => {
          localStorage.clear();
          this.router.navigate(['/login']);
        });
    })
    .catch(err => {
      console.log("err", err)
    })
  }
}
