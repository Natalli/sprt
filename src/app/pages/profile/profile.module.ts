import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { UserModule } from '../../components/user/user.module';
import { HeaderModule } from '../../components/header/header.module';
import { SubHeaderModule } from '../../components/sub-header/sub-header.module';
import { IcoSvgModule } from 'src/app/components/ico-svg/ico-svg.module';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    DirectivesModule,
    PipesModule,
    RoundProgressModule,
    UserModule,
    HeaderModule,
    SubHeaderModule, 
    IcoSvgModule
  ],
  declarations: [ProfilePage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class ProfilePageModule {}
