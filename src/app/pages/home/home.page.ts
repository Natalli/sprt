import { environment } from 'src/environments/environment';
import { Component, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController, IonIcon, ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { DatePipe } from '@angular/common'
import { IonContent } from '@ionic/angular';
import { Capacitor } from '@capacitor/core';
import { ActivityService } from '../../services/activity.service';
import { LayoutService } from '../../services/layout.service';
import { AppComponent } from '../../app.component';
import { UserService } from '../../service/user.service';
import { Constants } from '../../constants';
import firebase from "firebase/compat/app";
import { ModalUpdateAppComponent } from 'src/app/popups/new/modal-update-app/modal-update-app.component';

export interface Config {
}

@Component({
  selector: 'app-home',
  // templateUrl: 'home.page.html',
  templateUrl: 'new-home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild(IonContent) content: IonContent;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;

  Constants = Constants;

  gameDateFilter: string = '';
  selectedChallenge: string = '-';
  public appVersion: string = environment.version;
  public appBuild: string = environment.build;

  user: any;
  email: string = '';
  password: string = '';
  username: string = '';
  publicName: string = '';
  legalName: string = '';
  color: string = '';
  image: number;
  phone: number;
  error: string;
  userWantsToSignup: boolean = false;
  linkError: string = '';
  today = '';

  games: Array<Object> = [];
  teams: Object = {};
  teams2: Object = {};
  challenges: Array<Object> = [];
  sports: Array<any> = [{ name: 'NHL', icon: 'hockey', url: '/nhl' }, { name: 'NFL', icon: 'american-football', url: '/nfl' }, { name: 'NBA', icon: 'basketball', url: 'nba' }];
  history: Array<Object> = [];
  bids: Object = {};
  bidsLoading: boolean = true;
  userBids: Object = null;
  objectKeys = Object.keys;
  alert = alert;
  date = Date;
  challengeWinners: Object = {};
  users: Object = {};
  stats: Object = {};
  selectedSport;
  selectedTeams = {};
  showLeagues = true;
  activeLeague = '';
  showTeams = true;
  leagues = JSON.parse(JSON.stringify(Constants.DEFAULT_LEAGUES));
  sortedTeamKeys = {};
  moreGames;
  isPlatform: string;
  homeMessage = 'Pick Your Sport';
  removeBubble: boolean;

  constructor(
    public router: Router,
    private afs: AngularFirestore,
    private datePipe: DatePipe,
    public activityService: ActivityService,
    public popoverCtrl: PopoverController,
    protected layoutService: LayoutService,
    public myapp: AppComponent,
    protected modalController: ModalController,
    private userService: UserService) {
    if (!Capacitor || Capacitor.getPlatform() !== 'ios') {
      this.sports = [{ name: 'NHL', icon: 'hockey', url: 'nhl' }, { name: 'NFL', icon: 'american-football', url: 'nfl' }, { name: 'NBA', icon: 'basketball', url: 'nba' }, { name: 'MLB', icon: 'baseball' }, { name: 'EPL', icon: 'football' }];
    }
    this.checkVersion();
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    let startDate = new Date(Date.now() - 12096e5); // 2 weeks -
    let endDate = new Date(Date.now() + 12096e5); // 2 weeks +
    this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(users => {
      users.forEach(user => {
        this.users[user.id] = user;
        if (!this.users[user.id]['publicName']) {
          if (this.users[user.id]['name']) {
            this.users[user.id]['publicName'] = this.users[user.id]['name'].slice(0, 3).toUpperCase();
          }
          else {
            this.users[user.id]['publicName'] = 'ANON';
          }
        }
      });
    });
    this.loadLeague('NBA');
    this.loadLeague('NFL');
    this.loadLeague('NHL');
  }

  getGame(id) {
    if (this.games instanceof Array) {
      for (var i = 0; i < this.games.length; i++) {
        if (this.games[i]['games'].length) {
          for (var j = 0; j < this.games[i]['games'].length; j++) {
            if (this.games[i]['games'][j]['gamePk'] == id) {
              return this.games[i]['games'][j];
            }
          }
        }
      }
    }
    return;
  }

  isBidPlaced(id) {
    return typeof this.userBids[id] !== 'undefined';
  }

  selectChallenge(el) {
    this.calcChallengeWinners();
    setTimeout(() => {
      this.scrollToElement(el);
    }, 100);
  }

  scrollToElement(el) {
    const rect = document.getElementById(el).getBoundingClientRect();
    this.content.scrollToPoint(0, rect.top - 60, 1500);
  }

  async checkVersion() {
    this.afs.collection('info').doc('app').valueChanges().subscribe((app: any) => {
      if (app && (parseInt(app.build) > parseInt(this.appBuild))) {
        this.showPopup('There is a new version of the app available. Please update the app.');
      }
      if (app && app.homeMessage) {
        this.homeMessage = app.homeMessage;
      }
    });
  }

  async showPopup(text) {
    let platform = Capacitor.getPlatform();
    const popover = await this.popoverCtrl.create({
      component: ModalUpdateAppComponent,
      componentProps: {
        platform,
        text
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  getCurrentGames() {
    let games = [];
    if (this.games instanceof Array) {
      for (var i = 0; i < this.games.length; i++) {
        if ((this.selectedChallenge.length && (this.challenges[this.selectedChallenge] || { games: [] })['games'].length)
          || (!this.selectedChallenge && !(Date.parse(this.games[i]['date']) < (Date.now() - 1.728e8)))) {
          var game = Object.assign({}, this.games[i], { games: [] });
          for (var j = 0; j < this.games[i]['games'].length; j++) {

            if ((!this.selectedChallenge.length && new Date(Date.parse(this.games[i]['games'][j]['gameDate'])) > new Date(Date.now() - 10 * 60000))
            ) {
              game['games'].push(this.games[i]['games'][j]);
            }
            else if (this.selectedChallenge.length && this.challenges[this.selectedChallenge]['games'].includes(String(this.games[i]['games'][j]['gamePk']))
            ) {
              game['games'].push(this.games[i]['games'][j]);
            }
          }
          if (game['games'].length > 0) {
            games.push(game);
          }
        }
      }
    }
    return games;
  }

  calcChallengeWinners() {
    this.challengeWinners = [];
    if (this.games instanceof Array && Object.keys(this.teams2).length) {
      for (var i = 0; i < this.games.length; i++) {
        if (!this.selectedChallenge.length) {
          Object.values(this.games[i]['games'] || {}).forEach(game => {
            if (this.teams2[game['gamePk']]) {
              Object.keys(this.teams2[game['gamePk']]).forEach(team_id => {
                this.challengeWinners[game['gamePk']] = Array.from(new Set((this.challengeWinners[game['gamePk']] || []).concat(this.teams2[game['gamePk']][team_id]['winners'] || [])));
              });
            }
          });
        }
        else if ((this.selectedChallenge.length && (this.challenges[this.selectedChallenge] || { games: [] })['games'].length)) {
          this.challenges[this.selectedChallenge]['games'].forEach(gamePk => {
            if (this.teams2[gamePk]) {
              Object.keys(this.teams2[gamePk]).forEach(team_id => {
                this.challengeWinners[gamePk] = Array.from(new Set((this.challengeWinners[gamePk] || []).concat(this.teams2[gamePk][team_id]['winners'] || [])));
              });
            }
          });
        }
      }
    }
    this.calcWinnersStats();

    return this.challengeWinners;
  }

  calcWinnersStats() {
    this.stats = [];
    Object.keys(this.challengeWinners || {}).forEach(gamePk => {
      if (this.challengeWinners[gamePk]) {
        this.challengeWinners[gamePk].forEach(winner_uid => {
          this.stats[winner_uid] = this.stats[winner_uid] || 0;
          this.stats[winner_uid]++;
        });
      }
    });
    return Object.entries(this.stats).sort((a, b) => a[1] > b[1] ? -1 : a[1] === b[1] ? 0 : 1);
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  ionViewWillEnter() {
    //this.selectedChallenge = '';
  }

  ionViewDidEnter() {
    this.loadUserData();
  }

  loadUserData() {
    this.bidsLoading = true;
    setTimeout(() => {
      this.bidsLoading = false;
    }, 30000);

    this.userService.user$.subscribe((user) => {
    if (user) {
        this.user = user;
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(i => {
          var item = i;
          if (item && Object.values(item).length > 0) {
            this.username = item['name'] || this.user.displayName;
            this.color = item['color'] || '';
            this.publicName = item['publicName'] || '';
            this.legalName = item['legalName'] || '';
            var userBids = this.userBids || {};
            this.userBids = {};
            Object.keys(item['games'] || {}).forEach(game_id => {
              this.userBids[game_id] = userBids[game_id] || { [user.uid]: item['games'][game_id] };
            });
            if (!item['creationTime']) {
              let date = new Date(this.user.metadata.creationTime);
              this.saveUserData({ creationTime: date });
            }
            if (typeof item['creationTime'] === 'string') {
              let date = new Date(item['creationTime']);
              this.saveUserData({ creationTime: date });
            }
            if (!item['keywords'] || item['keywords'] && Array.isArray(item['keywords'])) {
              let keywords = this.username.toLowerCase() + ' ' + this.username.slice(0, 3).toLowerCase();
              this.saveUserData({ keywords });
            }
          }
          else {
            var data = {
              name: this.user.displayName,
              publicName: this.user.displayName.slice(0, 3),
              color: ('#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6)),
              balance: 10,
              email: this.user.email,
              keywords: this.user.displayName.toLowerCase() + ' ' + this.user.displayName.slice(0, 3).toLowerCase(),
            }
            this.saveUserData(data);
          }
        });

        this.loadUserPrefs();

        this.afs.collection("team").valueChanges({ idField: 'id' }).subscribe(games => {
          games.forEach(i => {
            var game = i;
            var game_id = i.id;
            delete game['id'];
            Object.keys(game).forEach(team_id => {
              var team = game[team_id];
              this.teams2[game_id] = this.teams2[game_id] || {};
              this.teams2[game_id][team_id] = team;
              if ((team.users || []).includes(this.user.uid) || !this.teams[game_id]) {
                this.teams[game_id] = team;
              }
              else if (!(this.teams[game_id].users || []).includes(this.user.uid) && (this.teams[game_id].users || []).length >= this.teams[game_id]['max_users']) {
                this.teams[game_id] = team;
              }
            });
          })
          this.bidsLoading = false;
          this.calcChallengeWinners();
        });

        this.afs.collection("challenge", ref => ref
          .where('start', '>', new Date(Date.now() - 7 * 24 * 60 * 60000))
          .where('start', '<', new Date(Date.now() + 30 * 24 * 60 * 60000))
          .where('disabled', "==", false)).valueChanges({ idField: 'id' }).subscribe(challenges => {
            this.challenges = [];
            challenges.forEach(i => {
              var challenge = i;
              if (challenge['start']['seconds'] * 1000 < (Date.now() - 1 * 24 * 60 * 60000)) {
                this.history = this.history || [];
                this.history.push(Object.assign({ name: '', text: '', games: [] }, challenge));
                this.challenges = this.challenges || [];
                this.challenges.push(Object.assign({ name: '', text: '', games: [] }, challenge));
              }
              else {
                this.challenges = this.challenges || [];
                this.challenges.push(Object.assign({ name: '', text: '', games: [] }, challenge));
              }
              Object.values(challenge['games']).forEach(game_id => {
                //console.log('game_id: '+game_id);
              });
            })
          });
        this.myapp.checkAuthState();
      }
    })
  }

  selectLeague(league) {
    if ( league ) {
      if ( typeof this.selectedTeams[league] !== 'undefined') {
        delete this.selectedTeams[league];
      }
      else {
        this.selectedTeams[league] = [];
      }
    }
    else {
      this.showLeagues = true;
    }
    this.userService.setUserPrefs({leagues: {...{nba: firebase.firestore.FieldValue.delete(), nfl: firebase.firestore.FieldValue.delete(), nhl: firebase.firestore.FieldValue.delete()},
                                  ...this.selectedTeams}}, true);
  }

  selectActiveLeague(league) {
    if ( league ) {
      if ( typeof this.selectedTeams[league] !== 'undefined') {
        if (this.activeLeague == league) {
          this.activeLeague = '';
          delete this.selectedTeams[league];
        }
        else {
          this.activeLeague = league;
        }
      }
      else {
        this.selectedTeams[league] = [];
        this.activeLeague = league;
      }
    }
    else {
      this.selectedTeams[this.activeLeague] = [];
      this.activeLeague = '';
      this.showLeagues = true;
    }
    this.userService.setUserPrefs({leagues: {...{nba: firebase.firestore.FieldValue.delete(), nfl: firebase.firestore.FieldValue.delete(), nhl: firebase.firestore.FieldValue.delete()},
                                  ...this.selectedTeams}}, true);
  }

  selectTeam(league, team) {
    if ( league && team ) {
      this.selectedTeams[league] = this.selectedTeams[league] || [];
      if ( this.selectedTeams[league].indexOf(team) !== -1 ) {
        this.selectedTeams[league].splice(this.selectedTeams[league].indexOf(team), 1);
      }
      else {
        this.selectedTeams[league].push(team);
      }
    }
    this.userService.setUserPrefs({leagues: {...{nba: firebase.firestore.FieldValue.delete(), nfl: firebase.firestore.FieldValue.delete(), nhl: firebase.firestore.FieldValue.delete()},
                                  ...this.selectedTeams}}, true);
  }

  getSelectedTeams() {
    return Object.values(this.selectedTeams).length > 0 ? 
            this.selectedTeams : Constants.DEFAULT_LEAGUES
  }

  cancelLeagues() {
    if (this.showTeams && this.isAnyLeagueSelected()) {
      this.showLeagues = false;
    }
    else {
      this.router.navigate(['/challenges']);
      this.resetAll(500);
    }
  }

  resetAll(delay?) {
    setTimeout(() => {
      this.showLeagues = true;
      this.showTeams = true;
    }, delay?delay:0);
  }

  cancelTeams() {
    if (this.showLeagues) {
      this.showTeams = false;
    }
    else {
      this.router.navigate(['/challenges']);
      this.resetAll(500);
    }
  }

  resetTeams(league) {
    this.selectedTeams[league] = [];
    this.userService.setUserPrefs({leagues: {[league]: []}}, true);
  }

  resetAllTeams() {
    Object.keys(this.selectedTeams).forEach(league => {
      this.selectedTeams[league] = [];
    });
    this.userService.setUserPrefs({leagues: this.selectedTeams}, true);
  }

  isAnyLeagueActive() {
    return this.activeLeague !== '';
  }

  isLeagueActive(league) {
    return this.activeLeague === league;
  }

  isLeagueSelected(league) {
    return league && typeof this.selectedTeams[league] !== 'undefined';
  }

  isAnyLeagueSelected() {
    return Object.keys(this.selectedTeams).length > 0;
  }

  isTeamActive(league, team) {
    return league && team && typeof this.selectedTeams[league] !== 'undefined' && this.selectedTeams[league].indexOf(team) !== -1;
  }

  loadLeague(league) {
    const lleague = league.toLowerCase()
    this.afs.collection(`${lleague}-team`, ref => ref
      .where('isActive', '==', true)
      .orderBy('location', 'asc'))
      .get().subscribe( snapshot => {
        this.sortedTeamKeys[lleague] = [];
        snapshot.docs.forEach(doc => {
          this.leagues[lleague][doc.id] = doc.data();
          this.sortedTeamKeys[lleague].push(doc.id);
        })
      });
  }

  calcPlayers(game_id) {
    var players = (Object.keys(this.teams).length || this.userBids) ? '0/8' : '-/-';
    if (this.teams[game_id] && this.userBids[game_id]) {
      players = (this.teams[game_id]['users'] || []).length + '/' + ((this.teams[game_id] || { max_users: 8 }).max_users);
    }
    else if (this.bids[game_id]) { // non team games
      players = '' + Object.keys(this.bids[game_id] || {}).length + '/' + ((this.teams[game_id] || { max_users: 8 }).max_users);
    }
    else {
      players = ((Object.keys((this.teams[game_id] || { users: {} }).users).length < (this.teams[game_id] || { max_users: 8 }).max_users) ? Object.keys((this.teams[game_id] || { users: {} }).users).length : '0') + '/' + ((this.teams[game_id] || { max_users: 8 }).max_users);
    }
    return players;
  }

  saveUserData(data) {
    this.afs.collection("user").doc(this.user.uid).get().subscribe(item => {
      if (!item.exists) {
        item.ref.set(data);
      } else {
        item.ref.update(data);
      }
    })
  }

  loadUserPrefs() {
    this.userService.loadUserPrefs().then((userPrefs:any) => {
      if ( typeof userPrefs.leagues !== 'undefined' &&
          Object.keys(userPrefs.leagues).length ) {
        this.selectedTeams = userPrefs.leagues;
      }
    });
  }

  getTimezone() {
    var parts = (new Date().toString()).match(/\(([^)]+)\)/i);
    var timezone = parts[1];
    if (timezone.search(/\W/) >= 0) {
      const timeReg = timezone.match(/\b\w/g); // Match first letter at each word boundary.
      if (timeReg) {
        timezone = timeReg.join('').toUpperCase();
        return timezone;
      } else {
        return '';
      }
    }
  }

}
