import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class EngineService {

  winners: any;
  winningBid = [];
  winningUser = [];

  constructor(private afs?: AngularFirestore, private commonService?: CommonService
              ) { }

  process2GameOver( game_id, game, bids, inviteTeams?) {
    const scoreMultiplier = (['nfl', 'nba'].includes(game.sport) ? 7 : 1);
    const scoreShift = (['nba'].includes(game.sport) ? 83 : 0);
    this.afs.collection("team/").doc(String(game_id)).get().subscribe(data => {
      let teams = data.data() || inviteTeams;
      Object.keys(teams).forEach(id => {
        let team = teams[id];
        let teamBids = this.commonService.getMapKeys(bids, team.users);
        if( !team.winners || !team.winners.length ) {
          let winners = this.calcBidWinner(game, {[game_id]: teamBids}, scoreMultiplier, scoreShift);
          if ( winners ) {
            //let winners_text = '';
            Object.keys(winners||{}).forEach(winner_uid => {
              //winners_text += users[winner_uid].name + ' placed highest bid for $' + this.winners[winner_uid] + "\n<br>";
            });
            team.winners = Object.keys(winners);
            data.ref.update( {[id]: team} );
          }
        }
      });
    });
  }

  calcWinScoreMiltiplied(goals, scoreMultiplier, scoreShift) {
    const score = (parseInt(scoreMultiplier > 1 ? Math.floor((goals - 1 - scoreShift) / scoreMultiplier) : goals));
    return  score < 0 ? 0 : score;
  }

  processGameOver(team_id, game_id, game, users, winners, bids, winningUser, winningBid, scoreMultiplier, scoreShift) {
    scoreMultiplier = scoreMultiplier || (['nfl', 'nba'].includes(game.sport) ? 7 : 1);
    this.afs.collection("team/").doc(game_id).get().subscribe(data => {
      let teams = data.data();
      Object.keys(teams).forEach(id => {
        let team = teams[id];
        if(!team.winners || !team.winners.length) {
          //console.log('game\'s winners were not registered');
          setTimeout(() => {
            if(team_id == parseInt(id) && winners) { // TODO: enable multi team support
              let winners_text = '';
              let row = Math.floor(this.calcWinScoreMiltiplied(game.liveData.linescore.teams.away.goals, scoreMultiplier, scoreShift)) > 6 ? 6 : Math.floor(this.calcWinScoreMiltiplied(game.liveData.linescore.teams.away.goals, scoreMultiplier, scoreShift));
              let col = Math.floor(this.calcWinScoreMiltiplied(game.liveData.linescore.teams.home.goals, scoreMultiplier, scoreShift)) > 6 ? 6 : Math.floor(this.calcWinScoreMiltiplied(game.liveData.linescore.teams.home.goals, scoreMultiplier, scoreShift));
              Object.keys(winners).forEach(winner_uid => {
                //console.log('winner: '+winner_uid);
                winners_text += users[winner_uid].name + ' placed highest bid for $' + winners[winner_uid] + "\n<br>";
              });
              team.users = Array.from(new Set(Object.keys(bids[game.gamePk]).concat(team.users)));
              this.calcBidWinner(game, bids, scoreMultiplier, scoreShift);
              this.calcWinningBids(game, bids, winners, winningUser, winningBid);
              team.winners = Object.keys(winners);
              data.ref.update( {[id]: team} );
              team.users.forEach(user_uid => {
                if( users[user_uid] && users[user_uid]['email']) {
                  let subject = 'Winning results for your ' + game.gameData.teams.away.name +' @ '+game.gameData.teams.home.name + ' bid on ' + game.gameData.datetime.dateTime;
                  let html = "Hello " + users[user_uid]['name'] + ','
                           + "<br><br>\n\n" + 'Winners for your ' + game.gameData.teams.away.name +' @ '+game.gameData.teams.home.name + ' bid are:'
                           + "<br><br>\n\n" + winners_text
                           + "<br><br>\n\n" + 'See your Bid details and status here https://sprtmtrx.com/bid/' + team_id + '/' + game.gamePk
                           + "<br><br>\n\n" + 'SPRT MTRX Team.';    
                  //console.log('sending email to ' + users[user_uid]['email']);
                  //this.sendEmail(users[user_uid]['email'], subject, html);
                }                          
              });  
              if(event && event.type === 'ionRefresh') {
                //event.target.complete(); // TODO: check case
              }
            }
          }, (Object.keys(users||{}).length&&Object.keys(winners||{}).length) ? 0 : 3000);
        }
        else {
          if(event && event.type === 'ionRefresh') {
            //event.target.complete(); // TODO: check case
          }                      
        }
      })
    });    

  }

  calcBidWinner(game, bids, scoreMultiplier, scoreShift) {
    if( Object.keys((game||{})).length === 0 || Object.keys((bids||{})).length === 0) {
      setTimeout(() => {
        this.calcBidWinner(game, bids, scoreMultiplier, scoreShift);
      }, 3000);
      return;
    };
    scoreMultiplier = scoreMultiplier || (['nfl', 'nba'].includes(game.sport) ? 7 : 1);
    scoreShift = scoreShift || (['nba'].includes(game.sport) ? 83 : 0);
    let linescore = game.linescore || game.liveData.linescore;
    let row = Math.floor(this.calcWinScoreMiltiplied(linescore.teams.away.goals, scoreMultiplier, scoreShift)) > 6 ? 6 : Math.floor(this.calcWinScoreMiltiplied(linescore.teams.away.goals, scoreMultiplier, scoreShift));
    let col = Math.floor(this.calcWinScoreMiltiplied(linescore.teams.home.goals, scoreMultiplier, scoreShift)) > 6 ? 6 : Math.floor(this.calcWinScoreMiltiplied(linescore.teams.home.goals, scoreMultiplier, scoreShift));
    let max_bid = 0;
    Object.keys(bids[game.gamePk]||{}).forEach(key => {
      if(bids[game.gamePk][key][row][col] > max_bid) {
        this.winners = {};
        this.winners[key] = bids[game.gamePk][key][row][col];
        max_bid = bids[game.gamePk][key][row][col];
      }
      else if(bids[game.gamePk][key][row][col] == max_bid) {
        //this.winners[key] = bids[game.gamePk][key][row][col];
      }
    });
    return this.winners;
  }

  calcWinningBids(game, bids, winners, winningUser?, winningBid?) {
    if ( Object.keys((game||{})).length === 0 || Object.keys((bids||{})).length === 0) {
      setTimeout(() => {
        this.calcWinningBids(game, bids, winners, winningUser, winningBid);
      }, 1000);
      return;
    }
    Object.keys(bids[game.gamePk] || {}).forEach(user_uid => {
      let bid = Object.values(bids[game.gamePk][user_uid]);
      for (let row=0; row<bid.length;row++) {
        winningBid[row] = winningBid[row] || [0, 0, 0, 0, 0, 0, 0];
        winningUser[row] = winningUser[row] || ['','','','','','',''];
        for (let col=0; col<Object.values(bid[row]).length;col++) {
          if (winningBid[row][col] < parseFloat(bid[row][col])) {
            winningBid[row][col] = parseFloat(bid[row][col]);
            winningUser[row][col] = user_uid;
          }
        }
      }
      this.winningBid = winningBid;
      this.winningUser = winningUser;
    });
    return;
  }

  calcUserWinningBids(grid, game, users, winners, user_uid, winningUser, winningBid, include_amount?, teams?) {
    let count = 0;
    let amount = 0;
    winningUser.forEach((row, i) => {
      row.forEach((uid, y) => {
        if(uid === user_uid) {
          count++;
          amount += winningBid[i][y];
        }
      })
    })
    if(!include_amount) {
      return count;
    }
    else {
      if(!winningUser.length || !users || !Object.keys(users).length || (game.gameData&&game.gameData.status.statusCode == 1)) {
        return 'Stats not Available';
      }
      else if(user_uid === 'winning') {
        if(['6','7'].includes(game.gameData.status.statusCode)) {
          let linescore = game.linescore || game.liveData.linescore;
          let winning_stats = '';
          Object.keys(teams[grid.team_id].winners).forEach(winner_id => {
            let winner_uid = teams[grid.team_id].winners[winner_id];
            if( winner_uid ) {
              winning_stats += (users[winner_uid].name) + '(' + (users[winner_uid].publicName||'XXX') + ') won for ('+linescore.teams.away.goals + '-' + linescore.teams.home.goals+') score' + "\n";
            }
            else {
              winning_stats += 'Info is not available' + "\n";
            }
          });
          return winning_stats;
        }
        else {
          return 'Stats not Available';
        }
      }
      else {
        if(!users[user_uid]) {
          return 'Stats not Available';
        }
        else {
          return ((users[user_uid]||{name: 'User'}).name) + '(' + ((users[user_uid]||{publicName: 'XXX'}).publicName) + '): ' +  count + ' winning bids for $' + amount.toString().substr(0,4);
        }
      }
    }
  }

  getTeamDetails(team, users, object?) {
    let details = '';
    let winners = [];
    if ( team && team.length ) {
      team.forEach(user_uid => {
        if ( users[user_uid] ) {
          if ( object ) {
            winners.push(users[user_uid]);
          } else {
            details += (users[user_uid] || {name: 'Anon'}).name + '(' + user_uid + ')' + "\n";
          }
        }
      });
    }
    return object ? winners : details;
  }

  calcCurrentWinner(game, teams, bids, scoreMultiplier, scoreShift) {
    scoreMultiplier = (['nfl', 'nba'].includes(game.sport) ? 7 : 1);
    scoreShift = (['nba'].includes(game.sport) ? 83 : 0);
    if ( typeof game === 'undefined' || !teams || !bids || (!game.linescore && !game.liveData) ) return [];
    let linescore = game.linescore || game.liveData.linescore;
    let row = Math.floor(this.calcWinScoreMiltiplied(linescore.teams.away.goals, scoreMultiplier, scoreShift)) > 6 ? 6 : Math.floor(this.calcWinScoreMiltiplied(linescore.teams.away.goals, scoreMultiplier, scoreShift));
    let col = Math.floor(this.calcWinScoreMiltiplied(linescore.teams.home.goals, scoreMultiplier, scoreShift)) > 6 ? 6 : Math.floor(this.calcWinScoreMiltiplied(linescore.teams.home.goals, scoreMultiplier, scoreShift));
    if ( (row === col && row < 6 && col < 6 && scoreMultiplier <= 1)
          || (scoreMultiplier <= 1 && row === 0 && col === 0)
          || (scoreMultiplier > 1 && parseInt(linescore.teams.away.goals) === 0 && parseInt(linescore.teams.home.goals) === 0) ) return [];
    let winners = [];
    let winningBids = []
    Object.keys(teams).forEach(team_id => {
      if ( teams[team_id] ) {
        teams[team_id].users.forEach(user_uid => {
          if ( typeof bids[user_uid] !== 'undefined' &&
            (typeof winners[team_id] === 'undefined' || bids[winners[team_id]][row][col] < bids[user_uid][row][col])
            && typeof bids[user_uid] !== 'undefined'
            ) {
              winners[team_id] = user_uid;
              winningBids[team_id] = bids[user_uid][row][col];
            }
        });
      }
    });
    return winners;
  }

  calcMaxUsers(users, isReversed?: boolean) {
    let remainders = [ users.length % 5, users.length % 6, users.length % 7, users.length % 8];
    return isReversed ? (remainders.lastIndexOf(Math.min(...remainders)) + 5) : (remainders.indexOf(Math.min(...remainders)) + 5);
  }

  shuffleTeams(teams, allowedUsers?: Array<string>, isReversed?: boolean) {
    let max_users = 8;
    let users = [];
    Object.keys(teams || {}).map( id => { // Concat multiple teams if any
      max_users = teams[id].max_users;
      users = users.concat(teams[id].users);
    });
    if ( allowedUsers ) { // Filter users with incomplete bids
      users = users.filter(value => allowedUsers.includes(value));
    }
    max_users = this.calcMaxUsers(users, isReversed); // Define user count in a team
    let teams_count = Math.floor( users.length / max_users ); // Define number of teams
    if ( users.length > max_users * teams_count ) { // exclude excess users
      users = users.slice(0, max_users * teams_count);
    }
    let orig_users =  [...users]; // preserve original user bids order
    if ( teams_count > 0 ) {
      let tmp_teams = [];
      let new_teams = {};
      if ( true ) { // new shuffle logic
        users = this.commonService.shuffle(users);
        for(let k=0; k<teams_count; k++) { // slice users into teams
          tmp_teams[ k ] = users.slice(k * max_users, (k + 1) * max_users);
        }
        tmp_teams.forEach((team, team_id) => { // recover original order
          new_teams[team_id] = {bid_amount: 10, id: 0, max_users: 8, users: [], winners: []};
          new_teams[team_id].max_users = max_users;
          new_teams[team_id].id = team_id;
          new_teams[team_id].users = orig_users.filter(value => team.includes(value));
          new_teams[team_id].locked = true;
        });
      }
      return new_teams;
    } else {
      return teams;
    }
  }

}
