import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { InvitePageRoutingModule } from './invite-routing.module';
import { InvitePage } from './invite.page';
import { UserModule } from '../components/user/user.module';
import { DirectivesModule } from '../directives/directives.module';
import { SearchServiceModule } from '../services/search/search.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvitePageRoutingModule,
    UserModule,
    DirectivesModule,
    SearchServiceModule
  ],
  declarations: [InvitePage]
})
export class InvitePageModule {}
