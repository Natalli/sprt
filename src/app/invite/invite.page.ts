import { Component, OnInit, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { IonSlides, IonItemSliding, Platform, LoadingController, AlertController, ModalController, IonContent, IonIcon } from '@ionic/angular';
import { Invite } from '../model/invite';
import { DatePipe } from '@angular/common';
import { CommonService } from '../common.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { UserService } from '../service/user.service';
import { InviteService } from '../service/invite.service';
import { Constants } from '../constants';
import { LayoutService } from '../services/layout.service';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { ModalService } from '../service/modal.service';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.page.html',
  styleUrls: ['./invite.page.scss'],
})
export class InvitePage implements OnInit {

  @ViewChild(IonItemSliding) slidingItem: IonItemSliding;
  @ViewChild('slider', { static: true }) slides: IonSlides;
  @ViewChild(IonContent) content: IonContent;
  @ViewChildren(IonIcon, { read: ElementRef }) icons: QueryList<ElementRef>;
  slideOpts = {
    initialSlide: 0
  };


  date = Date;
  objectKeys = Object.keys;
  constants = Constants;
  today: any;
  games = [];
  challenge_id: any;
  challenge: any;
  selectInvite: any;
  invite: Invite = new Invite();
  dayCount = 5;
  user: any;
  eventCollapsed = [];
  eventEdited = [];
  search: any;
  searchUsers = {};
  friends = [];
  searchFriendsUsers = {};
  users = {};
  inviteText: string;
  game: any;
  currentSlide: number;
  customInviteText: string;
  defaultInviteName: string;
  sport = 'na';

  constructor(private datePipe: DatePipe,
    public commonService: CommonService,
    public router: Router,
    public platform: Platform,
    public loadingController: LoadingController,
    private afs: AngularFirestore,
    public userService: UserService,
    public inviteService: InviteService,
    public alertController: AlertController,
    public modalController: ModalController,
    protected layoutService: LayoutService,
    private fns: AngularFireFunctions,
    protected modalService: ModalService
  ) {
    this.userService.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.loadUsers([user.uid]);
        this.invite = new Invite(user.uid);
        this.updateInvite();
      }
    });
    this.sport = this.router.url.split('/')[this.router.url.split('/').length - 1];
    this.loadGames();
  }

  ngOnInit() {
    this.lockSwipes();
  }

  ngAfterViewInit() {
    this.layoutService.fixIconEdge(this.icons);
  }

  async loadGames() {
    console.log('loadGames');
    const startDate = new Date((new Date()).getTime() - 0 * 3600 * 24 * 1000);
    const endDate = new Date((new Date()).getTime() + 30 * 3600 * 24 * 1000);
    this.afs.collection(this.sport, ref => ref
      .where('date', '>=', this.datePipe.transform(startDate, 'yyyy-MM-dd'))
      .where('date', '<=', this.datePipe.transform(endDate, 'yyyy-MM-dd')))
      .valueChanges().subscribe((games: Array<any>) => {
        this.games = games;
      });
  }

  getGames() {
    return (this.games || []).slice(0, this.dayCount);
  }

  addGame(gamePk, game?) {
    this.invite.games = [String(gamePk)]; // force single game Invite
    if (this.invite.games.length > 0) {
      this.slides.lockSwipes(false);
    }
    setTimeout(() => {
      this.slides.slideTo(1);
    }, 70);
    this.game = game;
    this.updateInvite();
  }

  lockSwipes() {
    this.slides.lockSwipes(true);
  }

  slideChanged() {
    this.slides.getActiveIndex().then(index => {
      this.currentSlide = index;
      if (this.invite.games.length && this.currentSlide === 0) {
        this.slides.lockSwipeToNext(false);
      }
      if (this.invite.games.length && this.currentSlide === 1) {
        this.slides.lockSwipeToPrev(false);
        this.slides.lockSwipeToNext(true);
      }
      if (this.invite.games.length > 0 && this.invite.users.length > 1 && this.currentSlide === 1) {
        this.slides.lockSwipeToNext(false)
      }
      const content = document.querySelector('.slide');
      if (content) {
        content.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
    });
  }

  isGameAdded(gamePk: string) {
    return this.user && this.invite.games.indexOf(String(gamePk)) !== -1;
  }

  isGameAdmin(date, inviteId) {
    return true;
  }

  isUserAdmin(userUid: string) {
    return this.invite && this.invite.admins.indexOf(userUid) !== -1;
  }

  updateInvite() {
    if (this.game) {
      const sport =  this.sport ? this.sport : 'NHL';
      this.inviteText = 'Hey, ' + this.user.displayName + ' wants you to play in a $' + this.invite.amount + ', winner takes all, SPRT MTRX ' + sport.toUpperCase() + ' game';
      this.defaultInviteName = (this.game.teams.away.team.name + ' vs ' + this.game.teams.home.team.name);
      this.invite.text = '$' + this.invite.amount;
      if (this.invite.games && this.invite.games.length && this.getGameDetails(this.invite.games[0])) {
        this.inviteText += ', ' + this.game.teams.away.team.name + ' vs ' + this.game.teams.home.team.name + '.';
        this.invite.start = new Date(this.game.gameDate);
      }
    }
  }

  getGameDetails(gamePk: string) { // placeholder for the single game setup
    return this.game;
  }

  sendInvite() {
    this.modalService.presentLoading();
    const inviteId = this.generateInviteId(this.game.gameDate);
    this.invite.name = this.invite.name || this.defaultInviteName;
    this.invite['sport'] = this.sport;
    this.afs.collection('invite/').doc(inviteId).set(Object.assign({}, this.invite));
    this.afs.collection('/notification/').add({
      title: 'Game Invite',
      text: "invited you to join a Game.\n" + (this.customInviteText || this.inviteText),
      platforms: ['ios', 'android', 'web'],
      start: new Date(),
      scheduling: 'Now',
      receivers: this.invite.users.filter(val => val !== this.user.uid),
      created: new Date(),
      friendRequest: false,
      sendRequest: true,
      inviteRequest: inviteId,
      sender: this.user.uid,
    })
      .then((docRef) => {
        this.commonService.sendEmail(this.users[this.user.uid].email, 'Private Game Created', this.users[this.user.uid].name + ",\n\n You've created a Private Game.\n\n\"" + (this.customInviteText || this.inviteText) + ("\"\n\n"+'http://sprtmtrx.com/detail/0/' + this.game.gamePk + '/invite/' + inviteId) );
        this.invite.users.filter(val => val !== this.user.uid).forEach(uid => {
          if (this.users[uid]) {
            if (this.users[uid].tokens) {
              const callable = this.fns.httpsCallable('sendNativeNotification');
              Object.values(this.users[uid].tokens).forEach(token => {
                callable({ tokens: token, title: 'Game Invite', body: this.users[this.user.uid].name + " invited you to join a Game.\n" + (this.customInviteText || this.inviteText) })
                  .toPromise().then((resp) => {
                    //console.log(resp);
                  });
              });
            }
          }
        });
        this.loadingController.dismiss();
        this.resetInvite();
        this.router.navigate(['/detail/0/' + this.game.gamePk + '/invite/' + inviteId]);
      });
  }

  resetInvite() {
    this.invite = new Invite(this.user.uid);
    this.searchUsers = {};
    this.inviteText = '';
    this.currentSlide = 0;
    this.customInviteText = '';
    this.defaultInviteName = '';
    setTimeout(() => {
      this.slides.slideTo(0);
    }, 1000);
  }

  generateInviteId(date, uid?) {
    return new Date(date).toJSON().substr(0, 10).replace(/-/g, '') + '-' + this.afs.createId();
  }

  searchUser(ev) {
    this.search = (ev.target.value.length > 1) ? ev.target.value : '';
    if (this.search.length > 1) { // TODO: implement case insensetive
      this.afs.collection('user/', ref => ref
        .orderBy('keywords')
        .startAt(this.search.toLowerCase())
        .endAt(this.search.toLowerCase() + "\uf8ff")
      ).valueChanges({ idField: 'id' }).subscribe(data => {
        if (data.length) {
          this.searchUsers = this.filterUsers(data, this.invite.users);
          if (false) this.friends.forEach(friend => {
            let a = Object.values(this.searchUsers).map(user => {
              user['id'] === friend.uid ? user : null
            })
          })
        } else { // search without capitalization
          this.afs.collection('user/', ref => ref
            .orderBy('keywords')
            .startAt(this.search)
            .endAt(this.search + "\uf8ff")
          ).valueChanges({ idField: 'id' }).subscribe(data => {
            this.searchUsers = this.filterUsers(data, this.invite.users);
          });
        }
      });
    } else {
      if (this.friends.length) {
        this.searchUsers = this.searchFriendsUsers;
      } else {
        this.searchUsers = {}
      }
    }
  }

  loadUsers(users) {
    if (users) {
      users.forEach(user_uid => {
        this.afs.doc('/user/' + user_uid).valueChanges().subscribe(user => {
          this.users[user_uid] = user;
          this.users[user_uid].uid = user_uid;
        });
      });
    }
  }

  deleteUser(userUid: string) {
    if (this.invite.users.indexOf(userUid) !== -1) {
      this.invite.users.splice(this.invite.users.indexOf(userUid), 1);
    }
  }

  async addUser(userUid: string, userData?) {
    this.users[userUid] = this.users[userUid] || userData;
    this.invite.users = this.invite.users || [this.user.uid];
    this.invite.users.push(userUid);
    if(this.invite.users.length > 0) {
      this.slides.lockSwipes(false);
    }
    return;
  }

  capitalize(text) {
    return text.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
  }

  filterUsers(users, excluded) {
    if (users && excluded) {
      return users.filter(user => excluded.indexOf(user.id) === -1);
    }
    return users;
  }

  async betInfoAlert() {
    const alert = await this.alertController.create({
      header: 'Your Bet',
      message: '"Your Bet" is different from "Your Bid". You will always be given 10 SPRT MTRX dollars to place your bids.',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }
      ]
    });

    await alert.present();
  }

}
