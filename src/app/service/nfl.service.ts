import { Injectable } from '@angular/core';
import { NflEvent } from '../model/nflEvent';

@Injectable({
  providedIn: 'root'
})
export class NflService {
  nflEvent: NflEvent;

  constructor() {
    
  }


  initEvent() {
    this.nflEvent = new NflEvent();
    return this.nflEvent;
  }

  processEspnEvent(event) {
    const game:any = this.generateGame(new Date(event.date), event.id, event.season, event.status,
                                  event.competitions[0].competitors.find(team => { return team.homeAway === 'away'}),
                                  event.competitions[0].competitors.find(team => { return team.homeAway === 'home'}),
                                  event.competitions[0], event.competitions[0].venue);

    const live:any = this.generateLive(new Date(event.date), event.id, event.season, event.status,
                                  event.competitions[0].competitors.find(team => { return team.homeAway === 'away'}),
                                  event.competitions[0].competitors.find(team => { return team.homeAway === 'home'}),
                                  event.competitions[0], event.competitions[0].venue);

    return {game: game, live: live};
  }

  generateLive(startDate: Date, gamePk: String, season: any, status: any, 
                awayTeam: any, homeTeam: any, linescore: any, venue: any) {
    let live = {
      "copyright" : "© NFL 2020. All Rights Reserved.",
      "gamePk" : gamePk,
      "sport" : 'nfl',
      "link" : "/api/v1/game/"+gamePk+"/feed/live",
      "metaData" : {  },
      "gameData" : {
        "game" : {
          "pk" : gamePk,
          "season" : season.year,
          "type" : "R"
        },
        "datetime" : {
          "dateTime" : startDate.toISOString()
        },
        "status" : this.getStatus(status),
        "teams" : {
          "away" : this.getLiveTeam(awayTeam),
          "home" : this.getLiveTeam(homeTeam)
        },
        "players" : {  },
        "venue" : this.getVenue(venue)
      },
      "liveData" : {
        "plays" : {
          "allPlays" : [  ],
          "scoringPlays" : [ ],
          "penaltyPlays" : [ ],
          "playsByPeriod" : [  ],
          "currentPlay" : {
            "result" : {  },
            "about" : {
              "period" : status.period,
              "periodTimeRemaining" : ( status.displayClock && status.displayClock !== '0:00') ? status.displayClock : '',
              // all below is not used
              "eventIdx" : 0,
              "eventId" : 1,
              "periodType" : "REGULAR",
              "ordinalNum" : "1st",
              "periodTime" : "00:00",
              "dateTime" : "2020-09-10T16:04:40Z",
              "goals" : {
                "away" : 0,
                "home" : 0
              }
            },
            "coordinates" : { }
          }
        },
        "linescore" : this.getLinescore(linescore, status, this.getTeam(homeTeam), this.getTeam(awayTeam)),
        //"boxscore" : {  },
        //"decisions" : {  }
      }
    };
    return live;
  }

  generateGame(startDate: Date, gamePk: String, season: any, status: any, 
                awayTeam: any, homeTeam: any, linescore: any, venue: any) {
    let game = {
      "gamePk" : gamePk, //2019030312
      "sport" : 'nfl',
      "link" : "summary?event="+gamePk, //"/api/v1/game/2019030312/feed/live"
      "gameType" : "R", // not used, season.type into (R=Regular; A=All Star; PR=Preseason; P=Playoff)
      "season" : season.year, //"20192020"
      "gameDate" : startDate.toISOString(), //"2020-09-10T00:00:00Z"
      "status" : this.getStatus(status),
      "teams" : {
          "away" : this.getTeam(awayTeam),
          "home" : this.getTeam(homeTeam)
      },
      "linescore" : this.getLinescore(linescore, status, this.getTeam(homeTeam), this.getTeam(awayTeam)),
      "venue" : this.getVenue(venue),
      "content" : {
          "link" : "/api/v1/game/"+gamePk+"/content" // TODO: MAP VALUE
      }
    };
    return game;
  }

  getLinescore(espnLinescore, status, homeTeam, awayTeam) {
    let linescore = {
      "currentPeriod" : status.period,
      "currentPeriodTimeRemaining" : ( status.displayClock && status.displayClock !== '0:00') ? status.displayClock : '',
      "periods" : [ ],
      "shootoutInfo" : { // not used
        "away" : {
          "scores" : 0,
          "attempts" : 0
        },
        "home" : {
          "scores" : 0,
          "attempts" : 0
        }
      },
      "teams" : {
        "home" : {
          "team" : homeTeam.team,
          "goals" : homeTeam.score || 0,
          "shotsOnGoal" : 0,
          "goaliePulled" : false,
          "numSkaters" : 0,
          "powerPlay" : false
        },
        "away" : {
          "team" : awayTeam.team,
          "goals" : awayTeam.score || 0,
          "shotsOnGoal" : 0,
          "goaliePulled" : false,
          "numSkaters" : 0,
          "powerPlay" : false
        }
      }, // all below not used
      "powerPlayStrength" : "Even",
      "hasShootout" : false,
      "intermissionInfo" : {
        "intermissionTimeRemaining" : 0,
        "intermissionTimeElapsed" : 0,
        "inIntermission" : false
      }
    };
    return linescore;
  }

  getStatus(espnStatus) {
    // TODO: CHECK LIVE MAP VALUES
    const mappedStatus = this.mapStatusType(espnStatus.type);
    const status = {
      "abstractGameState" : mappedStatus.state, // "Preview"
      "codedGameState" : mappedStatus.code, // "1"
      "detailedState" : mappedStatus.detail, // "Scheduled"
      "statusCode" : mappedStatus.code, // "1"
      "startTimeTBD" : false
    };
    return status;
  }

  mapStatusType(espnStatusType) {
    const statuses = [{code: '2', state: espnStatusType.description, detail: espnStatusType.description}];
    statuses[1] = {code: '1', state: 'Preview', detail: 'Scheduled'};
    statuses[2] = {code: '2', state: 'Live', detail: 'In Progress'};
    //statuses[22] = {code: '2', state: 'Live', detail: 'In Progress'};
    //statuses[23] = {code: '2', state: 'Live', detail: 'In Progress'};
    /*
      completed: true
      description: "Final"
      detail: "Final"
      id: "3"
      name: "STATUS_FINAL"
      shortDetail: "Final"
      state: "post"
      //
      completed: false
      description: "Halftime"
      detail: "Halftime"
      id: "23"
      name: "STATUS_HALFTIME"
      shortDetail: "Halftime"
      state: "in"
    */
    statuses[3] = {code: '7', state: 'Final', detail: 'Final'};
    return statuses[espnStatusType.id] || statuses[0];
  }

  getLiveTeam(espnTeam) {
    let team = {
      "id" : espnTeam.team.id,
      "name" : espnTeam.team.displayName,
      "link" : "/api/v1/teams/" + espnTeam.team.id,
      "venue" : { "id": espnTeam.team.venue?.id||0 },
      "abbreviation" : espnTeam.team.abbreviation,
      "triCode" : espnTeam.team.abbreviation+"X",
      "teamName" : espnTeam.team.name || '',
      "locationName" : espnTeam.team.location,
      "firstYearOfPlay" : "0000",
      "division" : {  },
      "conference" : {  },
      "franchise" : {  },
      "shortName" : espnTeam.team.shortDisplayName,
      "officialSiteUrl" : "",
      "franchiseId" : 0,
      "active" : espnTeam.team.isActive
    };
    return team;
  }

  getTeam(espnTeam) {
    const records = espnTeam.records?.find(record => { return record.type === 'total'}).summary.split('-');
    let team = {
      "leagueRecord" : {
        "wins" : records ? records[0] : 0,
        "losses" : records ? records[1] : 0,
        "ot" : 0,
        "type" : "league"
      },
      "score" : espnTeam.score || 0,
      "team" : {
        "id" : espnTeam.team.id,
        "name" : espnTeam.team.displayName,
        "link" : "/api/v1/teams/" + espnTeam.team.id
      }
    };
    return team;
  }

  getVenue(espnVenue) {
    let venue = {
      "id" : espnVenue.id,
      "name" : espnVenue.fullName,
      "link" : "/api/v1/venues/" + espnVenue.id
    };
    return venue;
  }

}
