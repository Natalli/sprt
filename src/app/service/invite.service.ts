import { Injectable } from '@angular/core';
import { Invite } from '../model/invite';

@Injectable({
  providedIn: 'root'
})
export class InviteService {

  invite: Invite;

  constructor() {
    this.invite = {disabled: false, games: [], name: '', amount: 10, start: new Date(), text: '', admins: [], users: [], winners: []};

  }

  startDate(start) {
    if ( start && start.seconds) {
        return (new Date(start.seconds*1000)).toJSON().substr(0,10).replace(/-/g,'');
    } else {
        return start.replace(/-/g,'');
    }
}

}
