import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public users = {};

  constructor( private afs: AngularFirestore ) { }

  load(uids) {
    return new Promise((resolve, reject) => {
      if ( uids && uids.length ) {
        const absent = uids.filter(uid => !this.users[uid]);
        if ( absent.length ) {
          absent.forEach(uid => {
            this.users[uid] = {};
            this.afs.doc('/user/' + uid).valueChanges().subscribe(user => {
              this.users[uid] = user || {};
              this.users[uid].uid = uid;
              if (!this.users[uid]['publicName']) {
                if (this.users[uid]['name']) {
                  this.users[uid]['publicName'] = this.users[uid]['name'].slice(0, 3).toUpperCase();
                }
                else {
                  this.users[uid]['publicName'] = 'ANON';
                }
              }
              if ( uid === absent[absent.length-1] ) {
                resolve(this.users);
              }
            });
          });
        }
        else {
          resolve(this.users);
        }
      }
      else if ( uids === null ) {
        this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(users => {
          users.forEach(user => {
              this.users[user.id] = user;
              if (!this.users[user.id]['publicName']) {
                  if (this.users[user.id]['name']) {
                      this.users[user.id]['publicName'] = this.users[user.id]['name'].slice(0, 3).toUpperCase();
                  } else {
                      this.users[user.id]['publicName'] = 'ANON';
                  }
              }
          });
          resolve(this.users);
        });
      }
    });
  }

}
