import { Injectable } from '@angular/core';
import { LoadingController, PopoverController } from '@ionic/angular';
import { ModalErrorComponent } from '../popups/new/modal-error/modal-error.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
 
  constructor(
    private popoverCtrl: PopoverController,
    protected loadingController: LoadingController
  ) {

  }

  async presentLoading(text?) {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: text || 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async errorModal(title, text) {
    const popover = await this.popoverCtrl.create({
      component: ModalErrorComponent,
      componentProps: {
        title,
        text,
        cssClass: 'modal-error'
      },
      cssClass: 'modal-wrap-default'
    });
    await popover.present();
    return popover.onDidDismiss();
  }

  async confirmModal(title, text) {
    const popover = await this.popoverCtrl.create({
      component: ModalErrorComponent,
      componentProps: {
        title,
        text
      },
      cssClass: 'modal-wrap-default'
    });
    await popover.present();
    return popover.onDidDismiss();
  }

}
