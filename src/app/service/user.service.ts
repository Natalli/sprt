import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import firebase from "firebase/compat/app";
import 'firebase/compat/firestore';
import { ISettings } from '../interfaces/settings';
import *  as deepEqual from 'fast-deep-equal';
import { take } from 'rxjs/operators';
import { getAuth, onAuthStateChanged, sendPasswordResetEmail, signOut, signInWithCustomToken } from 'firebase/auth';
import { SubjectService } from './subject.service';
import { LoadingController, PopoverController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalErrorComponent } from '../popups/new/modal-error/modal-error.component';
import { ModalService } from './modal.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends SubjectService {

  protected inner: {
    user?: any;
    userData?: any;
    prefs?: any;
    settings?: ISettings;
    token?: string
  } = {};
  auth;
  protected subjects: any = {};
  constructor(
    private afs: AngularFirestore,
    private toastController: ToastController,
    private router: Router,
    private popoverCtrl: PopoverController,
    protected loadingController: LoadingController,
    protected modalService: ModalService
  ) {
    super();
    this.inner.settings = {
      privateBids: false,
      challengeMissingBids: true,
      challengeStarted: true,
      gameLeader: false,
      gameWinner: true,
      challengeLeader: false,
      challengeWinner: true
    };
  }

  public get user$(): Observable<any> {
    return this.getSubject$('user');
  }

  public get user() {
    return this.getSubject('user').value;
  }

  public set setUser(value: any) {
    this.inner.user = value;
    this.getSubject('user').next(value);
  }

  get userData() {
    return this.inner.userData;
  }

  get userPrefs() {
    return this.inner.prefs;
  }

  public get userPrefs$(): Observable<any> {
    return this.getSubject$('userPrefs');
  }

  get uid() {
    return this.inner.user.uid;
  }

  set uid(uid: string) {
    this.afs.collection('user').doc(uid).valueChanges().subscribe(user => {
      this.inner.user = user;
      this.inner.user.uid = uid;
    })
  }

  get settings() {
    return this.inner.userData?.settings ? this.inner.userData?.settings : this.inner.settings;
  } 

  getAuth() {
    this.auth = getAuth();
    onAuthStateChanged(this.auth, (user) => {
      console.log("User", user)
      if (user) {
        this.setUser = user; 
        this.inner.token = user['accessToken'];
        localStorage.setItem('token', this.inner.token);
      } else {
        this.inner.token = null;
        this.router.navigate(['login'])
      }
      if (this.inner.user) {
        this.getUser();
      }
    })
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem('token')) {
      return true;
    }
  }

  getUser$(uid: string): Observable<any> {
    return this.afs.collection('user').doc(uid).valueChanges();
  }

  getUser() {
    this.afs.collection('user').doc(this.inner.user.uid).valueChanges().subscribe(user => {
      this.inner.userData = user;
    })
  }

  getTeams(teams: Array<string>): Observable<any> {
    return this.afs.collection('/team/', ref => ref
      .where(firebase.firestore.FieldPath.documentId(), 'in', teams)).get();
  }

  get gamesCount() {
    return Object.keys(this.inner.user['games'] || []).length;
  }

  loadUserPrefs() {
    return new Promise((resolve, reject) => {
      if (typeof this.inner.prefs === 'undefined') {
        let prefs = {};
        this.afs.collection('/user/' + this.inner.user.uid + '/prefs').valueChanges({ idField: 'id' }).subscribe(docs => {
          docs.forEach(doc => {
            if (['leagues', 'user'].includes(doc.id)) {
              prefs[doc.id] = doc;
              delete prefs[doc.id].id;
            }
          });
          this.inner.prefs = prefs||{};
          this.getSubject('userPrefs').next(prefs);
          resolve(prefs)
        })
      }
      else {
        resolve(this.inner.prefs);
      }
    })
  }

  setUserPrefs(data, merge) {
    if ( data ) {
      Object.keys(data).forEach(key => {
        this.afs.doc('/user/' + this.inner.user.uid + '/prefs/' + key).set(data[key], {merge});
      });
    }
  }

  calcStats(user_uid, enforce?, admin?) {
    if (!enforce) return; // TODO: this functions freezes app, do not activate until it's fixed
    console.log('recalculating stats', user_uid);
    this.getUser$(user_uid).pipe(take(1)).subscribe(user => {
      if (user && user.games) {
        const userGames = Object.keys(user.games);
        user.stats = user.stats || { challengesCount: 0, challengesWon: 0, gamesCount: 0, gamesWon: 0, challengesPrizes: 0 };
        let challengeGames = {};
        let challenges = [];
        this.afs.collection("challenge", ref => ref
          .where('start', '<', new Date(Date.now() - (admin ? 0 : 24) * 60 * 60000)) // Delay Stats update for non admins
          .where('disabled', '==', false)).get().subscribe(data => {
            data.forEach(ref => {
              challenges.push({ ...ref.data() as object, ...{ id: ref.id } });
            });
            var sports = [...challenges.map(v => v['sport'])];
            var chSports = sports.reduce((m, o) => (m[o] = challenges.filter(v => v['sport'] === o), m), {});
            Object.keys(chSports).forEach(sport => {
              challengeGames[sport] = [].concat.apply([], chSports[sport].map((challenge: any) => {
                return userGames.filter(val => challenge.games.includes(val))
              }));
              if (!challengeGames[sport].length) {
                delete chSports[sport];
                delete challengeGames[sport];
              }
            })

            let gamesWonBySport = {} // {nhl: number, nfl: number}
            // let gamesWon = 0;
            let challengesCountBySportWon = 0;
            let challengesWonBySport = {};
            let chCBySp = 0;
            let challengesCountBySport = {};
            let challengePksBySport = {};

            let challengeUsersAll = {};
            let challengesPrizes = { nba: {} };
            let stats = {};

            Object.keys(chSports).forEach((sport, index) => {
              let countGWon = 0;
              let challengePks = [];
              let challengeUsers = {};
              for (let i = 0; i * 10 < challengeGames[sport].length; i++) {

                this.getTeams(challengeGames[sport].slice(i * 10, i * 10 + 10)).subscribe(resp => {
                  resp.docs.forEach(teamsRef => {
                    const teams = teamsRef.data();
                    Object.values(teams || {}).forEach((team: any, idx: number) => {
                      if (!team.winners) {
                        // console.log('!!!!!! STATS CALC PROBLEM with game ', teamsRef.id, ' team ', idx);
                        team.winners = [];
                      }
                      const challengePlayed = chSports[sport].find((challenge: any) => challenge.games.includes(teamsRef.id));
                      challengeUsers[challengePlayed.id] = [...(challengeUsers[challengePlayed.id] || []), ...team.winners];
                      challengeUsersAll[sport] = challengeUsers;
                      if (team.winners && team.winners.includes(user_uid)) {
                        countGWon++;
                        gamesWonBySport[sport] = countGWon;
                      }
                      if (team.users && team.users.includes(user_uid)) {
                        challengePks.push(challengePlayed.id)
                      }
                    });
                  });
                  if (i * 10 + 10 >= challengeGames[sport].length) {
                    challengesCountBySportWon = 0;
                    let winnersCount = {};
                    let winnersCountReversed = {};
                    Object.keys(challengeUsers).forEach((challenge_id) => {
                      winnersCount[challenge_id] = {};
                      challengeUsers[challenge_id].forEach(user_uid => winnersCount[challenge_id][user_uid] = (winnersCount[challenge_id][user_uid] || 0) + 1);
                      winnersCountReversed[challenge_id] = winnersCountReversed[challenge_id] || [];
                      Object.keys(winnersCount[challenge_id]).forEach(user_uid => {
                        winnersCountReversed[challenge_id][winnersCount[challenge_id][user_uid]] = winnersCountReversed[challenge_id][winnersCount[challenge_id][user_uid]] || [];
                        winnersCountReversed[challenge_id][winnersCount[challenge_id][user_uid]].push(user_uid);
                      });
                      if (winnersCountReversed[challenge_id].length &&
                        winnersCountReversed[challenge_id][winnersCountReversed[challenge_id].length - 1].indexOf(user_uid) !== -1) {
                        if (sport === 'nba') {
                          const nbaChallenge: any = challenges.find((challenge: any) => challenge.id === challenge_id);
                          if (nbaChallenge.games.length === winnersCountReversed[challenge_id].length - 1) { // Won all NBA Challenge games
                            challengesCountBySportWon++;
                            // Refactor!
                            challengesPrizes['nba'] = challengesPrizes['nba'] || {};
                            challengesPrizes['nba'][nbaChallenge.id] = Math.round((nbaChallenge.amount || 100) / winnersCountReversed[challenge_id][winnersCountReversed[challenge_id].length - 1].length);
                          }
                        }
                        else {
                          challengesCountBySportWon++;
                          // TODO: Refactor and do not rely on challenge.winners property
                          const challenge: any = challenges.find((challenge: any) => challenge.id === challenge_id);
                          challengesPrizes[sport] = challengesPrizes[sport] || {};
                          challengesPrizes[sport][challenge_id] = Math.round((challenge.amount || 100) / winnersCountReversed[challenge_id][winnersCountReversed[challenge_id].length - 1].length);
                        }
                      }
                    });
                    chCBySp = challengePks.filter((item, index) => challengePks.indexOf(item) === index).length;
                    let challengeWinners = challenges.filter(challenge => challenge['winners'] && challenge['sport'] !== 'nba' ? challenge : null)
                    challengeWinners.forEach((challenge: any) => {
                      let winners = challenge['winners'];
                      winners.forEach(win => {
                        if (Object.keys(win)[0] === user_uid) {
                          challengesPrizes[challenge.sport || 'nhl'] = challengesPrizes[challenge.sport || 'nhl'] || {};
                          challengesPrizes[challenge.sport || 'nhl'][challenge.id] = Object.values(win)[0] || Math.round((challenge['amount'] || 100) / winners.length);
                        }
                      })
                    })

                    // challengesCountBySport
                    challengesCountBySport[sport] = chCBySp;
                    if (!user.stats.challengesCountBySport ||
                      user.stats.challengesCountBySport[sport] !== challengesCountBySport[sport]) {
                      stats['stats.challengesCountBySport'] = challengesCountBySport;
                    }

                    //challengesCount
                    let challengesCount = Object.values(challengesCountBySport).reduce((acc: number, val: number) => {
                      return acc + val || 0;
                    }, 0);
                    // console.log("🚀 ~ file: user.service.ts ~ line 204 ~ UserService ~ challengesCount ~ challengesCount", challengesCount)
                    if (index === Object.keys(chSports).length - 1 && user.stats.challengesCount !== challengesCount) {
                      stats['stats.challengesCount'] = challengesCount;
                    }

                    // challengesWonBySport
                    challengesWonBySport[sport] = challengesCountBySportWon;
                    if (!user.stats.challengesWonBySport ||
                      user.stats.challengesWonBySport[sport] !== challengesWonBySport[sport]) {
                      stats['stats.challengesWonBySport'] = challengesWonBySport;
                    }

                    // challengesWon
                    let challengesWon = Object.values(challengesWonBySport).reduce((acc: number, val: number) => {
                      return acc + val || 0;
                    }, 0);
                    if (index === Object.keys(chSports).length - 1 && user.stats.challengesWon !== challengesWon) {
                      stats['stats.challengesWon'] = challengesWon;
                    }

                    //gamesCountBySport
                    challengePksBySport[sport] = challengePks.length;
                    if (!user.stats.gamesCountBySport ||
                      user.stats.gamesCountBySport[sport] !== challengePksBySport[sport]) {
                      stats['stats.gamesCountBySport'] = challengePksBySport;
                    }

                    // gamesCount
                    let gamesCount = Object.values(challengePksBySport).reduce((acc: number, val: number) => {
                      return acc + val || 0;
                    }, 0);
                    if (index === Object.keys(chSports).length - 1 && user.stats.gamesCount !== gamesCount) {
                      stats['stats.gamesCount'] = gamesCount;
                    }

                    //gamesWonBySport
                    if (!user.stats.gamesWonBySport ||
                      user.stats.gamesWonBySport[sport] !== gamesWonBySport[sport]) {
                      stats['stats.gamesWonBySport'] = stats['stats.gamesWonBySport'] || {};
                      stats['stats.gamesWonBySport'] = gamesWonBySport;
                    }

                    // gamesWon 
                    let gamesWon = Object.values(gamesWonBySport).reduce((acc: number, val: number) => {
                      return acc + val || 0;
                    }, 0);
                    if (index === Object.keys(chSports).length - 1 && user.stats.gamesWon !== gamesWon) {
                      stats['stats.gamesWon'] = gamesWon;
                    }

                    if (!deepEqual(user.stats.challengesPrizesBySport, challengesPrizes)) {
                      stats['stats.challengesPrizes'] = [];
                      Object.values(challengesPrizes).forEach(sport => {
                        Object.keys(sport).forEach((challenge_id: any) => {
                          stats['stats.challengesPrizes'].push({ [challenge_id]: sport[challenge_id] });
                        });
                      });
                      stats['stats.challengesPrizesBySport'] = challengesPrizes;
                    }

                    if (index === Object.keys(chSports).length - 1 && Object.values(stats).length) {
                      stats['updated'] = new Date((new Date()).getTime() + (admin ? 7 : 0) * 3600 * 24 * 1000);
                      //console.log('update', stats);
                      this.afs.collection('user').doc(user_uid).update(stats);
                    } else {
                      //console.log('nothing to update');
                      this.afs.collection('user').doc(user_uid).update({ updated: new Date((new Date()).getTime() + (admin ? 7 : 0) * 3600 * 24 * 1000) });
                    }
                  }
                });
              }
            })
          });
      }
    });
  }

  sendPasswordResetEmail(email, showError?) {
    sendPasswordResetEmail(this.auth, email)
      .then(data => {
        this.modalService.confirmModal('Success', 'Password reset email sent');
        this.router.navigateByUrl('/login');
      })
      .catch(err => {
        if (showError) {
          this.modalService.errorModal('Error', err);
        }
        console.log(` failed ${err}`);
      });
  }

  logout() {
    signOut(this.auth).then(() => {
      localStorage.removeItem('token');
      this.router.navigate(['login']);
    })
  }

}
