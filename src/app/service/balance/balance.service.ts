import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SubjectService } from '../subject.service';


@Injectable({
  providedIn: 'root'
})
export class BalanceService extends SubjectService {

  protected inner: {
    balance?: any;
  } = {};
  
  constructor() {
    super();
  }

  public get balance$(): Observable<any> {
    return this.getSubject$('balance');
  }

  public get balance() {
    return this.getSubject('balance').value;
  }

  public set balance(value: any) {
    this.inner.balance = value;
    this.getSubject('balance').next(value);
  }
}
