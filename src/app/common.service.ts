import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/compat/functions';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private fns: AngularFireFunctions) { }

  getTimezone() {
    var parts = (new Date().toString()).match(/\(([^)]+)\)/i);
    var timezone = parts[1]; 
    if (timezone.search(/\W/) >= 0) {
      const timeReg = timezone.match(/\b\w/g); // Match first letter at each word boundary.
    if (timeReg) {
        timezone = timeReg.join('').toUpperCase(); 
        return timezone;
      } else {
        return '';
      }
    }
  }

  sendEmail(email, subject, html) {
    const callable = this.fns.httpsCallable('sendMail');
    callable({ email: email, subject: subject, html: html })
    .toPromise().then((data) => {
      //console.log(data);
    });
    /*
    this.http.get('https://us-central1-betting-game-hockey.cloudfunctions.net/testMail', {params: {} })
    .toPromise().then((data) => {
      console.log(data);
    });
    */
  }

  calcBidAmount(bid, id) {
    if(bid instanceof Object) {
      if(typeof id !== "undefined" && typeof bid[id] !== "undefined") {
        bid = Object.values(bid[id]);
      }
      else {
        bid = Object.values(bid[Object.keys(bid)[0]]);
      }
    }
    return Math.round(bid.reduce((a, b) => a + b.reduce((c, d) => c + d, 0), 0));
  }

  getObjectKeys(items, keys) {
    return Object.keys(items)
          .filter( key => keys.includes(key) )
          .reduce( (res, key) => (res[key] = items[key], res), {} );
  }

  getObjectOrderedKeys(items, keys) {
    let ret = {};
    Object.values(keys).forEach( (user_uid: string) => {
      if ( typeof items[user_uid] !== 'undefined' ) {
        ret[user_uid] = items[user_uid];
      }
    });
    return ret;
  }

  getMapKeys(items, keys) {  // ordered
    let ret = new Map();
    keys.forEach((value, key) => {
      if ( items[value] ) {
        ret[value] = items[value];
      }
    });
    return ret;
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  snapshotToObject = snapshot => {
    let item = snapshot.val();
    item.key = snapshot.key;
    return item;
  }

  snapshotToArray = snapshot => {
    let returnArr = [];
    snapshot.forEach(childSnapshot => {
        let item = childSnapshot.val();
        item.key = childSnapshot.key;
        returnArr.push(item);
    });
    return returnArr;
  }

  objectDeepKeys(obj, filter, min_length) {
    var keys = [];
    filter = filter || [];
    for (var key in obj) {
      if ( !filter.includes(key) && key.length >= min_length ) {
        keys.push(key);
      }
      if (typeof obj[key] === "object") {
        var subkeys = this.objectDeepKeys(obj[key], filter, min_length);
        keys = keys.concat(subkeys.map(function(subkey) {
          return subkey;
        }));
      }
    }
    return keys;
  }

}
