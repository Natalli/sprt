import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingsPageRoutingModule } from './settings-routing.module';

import { SettingsPage } from './settings.page';
import { DirectivesModule } from '../directives/directives.module';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsPageRoutingModule,
    DirectivesModule,
    HeaderModule,
    SubHeaderModule
  ],
  declarations: [SettingsPage]
})
export class SettingsPageModule {}
