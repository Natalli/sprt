import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { LoadingController } from '@ionic/angular';
import { SettingsNotification } from '../interfaces/enum/settings.enum';
import { ISettings } from '../interfaces/settings';
import { UserService } from '../service/user.service';
import { AppComponent } from '../app.component';
import { ModalService } from '../service/modal.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  settings: ISettings;
  settingsArray: Array<string> = [];
  SettingsNotification: typeof SettingsNotification = SettingsNotification;
  isChange: boolean;
  allEnabled = false;

  constructor(
    protected afs: AngularFirestore,
    protected userService: UserService,
    protected loadingController: LoadingController,
    public myapp: AppComponent,
    protected modalService: ModalService
  ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.settings = this.userService?.settings;
      this.settingsArray = Object.keys(this.settings);
    }, 3000)
  }

  change() {
    this.isChange = true;
    this.allEnabled = !Object.values(this.settings).includes(false);
  }

  toggleAll() {
    setTimeout(() => {
      Object.keys(this.settings).forEach(key => {
        this.settings[key] = this.allEnabled;
      });
    }, 0);
  }

  save() {
    this.afs.collection("user").doc(this.userService.uid).get().subscribe(item => {
      if (!item.exists) {
        item.ref.set({ settings: this.settings });
      }
      else {
        item.ref.update({ settings: this.settings });
      };
      this.modalService.confirmModal('Success', 'Changes saved!')
      setTimeout(() => {
        this.loadingController.dismiss();
        this.isChange = false;
      }, 3000)
    });
  }

}
