import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditProfilePage } from './edit-profile.page';
import { ImageCropperModule } from 'ngx-image-cropper';
import { UpdateEmailComponent } from './update-email/update-email.component';
import { HeaderModule } from '../components/header/header.module';
import { SubHeaderModule } from '../components/sub-header/sub-header.module';

 const routes: Routes = [
  {
    path: '',
    component: EditProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageCropperModule,
    RouterModule.forChild(routes),
    HeaderModule,
    SubHeaderModule
  ],
  declarations: [EditProfilePage, UpdateEmailComponent]
})
export class EditProfilePageModule {}
