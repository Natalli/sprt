import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Constants } from '../../constants';
import 'firebase/compat/auth';
import { signInWithEmailAndPassword, updateEmail } from 'firebase/auth';
import { UserService } from 'src/app/service/user.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/service/modal.service';

@Component({
  selector: 'app-update-email',
  templateUrl: './update-email.component.html',
  styleUrls: ['./update-email.component.scss'],
})
export class UpdateEmailComponent implements OnInit {
  @Output() emailChanges: EventEmitter<any> = new EventEmitter();

  public form: FormGroup;
  isVisible: boolean;
  user: any;

  @Input() set data(value) {
    if (value) {
      this.user = value;
    }
  }
  constructor(
    protected formBuilder: FormBuilder,
    private userService: UserService,
    private afs: AngularFirestore,
    private router: Router,
    private modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.createProfileForm();
  }

  createProfileForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(Constants.EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  update() {
    // console.log(this.form.value);
    const email = this.form.value.email;
    signInWithEmailAndPassword(this.userService.auth, this.user.email, this.form.value.password)
      .then(userCredential => {
        updateEmail(userCredential.user, email).then(() => {
          this.emailChanges.emit(email);
          this.afs.collection("user").doc(userCredential.user.uid).get().subscribe(item => {
            if (item && item.exists) {
              item.ref.update({ email })
                .then(() => { 
                  console.log('email updated'); 
                  this.router.navigate(['login']);
                })
                .catch((reason) => console.log(reason));
            }
          })
          this.modalService.confirmModal('Success', 'Email updated successfully')
        })
          .catch(err => {
            console.log(`${err}`);
          })
      })
  }

}
