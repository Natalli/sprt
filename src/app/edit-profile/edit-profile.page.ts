import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ToastController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Constants } from '../constants';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/compat/storage';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { tap, finalize } from 'rxjs/operators';
import { AppComponent } from '../app.component';
import { UserService } from '../service/user.service';
import { updateEmail, updatePassword, updateProfile } from 'firebase/auth';
import { ModalCropComponent } from '../popups/new/modal-crop/modal-crop.component';
import { ModalConfirmComponent } from '../popups/new/modal-confirm/modal-confirm.component';
import { ModalService } from '../service/modal.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  public ProfileForm: FormGroup;
  public PasswordsForm: FormGroup;

  isShowProgress: boolean = false;
  ShowProgressEmitter = new BehaviorSubject<boolean>(this.isShowProgress);

  url: string = '';
  UrlEmitter = new BehaviorSubject<string>(this.url);


  user: any;
  isVisible: boolean;
  isConfirmVisible: boolean;
  error: string;
  linkError: string = '';
  profileUrl: Observable<string | null>;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  file: string;
  isCropFailed: boolean;
  messageUniqueName: boolean;
  messageUniqueShortName: boolean;
  someUser: any;
  changedValue: boolean;

  percentage: Observable<number>;
  percent: number = 0;
  PercentageEmitter = new BehaviorSubject<number>(this.percent);

  task: AngularFireUploadTask;
  snapshot: Observable<any>;
  snap: any = {};
  SnapshotEmitter = new BehaviorSubject<any>(this.snap);
  popup;
  isEmailUpdated: boolean;
  constructor(
    private toastController: ToastController,
    private router: Router,
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private formBuilder: FormBuilder,
    private popoverCtrl: PopoverController,
    public myapp: AppComponent,
    private userService: UserService,
    protected modalService: ModalService) {
    }

  ngOnInit() {
    this.createProfileForm();
    this.createPasswordsForm();
    this.getUser();
  }

  getUser() {
    this.userService.user$.subscribe(user => {
      if (user) {
        this.user = user;
        if (user && user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
          this.myapp.loginAlert();
        }
        this.url = this.user['photoURL'] || Constants.AVATAR_DEMO;
        this.UrlEmitter.next(this.url);
        this.afs.collection("user").doc(this.user.uid).valueChanges().subscribe(i => {
          this.someUser = i;
          this.ShowProgressEmitter.next(this.isShowProgress = false);
          const item = i;
          if (item && Object.values(item).length > 0) {
            this.ProfileForm.get('username').setValue(this.ProfileForm.value.subject = item['name'] || this.user.displayName);
            this.ProfileForm.get('color').setValue(this.ProfileForm.value.subject = item['color'] || '');
            this.ProfileForm.get('publicName').setValue(this.ProfileForm.value.subject = item['publicName'] || '');
            this.ProfileForm.get('legalName').setValue(this.ProfileForm.value.subject = item['legalName'] || '');
            this.ProfileForm.get('email').setValue(this.ProfileForm.value.subject = (item['email'] || this.user.email));
          }
        });
      }
    })
  }

  createProfileForm() {
    this.ProfileForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, this.UniqueName.bind(this, 'name'), Validators.pattern(Constants.FULL_NAME_REGEXP)])],
      publicName: ['', Validators.compose([Validators.required, this.UniqueName.bind(this, 'publicName'), Validators.pattern(Constants.NAME_REGEXP)])],
      legalName: [''],
      color: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])]
    });
  }

  UniqueName(valueName: string, name: FormControl) {
    let newName = name.value;
    this.afs.collection("user").valueChanges({ idField: 'id' }).subscribe(data => {
      let users = data;
      if (this.someUser) {
        let uniqueName = users.find(data => {
          return data[valueName] !== this.someUser[valueName] && data[valueName] === newName
        })
        if (valueName === 'name') {
          uniqueName ? this.messageUniqueName = true : this.messageUniqueName = false;
        } else if (valueName === 'publicName') {
          uniqueName ? this.messageUniqueShortName = true : this.messageUniqueShortName = false
        }
      }
    })
  }

  createPasswordsForm() {
    this.PasswordsForm = this.formBuilder.group({
      password: ['', Validators.compose([Validators.required, Validators.pattern(Constants.PASSWORD_REGEXP)])],
      passwordConfirm: ['', Validators.compose([Validators.required])]
    })
  }

  saveProfile() { 
    if (this.user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
      this.router.navigate(['/profile']);
      return;
    }
    if (!this.messageUniqueName && !this.messageUniqueShortName) {
      updateEmail(this.userService.user, this.ProfileForm.value.email)
        .then(() => {
          updateProfile(this.userService.user, {
            displayName: this.ProfileForm.value.username,
            photoURL: this.UrlEmitter.getValue()
          })
            .then((data) => {
              let color;
              if (this.ProfileForm.value.color === '#ffffff' || this.ProfileForm.value.color === '#fff') {
                color = '#ff0000'
              } else {
                color = this.ProfileForm.value.color
              }
              let name = this.ProfileForm.value.username.replace(/((\s*\S+)*)\s*/, "$1");
              this.saveUserData({
                email: this.ProfileForm.value.email,
                name: name,
                color: color,
                publicName: this.ProfileForm.value.publicName.toUpperCase(),
                keywords: name.toLowerCase() + ' ' + this.ProfileForm.value.publicName.toLowerCase(),
                legalName: this.ProfileForm.value.legalName,
                photoURL: this.UrlEmitter.getValue(), // Auth photoURL ref is availble to the currently logged in user only
                creationTime: this.user.metadata.creationTime
              });
             
            }).then(() => {
               if (this.isEmailUpdated) {
                this.userService.logout();
              } else {
                this.router.navigate(['/profile']);
              }
            })
        })
        .catch(err => {
          console.log(` failed ${err}`);
          this.error = err.message;
        });
    }
  }

  updatePassword() {
    if (this.user.uid === 'DtMGj0ZZMYh5MSmBSR23jvxgmSw2') {
      this.router.navigate(['/profile']);
      return;
    }
    updatePassword(this.userService.user, this.PasswordsForm.value.password)
      .then(() => {
        this.PasswordsForm.get('password').setValue(this.PasswordsForm.value.subject = '');
        this.PasswordsForm.get('passwordConfirm').setValue(this.PasswordsForm.value.subject = '');
        this.modalService.confirmModal('Success', 'Password updated');
        this.error = '';
        this.router.navigate(['/login']);
      })
      .catch(err => {
        console.log(` failed ${err}`);
        this.error = err.message;
      });
  }

  updateEmail(email) {
    this.isEmailUpdated = true;
    this.ProfileForm.get('email').setValue(email);
  }

  saveUserData(data) {
    try {
      this.afs.collection("user").doc(this.user.uid).update(data);
    } catch (err) {
      alert(err)
    }
  }

  async addAvatar(event) {
    try {
      const fileType = event.target.files[0].type;
      if (fileType === 'image/png' ||
        fileType === 'image/jpg' ||
        fileType === 'image/jpeg' ||
        fileType === 'image/gif') {
        this.isCropFailed = false;
        this.imageChangedEvent = event;
        this.file = event.target.files[0];
        this.popup = await this.popoverCtrl.create({
          component: ModalCropComponent,
          componentProps: {
            imageChangedEvent: this.imageChangedEvent,
            imageCropped: this.imageCropped,
            imageLoaded: this.imageLoaded,
            loadImageFailed: this.loadImageFailed,
            confirmCropCancel: this.confirmCropCancel,
            imageCrop: this.imageCrop,
            file: this.file,
            cancelCrop: this.cancelCrop,
            popoverCtrl: this.popoverCtrl,
            storage: this.storage,
            user: this.user,
            croppedImage: this.croppedImage,
            task: this.task,
            percentage: this.percentage,
            snapshot: this.snapshot,
            url: this.url,
            changedValue: this.changedValue,
            isShowProgress: this.isShowProgress,
            ShowProgressEmitter: this.ShowProgressEmitter,
            UrlEmitter: this.UrlEmitter,
            PercentageEmitter: this.PercentageEmitter,
            SnapshotEmitter: this.SnapshotEmitter
          },
          cssClass: 'modal-wrap-default',
          id: 'cropPopup'
        });
        return await this.popup.present();
      } else {
        this.isCropFailed = true;
      }
    } catch (err) {
      alert(err)
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  async imageLoaded() {
    this.isCropFailed = false;
    // show cropper
  }

  loadImageFailed() {
    // img failed
    this.isCropFailed = true;
  }

  async imageCrop() {
    try {
      const imgBase = this.croppedImage.split(',')[1];
      this.changedValue = true;
      const path = `images/${this.user.uid}/${this.file['name']}`;
      const fireRef = this.storage.ref(path);
      if (this.croppedImage) {
        this.task = fireRef.putString(imgBase, 'base64', { contentType: 'image/jpg' });
        this.task.then(resp => {
          if (resp.state === 'success') {
            this.popoverCtrl.dismiss();
            this.ShowProgressEmitter.next(this.isShowProgress = true);
          }
        }).catch(err => {
          console.log(err.message)
        })
        this.percentage = this.task.percentageChanges();
        this.percentage.subscribe(item => {
          this.PercentageEmitter.next(item);
        });

        this.snapshot = this.task.snapshotChanges().pipe(
          tap(console.log),
          // The file's download URL
          finalize(async () => {
            await this.storage.ref(path).getDownloadURL()
              .subscribe(resp => {
                this.url = resp;
                this.UrlEmitter.next(resp);
                setTimeout(() => {
                  this.ShowProgressEmitter.next(this.isShowProgress = false);
                }, 1000)

              }, error => {
                console.log(error, 'ERROR')
              });
          }),
        );
        this.snapshot.subscribe(snap => {
          this.SnapshotEmitter.next(snap);
        })
      }
    } catch (err) {
      alert(err)
    }
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  async cancelCrop() {
    let popupCrop = document.getElementById('cropPopup') as any;
    popupCrop.dismiss();
  }

  async confirmCropCancel(ev) {
    const popover = await this.popoverCtrl.create({
      component: ModalConfirmComponent,
      event: ev,
      componentProps: {
        text: 'Are you sure you want to cancel editing?',
        submitConfirm: this.cancelCrop,
        popup: this.popup,
        btnNameConfirm: 'Ok'
      },
      cssClass: 'modal-wrap-default',
    });
    return await popover.present();
  }

  cancelEdit() {
    let newUser = this.ProfileForm.value;
    if (this.someUser.name !== newUser.username ||
      this.someUser.publicName !== newUser.publicName ||
      this.someUser.legalName !== newUser.legalName ||
      this.someUser.color !== newUser.color ||
      this.someUser.email !== newUser.email ||
      this.someUser.photoURL !== this.UrlEmitter.getValue()) {
      this.checkEditChanges();
    } else {
      this.cancelEditProfile()
    }
  }

  async checkEditChanges() {
    const popup = await this.popoverCtrl.create({
      component: ModalConfirmComponent,
      componentProps: {
        text: "You have unsaved changes. Want to save them?",
        cancelEvent: this.cancelEditProfile,
        submitConfirm: this.saveProfile,
        btnNameConfirm: "Save",
        user: this.user,
        ProfileForm: this.ProfileForm,
        afs: this.afs,
        router: this.router,
        url: this.url,
        userService: this.userService
      },
      cssClass: "modal-wrap-default"
    });
    return await popup.present();
  }

  cancelEditProfile() {
    this.router.navigate(['/profile'], { replaceUrl: true });
  }

  changedValues() {
    let newUser = this.ProfileForm.value;
    if (this.someUser.name !== newUser.username ||
      this.someUser.publicName !== newUser.publicName ||
      this.someUser.legalName !== newUser.legalName ||
      this.someUser.color !== newUser.color ||
      this.someUser.email !== newUser.email) {
      this.changedValue = true;
    } else {
      this.changedValue = false;
    }
  }

}
