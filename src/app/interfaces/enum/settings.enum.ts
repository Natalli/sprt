export enum SettingsNotification {
    gameStart = 'Game Started',
    gameEnd = 'Game End',
    gameCurrentWinner = 'Game Current Winner',
    goalScored = 'Goal Scored',
    gameWinner = 'Game Winner',
    gameBidsPlaced = 'Game Bids Placed',
    challengeMissingBids = 'Missing Bids',
    challengeStarted = 'Challenge Started',
    challengeCurrentWinner = 'Challenge Current Winner',
    challengeWinner = 'Challenge Winner',
    challengeLeader = 'Challenge Leader',
    challengeWon = 'Challenge Won',
    missingBids = 'Missing Bids',
    gameLeader = 'Game Leader', // Private Game only +  in the last 5 minute for Challenge Games
    privateBids = 'Bids Placed'
}