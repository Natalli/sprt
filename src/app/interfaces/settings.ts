export interface ISettings {
    gameWinner: boolean,
    challengeMissingBids: boolean,
    challengeStarted: boolean,
    challengeWinner: boolean,
    challengeLeader?: boolean,
    gameLeader?: boolean, // Private Game only +  in the last 5 minute for Challenge Games
    privateBids?: boolean
}
