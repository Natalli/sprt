const rp = require('request-promise');
var functions = require('firebase-functions');
var Firestore = require('@google-cloud/firestore');
var firestore = new Firestore({
    projectId: "betting-game-hockey",
    timestampsInSnapshots: true,
});

var admin = require('firebase-admin');
admin.initializeApp({
  credential: admin.credential.cert({
    "type": "service_account",
    "project_id": "betting-game-hockey",
    "private_key_id": "01f92f331a286f586d0afba5a78f5bb76d03708e",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDJ8KDwHgDQvy3s\n2miEyqHzmwIFV1qeeCLAqUf8f1X61rWfq/yAQgpP+HHuBoT/paJrx92w5C86fdZ8\nvIpPOn8jKi20NW+n0bLHnwkTDECW5Y1zmZqY8fdwAg+o1NNsJjsID6KcVvTb+sfh\norxVALoZAGOnl8M8EYnJ5iVqNIgQryWBYcwIiPS8JYA0hwWqXUHCxfjKgeCTNttN\n5+EFhKKoW3WnyYFiJGs8bnfN8K27GqkBer9X9EcIZOgW3Gcuk8dblXdp8N2hGb+Q\nKD1Zz/Fk/RjslhVwZhBgzCiMrcTpbJf/JluezQVAGxZ22CQ/q8PTeLa3HMozmly2\n188j++HVAgMBAAECggEAJKGkDBKhOrynV8sBwS7hxGcmrXHcZBkuUq0SfBzcK6ml\nni6MWfbA2KNchV1E5Hv3E96YWiV2ZMGddhicbUVS+T3GNyWnWLWrHNd+RaiIEedX\nqrPfLaXc6QJStWFP7dMci7iMWphSiBH8jrNevvuoBm+B5E2dEokq6cc4G8UmNoJA\nXXnTp7wDgZTB7AVIZsC+/+t4yg+vqFBdjqR0kuJypDLOLcP9ct1FVlZpXa/QbinC\ne3wsEEENFN9VNmMsPKoKkCt/4ejaSeI8AQIgbRLPC5NJfyQImYtfVST+IjiI/W0v\nPgnkrRL6t38npvMuO7kEI/yHKXmiwv5VuCl1NC5yGQKBgQDymIiXEyreWSlpJiFu\nvasUlMrUvS39MOt592ppGaw8Lm8paZyVPzxcNR3aLzdcD8h5hhG3wfSs0c84IzS4\nXqngGIbPmc7CgaceSXF4KmjNdt+a2vKhAvdXK77SF+DpAguG3JkmA1s96GrUDmXE\nFxYQuhz0NWqB6xxKFOq9Rbl2LQKBgQDVGQa/iHCxYqlTF5Dbr9UuAq6yTHcsEO8C\nEmT1T6CdwuWm4f2hIbOEYWQe+itB8HDPEzcftJh5kGuj/uivf9biUh0LXbU8ugtW\n/sx9Ex+1v8yp3W3Ry/CbjgMkDTf8RoMU2EHABxE/MfbE01ryvdzo39wAZRfQmFwI\nrxcXNKJLSQKBgG/6npn1nAUUbPv/QNFoNU8Du3hldpP3f8pFvVWUJx+EY4VfHYr6\nrHb3gbcTsl8JXgVSMF185t7a3ebQwWYVNB6f06KCIaFAzY46YmkQUl388Av/4p0V\nzGvtYmCB6yxC6GUw8HROelBiNuV5W9oeue2wqurCuU4M43sCmrQROiFlAoGBAJtc\n7KuV6TuayAK5qrZQoucxs3ZxA23QRsFG8e5cCKkcoFykZUsmDADXFKz1Dba8ezKu\nOsFV5Ni0b5YYUkMpvFwzK9POlUVaCsx5ZlNGLxdOaAPsWZXcz2ZKCoYtUQUPz5/R\n71X2kR926QglZCiYPkhWdcqf6dX+TxNRvE8B49VpAoGAA9Wym/FtpqRaKwd8f3yv\nus6k499N6YVdsQc2bggcVn4+KmhlLCHauYBzOXhGY4CpHha7JWstnicvSDfvVaYF\nejIs+oiOEf0UIrvyTw+zeztsSpp2tRMXR6rUsY71LPEOt3xrZikwXsFH15U1DZ4k\nAkTb/uWDOu7Zrpz2qCC8lS4=\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-l85f1@betting-game-hockey.iam.gserviceaccount.com",
    "client_id": "104196095200364015038",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-l85f1%40betting-game-hockey.iam.gserviceaccount.com"
  }),
  databaseURL: "https://betting-game-hockey.firebaseio.com"
}, 'triggers');

exports.handleGameStatus = functions.firestore
    .document('nfl1/{eventDate}')
    .onUpdate((change: any, context: any) => {
        return {result: 'success'};
    });

exports.handleWinnerStatus = functions.firestore
    .document('team/{gamePk}')
    .onUpdate((change: any, context: any) => {
        console.log('gamePk', context.params.gamePk);
        //console.log('context.params', context.params);
        //console.log('change.before.data()', change.before.data());
        const gamePk = context.params.gamePk
        const before = change.before.data();
        const after = change.after.data();

        if ( Object.keys(after).length ) {
            Object.keys(after).forEach(team_id => {
                if ( before[team_id] && before[team_id].winners &&
                    after[team_id] && after[team_id].winners &&
                    after[team_id].winners.length ) {
                    if ( before[team_id].winners.length !== after[team_id].winners.length ) {
                        if ( before[team_id].winners.length > after[team_id].winners.length ) {
                            console.log('game '+gamePk+' team #'+team_id+' winners removed');
                        }
                        else {
                            console.log(before[team_id].winners);
                            console.log(after[team_id].winners);
                            const winners = after[team_id].winners.filter(x => !before[team_id].winners.includes(x));
                            //const winners = after[team_id].winners;
                            const challenge = {};
                            console.log('game '+gamePk+' team #'+team_id+' winners added', JSON.stringify(winners));
                            firestore.collection('live').doc(gamePk).get().then((doc: any) => {
                                //console.log('returning data', typeof doc, typeof doc.data());
                                sendNotification('gameWinner', winners, challenge, doc.data());
                            }).catch((err: any) => console.log(`error getting game $gamePk `, err));
                        }
                    }
                    else if ( before[team_id].winners.every((val:any, i:any) => { val !== after[team_id][i] }) ) {
                        console.log('game '+gamePk+' team #'+team_id+' winners updated but count did not changed');
                    }
                    else {
                        console.log('game '+gamePk+' team #'+team_id+' winners did not change');
                    }
                }
            })
        }
        
        return {result: 'success'};
    }); 
       
exports.handleChallengeStatus = functions.firestore
    .document('challenge/{challengePk}')
    .onUpdate((change: any, context: any) => {
        const challengePk = context.params.challengePk
        const before = change.before.data();
        const after = change.after.data();

        if ( after && after.challengeWinners && Object.keys(after.challengeWinners).length ) {
          if ( !before.challengeWinners || Object.keys(before.challengeWinners).length !== Object.keys(after.challengeWinners).length ) {
              if ( Object.keys(before.challengeWinners).length > Object.keys(after.challengeWinners).length ) {
                  console.log('game '+challengePk+' winners removed');
              }
              else {
                  const winners = Object.keys(after.challengeWinners).filter(x => !Object.keys(before.challengeWinners).includes(x));
                  console.log('NFL/NHL Challenge '+challengePk+' winners added', JSON.stringify(winners));
                  if ( after.sport && after.sport !== 'nba' ) {
                    sendNotification('challengeWinner', winners, after);
                  }
              }
          }
          else if ( Object.keys(before.challengeWinners).every((val:any, i:any) => { val !== Object.keys(after.challengeWinners)[i] }) ) {
              console.log('Challenge '+challengePk+' winners updated but count did not changed');
          }
          else {
              console.log('Challenge '+challengePk+' winners did not change');
          }
        }
        else {
          console.log('empty after???');
        }
        
        if ( after && !after.challengeWinners && after.leader ) {
          if ( before.leader !== after.leader ) {
            console.log('Challenge '+challengePk+' leader updated', JSON.stringify([before.leader, after.leader]));
            //sendNotification('challengeLeader', [before.leader, after.leader], after);
          }
          else {
              console.log('Challenge '+challengePk+' leader did not change');
          }
        }

        return {result: 'success'};
    });    

    async function getUser(id: string) {
        const ref = await firestore.doc('user/'+id);
        ref.get().then((doc: any) => {
              return doc.data();
        }).catch((err: any) => { 
            console.log(`error getting user $id `, err);
            return {};
        });
    }

    async function getGame(id: string) {
        await new Promise((resolve, reject)=> {
            console.log('getting live/'+id +'_', typeof id);
            firestore.collection('live').doc(''+id).get().then((doc: any) => {
                console.log('returning data', typeof doc, typeof doc.data());
                resolve (doc.data());
            }).catch((err: any) => { 
                console.log(`error getting game $id `, err);
                resolve ({});
            });
        });
    }

    function sendNotification(type: string, recipients: Array<string>, challenge: any, game?: any) {
        console.log(type);
        console.log(recipients);
        //const temp = ['JTdh9Y9eO2MzzPJnEGboOIsdtxC3'];
        const temp = recipients;
        let gotoUrl: string;

        let title = '';
        let body = '';
        if ( challenge.id && challenge.sport ) {
          gotoUrl = '/'+challenge.sport+'/'+challenge.id;
        }

        if (type === 'challengeWinner') {
          title = 'CONGRATULATIONS!';
          body = 'You Won Challenge';
        } else if (type === 'challengeStarted') {
          //recipients = challenge.users;
          title = 'Challenge Started';
          body = challenge.name + ' is underway';
        } else if (type === 'gameWinner') {
          title = 'Game Won';
          game.teams = game.teams || game.liveData.linescore.teams;
          body = 'You Won ' + game.teams.away.team.name + ' @ ' + game.teams.home.team.name + ' Game';
        } else if ('challengeLeader') {
          title = 'Challenge Leader';
          body = 'There is a new Leader of the ' + challenge.name;
        } else if ('challengeCurrentWinner') {
          //recipients = [leader];
          title = 'Challenge Leader';
          body = true ? 'You are the current Leader of the ' + challenge.name : 'You are no longer Leader of the ' + challenge.name;
        } else if (type === 'missingBids') {
          title = 'Missing Bids';
          body = "Hey " + ({name: ''}).name + ", You have not placed bids for # Games for today's " + challenge.name; // name
        }
    
        //recipients
        temp.forEach((uid: string) => {
            firestore.collection('user').doc(uid).get().then((doc: any) => {
                //console.log('user data', typeof doc.data());
                const user = doc.data();
                if ( user ) {
                  //console.log(user.settings, 'user settings')
                  if ( user.tokens && 
                      ( (user.settings && user.settings[type]) ||
                      ( !user.settings && ['challengeStarted', 'gameWinner', 'challengeWinner', 'challengeMissingBids'].includes(type) ) ) ) {
                    let tokens = Object.values(user.tokens).filter(id => id);
                    if (type === 'challengeWinner') {
                      body = (user.name || 'You') + ' Won ' + challenge.name; // name
                    }
                    else if (type === 'challengeLeader') {
                      body = 'You are ' + (challenge.leader === uid ? 'the current' : 'no longer') +  ' Leader of the ' + challenge.name;
                    }              
                    console.log('SENDING TO ', tokens);
                    console.log({ tokens: tokens, title: title, body: body });
                    if (tokens) {
                    tokens.forEach(token => {
                        if (token && type !== 'challengeMissingBids') {
                          sendNativeNotification(token, title, body, gotoUrl);
                        }
                        else {
                          console.log('SENT disabled');
                        }
                    });
                    }
                  }
                } else console.log('empty user ', uid);
            }).catch((err: any) => console.log(`error getting user $uid `, err));
        });
        return;
      }

      exports.startChallengeNFLNotification = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('15 10 * * 0').onRun(async (context: any) => {
        //startChallenge('233');
        await new Promise(r => setTimeout(r, 8*60*1000));
        return {test: 'return'};
      });

      exports.startChallengeNBANotification = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('45 16 * * 1-5').onRun(async (context: any) => {
        const dates:any = {
          1: 216,
          3: 218,
          4: 219,
          5: 220
        };
        const localTime = new Date(new Date().getTime() - 8 * 3600 * 1000); // Force .timeZone('America/Los_Angeles')
        const challenge_id = dates[localTime.getDate()];
        console.log('startChallengeNBANotification', challenge_id, localTime.getDate());
        if ( !challenge_id ) return;
        //startChallenge(String(challenge_id));
        await new Promise(r => setTimeout(r, 8*60*1000));
        return {test: 'return'};
      });

      exports.startChallengeNBANotification2 = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('30 18 * * 1-5').onRun(async (context: any) => {
        const dates:any = {
          2: 217
        };
        const localTime = new Date(new Date().getTime() - 8 * 3600 * 1000); // Force .timeZone('America/Los_Angeles')
        const challenge_id = dates[localTime.getDate()];
        console.log('startChallengeNBANotification2', challenge_id, localTime.getDate());
        if ( !challenge_id ) return;
        //startChallenge(String(challenge_id));
        await new Promise(r => setTimeout(r, 8*60*1000));
        return {test: 'return'};
      });

      exports.startChallengeNHLNotification = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('00 13 * * 6').onRun(async (context: any) => {
        //startChallenge('182');
        await new Promise(r => setTimeout(r, 8*60*1000));
        return {test: 'return'};
      });
/*
      exports.scheduledTasks = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('every 1 minutes').onRun((context: any) => {
        startChallenge('113');
        return {test: 'return'};
    });
*/

    exports.startChallenge = functions.https.onRequest((req: any, res: any) => {    
      //startChallenge('78')
    });

    function startChallenge(challengePk: string) {    
      console.log('startChallenge ', challengePk);
      firestore.collection('challenge').doc(challengePk).get().then((doc: any) => {
          const challenge = doc.data();
          challenge.id = challengePk;
          if ( challenge && challenge.games && challenge.games[0] ) {
              const gamePk = challenge.games[0];
              firestore.collection('team').doc(gamePk).get().then((teamDoc: any) => {
                  const teams = teamDoc.data();
                  if ( teams ) {
                    Object.values(teams).forEach((team: any, index) => {
                      if ( team && team.users ) {
                        console.log('Challenge started to team users ', JSON.stringify(team.users));
                        setTimeout(() => {
                          sendNotification('challengeStarted', team.users, challenge);
                          if ( index === Object.values(teams).length-1 ) {
                            console.log('all teams are processed');
                          }
                        }, index * 100);
                      }
                    });
                  }
              });
          }
      });
    };

      function sendNativeNotification(tokens: any, title: string, body: string, gotoUrl?: string) {
        if ( title === 'CONGRATULATIONS!' ) return;
        console.log('sendNativeNotification-triggers to', tokens, gotoUrl);
        const message:any = {
          notification: {
            "title": title,
            "body": body
          },
          android: {
            notification: {
                sound: 'default'
            },
          },
          apns: {
              payload: {
                  aps: {
                      sound: 'default'
                  },
              },
          },      
          webpush: {
            headers: {
              Urgency: "high"
            },
            notification: {
              body: body,
              requireInteraction: "true",
              badge: "https://sprtmtrx.com/assets/icon/favicon.png"
            }
          },
          token: tokens
        };
        if ( gotoUrl ) {
          message.data = {gotoUrl: gotoUrl};
        }
        admin.messaging().send(message)
        .then((response: any) => {
          console.log('Successfully sent message', title, tokens, response);
          return {response: response, tokens: tokens};
        })
        .catch((error: any) => {
          console.log('Error sending message', title, tokens, error);
          return {error: error, tokens: tokens};
        });
      }
      