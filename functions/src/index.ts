//import * as functions from 'firebase-functions';

var Firestore = require('@google-cloud/firestore');
var PROJECTID = 'cloud-functions-firestore';
var COLLECTION_NAME = 'cloud-functions-firestore';
var firestore = new Firestore({
  projectId: "betting-game-hockey",
  timestampsInSnapshots: true,
});

var nodemailer=require('nodemailer');

const cors = require('cors')({origin: "*"});

var functions = require('firebase-functions');
exports.tasks = require('./tasks');
exports.triggers = require('./triggers');

var admin = require('firebase-admin');
admin.initializeApp({
  credential: admin.credential.cert({
    "type": "service_account",
    "project_id": "betting-game-hockey",
    "private_key_id": "01f92f331a286f586d0afba5a78f5bb76d03708e",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDJ8KDwHgDQvy3s\n2miEyqHzmwIFV1qeeCLAqUf8f1X61rWfq/yAQgpP+HHuBoT/paJrx92w5C86fdZ8\nvIpPOn8jKi20NW+n0bLHnwkTDECW5Y1zmZqY8fdwAg+o1NNsJjsID6KcVvTb+sfh\norxVALoZAGOnl8M8EYnJ5iVqNIgQryWBYcwIiPS8JYA0hwWqXUHCxfjKgeCTNttN\n5+EFhKKoW3WnyYFiJGs8bnfN8K27GqkBer9X9EcIZOgW3Gcuk8dblXdp8N2hGb+Q\nKD1Zz/Fk/RjslhVwZhBgzCiMrcTpbJf/JluezQVAGxZ22CQ/q8PTeLa3HMozmly2\n188j++HVAgMBAAECggEAJKGkDBKhOrynV8sBwS7hxGcmrXHcZBkuUq0SfBzcK6ml\nni6MWfbA2KNchV1E5Hv3E96YWiV2ZMGddhicbUVS+T3GNyWnWLWrHNd+RaiIEedX\nqrPfLaXc6QJStWFP7dMci7iMWphSiBH8jrNevvuoBm+B5E2dEokq6cc4G8UmNoJA\nXXnTp7wDgZTB7AVIZsC+/+t4yg+vqFBdjqR0kuJypDLOLcP9ct1FVlZpXa/QbinC\ne3wsEEENFN9VNmMsPKoKkCt/4ejaSeI8AQIgbRLPC5NJfyQImYtfVST+IjiI/W0v\nPgnkrRL6t38npvMuO7kEI/yHKXmiwv5VuCl1NC5yGQKBgQDymIiXEyreWSlpJiFu\nvasUlMrUvS39MOt592ppGaw8Lm8paZyVPzxcNR3aLzdcD8h5hhG3wfSs0c84IzS4\nXqngGIbPmc7CgaceSXF4KmjNdt+a2vKhAvdXK77SF+DpAguG3JkmA1s96GrUDmXE\nFxYQuhz0NWqB6xxKFOq9Rbl2LQKBgQDVGQa/iHCxYqlTF5Dbr9UuAq6yTHcsEO8C\nEmT1T6CdwuWm4f2hIbOEYWQe+itB8HDPEzcftJh5kGuj/uivf9biUh0LXbU8ugtW\n/sx9Ex+1v8yp3W3Ry/CbjgMkDTf8RoMU2EHABxE/MfbE01ryvdzo39wAZRfQmFwI\nrxcXNKJLSQKBgG/6npn1nAUUbPv/QNFoNU8Du3hldpP3f8pFvVWUJx+EY4VfHYr6\nrHb3gbcTsl8JXgVSMF185t7a3ebQwWYVNB6f06KCIaFAzY46YmkQUl388Av/4p0V\nzGvtYmCB6yxC6GUw8HROelBiNuV5W9oeue2wqurCuU4M43sCmrQROiFlAoGBAJtc\n7KuV6TuayAK5qrZQoucxs3ZxA23QRsFG8e5cCKkcoFykZUsmDADXFKz1Dba8ezKu\nOsFV5Ni0b5YYUkMpvFwzK9POlUVaCsx5ZlNGLxdOaAPsWZXcz2ZKCoYtUQUPz5/R\n71X2kR926QglZCiYPkhWdcqf6dX+TxNRvE8B49VpAoGAA9Wym/FtpqRaKwd8f3yv\nus6k499N6YVdsQc2bggcVn4+KmhlLCHauYBzOXhGY4CpHha7JWstnicvSDfvVaYF\nejIs+oiOEf0UIrvyTw+zeztsSpp2tRMXR6rUsY71LPEOt3xrZikwXsFH15U1DZ4k\nAkTb/uWDOu7Zrpz2qCC8lS4=\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-l85f1@betting-game-hockey.iam.gserviceaccount.com",
    "client_id": "104196095200364015038",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-l85f1%40betting-game-hockey.iam.gserviceaccount.com"
  }),
  databaseURL: "https://betting-game-hockey.firebaseio.com"
});

const serviceAccount = {
  "type": "service_account",
  "project_id": "betting-game-hockey",
  "private_key_id": "01f92f331a286f586d0afba5a78f5bb76d03708e",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDJ8KDwHgDQvy3s\n2miEyqHzmwIFV1qeeCLAqUf8f1X61rWfq/yAQgpP+HHuBoT/paJrx92w5C86fdZ8\nvIpPOn8jKi20NW+n0bLHnwkTDECW5Y1zmZqY8fdwAg+o1NNsJjsID6KcVvTb+sfh\norxVALoZAGOnl8M8EYnJ5iVqNIgQryWBYcwIiPS8JYA0hwWqXUHCxfjKgeCTNttN\n5+EFhKKoW3WnyYFiJGs8bnfN8K27GqkBer9X9EcIZOgW3Gcuk8dblXdp8N2hGb+Q\nKD1Zz/Fk/RjslhVwZhBgzCiMrcTpbJf/JluezQVAGxZ22CQ/q8PTeLa3HMozmly2\n188j++HVAgMBAAECggEAJKGkDBKhOrynV8sBwS7hxGcmrXHcZBkuUq0SfBzcK6ml\nni6MWfbA2KNchV1E5Hv3E96YWiV2ZMGddhicbUVS+T3GNyWnWLWrHNd+RaiIEedX\nqrPfLaXc6QJStWFP7dMci7iMWphSiBH8jrNevvuoBm+B5E2dEokq6cc4G8UmNoJA\nXXnTp7wDgZTB7AVIZsC+/+t4yg+vqFBdjqR0kuJypDLOLcP9ct1FVlZpXa/QbinC\ne3wsEEENFN9VNmMsPKoKkCt/4ejaSeI8AQIgbRLPC5NJfyQImYtfVST+IjiI/W0v\nPgnkrRL6t38npvMuO7kEI/yHKXmiwv5VuCl1NC5yGQKBgQDymIiXEyreWSlpJiFu\nvasUlMrUvS39MOt592ppGaw8Lm8paZyVPzxcNR3aLzdcD8h5hhG3wfSs0c84IzS4\nXqngGIbPmc7CgaceSXF4KmjNdt+a2vKhAvdXK77SF+DpAguG3JkmA1s96GrUDmXE\nFxYQuhz0NWqB6xxKFOq9Rbl2LQKBgQDVGQa/iHCxYqlTF5Dbr9UuAq6yTHcsEO8C\nEmT1T6CdwuWm4f2hIbOEYWQe+itB8HDPEzcftJh5kGuj/uivf9biUh0LXbU8ugtW\n/sx9Ex+1v8yp3W3Ry/CbjgMkDTf8RoMU2EHABxE/MfbE01ryvdzo39wAZRfQmFwI\nrxcXNKJLSQKBgG/6npn1nAUUbPv/QNFoNU8Du3hldpP3f8pFvVWUJx+EY4VfHYr6\nrHb3gbcTsl8JXgVSMF185t7a3ebQwWYVNB6f06KCIaFAzY46YmkQUl388Av/4p0V\nzGvtYmCB6yxC6GUw8HROelBiNuV5W9oeue2wqurCuU4M43sCmrQROiFlAoGBAJtc\n7KuV6TuayAK5qrZQoucxs3ZxA23QRsFG8e5cCKkcoFykZUsmDADXFKz1Dba8ezKu\nOsFV5Ni0b5YYUkMpvFwzK9POlUVaCsx5ZlNGLxdOaAPsWZXcz2ZKCoYtUQUPz5/R\n71X2kR926QglZCiYPkhWdcqf6dX+TxNRvE8B49VpAoGAA9Wym/FtpqRaKwd8f3yv\nus6k499N6YVdsQc2bggcVn4+KmhlLCHauYBzOXhGY4CpHha7JWstnicvSDfvVaYF\nejIs+oiOEf0UIrvyTw+zeztsSpp2tRMXR6rUsY71LPEOt3xrZikwXsFH15U1DZ4k\nAkTb/uWDOu7Zrpz2qCC8lS4=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-l85f1@betting-game-hockey.iam.gserviceaccount.com",
  "client_id": "104196095200364015038",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-l85f1%40betting-game-hockey.iam.gserviceaccount.com"
};

/*
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'sprt.mtrx@gmail.com',
        pass: 'di479854',
    },
});
*/
var transporter = nodemailer.createTransport({
    host: 'smtp.sprtmtrx.com', // Office 365 server
    port: 587,     // secure SMTP
    secure: false, // false for TLS - as a boolean not string - but the default is false so just remove this completely
    auth: {
        user: 'info@sprtmtrx.com',
        pass: 'Kor46356'
    },
    ignoreTLS: true
});


exports.sendMail = functions.https.onRequest((req: any, res: any) => {    
  return cors(req, res, () => {
    res.set('Access-Control-Allow-Origin', '*');
    var mailOptions={
        from: 'SPRT MTRX Notification <info@sprtmtrx.com>',
        to: (req.body.data||req.body).email||'ddvinyaninov@gmail.com',
        subject: req.body.data.subject || req.body.subject,
        html: req.body.data.html || req.body.html,
        bcc: ['ddvinyaninov@gmail.com', 'david@livecurrent.com']
    }
    transporter.sendMail(mailOptions,function(err: any, response: any) {
        if(err){
            res.end(err + response);
        }
        else{
            res.end(JSON.stringify({data: {text: 'Mail sent'}}));
        }
    });
  });
});

exports.testMail = functions.https.onRequest((req: any, res: any) => {    
    var mailOptions={        
        from: 'SPRT MTRX Notification <info@sprtmtrx.com>',
        to: 'ddvinyaninov@gmail.com',
        subject: 'Test subject',
        html: 'Test email body.'
    }
    transporter.sendMail(mailOptions,function(err: any, response: any) {
        if(err){
            res.end(err + response);
        }
        else{
            res.end(JSON.stringify({text: 'Mail sent'}));
        }
    });
});

async function getChallenge(challengePK: string) {
    let challenge: any = {};

    firestore.collection('challenge')
    .doc('' + challengePK)
    .get()
    .then((doc: any) => {
      if (!(doc && doc.exists)) {
          console.log('Can\'t find Challenge: ' + challengePK);
      }
      challenge = doc.data();
      return challenge;
    }).catch((err: any) => {
      console.error(err);
      console.log('Unable to retrieve Challenge: ' + challengePK);
      return challenge;
    });    

}

async function shuffleChallenge(challengePK: any, gamePK?: any) {
  console.log('shuffle start, challengePK: ' + challengePK + ', gamePK: ' + gamePK);
  let challenge: any = {};
  await firestore.collection('challenge')
  .doc('' + challengePK)
  .get()
  .then((doc: any) => {
    if (!(doc && doc.exists)) {
        console.log('Can\'t find Challenge: ' + challengePK);
    }
    else {
      console.log('Found Challenge: ' + challengePK);
    }
    challenge = doc.data();
    challenge.id = challengePK;
    if ( !gamePK && challenge.games ) {
      challenge.games.forEach((gamePk:any) => {
        shuffleChallengeGame(challenge, gamePk)
        .catch(err => console.log(err))
        .then(() => console.log('this will succeed'))
        .catch(() => 'obligatory catch')
      });
      return {data: {text: 'Teams for Challenge ' + challengePK + ' games '+JSON.stringify(challenge.games)+' are shuffled and reassigned'}};
    }
    else {
      return shuffleChallengeGame(challenge, gamePK);
    }
  }).catch((err: any) => {
    console.error(err);
    console.log('Unable to retrieve Challenge: ' + challengePK);
    return { error: 'Unable to retrieve Challenge: ' + challengePK + ', errror: ' + JSON.stringify(err) };
  });
};

async function shuffleChallengeGame(challenge: any, gamePK: any) {
  console.log('Game shuffle start, challengePK: ' + challenge.id + ', gamePK: ' + gamePK);
  if ( challenge ) {
    let teams2: any = {};
    await firestore.collection('team')
    .doc(gamePK)
    .get()
    .then(function(teamDoc: any) {
      teams2[gamePK] = teamDoc.data();
      let allowedUsers: Array<String> = checkChallenge(challenge, teams2);
      if ( typeof teams2[gamePK][0].locked === 'undefined' || !teams2[gamePK][0].locked ) {
          console.log('locking game bidding and shuffling Teams for game ' + gamePK);
          let new_teams = Object.assign({}, shuffleTeams(teams2[gamePK], allowedUsers));
          //console.log(new_teams);
          firestore.collection('team').doc(gamePK).set(new_teams);
          Object.keys(new_teams || {}).map( team_id => {
              if (parseInt(team_id) > 0) {
                  console.log('reassign team id ' + team_id + ' for ' + gamePK);
                  if ( new_teams[team_id].users ) {
                      new_teams[team_id].users.forEach((user_uid: string) => {
                          console.log('reassign user ' + user_uid + ' for ' + gamePK + 'to team ' + team_id);
                          firestore.collection('user').doc(user_uid)
                          .get()
                          .then((user: any) => {
                            if (!(user && user.exists)) {
                              console.error( 'Unable to find ' + 'user/' + user_uid );
                            } else {
                              const data = user.data();
                              if ( data.games && data.games[gamePK] ) {
                                if(data.games[gamePK] !== team_id) {
                                  data.games[gamePK] = team_id;
                                  const update: any = {};
                                  update['games.'+gamePK] = team_id;
                                  user.ref.update(update).finally(()=>{
                                    console.log('reassigned user ' + user_uid + ' to team '+team_id);
                                  });
                                }
                              } else {
                                console.error('Unable to find ' + 'user/' + user_uid + '/' + gamePK);
                              }
                              //console.log(data);
                            }
                          });
                      });
                  }
              }
          });
          challenge.games.forEach((shuffleGamePk: string) => {
              //
          });
      } else {
          console.log('Teams for game ' + gamePK + ' are already locked');
          return {data: {text: 'Teams for game ' + gamePK + ' are already locked'}};
      }
      console.log ( 'Teams for game ' + gamePK + ' are shuffled and reassigned');
      return {data: {text: 'Teams for game ' + gamePK + ' are shuffled and reassigned'}};
    }).catch((err: any) => {
      console.error(err);
      return { error: 'Unable to retrieve the document: ' + err};
    });    

  }
  else {
    console.log('Unable to retrieve Challenge: ' + challenge.id);
    return { error: 'Unable to retrieve Challenge: ' + challenge.id + ', errror: ' };
  };
};

function shuffleTeams(teams: any, allowedUsers?: Array<String>) {
    let max_users = 8;
    let users: any = [];
    Object.keys(teams || {}).map( id => { // Concat multiple teams if any
      max_users = teams[id].max_users;
      users = users.concat(teams[id].users);
    });
    if ( allowedUsers ) { // Filter users with incomplete bids
      users = users.filter((value: any) => allowedUsers.includes(value));
    }
    max_users = calcMaxUsers(users); // Define user count in a team
    let teams_count = Math.floor( users.length / max_users ); // Define number of teams
    if ( users.length > max_users * teams_count ) { // exclude excess users
      users = users.slice(0, max_users * teams_count);
    }
    let orig_users =  [...users]; // preserve original user bids order
    if ( teams_count > 0 ) {
      let tmp_teams: any = [];
      let new_teams: any = {};
      if ( true ) { // new shuffle logic
        users = shuffle(users);
        for(let k=0; k<teams_count; k++) { // slice users into teams
          tmp_teams[ k ] = users.slice(k * max_users, (k + 1) * max_users);
        }
        tmp_teams.forEach((team: any, team_id: string) => { // recover original order
          new_teams[team_id] = {bid_amount: 10, id: 0, max_users: 8, users: [], winners: []};
          new_teams[team_id].max_users = max_users;
          new_teams[team_id].id = team_id;
          new_teams[team_id].users = orig_users.filter(value => team.includes(value));
          new_teams[team_id].locked = true;
        });
      }
      return new_teams;
    } else {
      return teams;
    }
  }

  function  calcMaxUsers(users: any) {
    let remainders = [ users.length % 5, users.length % 6, users.length % 7, users.length % 8];
    return remainders.indexOf(Math.min(...remainders)) + 5;
  }

  function shuffle(array: any) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }


  function checkChallenge(challenge: any, teams2: any) {
    let allowedUsers: Array<String> = [];
    if ( challenge && teams2 ) {
      if ( challenge.games ) {
        let challengeUsers: any = {};
        challenge.games.forEach((gamePk: string) => {
          if ( true ) {
            if ( teams2[gamePk] && Object.keys(teams2[gamePk]).length > 0 ) {
              let users:Array<String> = [];
              Object.keys(teams2[gamePk]).forEach(team_id => {
                users = users.concat(teams2[gamePk][team_id].users || []); // TODO: Check if winners calculated
              });
              challengeUsers[gamePk] = users;
            }
          }
        });
        Object.values(challengeUsers).forEach((gameUsers: any) => {
          if ( allowedUsers.length === 0 ) {
            allowedUsers = gameUsers;
          } else {
            allowedUsers = allowedUsers.filter((value: any) => gameUsers.includes(value));
          }
        });
      }
    }
    let max_users = calcMaxUsers(allowedUsers);
    let teams_count = Math.floor( allowedUsers.length / max_users );
    if ( allowedUsers.length > max_users * teams_count ) {
      allowedUsers = allowedUsers.slice(0, max_users * teams_count);
    }
    return allowedUsers;
  }

  exports.shuffle = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).https.onRequest((req: any, res: any) => {
    return cors(req, res, () => {
      res.set('Access-Control-Allow-Origin', '*');
      if ( req.body && req.body.data ) {
        let challengePk = req.body.data.challengePk;
        let gamePk = req.body.data.gamePk;
        if ( req.body.data.challengePk && req.body.data.gamePk ) {
          let ret: any = {empty: null}
          console.log(challengePk + ':' + gamePk);
          ret = shuffleChallenge(challengePk, gamePk);
          res.status(200).send({data: {response: ret, request: {challengePk: challengePk, gamePk: gamePk}}});
        }
        else {
          console.error('Unable to retrieve PKs: ' + JSON.stringify(req.body.data));
          return res.status(404).send({ error: 'Unable to retrieve PKs: ' + JSON.stringify(req.body.data) });
        }
      }
      else {
        console.error('Unable to get POST parameters: ' + JSON.stringify(req.body));
        return res.status(404).send({ error: 'Unable to get POST parameters: ' + JSON.stringify(req.body) });
      }
    });
  });

  function sendNativeNotification(tokens: any, title: string, body: string, gotoUrl?: string) {
    if ( title === 'CONGRATULATIONS!' ) return;
    console.log('sendNativeNotification-index to', tokens, gotoUrl);
    const message:any = {
      notification: {
        "title": title,
        "body": body
      },
      android: {
        notification: {
            sound: 'default'
        },
      },
      apns: {
          payload: {
              aps: {
                  sound: 'default'
              },
          },
      },      
      webpush: {
        headers: {
          Urgency: "high"
        },
        notification: {
          body: body,
          requireInteraction: "true",
          badge: "https://sprtmtrx.com/assets/icon/favicon.png"
        }
      },
      token: tokens
    };
    if ( gotoUrl ) {
      message.data = {gotoUrl: gotoUrl};
    }
    admin.messaging().send(message)
    .then((response: any) => {
      console.log('Successfully sent message', title, tokens, response);
      return {response: response, tokens: tokens};
    })
    .catch((error: any) => {
      console.log('Error sending message', title, tokens, error);
      return {error: error, tokens: tokens};
    });
  }
  
  exports.sendNativeNotification = functions.https.onRequest((req: any, res: any) => {    
    //res.set({ 'Access-Control-Allow-Origin': '*' }).sendStatus(200);
    return cors(req, res, () => {
      res.set('Access-Control-Allow-Origin', '*');
      let ret = {data: {}};
      console.log(req.body);
      if ( req.body && req.body.data ) {
        sendNativeNotification(req.body.data.tokens, req.body.data.title, req.body.data.body, req.body.data.gotoUrl);
      }
      res.end(JSON.stringify(ret));
    });
  });

  exports.testNativeNotification2 = functions.https.onRequest((req: any, res: any) => {    
    let ret: any;
    console.log(req.body);
    if ( req.body ) {
      ret = sendNativeNotification(req.body.tokens, req.body.title, req.body.body);
    }
    res.end(JSON.stringify(ret));
  });  

  exports.testNativeNotification = functions.https.onRequest((req: any, res: any) => {
    console.log('sendNotification', );
    if ( (req.params && req.params[0]) ) {
      console.log('req.params', req.params);
      let params = req.params[0].match(/\/(.+)\/(.+)\/?/);
      console.log(params);
      let registrationToken = params[1] || "fo2VKCVAN8V2XxbXRX8oHF:APA91bF3SqO0t19ouYzgVK7ACexyIrESHNue5KTaFqCNXt-o7Msiaad8ZJeITljWsj-28tCzGtuGweMu8r-QpXGP4Sdqqe2M_rjWV6TsJBNeiVMvg8jqhkFa9GhLC51Q9Ly64s8faxaz";
      let title = params[2] || "Default message";
      let body = params[3] || "Default body";
      let ret:any;
      ret = sendNativeNotification(registrationToken, title, body);
      res.status(200).send({response: ret, registrationToken: registrationToken});
    } else {
      console.error('Unable to parse parameters' + JSON.stringify(req.params));
      return res.status(404).send({ error: 'Unable to parse parameters' });
    }
  });

  exports.sendNotification = functions.https.onRequest((req: any, res: any) => {
    console.log('sendNotification', );
    
    if ( (req.params && req.params[0]) ) {
      console.log('req.params', req.params);
      
      let params = req.params[0].match(/\/(.+)\/(.+)\/?/);
      if ( true ) {
        console.log(params);
        
          let registrationToken = params[1] || "fo2VKCVAN8V2XxbXRX8oHF:APA91bF3SqO0t19ouYzgVK7ACexyIrESHNue5KTaFqCNXt-o7Msiaad8ZJeITljWsj-28tCzGtuGweMu8r-QpXGP4Sdqqe2M_rjWV6TsJBNeiVMvg8jqhkFa9GhLC51Q9Ly64s8faxaz";
          let text = params[2] || "Default message";

          let message = {
            data: {
              score: '850',
              time: '2:45'
            },
            notification: {
              "title": "FCM Message",
              "body": text
            },
            webpush: {
              headers: {
                Urgency: "high"
              },
              notification: {
                body: text + " to web",
                requireInteraction: "true",
              badge: "https://sprtmtrx.com/assets/icon/favicon.png"
              }
            },
            token: registrationToken
          };

          // Send a message to the device corresponding to the provided
          // registration token.
          admin.messaging().send(message)
            .then((response: any) => {
              // Response is a message ID string.
              console.log('Successfully sent message:', response);
              res.status(200).send({response: response, registrationToken: registrationToken});
            })
            .catch((error: any) => {
              console.log('Error sending message:', error);
              res.status(200).send({error: error, registrationToken: registrationToken});
            });          

        } else {
          console.error('Unable to retrieve the document ' + JSON.stringify(params));
          return res.status(404).send({ error: 'Unable to retrieve the document ' + params });
        }
    } else {
      console.error('Unable to parse parameters' + JSON.stringify(req.params));
      return res.status(404).send({ error: 'Unable to parse parameters' });
    }
  });
    
//  exports.scheduledSaturday10am = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('55 9 * * 0').onRun((context: any) => {
//    console.log('Shuffle Challenge 77');
//    //return shuffleChallenge(77);
//  });
