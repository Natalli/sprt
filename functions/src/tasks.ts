const rp = require('request-promise');
var functions = require('firebase-functions');
var Firestore = require('@google-cloud/firestore');
var firestore = new Firestore({
    projectId: "betting-game-hockey",
    timestampsInSnapshots: true,
});
// https://stackoverflow.com/questions/50630209/socket-hang-up-error-in-cloud-functions-for-firebase
    exports.scheduledTasks = functions.runWith({timeoutSeconds: 540, memory: '2GB'}).pubsub.schedule('every 1 minutes').onRun((context: any) => {
        const localTime = new Date(new Date().getTime() - 8 * 3600 * 1000); // Force .timeZone('America/Los_Angeles')
        if ( localTime.getHours() > 8 && [6,0].includes(localTime.getDay()) ) {
            updateActiveChallenge('344', 'nfl', localTime);
            setTimeout(() => {
                updateActiveChallenge('344', 'nfl', localTime);
                //console.log('30 second delay exec');
            }, 30 * 1000);
        }
        if ( localTime.getHours() > 14 && [1,2,3,4,5,6].includes(localTime.getDay()) ) {
            updateActiveChallenge('210', 'nba', localTime);
            setTimeout(() => {
                //updateActiveChallenge('210', 'nba', localTime);
                //console.log('30 second delay exec');
            }, 30 * 1000);
        }
        if ( localTime.getHours() > 9 && [6].includes(localTime.getDay()) ) {
            //processNHLSchedule('nhl', (localTime||new Date()), (localTime||new Date()), 139);
            setTimeout(() => {
                //processNHLSchedule('nhl', (localTime||new Date()), (localTime||new Date()), 139);
            }, 30 * 1000);
        }
        return {test: 'return'};
    });

    exports.updateActiveChallenge = functions.https.onRequest((req: any, res: any) => {    
        //updateActiveChallenge('79');
        res.end(JSON.stringify({text: 'updateActiveChallenge executed'}));
    });


    function updateActiveChallenge(challengePK: string, sport?: string, localTime?: Date) {
        processSchedule(sport||'nba', (localTime||new Date()), (localTime||new Date()), challengePK);
    }

    function processSchedule(sport: string, start: Date, end: Date, challengePK?: any) {
        console.log('processSchedule', sport, start.getHours());
        const sport_legacy_api = 'https://site.api.espn.com/apis/site/v2/sports/' + (sport === 'nba' ? 'basketball/nba/' : 'football/nfl/');
        let sportService = new SportService(sport);
        const url = sport_legacy_api + 'scoreboard?dates=' + (start.toISOString().substr(0, 10).replace(/\-/g, '')) + '-' + (end.toISOString().substr(0, 10).replace(/\-/g, ''));
        //console.log(url);
        var options = {
            uri: url,
            json: true
        };
        rp(options).then((data: any) => {
            sportService.initEvent(sport);
            //console.log(sportService.sportEvent);
            if ( data.events ) {
              data.events.forEach((espnEvent: any) => {
                const convertedData = sportService.processEspnEvent(espnEvent);
                const game:any = convertedData.game;
                const live:any = convertedData.live;
                const gameDate = new Date(new Date(game.gameDate).getTime() - (480 * 60000));
                if ( !sportService.sportEvent.date || sportService.sportEvent.date !== new Date(gameDate.getTime() - (0 * 60000)).toISOString().substr(0, 10) ) {
                  if ( sportService.sportEvent.date ) {
                    persistSportEvent(sportService.sportEvent, challengePK);
                  }
                  //console.log('new date', new Date(gameDate.getTime() - (0 * 60000)).toISOString().substr(0, 10), gameDate);
                  sportService.initEvent(sport);
                  sportService.sportEvent.date = new Date(gameDate.getTime() - (0 * 60000)).toISOString().substr(0, 10);
                  sportService.sportEvent.games.push(game);
                }
                else {
                  //console.log('existing date', new Date(gameDate.getTime() - (0 * 60000)).toISOString().substr(0, 10), gameDate);
                  sportService.sportEvent.games.push(game);
                }
                persistSportLive(live);
              });
              persistSportEvent(sportService.sportEvent, challengePK);
              //console.log(sportService.sportEvent);
            }
            else {
                console.log('no events in data');
                
            }
          });

      }

function processNHLSchedule(sport: string, start: Date, end: Date, challengePK?: any) {
    console.log('processNHLSchedule', start.getHours());
    const sport_legacy_api = 'https://statsapi.web.nhl.com/api/v1/';
    const url = sport_legacy_api + 'schedule?expand=schedule.linescore&startDate=' + (start.toISOString().substr(0, 10)) + '&endDate=' + (end.toISOString().substr(0, 10));
    //console.log(url);
    var options = {
        uri: url,
        json: true
    };
    rp(options).then((data: any) => {
        if ( data.dates ) {
            data.dates.forEach((event: any) => {
                persistSportEvent({...event, ...{sport: 'nhl'}}, challengePK);
                if ( event.games ) {
                    event.games.forEach((live: any) => {
                        //persistSportLive({...live, ...{sport: 'nhl'}});
                    });
                }
            });
        }
        else {
            console.log('no events in data');
            
        }
    });
}      

function persistSportLive(game: any) {
    //console.log('updateGame:', game.gamePk);
    firestore.doc('live/'+game.gamePk).get().then((doc: any) => {
      if (!doc || !doc.exists) {
        let saved = doc.data();
        game.currentDrive = saved.currentDrive || '';
        game.scoringPlays = saved.scoringPlays || [];
        game.odds = saved.odds || [];
        doc.ref.set(game)
        .then(() => {console.log('set game', game.gamePk)});
      }
      else {
        let saved = doc.data();
        game.currentDrive = saved.currentDrive || '';
        game.scoringPlays = saved.scoringPlays || [];
        game.odds = saved.odds || [];
        if ( game.liveData && (parseInt(game.liveData.linescore.teams.home.goals) <  parseInt(saved.liveData.linescore.teams.home.goals) ||
                parseInt(game.liveData.linescore.teams.away.goals) <  parseInt(saved.liveData.linescore.teams.away.goals) ||
                parseInt(game.liveData?.plays.currentPlay.about.periodTimeRemaining.replace(':','')) > parseInt(saved.liveData.plays.currentPlay.about.periodTimeRemaining.replace(':','')) && game.liveData?.plays.currentPlay.about.periodTimeRemaining !== 'Halftime') ) {
            //console.log('game data is outdated', game.gamePk, new Date().toISOString());
        }
        else if ( game.liveData && parseInt(game.liveData.linescore.teams.home.goals) !==  parseInt(saved.liveData.linescore.teams.home.goals) ||
                parseInt(game.liveData.linescore.teams.away.goals) !==  parseInt(saved.liveData.linescore.teams.away.goals) ||
                game.liveData?.plays.currentPlay.about.periodTimeRemaining !== saved.liveData.plays.currentPlay.about.periodTimeRemaining ) {
            console.log('game has to be updated:', game.gamePk, new Date().toISOString());
            doc.ref.update(game)
            .then(() => {
                //console.log('updated game', game.gamePk)
            });
        }
        else if ( !game.liveData ) {
            console.log('liveData is empty - skipping:', game.gamePk, new Date().toISOString());
        }
        else {
            //console.log('no update for game:', game.gamePk, new Date().toISOString());
        }
      }
    }).catch((err: any) => {
      console.error(err);
      console.log('Unable to retrieve Challenge: ' + game.gamePk);
    });    
}

function persistSportEvent(sportEvent: any, challengePK?: any) {
    console.log('updateEvent:', sportEvent.sport, sportEvent.date, challengePK);
    firestore.collection(sportEvent.sport).doc(sportEvent.date).get().then((doc: any) => {
      if (!(doc && doc.exists)) {
        doc.ref.set( Object.assign({}, sportEvent) )
        .then(() => {console.log('ADDED NEW EVENT', sportEvent.date, challengePK)});
      }
      else {
        doc.ref.update( Object.assign({}, sportEvent) )
        .then(() => {console.log('updated event', sportEvent.date, challengePK)});
      }
    }).catch((err: any) => {
      console.error(err);
      console.log('Unable to retrieve Challenge: ' + sportEvent.date, challengePK);
    });
}

export class SportService {
    sportEvent: SportEvent = new SportEvent('');
    sport: string = '';
    
    constructor(sport: string) {
        this.sport = sport;
        this.sportEvent = new SportEvent(this.sport);
    }
    
    initEvent(sport?: string) {
        this.sport = sport || this.sport;
        this.sportEvent = new SportEvent(this.sport);
        return this.sportEvent;
    }
    
    processEspnEvent(event: any) {
        const game:any = this.generateGame(new Date(event.date), event.id, event.season, event.status,
                                    event.competitions[0].competitors.find((team: any) => { return team.homeAway === 'away'}),
                                    event.competitions[0].competitors.find((team: any) => { return team.homeAway === 'home'}),
                                    event.competitions[0], event.competitions[0].venue);
    
        const live:any = this.generateLive(new Date(event.date), event.id, event.season, event.status,
                                    event.competitions[0].competitors.find((team: any) => { return team.homeAway === 'away'}),
                                    event.competitions[0].competitors.find((team: any) => { return team.homeAway === 'home'}),
                                    event.competitions[0], event.competitions[0].venue);
    
        return {game: game, live: live};
    }
    
    generateLive(startDate: Date, gamePk: String, season: any, status: any, 
                    awayTeam: any, homeTeam: any, linescore: any, venue: any) {
        let live = {
        "copyright" : "© NFL 2020. All Rights Reserved.",
        "gamePk" : gamePk,
        "sport" : this.sport,
        "link" : "/api/v1/game/"+gamePk+"/feed/live",
        "metaData" : {  },
        "gameData" : {
            "game" : {
            "pk" : gamePk,
            "season" : season.year,
            "type" : "R"
            },
            "datetime" : {
            "dateTime" : startDate.toISOString()
            },
            "status" : this.getStatus(status),
            "teams" : {
            "away" : this.getLiveTeam(awayTeam),
            "home" : this.getLiveTeam(homeTeam)
            },
            //"players" : {  },
            "venue" : this.getVenue(venue)
        },
        "liveData" : {
            "plays" : {
            //"allPlays" : [  ],
            //"scoringPlays" : [ ],
            //"penaltyPlays" : [ ],
            //"playsByPeriod" : [  ],
            "currentPlay" : {
                "result" : {  },
                "about" : {
                "period" : status.period,
                "periodTimeRemaining" : ( status.displayClock && status.displayClock !== '0:00') ? status.displayClock : '',
                // all below is not used
                "eventIdx" : 0,
                "eventId" : 1,
                "periodType" : "REGULAR",
                "ordinalNum" : "1st",
                "periodTime" : "00:00",
                "dateTime" : "2020-09-10T16:04:40Z",
                "goals" : {
                    "away" : 0,
                    "home" : 0
                }
                },
                "coordinates" : { }
            }
            },
            "linescore" : this.getLinescore(linescore, status, this.getTeam(homeTeam), this.getTeam(awayTeam)),
            //"boxscore" : {  },
            //"decisions" : {  }
        }
        };
        return live;
    }
    
    generateGame(startDate: Date, gamePk: String, season: any, status: any, 
                    awayTeam: any, homeTeam: any, linescore: any, venue: any) {
        let game = {
        "gamePk" : gamePk, //2019030312
        "sport" : this.sport,
        "link" : "summary?event="+gamePk, //"/api/v1/game/2019030312/feed/live"
        "gameType" : "R", // not used, season.type into (R=Regular; A=All Star; PR=Preseason; P=Playoff)
        "season" : season.year, //"20192020"
        "gameDate" : startDate.toISOString(), //"2020-09-10T00:00:00Z"
        "status" : this.getStatus(status),
        "teams" : {
            "away" : this.getTeam(awayTeam),
            "home" : this.getTeam(homeTeam)
        },
        "linescore" : this.getLinescore(linescore, status, this.getTeam(homeTeam), this.getTeam(awayTeam)),
        "venue" : this.getVenue(venue),
        "content" : {
            "link" : "/api/v1/game/"+gamePk+"/content" // TODO: MAP VALUE
        }
        };
        return game;
    }
    
    getLinescore(espnLinescore: any, status: any, homeTeam: any, awayTeam: any) {
        let linescore = {
        "currentPeriod" : status.period,
        "currentPeriodTimeRemaining" : ( status.displayClock && status.displayClock !== '0:00') ? status.displayClock : '',
        "periods" : [ ],
        "shootoutInfo" : { // not used
            "away" : {
            "scores" : 0,
            "attempts" : 0
            },
            "home" : {
            "scores" : 0,
            "attempts" : 0
            }
        },
        "teams" : {
            "home" : {
            "team" : homeTeam.team,
            "goals" : homeTeam.score || 0,
            "shotsOnGoal" : 0,
            "goaliePulled" : false,
            "numSkaters" : 0,
            "powerPlay" : false
            },
            "away" : {
            "team" : awayTeam.team,
            "goals" : awayTeam.score || 0,
            "shotsOnGoal" : 0,
            "goaliePulled" : false,
            "numSkaters" : 0,
            "powerPlay" : false
            }
        }, // all below not used
        "powerPlayStrength" : "Even",
        "hasShootout" : false,
        "intermissionInfo" : {
            "intermissionTimeRemaining" : 0,
            "intermissionTimeElapsed" : 0,
            "inIntermission" : false
        }
        };
        return linescore;
    }
    
    getStatus(espnStatus: any) {
        // TODO: CHECK LIVE MAP VALUES
        const mappedStatus = this.mapStatusType(espnStatus.type);
        const status = {
        "abstractGameState" : mappedStatus.state, // "Preview"
        "codedGameState" : mappedStatus.code, // "1"
        "detailedState" : mappedStatus.detail, // "Scheduled"
        "statusCode" : mappedStatus.code, // "1"
        "startTimeTBD" : false
        };
        return status;
    }
    
    mapStatusType(espnStatusType: any) {
        const statuses = [{code: '2', state: espnStatusType.description, detail: espnStatusType.description}];
        statuses[1] = {code: '1', state: 'Preview', detail: 'Scheduled'};
        statuses[2] = {code: '2', state: 'Live', detail: 'In Progress'};
        statuses[3] = {code: '7', state: 'Final', detail: 'Final'};
        return statuses[espnStatusType.id] || statuses[0];
    }
    
    getLiveTeam(espnTeam: any) {
        let team = {
        "id" : espnTeam.team.id,
        "name" : espnTeam.team.displayName,
        "link" : "/api/v1/teams/" + espnTeam.team.id,
        "venue" : { "id": espnTeam.team.venue?.id||0 },
        "abbreviation" : espnTeam.team.abbreviation,
        "triCode" : espnTeam.team.abbreviation+"X",
        "teamName" : espnTeam.team.name || '',
        "locationName" : espnTeam.team.location,
        "firstYearOfPlay" : "0000",
        "division" : {  },
        "conference" : {  },
        "franchise" : {  },
        "shortName" : espnTeam.team.shortDisplayName,
        "officialSiteUrl" : "",
        "franchiseId" : 0,
        "active" : espnTeam.team.isActive
        };
        return team;
    }
    
    getTeam(espnTeam: any) {
        const records = espnTeam.records?.find(record => { return record.type === 'total'}).summary.split('-');
        let team = {
        "leagueRecord" : {
            "wins" : records ? records[0] : 0,
            "losses" : records ? records[1] : 0,
            "ot" : 0,
            "type" : "league"
        },
        "score" : espnTeam.score || 0,
        "team" : {
            "id" : espnTeam.team.id,
            "name" : espnTeam.team.displayName,
            "link" : "/api/v1/teams/" + espnTeam.team.id
        }
        };
        return team;
    }
    
    getVenue(espnVenue: any) {
        let venue = {
        "id" : espnVenue.id,
        "name" : espnVenue.fullName,
        "link" : "/api/v1/venues/" + espnVenue.id
        };
        return venue;
    }
    
    }
      

export class SportEvent {
    date: string = '';
    events = [];
    games: {}[] = []
    matches = [];
    totalEvents = 0;
    totalMatches = 0;
    sport = '';

    constructor(sport: string) {
        this.sport = sport
    }

    get totalGames() {
        return this.games.length;
    }

    get totalItems() {
        return this.games.length;
    }

    static startDate(start: any) {
        if ( start && start.seconds) {
            return (new Date(start.seconds*1000)).toJSON().substr(0,10).replace(/-/g,'');
        } else {
            return start.replace(/-/g,'');
        }
    }





}
    